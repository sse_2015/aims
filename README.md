# checkout a repository #

create a working copy of a local repository by running the command


```
#!ruby

git clone https://balx@bitbucket.org/sse_2015/aims.git
```



# add & commit #

You can propose changes (add it to the Index) using

```
#!ruby

git add <filename>
git add *
```

This is the first step in the basic git workflow. To actually commit these changes use

```
#!ruby

git commit -m "Commit message"
```

Now the file is committed to the HEAD, but not in your remote repository yet.

# pushing changes #

Your changes are now in the HEAD of your local working copy. To send those changes to your remote repository, execute

```
#!ruby

git push origin master
```
# show changes #
Show changes between commits, commit and working tree, etc

```
#!ruby

git diff
```


# update & merge #

to update your local repository to the newest commit, execute

```
#!ruby

git pull
```

in your working directory to fetch and merge remote changes.

# log #

in its simplest form, you can study repository history using.. 
```
#!ruby

git log
```

# references #
[http://rogerdudler.github.io/git-guide/](http://rogerdudler.github.io/git-guide/)

[https://git-scm.com/docs](https://git-scm.com/docs)