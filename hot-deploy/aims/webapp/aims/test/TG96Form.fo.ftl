<#escape x as x?xml>
<fo:table table-layout="fixed" width="100%">
  <fo:table-body>
  <fo:table-row height="5px">
      <fo:table-cell number-columns-spanned="25"><fo:block font-weight="bold" text-align="right">TG96</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="25">
      <fo:block font-weight="bold" text-align="center">Request Form for Special Consideration</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="25">
        <fo:block font-weight="bold" text-align="center">for Approval of English Language Score Requirement</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="25">
        <fo:block font-weight="bold" text-align="center">The Sirindhorn International Thai - German Graduate School of Engineering</fo:block>
      </fo:table-cell>
    </fo:table-row>

    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="25">
        <fo:block font-weight="bold" text-align="center">
          King Mongkut's University of Technology North Bangkok
        </fo:block>
      </fo:table-cell>
    </fo:table-row>

    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="25" text-align="right">
        <fo:block >
          Date..........Month..................Year........
        </fo:block>
      </fo:table-cell>
    </fo:table-row>

    <fo:table-row height="40px" number-rows-spanned="20">
      <fo:table-cell number-columns-spanned="20">
        <fo:block font-weight="bold">
          Subject: Special consideration for approval of English language score requirement
        </fo:block>
        <fo:block margin="5"></fo:block>
        <fo:block font-weight="bold">
          To : TGGS Academic Affair
        </fo:block>
        <fo:block margin-bottom="20"></fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="13">
        <fo:block>
          My Name : ${parameters.firstName?if_exists} ${parameters.middleName?if_exists} ${parameters.lastName?if_exists}
        </fo:block>
      </fo:table-cell>
      <fo:table-cell number-columns-spanned="12">
        <fo:block>
          Student ID : ${parameters.cardId?if_exists}
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="25">
        <fo:block>
          I am studying Master program in ${parameters.program?if_exists} - ${parameters.groupName?if_exists}
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="15">
        <fo:block>
          Home Address : ${parameters.address1?if_exists} ${parameters.homeaddress?if_exists}
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="13">
        <fo:block>
          Phone Number : ${parameters.contactNumber?if_exists}${parameters.contactnumber?if_exists}
        </fo:block>
      </fo:table-cell>
      <fo:table-cell number-columns-spanned="12">
        <fo:block>
          Email : ${parameters.email?if_exists}${parameters.contactnumber?if_exists}
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="25">
        <fo:block>
          would like to request for a special condition of the English language score requirement to be graduated. English language test score:
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px" margin-left="40">
      <fo:table-cell number-columns-spanned="12">
        <fo:block>
          TOEFL (Paper Based)     : ${parameters.toeflpaper?if_exists}
        </fo:block>
      </fo:table-cell>
      <fo:table-cell number-columns-spanned="12">
        <fo:block>
          IELTS (Academic Module) : ${parameters.ielts?if_exists}
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px" margin-left="40">
      <fo:table-cell number-columns-spanned="12">
        <fo:block>
          TOEFL (Computer Based) : ${parameters.toeflcomputer?if_exists}
        </fo:block>
      </fo:table-cell>
      <fo:table-cell number-columns-spanned="12">
        <fo:block>
          CU-TEP : ${cutep?if_exists}
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
     <fo:table-row height="20px" margin-left="40">
      <fo:table-cell number-columns-spanned="12">
        <fo:block>
          TOEFL (Internet Based) : ${parameters.toeflinternet?if_exists}
        </fo:block>
      </fo:table-cell>
    </fo:table-row>

    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="25">
        <fo:block margin-top="20">
          Please Consider,
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="5">
      </fo:table-cell>
      <fo:table-cell number-columns-spanned="20" text-align="right">
        <fo:block>
          Signature.........................................Student
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="6">
      </fo:table-cell>
      <fo:table-cell number-columns-spanned="15" text-align="right">
        <fo:block>
          (${parameters.firstName?if_exists} ${parameters.middleName?if_exists} ${parameters.lastName?if_exists})
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="12">
        <fo:block>
          Advisor's Opinion
        </fo:block>
      </fo:table-cell>
      <fo:table-cell number-columns-spanned="12" margin-left="50">
        <fo:block>
          Coordinator's Opinion
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="12">
        <fo:block>
          ...............................................................
        </fo:block>
      </fo:table-cell>
      <fo:table-cell number-columns-spanned="12" margin-left="50">
        <fo:block>
          ...............................................................
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="12">
        <fo:block>
          ...............................................................
        </fo:block>
      </fo:table-cell >
      <fo:table-cell number-columns-spanned="12" margin-left="50">
        <fo:block>
          ...............................................................
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="12">
        <fo:block>
          Signature............................Advisor
        </fo:block>
      </fo:table-cell >
      <fo:table-cell number-columns-spanned="12" margin-left="50">
        <fo:block>
          Signature............................Coordinator
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px" margin-left="35">
      <fo:table-cell number-columns-spanned="12">
        <fo:block>
          (....................................)
        </fo:block>
      </fo:table-cell >
      <fo:table-cell number-columns-spanned="12" margin-left="105">
        <fo:block>
          (....................................)
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="12">
        <fo:block>
          Head of Department's Opinion
        </fo:block>
      </fo:table-cell >
      <fo:table-cell number-columns-spanned="12">
        <fo:block text-decoration="underline">
          TGGS Academic Affairs' Memo
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px" number-rows-spanned="2">
      <fo:table-cell number-columns-spanned="12">
        <fo:block>
          ......................................................................
        </fo:block>
      </fo:table-cell >
      <fo:table-cell number-columns-spanned="12">
        <fo:block margin-left="45">
          O To be Considered
        </fo:block>
        <fo:block margin-left="45">
          O To be Approved
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px" number-rows-spanned="4">
      <fo:table-cell number-columns-spanned="12">
        <fo:block>
          ......................................................................
        </fo:block>
      </fo:table-cell >
      <fo:table-cell number-columns-spanned="12">
        <fo:block margin-left="45">
          O .........................
        </fo:block>
        <fo:block margin-left="56">
          .........................
        </fo:block>
        <fo:block margin-left="56">
          .........................
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="7">
        <fo:block>
          ......................................................................
        </fo:block>
      </fo:table-cell >
      <fo:table-cell number-columns-spanned="15" margin-left="150">
        <fo:block>
          Signature.........................
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px" number-rows-spanned="2">
      <fo:table-cell number-columns-spanned="15">
        <fo:block>
          Signature......................................Head of Department
        </fo:block>
        <fo:block margin-left="50">
          (..........................................)
        </fo:block>
      </fo:table-cell>
    </fo:table-row>



    <fo:table-row height="40px" number-rows-spanned="5">
      <fo:table-cell number-columns-spanned="23">
        <fo:block>
          Approval of Associate Dean for Academic Affairs
        </fo:block>
        <fo:block>
          O Approved
        </fo:block>
        <fo:block>
          O Approved in compliant with TGGS Consideration Procedure Guideline (TG97)
        </fo:block>
        <fo:block>
          O Approved with condition ..............
        </fo:block>
        <fo:block>
          O Not approved
        </fo:block>
        <fo:block>
          O ...........................................................
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px" number-rows-spanned="5">
      <fo:table-cell number-columns-spanned="10">
      	<fo:block></fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px" number-rows-spanned="5">
      <fo:table-cell number-columns-spanned="3">
      </fo:table-cell>
      <fo:table-cell number-columns-spanned="19" margin-left="150">
      	<fo:block text-align="right">
          Signature....................................................Associate Dean
        </fo:block>
        <fo:block margin="5"></fo:block>
        <fo:block text-align="center">
          (...............................................)
        </fo:block>
        <fo:block margin="5"></fo:block>
        <fo:block text-align="center">
          ....../.................../.......
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px" max-rows-spanned="5">
      <fo:table-cell number-columns-spanned="10">
      	<fo:block>
          Approval of TGGS Dean
        </fo:block>
        <fo:block margin="5">
        </fo:block>
        <fo:block>
          ....................................................................................................................................................
        </fo:block>
        <fo:block margin="5">
        </fo:block>
        <fo:block>
          ....................................................................................................................................................
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px" number-rows-spanned="6">
      <fo:table-cell number-columns-spanned="3">
      </fo:table-cell>
      <fo:table-cell number-columns-spanned="19" margin-left="155">
        <fo:block margin="5"></fo:block>
      	<fo:block text-align="center">
          Signature.............................................................Dean
        </fo:block>
        <fo:block margin="5"></fo:block>
        <fo:block text-align="center">
          (................................................)
        </fo:block>
        <fo:block margin="5"></fo:block>
        <fo:block text-align="center">
          ....../.................../.......
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
  </fo:table-body>
</fo:table>
</#escape>