<#escape x as x?xml>
<fo:table table-layout="fixed" width="100%">
  <fo:table-body>
  <fo:table-row height="5px">
      <fo:table-cell number-columns-spanned="10"><fo:block font-weight="bold" text-align="right">TG91</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="10"><fo:block font-weight="bold" text-align="center">Request Form</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="10">
        <fo:block font-weight="bold" text-align="center">The Sirindhorn International Thai - German Graduate School of Engineering</fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="10">
        <fo:block font-weight="bold" text-align="center">
          King Mongkut's University of Technology North Bangkok
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="5">
        <fo:block font-weight="bold">
          Subject : ${subjects?if_exists}
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="5">
        <fo:block font-weight="bold">
          To : TGGS Academic Affairs
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          My Name : ${firstName?if_exists} ${middleName?if_exists} ${lastName?if_exists}
        </fo:block>
      </fo:table-cell>
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          Student ID : ${studentId?if_exists}
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          I am studying Master program in ${program?if_exists}
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          Home Address : ${homeaddress1?if_exists} ${homeaddress2?if_exists}
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          Phone Number : ${countrycode?if_exists}${contactnumber?if_exists}
        </fo:block>
      </fo:table-cell>
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          Email : leo.a-sse2015@tggs.bangkok.org
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="10">
        <fo:block>
          Would like to ${contents?if_exists}
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          Please Consider,
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="5">
      </fo:table-cell>
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          Signature.........................................Student
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="6">
      </fo:table-cell>
      <fo:table-cell number-columns-spanned="4">
        <fo:block>
          (....................................)
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          Advisor's Opinion
        </fo:block>
      </fo:table-cell>
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          Coordinator's Opinion
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          ...............................................................
        </fo:block>
      </fo:table-cell>
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          ...............................................................
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          ...............................................................
        </fo:block>
      </fo:table-cell >
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          ...............................................................
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          Signature............................Advisor
        </fo:block>
      </fo:table-cell >
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          Signature............................Coordinator
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px" margin-left="35">
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          (....................................)
        </fo:block>
      </fo:table-cell >
      <fo:table-cell number-columns-spanned="5" margin-left="53">
        <fo:block>
          (....................................)
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px" margin-left="40">
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          ......./.................../......
        </fo:block>
      </fo:table-cell >
      <fo:table-cell number-columns-spanned="5" margin-left="60">
        <fo:block>
          ......./.................../......
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="7">
        <fo:block>
          Approval of Head of Department
        </fo:block>
      </fo:table-cell >
      <fo:table-cell number-columns-spanned="3">
        <fo:block text-decoration="underline">
          TGGS Academic Affairs' Memo
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px" number-rows-spanned="2">
      <fo:table-cell number-columns-spanned="7">
        <fo:block>
          ......................................................................
        </fo:block>
      </fo:table-cell >
      <fo:table-cell number-columns-spanned="5">
        <fo:block margin-left="33">
          O To be Considered
        </fo:block>
        <fo:block margin-left="33">
          O To be Approved
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px" number-rows-spanned="4">
      <fo:table-cell number-columns-spanned="7">
        <fo:block>
          ......................................................................
        </fo:block>
      </fo:table-cell >
      <fo:table-cell number-columns-spanned="5">
        <fo:block margin-left="33">
          O .........................
        </fo:block>
        <fo:block margin-left="45">
          .........................
        </fo:block>
        <fo:block margin-left="45">
          .........................
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="7">
        <fo:block>
          ......................................................................
        </fo:block>
      </fo:table-cell >
      <fo:table-cell number-columns-spanned="3">
        <fo:block>
          Signature.........................
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px" number-rows-spanned="3">
      <fo:table-cell number-columns-spanned="7">
        <fo:block>
          Signature......................................Head of Department
        </fo:block>
        <fo:block margin-left="50">
          (..........................................)
        </fo:block>
        <fo:block margin-left="60">
          ........./............./...........
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px" number-rows-spanned="5">
      <fo:table-cell number-columns-spanned="10">
        <fo:block>
          Approval of Associate Dean for Academic Affairs
        </fo:block>
        <fo:block>
          O Approved
        </fo:block>
        <fo:block>
          O Approved with condition ..............
        </fo:block>
        <fo:block>
          O Not approved
        </fo:block>
        <fo:block>
          O ...........................................................
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px" number-rows-spanned="5">
      <fo:table-cell number-columns-spanned="10">
      	<fo:block></fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px" number-rows-spanned="3">
      <fo:table-cell number-columns-spanned="3">
      </fo:table-cell>
      <fo:table-cell number-columns-spanned="7">
      	<fo:block text-align="right">
          Signature....................................................Associate Dean
        </fo:block>
        <fo:block text-align="center">
          (......Asst. Prof. Dr. Tawiwan Kangsadan......)
        </fo:block>
        <fo:block text-align="center">
          ....../.................../.......
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px" max-rows-spanned="5">
      <fo:table-cell number-columns-spanned="10">
      	<fo:block>
          Dean's Opinion
        </fo:block>
        <fo:block margin="5">
        </fo:block>
        <fo:block>
          ....................................................................................................................................................
        </fo:block>
        <fo:block margin="5">
        </fo:block>
        <fo:block>
          ....................................................................................................................................................
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px" number-rows-spanned="3">
      <fo:table-cell number-columns-spanned="3">
      </fo:table-cell>
      <fo:table-cell number-columns-spanned="7" margin-left="50">
      	<fo:block text-align="center">
          Signature.............................................................Dean
        </fo:block>
        <fo:block text-align="center">
          (......Asst. Prof. Dr. Monpilai Narasingha......)
        </fo:block>
        <fo:block text-align="center">
          ....../.................../.......
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
  </fo:table-body>
</fo:table>
</#escape>