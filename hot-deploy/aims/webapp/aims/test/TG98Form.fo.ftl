<#escape x as x?xml>
<fo:table table-layout="fixed" width="100%">
  <fo:table-body>
  <fo:table-row height="5px">
      <fo:table-cell number-columns-spanned="10"><fo:block font-weight="bold" text-align="right">TG98</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="10"><fo:block font-weight="bold" text-align="center">Application for Thesis Access Restriction</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="10">
        <fo:block font-weight="bold" text-align="center">The Sirindhorn International Thai - German Graduate School of Engineering</fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="10">
        <fo:block font-weight="bold" text-align="center">
          King Mongkut's University of Technology North Bangkok
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px" number-rows-spanned="4">
      <fo:table-cell number-columns-spanned="10">
        <fo:block font-weight="bold">
          Subject : Request for Thesis Access Restriction
        </fo:block>
        <fo:block margin="5"></fo:block>
        <fo:block font-weight="bold">
          To : TGGS Dean
        </fo:block>
        <fo:block margin-bottom="20"></fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          Name: (Mr./Mrs./Miss) ${firstName?if_exists} ${middleName?if_exists}
        </fo:block>
      </fo:table-cell>
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          Last Name: ${lastName?if_exists}
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="10">
        <fo:block>
          ID. No. : ${studentId?if_exists}
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          Education Level : O Master Degree
        </fo:block>
      </fo:table-cell>
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          O Doctoral Degree
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          Program : ${program?if_exists}
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          Address : ${homeaddress1?if_exists} ${homeaddress2?if_exists}
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          Mobile : ${countrycode?if_exists}${contactnumber?if_exists}
        </fo:block>
      </fo:table-cell>
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          Email : leo.a-sse2015@tggs.bangkok.org
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px" number-rows-spanned="8">
      <fo:table-cell number-columns-spanned="10">
        <fo:block>
          Thesis title : ${thesistitle?if_exists}
        </fo:block>
        <fo:block margin="5"></fo:block>
        <fo:block>
          I wish to secure the entire work for a period of ${period?if_exists} year(s) effective on ${effectivedate?if_exists}
        </fo:block>
        <fo:block margin="5"></fo:block>
        <fo:block>
          For consideration because of ${reason?if_exists}
        </fo:block>
        <fo:block margin-bottom="5"></fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="5">
        <fo:block margin-top="20">
          Please Consider,
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="5">
      </fo:table-cell>
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          Signature.........................................Student
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="6">
      </fo:table-cell>
      <fo:table-cell number-columns-spanned="4">
        <fo:block>
          (....................................)
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="10" text-align="center">
      	<fo:block font-weight="bold">
      		Approved by Studentís Advisory Committee
      	</fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="5">
      	<fo:block>
      		1................................................Advisor
      	</fo:block>
      </fo:table-cell>
      <fo:table-cell number-columns-spanned="5" text-align="right">
      	<fo:block>
      		Signature................................................
      	</fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="5">
      	<fo:block>
      		2...............................................Co-Advisor
      	</fo:block>
      </fo:table-cell>
      <fo:table-cell number-columns-spanned="5" text-align="right">
      	<fo:block>
      		Signature................................................
      	</fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="5">
      	<fo:block>
      		3...............................................Co-Advisor
      	</fo:block>
      </fo:table-cell>
      <fo:table-cell number-columns-spanned="5" text-align="right">
      	<fo:block>
      		Signature................................................
      	</fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px" number-rows-spanned="2">
      <fo:table-cell number-columns-spanned="10" text-align="justify">
      	<fo:block font-weight="bold">
      		Remarks: Student who wish to restrict access to thesis, please complete and submit the thesis access restriction application form (TG. 05) on the same day that submit the thesis to the TGGS Academic Affairs. Afterward, this application will not be considered.
      	</fo:block>
      	<fo:block margin="5"></fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          Advisor's Opinion
        </fo:block>
      </fo:table-cell>
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          Coordinator's Opinion
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          ...............................................................
        </fo:block>
      </fo:table-cell>
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          ...............................................................
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          ...............................................................
        </fo:block>
      </fo:table-cell >
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          ...............................................................
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          Signature............................Advisor
        </fo:block>
      </fo:table-cell >
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          Signature............................Coordinator
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px" margin-left="35">
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          (....................................)
        </fo:block>
      </fo:table-cell >
      <fo:table-cell number-columns-spanned="5" margin-left="53">
        <fo:block>
          (....................................)
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="7">
        <fo:block>
          Head of Department's Opinion
        </fo:block>
      </fo:table-cell >
      <fo:table-cell number-columns-spanned="3">
        <fo:block text-decoration="underline">
          TGGS Academic Affairs' Memo
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px" number-rows-spanned="2">
      <fo:table-cell number-columns-spanned="7">
        <fo:block>
          ......................................................................
        </fo:block>
      </fo:table-cell >
      <fo:table-cell number-columns-spanned="5">
        <fo:block margin-left="33">
          O To be Considered
        </fo:block>
        <fo:block margin-left="33">
          O To be Approved
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px" number-rows-spanned="4">
      <fo:table-cell number-columns-spanned="7">
        <fo:block>
          ......................................................................
        </fo:block>
      </fo:table-cell >
      <fo:table-cell number-columns-spanned="5">
        <fo:block margin-left="33">
          O .........................
        </fo:block>
        <fo:block margin-left="45">
          .........................
        </fo:block>
        <fo:block margin-left="45">
          .........................
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="7">
        <fo:block>
          ......................................................................
        </fo:block>
      </fo:table-cell >
      <fo:table-cell number-columns-spanned="3">
        <fo:block>
          Signature.........................
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px" number-rows-spanned="4">
      <fo:table-cell number-columns-spanned="7">
        <fo:block>
          Signature......................................Head of Department
        </fo:block>
        <fo:block margin="5"></fo:block>
        <fo:block margin-left="50">
          (..........................................)
        </fo:block>
        <fo:block margin="5"></fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px" number-rows-spanned="9">
      <fo:table-cell number-columns-spanned="10">
        <fo:block>
          Approval of TGGS Associate Dean for Academic Affairs
        </fo:block>
        <fo:block margin="5"></fo:block>
        <fo:block>
          O Approved
        </fo:block>
        <fo:block margin="5"></fo:block>
        <fo:block>
          O Approved with condition ..................................................................................
        </fo:block>
        <fo:block margin="5"></fo:block>
        <fo:block>
          O Not approved
        </fo:block>
        <fo:block margin="5"></fo:block>
        <fo:block>
          O .........................................................................................................................
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px" number-rows-spanned="5">
      <fo:table-cell number-columns-spanned="10">
      	<fo:block></fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px" number-rows-spanned="5">
      <fo:table-cell number-columns-spanned="3">
      </fo:table-cell>
      <fo:table-cell number-columns-spanned="7">
      	<fo:block text-align="right">
          Signature....................................................Associate Dean
        </fo:block>
        <fo:block margin="5"></fo:block>
        <fo:block text-align="center">
          (......Asst. Prof. Dr. Tawiwan Kangsadan......)
        </fo:block>
        <fo:block margin="5"></fo:block>
        <fo:block text-align="center">
          ....../.................../.......
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px" max-rows-spanned="3">
      <fo:table-cell number-columns-spanned="10">
      	<fo:block>
          Approval of TGGS Dean
        </fo:block>
        <fo:block margin="5">
        </fo:block>
        <fo:block>
          ....................................................................................................................................................
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px" number-rows-spanned="6">
      <fo:table-cell number-columns-spanned="3">
      </fo:table-cell>
      <fo:table-cell number-columns-spanned="7" margin-left="50">
        <fo:block margin="5"></fo:block>
      	<fo:block text-align="center">
          Signature.............................................................Dean
        </fo:block>
        <fo:block margin="5"></fo:block>
        <fo:block text-align="center">
          (......Asst. Prof. Dr. Monpilai Narasingha......)
        </fo:block>
        <fo:block margin="5"></fo:block>
        <fo:block text-align="center">
          ....../.................../.......
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
  </fo:table-body>
</fo:table>
</#escape>