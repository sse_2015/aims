<#escape x as x?xml>
<fo:table table-layout="fixed" width="100%">
  <fo:table-body>
  <fo:table-row height="5px">
      <fo:table-cell number-columns-spanned="10"><fo:block font-weight="bold" text-align="right">TG94</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="10"><fo:block font-weight="bold" text-align="center">Request Form for Student Certification for VISA Application</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="10"><fo:block font-weight="bold" text-align="center">(Doing Industrial Internship Abroad)</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="10">
        <fo:block font-weight="bold" text-align="center">The Sirindhorn International Thai - German Graduate School of Engineering</fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="10">
        <fo:block font-weight="bold" text-align="center">
          King Mongkut's University of Technology North Bangkok
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px" number-rows-spanned="4">
      <fo:table-cell number-columns-spanned="10">
        <fo:block font-weight="bold">
          Subject : Request for the Student Certification for Doing Industrial Internship
        </fo:block>
        <fo:block margin="5"></fo:block>
        <fo:block font-weight="bold">
          To : TGGS Academic Affairs
        </fo:block>
        <fo:block margin-bottom="20"></fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          My Name : ${firstName?if_exists} ${middleName?if_exists} ${lastName?if_exists}
        </fo:block>
      </fo:table-cell>
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          Student ID : ${studentId?if_exists}
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          I am studying Master program in ${program?if_exists}
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          Home Address : ${homeaddress1?if_exists} ${homeaddress2?if_exists}
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          Phone Number : ${countrycode?if_exists}${contactnumber?if_exists}
        </fo:block>
      </fo:table-cell>
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          Email : leo.a-sse2015@tggs.bangkok.org
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px" number-rows-spanned="10">
      <fo:table-cell number-columns-spanned="10">
        <fo:block>
          would like to request for the student certification to present
        </fo:block>
        <fo:block margin="5"></fo:block>
        <fo:block>
          Company name: ${companyname?if_exists}
        </fo:block>
        <fo:block margin="5"></fo:block>
        <fo:block>
          Company address/country: ${address?if_exists}
        </fo:block>
        <fo:block margin="5"></fo:block>
        <fo:block>
          Internship duration start/end: ${startdate?if_exists} / ${enddate?if_exists}
        </fo:block>
        <fo:block margin="5"></fo:block>
        <fo:block>
          Internship advisor's name: ${advisorname?if_exists}
        </fo:block>
        <fo:block margin-bottom="15"></fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          Please Consider,
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="5">
      </fo:table-cell>
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          Signature.........................................Student
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="6">
      </fo:table-cell>
      <fo:table-cell number-columns-spanned="4">
        <fo:block>
          (....................................)
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="10">
        <fo:block margin-top="27" margin-bottom="10">
          Department Academic Affair's Memo
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="10">
        <fo:block>
          ...........................................................................................................................................................
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="10">
        <fo:block>
          ...........................................................................................................................................................
        </fo:block>
      </fo:table-cell >
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="3">  
      </fo:table-cell >
      <fo:table-cell number-columns-spanned="7">
        <fo:block text-align="center">
          Signature.................................................Officer
        </fo:block>
      </fo:table-cell >
    </fo:table-row>
    <fo:table-row height="30px" margin-left="35">
      <fo:table-cell number-columns-spanned="3">  
      </fo:table-cell >
      <fo:table-cell number-columns-spanned="7">
        <fo:block text-align="center">
          (................................................)
        </fo:block>
      </fo:table-cell >
    </fo:table-row>
    <fo:table-row height="30px" margin-left="40">
      <fo:table-cell number-columns-spanned="3">  
      </fo:table-cell >
      <fo:table-cell number-columns-spanned="7">
        <fo:block text-align="center">
          ......./.................../......
        </fo:block>
      </fo:table-cell >
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="10">
        <fo:block>
          Program Coordinator�s Opinion
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="10">
        <fo:block>
          ...........................................................................................................................................................
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="10">
        <fo:block>
          ...........................................................................................................................................................
        </fo:block>
      </fo:table-cell >
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="3">  
      </fo:table-cell >
      <fo:table-cell number-columns-spanned="7">
        <fo:block text-align="center">
          Signature.................................................Program Coordinator
        </fo:block>
      </fo:table-cell >
    </fo:table-row>
    <fo:table-row height="30px" margin-left="35">
      <fo:table-cell number-columns-spanned="3">  
      </fo:table-cell >
      <fo:table-cell number-columns-spanned="7">
        <fo:block text-align="center">
          (................................................)
        </fo:block>
      </fo:table-cell >
    </fo:table-row>
    <fo:table-row height="30px" margin-left="40">
      <fo:table-cell number-columns-spanned="3">  
      </fo:table-cell >
      <fo:table-cell number-columns-spanned="7">
        <fo:block text-align="center">
          ......./.................../......
        </fo:block>
      </fo:table-cell >
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="10">
        <fo:block margin-top="27" margin-bottom="10">
          Head of Department's Opinion
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="10">
        <fo:block>
          ...........................................................................................................................................................
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="10">
        <fo:block>
          ...........................................................................................................................................................
        </fo:block>
      </fo:table-cell >
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="3">  
      </fo:table-cell >
      <fo:table-cell number-columns-spanned="7">
        <fo:block text-align="center">
          Signature.................................................Head of Department
        </fo:block>
      </fo:table-cell >
    </fo:table-row>
    <fo:table-row height="30px" margin-left="35">
      <fo:table-cell number-columns-spanned="3">  
      </fo:table-cell >
      <fo:table-cell number-columns-spanned="7">
        <fo:block text-align="center">
          (................................................)
        </fo:block>
      </fo:table-cell >
    </fo:table-row>
    <fo:table-row height="30px" margin-left="40">
      <fo:table-cell number-columns-spanned="3">  
      </fo:table-cell >
      <fo:table-cell number-columns-spanned="7">
        <fo:block text-align="center">
          ......./.................../......
        </fo:block>
      </fo:table-cell >
    </fo:table-row>
    <fo:table-row height="30px" border-top-style="solid"  border-left-style="solid"  border-right-style="solid">
      <fo:table-cell number-columns-spanned="5" border-right-style="solid">
        <fo:block text-decoration="underline">
          TGGS Academic Affairs� Action
        </fo:block>
      </fo:table-cell >
      <fo:table-cell number-columns-spanned="5">
        <fo:block text-decoration="underline">
          TGGS Associate Dean for Academic Affairs
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px"  border-left-style="solid"  border-right-style="solid">
      <fo:table-cell number-columns-spanned="2">
        <fo:block>
          O To be considered
        </fo:block>
      </fo:table-cell >
      <fo:table-cell number-columns-spanned="3" margin-left="15" border-right-style="solid">
        <fo:block>
          O To be approved
        </fo:block>
      </fo:table-cell >
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          ......................................................................
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px" border-left-style="solid"  border-right-style="solid">
      <fo:table-cell number-columns-spanned="5" border-right-style="solid">
        <fo:block>
          O .......................................
        </fo:block>
      </fo:table-cell >
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          ......................................................................
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px" border-left-style="solid"  border-right-style="solid" border-bottom-style="solid" >
      <fo:table-cell number-columns-spanned="5" border-right-style="solid">
        <fo:block> 
          Signature............................Officer
        </fo:block>
      </fo:table-cell >
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          Signature............................Associate Dean
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
  </fo:table-body>
</fo:table>
</#escape>