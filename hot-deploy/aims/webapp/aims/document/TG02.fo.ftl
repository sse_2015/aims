<#escape x as x?xml>
<fo:table table-layout="fixed" width="95%">
  <fo:table-body>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="4">
        <fo:block font-weight="bold" text-align="center">Application for Appointment of Thesis Advisory Comittee</fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="50px">
      <fo:table-cell number-columns-spanned="4">
        <fo:block font-weight="bold" text-align="center">The Sirindhorn Internation Thai - German Graduate School of Engineering</fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block>Date ${date?default(nowTimestamp)}</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="3"><fo:block font-weight="bold">Subject : Request for appointment of thesis advisory comittee</fo:block></fo:table-cell>
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="3"><fo:block font-weight="bold">To : TGGS Dean</fo:block></fo:table-cell>
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block>Name </fo:block></fo:table-cell>
      <fo:table-cell><fo:block font-weight="bold"> ${salutation?if_exists} ${firstName?if_exists} ${middleName?if_exists}</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>Last Name </fo:block></fo:table-cell>
      <fo:table-cell><fo:block font-weight="bold">${lastName?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block>ID. No. </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block font-weight="bold">${studentId?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block>Education Level: </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block font-weight="bold">${educationLevel?if_exists}</fo:block></fo:table-cell>
      <fo:table-cell><fo:block/></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block>Program : </fo:block></fo:table-cell>
      <fo:table-cell><fo:block font-weight="bold">${program?if_exists}</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>Department : </fo:block></fo:table-cell>
      <fo:table-cell><fo:block font-weight="bold">${department?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block>Address: </fo:block></fo:table-cell>
      <fo:table-cell number-colums-spanned="3"><fo:block font-weight="bold">${address?if_exists} </fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row>
      <fo:table-cell><fo:block>Mobile : </fo:block></fo:table-cell>
      <fo:table-cell><fo:block font-weight="bold">${telephone?if_exists}</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>E-mail Address : </fo:block></fo:table-cell>
      <fo:table-cell><fo:block font-weight="bold">${email?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="60px">
      <fo:table-cell number-columns-spanned="4"><fo:block>Proposed Thesis Title : ${title?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="4"><fo:block>I wish to apply for approval of appointment of thesis advisory comittee</fo:block></fo:table-cell>
    </fo:table-row>    
    <fo:table-row height="20px">
      <fo:table-cell>
        <fo:block/>
      </fo:table-cell>
      <fo:table-cell>
        <fo:block>
          O For renewal
        </fo:block>
      </fo:table-cell>
      <fo:table-cell>
        <fo:block>
          O For additional
        </fo:block>
      </fo:table-cell>
      <fo:table-cell>
        <fo:block>
          O For change
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="2"><fo:block>1 Advisor ${newAdvisorName?default('.............................................')}</fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block>Signature................................................................</fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="2"><fo:block>2 Co-Advisor ${newCoAdvisorName?default('.............................................')}</fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block>Signature................................................................</fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="120px">
      <fo:table-cell number-columns-spanned="2"><fo:block>3 Co-Advisor ${newCoAdvisorName1?default('.............................................')}</fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block>Signature................................................................</fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
        <fo:table-cell number-columns-spanned="4">
          <fo:block text-align="center" font-weight="bold">
            Former Advisory Comittee (In Case of Requesting Additional or Change)
          </fo:block>
        </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="2"><fo:block>1 Advisor ${advisorName?default('.............................................')}</fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block>Signature................................................................</fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="2"><fo:block>2 Co-Advisor ${coadvisorName?default('.............................................')}</fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block>Signature................................................................</fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="2"><fo:block>3 Co-Advisor ${coadvisorName1?default('.............................................')}</fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block>Signature................................................................</fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
        <fo:table-cell><fo:block></fo:block></fo:table-cell>
        <fo:table-cell number-columns-spanned="3"><fo:block text-align="right">
          Student's Signature...............................................
        </fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
        <fo:table-cell><fo:block></fo:block></fo:table-cell>
        <fo:table-cell number-columns-spanned="3">
          <fo:block text-align="right">
            (${firstName?if_exists} ${middleName?if_exists} ${lastName?if_exists})
          </fo:block>
        </fo:table-cell>
    </fo:table-row>

    <fo:table-row height="20px">
      <fo:table-cell><fo:block text-align="left" font-weight="bold">Remarks : </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3">
        <fo:block text-align="left" font-weight="bold">
          1. 1-3 advisors for doctoral degree student, 1-2 advisors for master degree student.
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block text-align="left" font-weight="bold"></fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3">
        <fo:block text-align="left" font-weight="bold">
          2. Students must complete coursework and industrial internship
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="60px">
      <fo:table-cell><fo:block text-align="left" font-weight="bold"></fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3">
        <fo:block text-align="left" font-weight="bold">
          3. Enclose advisor/s resume for new advisor who has not been in the TGGS advisor list.
        </fo:block>
      </fo:table-cell>
    </fo:table-row>

    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="4"><fo:block text-align="left" font-weight="bold">Consent of Program Coordinator and Head of Department</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="4"><fo:block>Thesis proposal and the title have been checked for its uniqueness.</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="4"><fo:block>O For Approval</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="50px">
      <fo:table-cell number-columns-spanned="4"><fo:block>O Other................................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="2"><fo:block>Department Academic Affair's Officer:</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>Program Coordinator:</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>Head of Department:</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="2"><fo:block>Signature....................</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>Signature....................</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>Signature....................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="2"><fo:block>(...........................)</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>(...........................)</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>(...........................)</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="50px">
      <fo:table-cell number-columns-spanned="2"><fo:block>Date.........................</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>Date.........................</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>Date.........................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="4"><fo:block text-align="left" font-weight="bold">Memo by the TGGS Academic Affairs</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="4"><fo:block text-align="left" font-weight="bold">To TGGS Associate Dean for Academic Affairs</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="center" font-weight="bold"> Number of 
      Advisees</fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="center" font-weight="bold"> Qualification</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="2"><fo:block>Advisor.............................................</fo:block></fo:table-cell>
      <fo:table-cell><fo:block text-align="center">O Qualified</fo:block></fo:table-cell>
      <fo:table-cell><fo:block text-align="center">O Not Qualified</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="2"><fo:block>Co-Advisor..........................................</fo:block></fo:table-cell>
      <fo:table-cell><fo:block text-align="center">O Qualified</fo:block></fo:table-cell>
      <fo:table-cell><fo:block text-align="center">O Not Qualified</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="2"><fo:block>Co-Advisor..........................................</fo:block></fo:table-cell>
      <fo:table-cell><fo:block text-align="center">O Qualified</fo:block></fo:table-cell>
      <fo:table-cell><fo:block text-align="center">O Not Qualified</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell><fo:block/></fo:table-cell>
      <fo:table-cell><fo:block text-align="center">O For Approval</fo:block></fo:table-cell>
      <fo:table-cell><fo:block text-align="center">O For Consideration</fo:block></fo:table-cell>
      <fo:table-cell><fo:block/></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="right">Signature.........................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="right">(...............................)</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="right">Date..............................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="4"><fo:block text-align="left" font-weight="bold">Approval of TGGS Associate Dean for Academic Affairs</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block text-align="left">O Approved</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block text-align="left">O Approved with condition....................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block text-align="left">O Not Approved</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block text-align="left">O Other......................................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="right">Signature.........................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="right">(...............................)</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="right">Date..............................</fo:block></fo:table-cell>
    </fo:table-row>
  </fo:table-body>
</fo:table>
</#escape>
