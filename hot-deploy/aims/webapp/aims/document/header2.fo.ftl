<#--
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
-->
<#escape x as x?xml>

<fo:table table-layout="fixed" width="100%" margin-top="15pt">
    <fo:table-body>
        <fo:table-row>
            <fo:table-cell>
                <fo:block text-align="left" font-weight="bold">
                	KMUTNB
                </fo:block>
            </fo:table-cell>
        </fo:table-row>
        <fo:table-row>
            <fo:table-cell>
                <fo:block text-align="left" font-size="10px">
                	King Mongkut's University of North Bangkok
                </fo:block>
            </fo:table-cell>
        </fo:table-row>
        <fo:table-row>
            <fo:table-cell>
                <fo:block text-align="left" font-size="10px">
                	Address 1518 Pracharat 1 Road,Wongsawang, Bangsue, Bangkok 10800
                </fo:block>
            </fo:table-cell>
        </fo:table-row>
       	<fo:table-row height="40.5px">
            <fo:table-cell border-bottom-style="solid" >
                <fo:block text-align="left" font-size="10px">
                	Tel. +66 2 555-2000 Fax +66 2 587-4350 
                </fo:block>
            </fo:table-cell>
        </fo:table-row>
    </fo:table-body>
</fo:table>
</#escape>
