<#escape x as x?xml>
  <fo:table table-layout="fixed" width="100%">
      <fo:table-body>

        <fo:table-row height="30px">
          <fo:table-cell number-columns-spanned="4"><fo:block text-align="center">The Sirindhorn International Thai - German Graduate School of Engineering</fo:block></fo:table-cell>
        </fo:table-row>

        <fo:table-row height="30px">
          <fo:table-cell number-columns-spanned="4"><fo:block text-align="center">King Mongkut’s University of Technology North Bangkok</fo:block></fo:table-cell>
        </fo:table-row>

        <fo:table-row height="30px">
          <fo:table-cell number-columns-spanned="4"><fo:block text-align="center">No. GS.__________ / ${year?if_exists} </fo:block></fo:table-cell>
        </fo:table-row>

        <fo:table-row height="30px">
          <fo:table-cell number-columns-spanned="4"><fo:block text-align="center">Subject: Approval of ${kind?if_exists} Title and Proposal  </fo:block></fo:table-cell>
        </fo:table-row>

        <fo:table-row height="60px">
          <fo:table-cell number-columns-spanned="4"><fo:block text-align="center">Semester ${semester?if_exists}  Academic Year ${year?if_exists}</fo:block></fo:table-cell>
        </fo:table-row>

        

        <fo:table-row height="60px">
          <fo:table-cell number-columns-spanned="4">
            <fo:block text-align="justify">
              The ${kind?if_exists} title and proposal of the Master of Science/Engineering Program in ${program?if_exists} (International Program) is approved by The Sirindhorn International Thai – German Graduate School of Engineering of King Mongkut’s University of Technology North Bangkok as follows:
            </fo:block>
          </fo:table-cell>
        </fo:table-row>

        <fo:table-row height="20px">
          <fo:table-cell border-style="solid" border-width="thin" border-color="black">
            <fo:block text-align="center" font-weight="bold">
              Name - Surname ID No.
            </fo:block>
          </fo:table-cell>
          <fo:table-cell border-style="solid" border-width="thin" border-color="black">
            <fo:block text-align="center" font-weight="bold">
              ${kind?if_exists} Title
            </fo:block>
          </fo:table-cell>
          <fo:table-cell border-style="solid" border-width="thin" border-color="black">
            <fo:block text-align="center" font-weight="bold">
              ${kind?if_exists} Advisor
            </fo:block>
          </fo:table-cell>
          <fo:table-cell border-style="solid" border-width="thin" border-color="black">
            <fo:block text-align="center" font-weight="bold">
              Approved Date
            </fo:block>
          </fo:table-cell>
        </fo:table-row>

        <fo:table-row height="60px">
          <fo:table-cell border-style="solid" border-width="thin" border-color="black">
            <fo:block>
              ${salutation?if_exists} ${firstName?if_exists} ${lastName?if_exists} (${studentId?if_exists}) 
            </fo:block>
          </fo:table-cell>
          <fo:table-cell border-style="solid" border-width="thin" border-color="black">
            <fo:block>
              ${title?if_exists}
            </fo:block>
          </fo:table-cell>
          <fo:table-cell border-style="solid" border-width="thin" border-color="black">
            <fo:block>
              ${advisorName?if_exists}
            </fo:block>
          </fo:table-cell>
          <fo:table-cell border-style="solid" border-width="thin" border-color="black">
            <fo:block>
              ${appDate?if_exists}
            </fo:block>
          </fo:table-cell>
        </fo:table-row>

        <fo:table-row height="80px">
          <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
          <fo:table-cell number-columns-spanned="2">
            <fo:block text-align="right">
                Declared on ____________________
            </fo:block>
          </fo:table-cell>
        </fo:table-row>

        <fo:table-row height="20px">
          <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
          <fo:table-cell number-columns-spanned="2">
            <fo:block text-align="center">
                (Asst. Prof. Dr. Monpilai  Narasingha)
            </fo:block>
          </fo:table-cell>
        </fo:table-row>

        <fo:table-row height="20px">
          <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
          <fo:table-cell number-columns-spanned="2">
            <fo:block text-align="center">
                Dean
            </fo:block>
          </fo:table-cell>
        </fo:table-row>

        <fo:table-row height="20px">
          <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
          <fo:table-cell number-columns-spanned="2">
            <fo:block text-align="center">
                The Sirindhorn International Thai – German Graduate School of Engineering
            </fo:block>
          </fo:table-cell>
        </fo:table-row>

      </fo:table-body>
  </fo:table>
</#escape>