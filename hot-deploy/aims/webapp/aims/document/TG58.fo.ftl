<#escape x as x?xml>
  <fo:table table-layout="fixed" width="100%">
      <fo:table-body>
        <fo:table-row height="30px">
          <fo:table-cell number-columns-spanned="4"><fo:block text-align="center">The Sirindhorn International Thai - German Graduate School of Engineering</fo:block></fo:table-cell>
        </fo:table-row>
        <fo:table-row height="30px">
          <fo:table-cell number-columns-spanned="4"><fo:block text-align="center">King Mongkut’s University of Technology North Bangkok</fo:block></fo:table-cell>
        </fo:table-row>
        <fo:table-row height="30px">
          <fo:table-cell number-columns-spanned="4"><fo:block text-align="center">No. GS. __________ / ${year?if_exists} </fo:block></fo:table-cell>
        </fo:table-row>
        <fo:table-row height="30px">
          <fo:table-cell number-columns-spanned="4"><fo:block text-align="center">Subject: Appointment of Advisory Comittee</fo:block></fo:table-cell>
        </fo:table-row>
        <fo:table-row height="60px">
          <fo:table-cell number-columns-spanned="4"><fo:block text-align="center">Semester ${semester?if_exists}  Academic Year ${year?if_exists}</fo:block></fo:table-cell> 
        </fo:table-row>
        <fo:table-row height="80px">
          <fo:table-cell number-columns-spanned="4">
            <fo:block text-align="justify">
              The Sirindhorn International Thai – German Graduate School of Engineering of King Mongkut’s University of Technology North Bangkok appointed the following members as a ${kind?if_exists} Advisory Committee for the thesis entitled of “${title?if_exists}” submitted by ${salutation?if_exists} ${firstName?if_exists} ${lastName?if_exists}, ID. No. ${studentId?if_exists}, Master of Science/Engineering Program in ${program?if_exists} (International Program):
            </fo:block>
          </fo:table-cell>
        </fo:table-row>
        <fo:table-row height="20px">
          <fo:table-cell border-style="solid" border-width="thin" border-color="black" number-columns-spanned="2">
            <fo:block text-align="center" font-weight="bold">
              Advisor/Co-Advisor
            </fo:block>
          </fo:table-cell>
          <fo:table-cell border-style="solid" border-width="thin" border-color="black" number-columns-spanned="2">
            <fo:block text-align="center" font-weight="bold">
              Name
            </fo:block>
          </fo:table-cell>
        </fo:table-row>

        <fo:table-row height="60px">
          <fo:table-cell border-style="solid" border-width="thin" border-color="black" number-columns-spanned="2">
            <fo:block text-align="center">
              Advisor               
            </fo:block>
          </fo:table-cell>
          <fo:table-cell border-style="solid" border-width="thin" border-color="black" number-columns-spanned="2">
            <fo:block>
              ${asalutation?if_exists} ${afirstName?if_exists} ${alastName?if_exists}
            </fo:block>
          </fo:table-cell>
        </fo:table-row>
        <fo:table-row height="60px">
          <fo:table-cell border-style="solid" border-width="thin" border-color="black" number-columns-spanned="2">
            <fo:block text-align="center">
              Co-Advisor               
            </fo:block>
          </fo:table-cell>
          <fo:table-cell border-style="solid" border-width="thin" border-color="black" number-columns-spanned="2">
            <fo:block>
              ${casalutation?if_exists} ${cafirstName?if_exists} ${calastName?if_exists}
            </fo:block>
          </fo:table-cell>
        </fo:table-row>
        <fo:table-row height="80px">
          <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
          <fo:table-cell number-columns-spanned="2">
            <fo:block text-align="right">
                Declared on ____________________
            </fo:block>
          </fo:table-cell>
        </fo:table-row>
        <fo:table-row height="20px">
          <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
          <fo:table-cell number-columns-spanned="2">
            <fo:block text-align="center">
                (Asst. Prof. Dr. Monpilai  Narasingha)
            </fo:block>
          </fo:table-cell>
        </fo:table-row>
        <fo:table-row height="20px">
          <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
          <fo:table-cell number-columns-spanned="2">
            <fo:block text-align="center">
                Dean
            </fo:block>
          </fo:table-cell>
        </fo:table-row>
        <fo:table-row height="20px">
          <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
          <fo:table-cell number-columns-spanned="2">
            <fo:block text-align="center">
                The Sirindhorn International Thai – German Graduate School of Engineering
            </fo:block>
          </fo:table-cell>
        </fo:table-row>
      </fo:table-body>
  </fo:table>
</#escape>