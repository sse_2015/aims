<#escape x as x?xml>
<fo:table table-layout="fixed" width="100%">
  <fo:table-body>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="4">
        <fo:block font-weight="bold" text-align="center">Application for Thesis Proposal Examination</fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="50px">
      <fo:table-cell number-columns-spanned="4">
        <fo:block font-weight="bold" text-align="center">The Sirindhorn Internation Thai - German Graduate School of Engineering</fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block>Date ${nowTimestamp}</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="3"><fo:block font-weight="bold">Subject : Request for Submitting Thesis Proposal</fo:block></fo:table-cell>
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="3"><fo:block font-weight="bold">To : TGGS Dean</fo:block></fo:table-cell>
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block>Name (Mr./Mrs./Miss) </fo:block></fo:table-cell>
      <fo:table-cell><fo:block font-weight="bold">${firstName} ${middleName?if_exists}</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>Last Name </fo:block></fo:table-cell>
      <fo:table-cell><fo:block font-weight="bold">${lastName?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block>ID. No. </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block font-weight="bold">${studentId?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block>Education Level: </fo:block></fo:table-cell>
      <fo:table-cell><fo:block>O Master's Degree</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>O Doctoral Degree</fo:block></fo:table-cell>
      <fo:table-cell><fo:block/></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block>Program : </fo:block></fo:table-cell>
      <fo:table-cell><fo:block font-weight="bold">${program}</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>Department : </fo:block></fo:table-cell>
      <fo:table-cell><fo:block font-weight="bold">${department}</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-colums-spanned="4"><fo:block>Address: </fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row>
      <fo:table-cell><fo:block>Mobile : </fo:block></fo:table-cell>
      <fo:table-cell><fo:block font-weight="bold">${mobile?if_exists}</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>E-mail Address : </fo:block></fo:table-cell>
      <fo:table-cell><fo:block font-weight="bold">${email?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="60px">
      <fo:table-cell number-columns-spanned="4"><fo:block>Proposed Thesis Title : </fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
        <fo:table-cell number-columns-spanned="2"><fo:block>Thesis enrolls in this semester..........credits</fo:block></fo:table-cell>
        <fo:table-cell number-columns-spanned="2"><fo:block>Average GPA (completed course work)................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
        <fo:table-cell number-columns-spanned="2"><fo:block></fo:block></fo:table-cell>
        <fo:table-cell number-columns-spanned="2"><fo:block text-align="left">
          Student's Signature...............................................
        </fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
        <fo:table-cell number-columns-spanned="3"><fo:block></fo:block></fo:table-cell>
        <fo:table-cell>
          <fo:block text-align="left">
            (${firstName} ${middleName?if_exists} ${lastName?if_exists})
          </fo:block>
        </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
        <fo:table-cell number-columns-spanned="4">
          <fo:block text-align="center" font-weight="bold">
            Approved by Student's Advisory Comittee
          </fo:block>
        </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="2"><fo:block>1....................................................Advisor</fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block>Signature................................................................</fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="2"><fo:block>2....................................................Co-Advisor</fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block>Signature................................................................</fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="2"><fo:block>3....................................................Co-Advisor</fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block>Signature................................................................</fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row>
      <fo:table-cell number-columns-spanned="4"><fo:block text-align="left">Enclose the thesis proposal and application for approval of appointment of thesis advisor (TG.51 and TG.02)</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block text-align="left" font-weight="bold">Remarks : </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3">
        <fo:block text-align="left" font-weight="bold">
          1. Students must complete coursework and industrial internship (Master Degree only)
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell><fo:block text-align="left" font-weight="bold"></fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3">
        <fo:block text-align="left" font-weight="bold">
          2. Students must submit TG.02, TG.51 and TG.52 forms together with this TG.01 form.
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="4"><fo:block text-align="left" font-weight="bold">Consent of Program Coordinator and Head of Department</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="4"><fo:block>Request Checked</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell><fo:block>O Detailed proposal</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>O TG.02</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>O TG.51</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>O TG.52</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="50px">
      <fo:table-cell number-columns-spanned="2"><fo:block>O Appointment of Advisory Comittee</fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block>O Approval of Thesis Title and Proposal</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="2"><fo:block>Department Academic Affair's Officer:</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>Program Coordinator:</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>Head of Department:</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="2"><fo:block>Signature....................</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>Signature....................</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>Signature....................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="2"><fo:block>(...........................)</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>(...........................)</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>(...........................)</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="50px">
      <fo:table-cell number-columns-spanned="2"><fo:block>Date.........................</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>Date.........................</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>Date.........................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="4"><fo:block text-align="left" font-weight="bold">Memo by the TGGS Academic Affairs</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="4"><fo:block text-align="left" font-weight="bold">To TGGS Associate Dean for Academic Affairs</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block text-align="left">O Complete course work requirement with GPA not less than 3.00</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block text-align="left">O Complete internship requirement</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="90px">
      <fo:table-cell><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block text-align="left">O For approval</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="right">Signature.........................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="right">(...............................)</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="right">Date..............................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="4"><fo:block text-align="left" font-weight="bold">Approval of TGGS Associate Dean for Academic Affairs</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block text-align="left">O Approved</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block text-align="left">O Approved with condition....................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block text-align="left">O Not Approved</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block text-align="left">O Other......................................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="right">Signature.........................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="right">(...............................)</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="right">Date..............................</fo:block></fo:table-cell>
    </fo:table-row>
  </fo:table-body>
</fo:table>
</#escape>