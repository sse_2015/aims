<#escape x as x?xml>
<fo:table table-layout="fixed" width="100%">
  <fo:table-body>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="4">
        <fo:block font-weight="bold" text-align="center">
          Thesis Proposal Examination Evaluation
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="4">
        <fo:block font-weight="bold">
          ${kind?if_exists}
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block>Title</fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3">
        <fo:block font-weight="bold">
          ${title?if_exists}
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block>By</fo:block></fo:table-cell>
      <fo:table-cell><fo:block font-weight="bold">${firstName?if_exists} ${middleName?if_exists} ${lastName?if_exists}</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>Student ID. No. </fo:block></fo:table-cell>
      <fo:table-cell><fo:block font-weight="bold">${studentId?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block>Major Field</fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block font-weight="bold">${program?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="4"><fo:block font-weight="bold">The Examination Comittee:</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block text-align="right">1. </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block font-weight="bold">${comittee?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="4"><fo:block font-weight="bold">Results</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block text-align="center">O </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block font-weight="bold">Pass</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block text-align="center">O </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block font-weight="bold">Pass with condition</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block text-align="center">O </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block font-weight="bold">Fail</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="4"><fo:block>Recommendation for Correction.................................................................................................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="4"><fo:block>...................................................................................................................................................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="4"><fo:block>...................................................................................................................................................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="60px">
      <fo:table-cell number-columns-spanned="4"><fo:block>...................................................................................................................................................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="center">Signature.........................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="center">(...............................)</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="center">Chairperson</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="center">..............................Member</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="center">Date ${date?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
  </fo:table-body>
</fo:table>

<fo:table table-layout="fixed" width="100%">
  <fo:table-body>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="4">
        <fo:block font-weight="bold" text-align="center">
          Thesis Proposal Examination Evaluation
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="4">
        <fo:block font-weight="bold">
          ${kind?if_exists}
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block>Title</fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3">
        <fo:block font-weight="bold">
          ${title?if_exists}
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block>By</fo:block></fo:table-cell>
      <fo:table-cell><fo:block font-weight="bold">${firstName?if_exists} ${middleName?if_exists} ${lastName?if_exists}</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>Student ID. No. </fo:block></fo:table-cell>
      <fo:table-cell><fo:block font-weight="bold">${studentId?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block>Major Field</fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block font-weight="bold">${program?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="4"><fo:block font-weight="bold">The Examination Comittee:</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block text-align="right">1. </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block font-weight="bold">${comittee1?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="4"><fo:block font-weight="bold">Results</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block text-align="center">O </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block font-weight="bold">Pass</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block text-align="center">O </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block font-weight="bold">Pass with condition</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block text-align="center">O </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block font-weight="bold">Fail</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="4"><fo:block>Recommendation for Correction.................................................................................................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="4"><fo:block>...................................................................................................................................................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="4"><fo:block>...................................................................................................................................................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="60px">
      <fo:table-cell number-columns-spanned="4"><fo:block>...................................................................................................................................................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="center">Signature.........................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="center">(...............................)</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="center">Chairperson</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="center">..............................Member</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="center">Date ${date?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
  </fo:table-body>
</fo:table>

<fo:table table-layout="fixed" width="100%">
  <fo:table-body>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="4">
        <fo:block font-weight="bold" text-align="center">
          Thesis Proposal Examination Evaluation
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="4">
        <fo:block font-weight="bold">
          ${kind?if_exists}
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block>Title</fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3">
        <fo:block font-weight="bold">
          ${title?if_exists}
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block>By</fo:block></fo:table-cell>
      <fo:table-cell><fo:block font-weight="bold">${firstName?if_exists} ${middleName?if_exists} ${lastName?if_exists}</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>Student ID. No. </fo:block></fo:table-cell>
      <fo:table-cell><fo:block font-weight="bold">${studentId?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block>Major Field</fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block font-weight="bold">${program?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="4"><fo:block font-weight="bold">The Examination Comittee:</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block text-align="right">1. </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block font-weight="bold">${comittee2?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="4"><fo:block font-weight="bold">Results</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block text-align="center">O </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block font-weight="bold">Pass</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block text-align="center">O </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block font-weight="bold">Pass with condition</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block text-align="center">O </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block font-weight="bold">Fail</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="4"><fo:block>Recommendation for Correction.................................................................................................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="4"><fo:block>...................................................................................................................................................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="4"><fo:block>...................................................................................................................................................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="60px">
      <fo:table-cell number-columns-spanned="4"><fo:block>...................................................................................................................................................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="center">Signature.........................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="center">(...............................)</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="center">Chairperson</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="center">..............................Member</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="center">Date ${date?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
  </fo:table-body>
</fo:table>
</#escape>
