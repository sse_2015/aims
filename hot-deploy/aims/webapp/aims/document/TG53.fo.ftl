<#escape x as x?xml>
  <fo:table table-layout="fixed" width="100%">
      <fo:table-body>

        <fo:table-row height="40px">
          <fo:table-cell number-columns-spanned="4"><fo:block font-weight="bold" text-align="center">Thesis Progress Examination</fo:block></fo:table-cell>
        </fo:table-row>

        <fo:table-row height="20px">
          <fo:table-cell>
              <fo:block>Topic</fo:block>
          </fo:table-cell>
          <fo:table-cell number-columns-spanned="3">
              <fo:block font-weight="bold">${topic?if_exists}</fo:block>
          </fo:table-cell>
        </fo:table-row>

         <fo:table-row height="20px">
          <fo:table-cell>
              <fo:block>By</fo:block>
          </fo:table-cell>
          <fo:table-cell number-columns-spanned="3">
              <fo:block font-weight="bold">${salutation?if_exists} ${firstName?if_exists} ${middleName?if_exists} ${lastName?if_exists}</fo:block>
          </fo:table-cell>
        </fo:table-row>

        <fo:table-row height="40px">
          <fo:table-cell>
              <fo:block>Program</fo:block>
          </fo:table-cell>
          <fo:table-cell>
              <fo:block font-weight="bold">${program?if_exists}</fo:block>
          </fo:table-cell>
          <fo:table-cell>
              <fo:block>Student ID.No.</fo:block>
          </fo:table-cell>
          <fo:table-cell>
              <fo:block font-weight="bold">${studentId?if_exists}</fo:block>
          </fo:table-cell>
        </fo:table-row>

        <fo:table-row height="20px">
          <fo:table-cell>
              <fo:block>Advisor</fo:block>
          </fo:table-cell>
          <fo:table-cell number-columns-spanned="3">
              <fo:block >1. ${advisor?if_exists}</fo:block>
          </fo:table-cell>
        </fo:table-row>

        <fo:table-row height="20px">
          <fo:table-cell>
              <fo:block>Co-Advisor</fo:block>
          </fo:table-cell>
          <fo:table-cell number-columns-spanned="3">
              <fo:block>2. ${coadvisor?if_exists}</fo:block>
          </fo:table-cell>
        </fo:table-row>

        <fo:table-row height="40px">
          <fo:table-cell>
              <fo:block>Co-Advisor</fo:block>
          </fo:table-cell>
          <fo:table-cell number-columns-spanned="3">
              <fo:block>3. ${coadvisor2?if_exists}</fo:block>
          </fo:table-cell>
        </fo:table-row>

        <fo:table-row height="20px">
          <fo:table-cell>
              <fo:block>Abstract</fo:block>
          </fo:table-cell>
        </fo:table-row>

        <fo:table-row height="120px" number-columns-spanned="2">
          <fo:table-cell number-columns-spanned="4">
              <fo:block text-align="justify">${studentAbstract?if_exists}</fo:block>
          </fo:table-cell>
        </fo:table-row>

        <fo:table-row height="20px">
          <fo:table-cell number-columns-spanned="2">
              <fo:block>Thesis Progress Examination</fo:block>
          </fo:table-cell>
        </fo:table-row>

        <fo:table-row height="20px" number-columns-spanned="2">
          <fo:table-cell>
              <fo:block text-align="left">Date + Time : </fo:block>
          </fo:table-cell>
          <fo:table-cell number-columns-spanned="3">
              <fo:block font-weight="bold">${thesisProgressExamination?if_exists}</fo:block>
          </fo:table-cell>
        </fo:table-row>

        <fo:table-row height="20px" number-columns-spanned="2">
          <fo:table-cell>
              <fo:block text-align="left">Room No : </fo:block>
          </fo:table-cell>
          <fo:table-cell number-columns-spanned="3">
              <fo:block font-weight="bold">${roomNo?if_exists}</fo:block>
          </fo:table-cell>
        </fo:table-row>

        <fo:table-row height="20px" number-columns-spanned="2">
          <fo:table-cell>
              <fo:block text-align="left">Building : </fo:block>
          </fo:table-cell>
          <fo:table-cell number-columns-spanned="3">
              <fo:block font-weight="bold">${building?if_exists}</fo:block>
          </fo:table-cell>
        </fo:table-row>

        <fo:table-row height="60px" >
          <fo:table-cell number-columns-spanned="4">
              <fo:block text-align="center">Advisory Signature</fo:block>
          </fo:table-cell>
        </fo:table-row>

        <fo:table-row height="20px">
          <fo:table-cell number-columns-spanned="4">
              <fo:block text-align="center">......................</fo:block>
          </fo:table-cell>
        </fo:table-row>

        <fo:table-row height="20px">
          <fo:table-cell number-columns-spanned="4">
              <fo:block text-align="center">(${advisor?default('...........................')})</fo:block>
          </fo:table-cell>
        </fo:table-row>

        <fo:table-row height="20px">
          <fo:table-cell number-columns-spanned="4">
              <fo:block text-align="center">${date?if_exists}</fo:block>
          </fo:table-cell>
        </fo:table-row>

      </fo:table-body>
  </fo:table>
</#escape>