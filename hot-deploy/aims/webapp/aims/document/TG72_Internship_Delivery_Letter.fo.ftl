<#escape x as x?xml>
<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <fo:layout-master-set>
        <fo:simple-page-master margin-right="20mm" margin-left="20mm" margin-bottom="20mm" margin-top="0mm" page-width="210mm" page-height="297mm" master-name="A4-portrail">
            <fo:region-body margin-bottom="20mm" margin-top="35mm"/>
            <fo:region-before precedence="true" display-align="before" extent="25mm" region-name="xsl-region-before"/>
        </fo:simple-page-master>
    </fo:layout-master-set>

    <fo:page-sequence master-reference="A4-portrail">
        <fo:static-content flow-name="xsl-region-before">
            <fo:table width="100%" table-layout="fixed">
                <fo:table-column/>
                <fo:table-body>
                    <fo:table-row>
                        <fo:table-cell display-align="center" text-align="center">
							<fo:block text-align="center">
						        <fo:external-graphic src="images/kmutnb.jpg" overflow="hidden" height="100px" content-width="1.00in"/>
						    </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-body>
            </fo:table>
        </fo:static-content>

        <fo:flow reference-orientation="0" border-collapse="collapse" flow-name="xsl-region-body">
            <fo:table width="100%" table-layout="fixed">
                <fo:table-column column-width="proportional-column-width(25)"/>
                <fo:table-column column-width="proportional-column-width(25)"/>
                <fo:table-column column-width="proportional-column-width(25)"/>
                <fo:table-column column-width="proportional-column-width(25)"/>
                <fo:table-body>
                    <fo:table-row height="10mm">
                        <fo:table-cell number-columns-spanned="4">
                            <fo:block font-size="140%" font-weight="bold" text-align="center">Form for Internship Delivery Letter</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row height="15mm">
                        <fo:table-cell number-columns-spanned="4">
                            <fo:block font-size="100%" font-weight="bold" text-align="center">The Sirindhorn International Thai - German Graduate School of Engineering</fo:block> 
							<fo:block font-size="100%" font-weight="bold" text-align="center">King Mongkut’s University of Technology North Bangkok</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row height="10mm">
                        <fo:table-cell number-columns-spanned="4">
                            <fo:block font-size="100%" text-align="right">Date ${dd} Month ${mm} Year ${yy}</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row height="10mm">
                        <fo:table-cell number-columns-spanned="3">
                            <fo:block font-weight="bold">Dear TGGS Director,</fo:block>
                        </fo:table-cell>
                        <fo:table-cell>
                            <fo:block></fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row height="7mm">
                        <fo:table-cell>
                            <fo:block>My name</fo:block>
                        </fo:table-cell>
                        <fo:table-cell>
                            <fo:block font-weight="bold" text-align="center">${name?if_exists}</fo:block>
                        </fo:table-cell>
                        <fo:table-cell>
                            <fo:block>Student ID</fo:block>
                        </fo:table-cell>
                        <fo:table-cell>
                            <fo:block font-weight="bold" text-align="center">${studentId?if_exists}</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row height="7mm">
                        <fo:table-cell number-columns-spanned="2">
                            <fo:block>I’m studying in Master program in</fo:block>
                        </fo:table-cell>
                        <fo:table-cell number-columns-spanned="2">
                            <fo:block font-weight="bold" text-align="center">${program?if_exists}</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row height="7mm">
                        <fo:table-cell>
                            <fo:block>Cell phone</fo:block>
                        </fo:table-cell>
                        <fo:table-cell>
                            <fo:block font-weight="bold" text-align="center">${studentTelecomNumber?if_exists}</fo:block>
                        </fo:table-cell>
                        <fo:table-cell>
                            <fo:block>Email</fo:block>
                        </fo:table-cell>
                        <fo:table-cell>
                            <fo:block font-weight="bold" text-align="center">${studentEmail?if_exists}</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row height="12mm">
                        <fo:table-cell number-columns-spanned="4">
                            <fo:block>Hereby, I  would like to request an application letter for doing industrial internship in the 
following enterprise. </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row height="7mm">
                        <fo:table-cell>
                            <fo:block>Company</fo:block>
                        </fo:table-cell>
                        <fo:table-cell number-columns-spanned="3">
                            <fo:block font-weight="bold" text-align="center">${company?if_exists}</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row height="7mm">
                        <fo:table-cell>
                            <fo:block>Company's Address</fo:block>
                        </fo:table-cell>
                        <fo:table-cell number-columns-spanned="3">
                            <fo:block font-weight="bold" text-align="center">${companyPostalAddress?if_exists}</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row height="7mm">
                        <fo:table-cell>
                            <fo:block>Telephone</fo:block>
                        </fo:table-cell>
                        <fo:table-cell>
                            <fo:block font-weight="bold" text-align="center">${companyTelecomNumber?if_exists}</fo:block>
                        </fo:table-cell>
                        <fo:table-cell>
                            <fo:block>Fax</fo:block>
                        </fo:table-cell>
                        <fo:table-cell>
                            <fo:block font-weight="bold" text-align="center"></fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row height="7mm">
                        <fo:table-cell number-columns-spanned="4">
                            <fo:block>Project Title: ${projectTitle?if_exists}</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row height="7mm">
                        <fo:table-cell>
                            <fo:block>Desired period</fo:block>
                        </fo:table-cell>
                        <fo:table-cell number-columns-spanned="3">
                            <fo:block font-weight="bold" text-align="center">${desiredPeriod?if_exists}</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row height="7mm">
                        <fo:table-cell number-columns-spanned="2">
                            <fo:block>Company’s Project Advisor:</fo:block>
                        </fo:table-cell>
                        <fo:table-cell number-columns-spanned="2">
                            <fo:block font-weight="bold" text-align="center">${companyAdvisor?if_exists}</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row height="7mm">
                        <fo:table-cell>
                            <fo:block>Position:</fo:block>
                        </fo:table-cell>
                        <fo:table-cell number-columns-spanned="3">
                            <fo:block font-weight="bold" text-align="center">${companyAdvisorPosition?if_exists}</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row height="7mm">
                        <fo:table-cell>
                            <fo:block>Telephone</fo:block>
                        </fo:table-cell>
                        <fo:table-cell number-columns-spanned="3">
                            <fo:block font-weight="bold" text-align="center">${companyAdvisorTelecomNumber?if_exists}</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row height="7mm">
                        <fo:table-cell>
                            <fo:block>Email</fo:block>
                        </fo:table-cell>
                        <fo:table-cell number-columns-spanned="3">
                            <fo:block font-weight="bold" text-align="center">${companyAdvisorEmail?if_exists}</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row height="7mm">
                        <fo:table-cell number-columns-spanned="2">
                            <fo:block>TGGS Project Advisor (TGGS Lecturer):</fo:block>
                        </fo:table-cell>
                        <fo:table-cell number-columns-spanned="2">
                            <fo:block font-weight="bold" text-align="center">${advisor?if_exists}</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row height="7mm">
                        <fo:table-cell>
                            <fo:block>Telephone</fo:block>
                        </fo:table-cell>
                        <fo:table-cell number-columns-spanned="3">
                            <fo:block font-weight="bold" text-align="center">${advisorTelecomNumber?if_exists}</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row height="7mm">
                        <fo:table-cell>
                            <fo:block>Email</fo:block>
                        </fo:table-cell>
                        <fo:table-cell number-columns-spanned="3">
                            <fo:block font-weight="bold" text-align="center">${advisorEmail?if_exists}</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row height="7mm">
                        <fo:table-cell number-columns-spanned="4">
                            <fo:block>Project Description: ${projectDescription?if_exists} </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row height="7mm">
                        <fo:table-cell number-columns-spanned="4">
                            <fo:block>I have been accepted to do an industrial internship. I request the following documents,  ${requestedDocuments?if_exists} </fo:block>
                        </fo:table-cell>
                    </fo:table-row>

                    <fo:table-row height="5mm">
                        <fo:table-cell number-columns-spanned="4">
                            <fo:block></fo:block>
                        </fo:table-cell>
                    </fo:table-row>

                    <fo:table-row height="7mm">
                        <fo:table-cell number-columns-spanned="2">
                        </fo:table-cell>
                        <fo:table-cell number-columns-spanned="2">
                            <fo:block text-align="center">Signature.............................Student</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row height="7mm">
                        <fo:table-cell number-columns-spanned="2">
                        </fo:table-cell>
                        <fo:table-cell number-columns-spanned="2">
                            <fo:block text-align="center">(${name?if_exists})</fo:block>
                        </fo:table-cell>
                    </fo:table-row>

                    <fo:table-row height="10mm">
                        <fo:table-cell number-columns-spanned="4">
                            <fo:block></fo:block>
                        </fo:table-cell>
                    </fo:table-row>

                    <fo:table-row height="7mm">
                        <fo:table-cell number-columns-spanned="4">
                            <fo:block text-align="right">(Front page)</fo:block>
                        </fo:table-cell>
                    </fo:table-row>


                    <fo:table-row height="7mm">
                        <fo:table-cell number-columns-spanned="2">
							<fo:block margin-right="6mm" page-break-before="always">TGGS Advisor’s Opinion</fo:block>
                        </fo:table-cell>
                        <fo:table-cell number-columns-spanned="2">
                            <fo:block margin-left="6mm" page-break-before="always">Coordinator’s Opinion</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row height="7mm">
                        <fo:table-cell number-columns-spanned="2">
							<fo:block margin-right="6mm">Herewith, I recommend the student to </fo:block>
                        </fo:table-cell>
                        <fo:table-cell number-columns-spanned="2">
                            <fo:block margin-left="6mm">.................................................................</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row height="7mm">
                        <fo:table-cell number-columns-spanned="2">
							<fo:block margin-right="6mm">apply for doing internship at the stated</fo:block>
                        </fo:table-cell>
                        <fo:table-cell number-columns-spanned="2">
                            <fo:block margin-left="6mm">.................................................................</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row height="7mm">
                        <fo:table-cell number-columns-spanned="2">
							<fo:block margin-right="6mm">company</fo:block>
                        </fo:table-cell>
                        <fo:table-cell number-columns-spanned="2">
                            <fo:block margin-left="6mm">.................................................................</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row height="7mm">
                        <fo:table-cell number-columns-spanned="2">
                            <fo:block text-align="center" margin-right="6mm">Signature.............................Advisor</fo:block>
                        </fo:table-cell>
                        <fo:table-cell number-columns-spanned="2">
                            <fo:block text-align="center" margin-left="6mm">Signature.............................Coordinator</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row height="7mm">
                        <fo:table-cell number-columns-spanned="2">
                            <fo:block text-align="center" margin-right="6mm">(${advisor?if_exists})</fo:block>
                        </fo:table-cell>
                        <fo:table-cell number-columns-spanned="2">
                            <fo:block text-align="center" margin-left="6mm">(.............................)</fo:block>
                        </fo:table-cell>
                    </fo:table-row>

                    <fo:table-row height="10mm">
                        <fo:table-cell number-columns-spanned="4">
                            <fo:block></fo:block>
                        </fo:table-cell>
                    </fo:table-row>

                    <fo:table-row height="7mm">
                        <fo:table-cell number-columns-spanned="2">
							<fo:block margin-right="6mm">Head of Department’s Opinion</fo:block>
                        </fo:table-cell>
                        <fo:table-cell number-columns-spanned="2">
                            <fo:block margin-left="6mm">TGGS Academic Affairs’ Memo</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row height="7mm">
                        <fo:table-cell number-columns-spanned="2">
                            <fo:block margin-right="6mm">.................................................................</fo:block>
                        </fo:table-cell>
                        <fo:table-cell number-columns-spanned="2">
							<fo:block margin-left="12mm">O To be considered</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row height="7mm">
                        <fo:table-cell number-columns-spanned="2">
                            <fo:block margin-right="6mm">.................................................................</fo:block>
                        </fo:table-cell>
                        <fo:table-cell number-columns-spanned="2">
							<fo:block margin-left="12mm">O To be approved</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row height="7mm">
                        <fo:table-cell number-columns-spanned="2">
                            <fo:block margin-right="6mm">.................................................................</fo:block>
                        </fo:table-cell>
                        <fo:table-cell number-columns-spanned="2">
                            <fo:block margin-left="12mm">O ..........................................................</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row height="7mm">
                        <fo:table-cell number-columns-spanned="2">
                            <fo:block text-align="center" margin-right="6mm">Signature.............................Head of Department</fo:block>
                        </fo:table-cell>
                        <fo:table-cell number-columns-spanned="2">
                            <fo:block text-align="center" margin-left="6mm">Signature.............................</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row height="7mm">
                        <fo:table-cell number-columns-spanned="2">
                            <fo:block text-align="center" margin-right="6mm">(.............................)</fo:block>
                        </fo:table-cell>
                        <fo:table-cell number-columns-spanned="2">
                            <fo:block text-align="center" margin-left="6mm"></fo:block>
                        </fo:table-cell>
                    </fo:table-row>

                    <fo:table-row height="10mm">
                        <fo:table-cell number-columns-spanned="4">
                            <fo:block></fo:block>
                        </fo:table-cell>
                    </fo:table-row>

                    <fo:table-row height="7mm">
                        <fo:table-cell number-columns-spanned="4">
                            <fo:block>TGGS Associate Dean for Academic Affair’s Opinion</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row height="7mm">
                        <fo:table-cell number-columns-spanned="4">
                            <fo:block>................................................................................................................................................</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row height="7mm">
                        <fo:table-cell number-columns-spanned="4">
                            <fo:block>................................................................................................................................................</fo:block>
                        </fo:table-cell>
                    </fo:table-row>

                    <fo:table-row height="10mm">
                        <fo:table-cell number-columns-spanned="4">
                            <fo:block></fo:block>
                        </fo:table-cell>
                    </fo:table-row>

                    <fo:table-row height="7mm">
						<fo:table-cell/>
                        <fo:table-cell number-columns-spanned="3">
                            <fo:block text-align="center">Signature............................................Associate Dean</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row height="7mm">
						<fo:table-cell/>
                        <fo:table-cell number-columns-spanned="3">
                            <fo:block text-align="center">(Asst. Prof. Dr. Tawiwan  Kangsadan)</fo:block>
                        </fo:table-cell>
                    </fo:table-row>

                    <fo:table-row height="7mm">
                        <fo:table-cell number-columns-spanned="4">
                            <fo:block>Dean Opinion</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row height="7mm">
                        <fo:table-cell number-columns-spanned="4">
                            <fo:block>................................................................................................................................................</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row height="7mm">
                        <fo:table-cell number-columns-spanned="4">
                            <fo:block>................................................................................................................................................</fo:block>
                        </fo:table-cell>
                    </fo:table-row>

                    <fo:table-row height="10mm">
                        <fo:table-cell number-columns-spanned="4">
                            <fo:block></fo:block>
                        </fo:table-cell>
                    </fo:table-row>

                    <fo:table-row height="7mm">
						<fo:table-cell/>
                        <fo:table-cell number-columns-spanned="3">
                            <fo:block text-align="center">Signature............................................Dean</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row height="7mm">
						<fo:table-cell/>
                        <fo:table-cell number-columns-spanned="3">
                            <fo:block text-align="center">(Asst. Prof. Dr. Monpilai  Narasingha)</fo:block>
                        </fo:table-cell>
                    </fo:table-row>

                    <fo:table-row height="15mm">
                        <fo:table-cell number-columns-spanned="4">
                            <fo:block></fo:block>
                        </fo:table-cell>
                    </fo:table-row>

                    <fo:table-row height="7mm">
                        <fo:table-cell number-columns-spanned="4">
                            <fo:block text-align="right">(Back page)</fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-body>
            </fo:table>
        </fo:flow>
    </fo:page-sequence>
</fo:root>
</#escape>
