<#escape x as x?xml>
<fo:table table-layout="fixed" width="100%">
  <fo:table-body>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="4">
        <fo:block font-weight="bold" text-align="center">
          Thesis Defense Examination Evaluation
        </fo:block>
      </fo:table-cell>
    </fo:table-row>

    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned= "4">
        <fo:block text-align = "center">
          The Sirindhorn International Thai-German Graduate School of Engineering
        </fo:block>
      </fo:table-cell>
    </fo:table-row>

    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned = "4">
        <fo:block text-align = "center">
          King Mongkut’s University of Technology North Bangkok
        </fo:block>
      </fo:table-cell>
    </fo:table-row>

    <fo:table-row height="30px">
      <fo:table-cell><fo:block>${kind?if_exists} Title: </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3">
        <fo:block font-weight="bold">
           ${title?if_exists}
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell><fo:block>Student's Name : </fo:block></fo:table-cell>
      <fo:table-cell><fo:block font-weight="bold">${firstName?if_exists} ${middleName?if_exists} ${lastName?if_exists}</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>Student ID. No. : </fo:block></fo:table-cell>
      <fo:table-cell><fo:block font-weight="bold">${studentId?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell><fo:block>Major Field : </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block font-weight="bold">${program?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell><fo:block>Department : </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block font-weight="bold">${program?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell><fo:block>Semester/Year : </fo:block></fo:table-cell>
      <fo:table-cell><fo:block font-weight="bold">${semester?if_exists}</fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block>Master Thesis : 12 Credits</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell><fo:block>Date</fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block font-weight="bold">${date?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="4"><fo:block font-weight="bold">Advice/Recommendation</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell><fo:block>Results : </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block font-weight="bold">${defenseResult?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="80px">
      <fo:table-cell number-columns-spanned="3"><fo:block>Grading scale for student's Thesis is : </fo:block></fo:table-cell>
      <fo:table-cell><fo:block font-weight="bold">${thesisGrade?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>

    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="center">Signature.........................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="center">(...............................)</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="center">Chairperson</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="center">..............................Member</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="center">Date ${date?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
  </fo:table-body>
</fo:table>

<fo:table table-layout="fixed" width="100%" margin-top="20px">
  <fo:table-body>
    <fo:table-row height="30px">
      <fo:table-cell><fo:block>Committee Name : </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block font-weight="bold">${comittee?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell><fo:block>Student Name : </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block font-weight="bold">${firstName?if_exists} ${middleName?if_exists} ${lastName?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell><fo:block>Student ID. No. : </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block font-weight="bold">${studentId?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell><fo:block>Major Field : </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block font-weight="bold">${program?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell><fo:block>Thesis Title : </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block font-weight="bold">${title?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="4"><fo:block font-weight="bold">
        Evaluated Criterion
      </fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="4"><fo:block>
        Part  1 :    To evaluate the correctness of the research for this thesis. Please fill the form with a symbol (V) to evaluate the following items for correction and completion of the thesis.
      </fo:block></fo:table-cell>
    </fo:table-row>
  </fo:table-body>
</fo:table>

<fo:table table-layout="fixed" width="100%" border-style="solid" margin-top="20px">
  <fo:table-body>
    <fo:table-row height="10px">
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center" font-weight="bold"> Evaluation Item</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center" font-weight="bold">Satisfy</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center" font-weight="bold">Correction on Needed</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center" font-weight="bold">Comment</fo:block></fo:table-cell>
    </fo:table-row>

    <fo:table-row height="10px">
      <fo:table-cell border-style="solid" padding="5px"><fo:block> Thesis Title in Thai</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">__________________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">__________________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">__________________</fo:block></fo:table-cell>
    </fo:table-row>

    <fo:table-row height="10px">
      <fo:table-cell border-style="solid" padding="5px"><fo:block> Thesis Title in English</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">__________________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">__________________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">__________________</fo:block></fo:table-cell>
    </fo:table-row> 

    <fo:table-row height="10px">
      <fo:table-cell padding="5px"><fo:block font-weight="bold">Detail of thesis</fo:block></fo:table-cell>
    </fo:table-row>

    <fo:table-row height="10px">
      <fo:table-cell border-style="solid" padding="5px"><fo:block>Introduction and motivation</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">__________________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">__________________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">__________________</fo:block></fo:table-cell>
    </fo:table-row>

    <fo:table-row height="10px">
      <fo:table-cell border-style="solid" padding="5px"><fo:block>Objective of the research</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">__________________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">__________________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">__________________</fo:block></fo:table-cell>
    </fo:table-row>

    <fo:table-row height="10px">
      <fo:table-cell border-style="solid" padding="5px"><fo:block>Scope of the research</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">__________________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">__________________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">__________________</fo:block></fo:table-cell>
    </fo:table-row>

    <fo:table-row height="10px">
      <fo:table-cell border-style="solid" padding="5px"><fo:block>Literature reviews</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">__________________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">__________________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">__________________</fo:block></fo:table-cell>
    </fo:table-row>

    <fo:table-row height="10px">
      <fo:table-cell border-style="solid" padding="5px"><fo:block>Research Methodology</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">__________________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">__________________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">__________________</fo:block></fo:table-cell>
    </fo:table-row>

    <fo:table-row height="10px">
      <fo:table-cell border-style="solid" padding="5px"><fo:block>Results and discussion</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">__________________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">__________________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">__________________</fo:block></fo:table-cell>
    </fo:table-row>

    <fo:table-row height="10px">
      <fo:table-cell border-style="solid" padding="5px"><fo:block>Conclusion and suggestions</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">__________________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">__________________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">__________________</fo:block></fo:table-cell>
    </fo:table-row>

    <fo:table-row height="10px">
      <fo:table-cell padding="5px"><fo:block>Others, please specify</fo:block></fo:table-cell>
    </fo:table-row>

    <fo:table-row height="10px">
      <fo:table-cell border-style="solid" padding="5px"><fo:block>__________________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">__________________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">__________________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">__________________</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="10px">
      <fo:table-cell border-style="solid" padding="5px"><fo:block>__________________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">__________________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">__________________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">__________________</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="10px">
      <fo:table-cell border-style="solid" padding="5px"><fo:block>__________________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">__________________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">__________________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">__________________</fo:block></fo:table-cell>
    </fo:table-row>
  </fo:table-body>
</fo:table>

<fo:table table-layout="fixed" width="100%" margin-top="20px">
  <fo:table-body>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="4"><fo:block>
        Remark : If the thesis title is changed, please fill the thesis title changing form TG. and submit together with the Thesis Evaluation Form.
      </fo:block></fo:table-cell>
    </fo:table-row>
  </fo:table-body>
</fo:table>

<fo:table table-layout="fixed" width="100%" margin-top="20px">
  <fo:table-body>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="4"><fo:block>
        Part 2 :    To evaluate the correctness of the research for this thesis. Please fill the form with a symbol (V) to evaluate the following items for correction and completion of the thesis.
      </fo:block></fo:table-cell>
    </fo:table-row>
  </fo:table-body>
</fo:table>

<fo:table table-layout="fixed" width="100%" border-style="solid" margin-top="20px">
  <fo:table-body>
    <fo:table-row height="10px">
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center" font-weight="bold"> Evaluation Item</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center" font-weight="bold">Very Good</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center" font-weight="bold">Good</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center" font-weight="bold">Average</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center" font-weight="bold">Poor</fo:block></fo:table-cell>
    </fo:table-row>

    <fo:table-row height="10px">
      <fo:table-cell padding="5px"><fo:block font-weight="bold"> Thesis Manuscript</fo:block></fo:table-cell>
    </fo:table-row>

    <fo:table-row height="10px">
      <fo:table-cell border-style="solid" padding="5px"><fo:block> Correctness of language which has been written</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">_______________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">_______________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">_______________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">_______________</fo:block></fo:table-cell>
    </fo:table-row>

    <fo:table-row height="10px">
      <fo:table-cell border-style="solid" padding="5px"><fo:block> Correctness of thesis contents</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">_______________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">_______________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">_______________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">_______________</fo:block></fo:table-cell>
    </fo:table-row>

    <fo:table-row height="10px">
      <fo:table-cell border-style="solid" padding="5px"><fo:block> Completion of thesis</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">_______________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">_______________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">_______________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">_______________</fo:block></fo:table-cell>
    </fo:table-row>

    <fo:table-row height="10px">
      <fo:table-cell border-style="solid" padding="5px"><fo:block> Amount of thesis work</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">_______________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">_______________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">_______________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">_______________</fo:block></fo:table-cell>
    </fo:table-row>

    <fo:table-row height="10px">
      <fo:table-cell padding="5px"><fo:block font-weight="bold"> Thesis Presentation</fo:block></fo:table-cell>
    </fo:table-row>

    <fo:table-row height="10px">
      <fo:table-cell border-style="solid" padding="5px"><fo:block> Format of presentation</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">_______________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">_______________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">_______________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">_______________</fo:block></fo:table-cell>
    </fo:table-row>

    <fo:table-row height="10px">
      <fo:table-cell border-style="solid" padding="5px"><fo:block> Correctness of presentation</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">_______________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">_______________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">_______________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">_______________</fo:block></fo:table-cell>
    </fo:table-row>

    <fo:table-row height="10px">
      <fo:table-cell border-style="solid" padding="5px"><fo:block> Explanation and understanding of subjects</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">_______________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">_______________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">_______________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">_______________</fo:block></fo:table-cell>
    </fo:table-row>

    <fo:table-row height="10px">
      <fo:table-cell border-style="solid" padding="5px"><fo:block> Answering questions</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">_______________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">_______________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">_______________</fo:block></fo:table-cell>
      <fo:table-cell border-style="solid" padding="5px"><fo:block text-align="center">_______________</fo:block></fo:table-cell>
    </fo:table-row>
  </fo:table-body>
</fo:table>

<fo:table table-layout="fixed" width="100%" margin-top="20px">
  <fo:table-body>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="4"><fo:block>
        Other Suggestions
      </fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="4"><fo:block>
        _________________________________________________________________________________
      </fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="4"><fo:block>
        _________________________________________________________________________________
      </fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="4"><fo:block>
        _________________________________________________________________________________
      </fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="4"><fo:block>
        _________________________________________________________________________________
      </fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="4"><fo:block>
        _________________________________________________________________________________
      </fo:block></fo:table-cell>
    </fo:table-row>

    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="4"><fo:block font-weight="bold">
        Thesis Defense Examination Result
      </fo:block></fo:table-cell>
    </fo:table-row>

    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="4"><fo:block font-weight="bold">
        O Pass
      </fo:block></fo:table-cell>
    </fo:table-row>

    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="4"><fo:block font-weight="bold">
        O Pass with revision
      </fo:block></fo:table-cell>
    </fo:table-row>

    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="4"><fo:block font-weight="bold">
        O Fail
      </fo:block></fo:table-cell>
    </fo:table-row>

    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="center">Signature.........................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="center">(...............................)</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="center">Comittee</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="center">Date ${date?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="4"><fo:block>
        Remark :  The weighting of each item depends on the judgement of the committee. 
      </fo:block></fo:table-cell>
    </fo:table-row>
  </fo:table-body>
</fo:table>
</#escape>