<#escape x as x?xml>
<fo:table table-layout="fixed" width="100%">
  <fo:table-body>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="5"><fo:block font-weight="bold" text-align="center">A Thesis Proposal</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="5"><fo:block font-weight="bold" text-align="center">Presented to</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="5">
        <fo:block font-weight="bold" text-align="center">The Sirindhorn International Thai - German Graduate School of Engineering</fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="5">
        <fo:block font-weight="bold" text-align="center">
          King Mongkut's University of Technology North Bangkok
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="100px">
      <fo:table-cell><fo:block>Title : </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="4">
        <fo:block font-weight="bold">
          ${title?if_exists}
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="5">
        <fo:block text-align="center">
          By
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block/></fo:table-cell>
      <fo:table-cell><fo:block>Name : </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3">
        <fo:block font-weight="bold">
          ${salutation?if_exists} ${firstName?if_exists} ${middleName?if_exists} ${lastName?if_exists}
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block/></fo:table-cell>
      <fo:table-cell><fo:block>ID. NO : </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3">
        <fo:block font-weight="bold">
          ${studentId?if_exists}
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="60px">
      <fo:table-cell><fo:block/></fo:table-cell>
      <fo:table-cell><fo:block>Program : </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3">
        <fo:block font-weight="bold">
          ${program?if_exists}
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block/></fo:table-cell>
      <fo:table-cell><fo:block/></fo:table-cell>
      <fo:table-cell><fo:block/></fo:table-cell>
      <fo:table-cell><fo:block>Advisor : </fo:block></fo:table-cell>
      <fo:table-cell>
        <fo:block font-weight="bold">
          ${advisorName?if_exists}
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block/></fo:table-cell>
      <fo:table-cell><fo:block/></fo:table-cell>
      <fo:table-cell><fo:block/></fo:table-cell>
      <fo:table-cell><fo:block>Co-Advisor : </fo:block></fo:table-cell>
      <fo:table-cell>
        <fo:block font-weight="bold">
          ${coadvisorName?if_exists}
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell><fo:block/></fo:table-cell>
      <fo:table-cell><fo:block/></fo:table-cell>
      <fo:table-cell><fo:block/></fo:table-cell>
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
      <fo:table-cell>
        <fo:block font-weight="bold">
          ${coadvisorName1?if_exists}
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          Thesis Proposal Examination
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell><fo:block>Date + Time : </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="2">
        <fo:block font-weight="bold">
          ${dateAndTime?if_exists}
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell><fo:block>Room No : </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="2">
        <fo:block font-weight="bold">
          ${roomNo?if_exists}
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="100px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell><fo:block>Building : </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="2">
        <fo:block font-weight="bold">
          ${building?if_exists}
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          Details of a proposal for a thesis are composed of:
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          1. Background and General Statement of the Problem
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          2. Purpose of the Study
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          3. Scope of the Study
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          4. Methodology
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          5. Reference Materials
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="60px">
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          6. Utilization of the Study
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="5">
        <fo:block>
          Remarks : The proposal should not be more than 5 pages.
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
  </fo:table-body>
</fo:table>
</#escape>