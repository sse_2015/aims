<#escape x as x?xml>
<fo:table table-layout="fixed" width="100%">
  <fo:table-column column-width="4%"/>
  <fo:table-column column-width="24%"/>
  <fo:table-column column-width="24%"/>
  <fo:table-column column-width="24%"/>
  <fo:table-column column-width="24%"/>
  <fo:table-body>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="5"><fo:block font-weight="bold" text-align="center">Advisor's Professional Qualification</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block>1. </fo:block></fo:table-cell>
      <fo:table-cell><fo:block>First Name </fo:block></fo:table-cell>
      <fo:table-cell><fo:block font-weight="bold">${firstName?if_exists} ${middleName?if_exists}</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>Last Name </fo:block></fo:table-cell>
      <fo:table-cell><fo:block font-weight="bold">${lastName?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
      <fo:table-cell><fo:block>Position </fo:block></fo:table-cell>
      <fo:table-cell><fo:block font-weight="bold">________________</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>Telephone No </fo:block></fo:table-cell>
      <fo:table-cell><fo:block font-weight="bold">________________</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
      <fo:table-cell><fo:block>Place of Present Work </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block font-weight="bold">_______________________________________________________</fo:block></fo:table-cell>
      </fo:table-row>
    <fo:table-row height="60px">
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
      <fo:table-cell><fo:block>E-mail Address </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block font-weight="bold">${email?if_exists}</fo:block></fo:table-cell>
      </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell><fo:block>2. </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="4"><fo:block>Education/Qualification</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="4"><fo:block>2.1 Doctoral Degree</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
      <fo:table-cell><fo:block>Name of the Degree</fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3" font-weight="bold"><fo:block>_______________</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
      <fo:table-cell><fo:block>Specialization </fo:block></fo:table-cell>
      <fo:table-cell><fo:block font-weight="bold">_______________</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>Graduation Date </fo:block></fo:table-cell>
      <fo:table-cell><fo:block font-weight="bold">_______________</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
      <fo:table-cell><fo:block>Institution</fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3" font-weight="bold"><fo:block>_______________</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
      <fo:table-cell><fo:block>Place of Institution</fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3" font-weight="bold"><fo:block>_______________</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="4"><fo:block>2.2 Master Degree</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
      <fo:table-cell><fo:block>Name of the Degree</fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3" font-weight="bold"><fo:block>_______________</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
      <fo:table-cell><fo:block>Specialization </fo:block></fo:table-cell>
      <fo:table-cell><fo:block font-weight="bold">_______________</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>Graduation Date </fo:block></fo:table-cell>
      <fo:table-cell><fo:block font-weight="bold">_______________</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
      <fo:table-cell><fo:block>Institution</fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3" font-weight="bold"><fo:block>_______________</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
      <fo:table-cell><fo:block>Place of Institution</fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3" font-weight="bold"><fo:block>_______________</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="4"><fo:block>2.3 Other Degree</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="4" font-weight="bold"><fo:block>_________________________________________________________________________</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="4" font-weight="bold"><fo:block>_________________________________________________________________________</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="100px">
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="4" font-weight="bold"><fo:block>_________________________________________________________________________</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell><fo:block>3. </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="4"><fo:block>Experience</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block>3.1 Doctoral Degree</fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="2" font-weight="bold"><fo:block>_______________ years</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block>Master Degree</fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="2" font-weight="bold"><fo:block>_______________ years</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block>Teaching Bachelor's Degree</fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="2" font-weight="bold"><fo:block>_______________ years</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell ><fo:block></fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block>Teaching Subjects</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="4"><fo:block>_________________________________________________________________________</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="4"><fo:block>_________________________________________________________________________</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="4"><fo:block>3.2 Main Academic Duties</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="4"><fo:block>_________________________________________________________________________</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="4"><fo:block>_________________________________________________________________________</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="4"><fo:block>3.3 Other Training</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="4"><fo:block>_________________________________________________________________________</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="4"><fo:block>_________________________________________________________________________</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell><fo:block>4. </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="4"><fo:block>Research and/or Publications</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="4"><fo:block>4.1______________________________________________________________________</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="4"><fo:block>________________________________________________________________________</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="4"><fo:block>4.2______________________________________________________________________</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="4"><fo:block>________________________________________________________________________</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="4"><fo:block>4.3______________________________________________________________________</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="4"><fo:block>________________________________________________________________________</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="3"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="center">Signature.........................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="3"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="center">(${firstName?if_exists} ${middleName?if_exists} ${lastName?if_exists})</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="60px">
      <fo:table-cell number-columns-spanned="3"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="center">Date ${date?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell><fo:block font-weight="bold">Remark : </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="left">Coordinator must sign the CV. To certify all information</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="3"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="center">Signature.........................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="3"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="center">(...............................)</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="3"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="center">Program Coordinator</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="3"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="center">Date ${date?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
  </fo:table-body>
</fo:table>
</#escape>