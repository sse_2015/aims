<#escape x as x?xml>
<fo:table table-layout="fixed" width="95%">
  <fo:table-body>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="4">
        <fo:block font-weight="bold" text-align="center">Application for Thesis Defense Examination</fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="50px">
      <fo:table-cell number-columns-spanned="4">
        <fo:block font-weight="bold" text-align="center">The Sirindhorn Internation Thai - German Graduate School of Engineering</fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block>Date ${date?default(nowTimestamp)}</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="3"><fo:block font-weight="bold">Subject : Request for Defense Examination</fo:block></fo:table-cell>
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="3"><fo:block font-weight="bold">To : TGGS Dean</fo:block></fo:table-cell>
      <fo:table-cell><fo:block></fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block>Name </fo:block></fo:table-cell>
      <fo:table-cell><fo:block font-weight="bold">${salutation?if_exists} ${firstName?if_exists} ${middleName?if_exists}</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>Last Name </fo:block></fo:table-cell>
      <fo:table-cell><fo:block font-weight="bold">${lastName?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block>ID. No. </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block font-weight="bold">${studentId?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block>Education Level: </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned = "2"><fo:block font-weight="bold">${kind?if_exists}</fo:block></fo:table-cell>
      <fo:table-cell><fo:block/></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block>Program : </fo:block></fo:table-cell>
      <fo:table-cell><fo:block font-weight="bold">${program?if_exists}</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>Department : </fo:block></fo:table-cell>
      <fo:table-cell><fo:block font-weight="bold">${department?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block>Address: </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block font-weight="bold">${address?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row>
      <fo:table-cell><fo:block>Mobile : </fo:block></fo:table-cell>
      <fo:table-cell><fo:block font-weight="bold">${telephone?if_exists}</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>E-mail Address : </fo:block></fo:table-cell>
      <fo:table-cell><fo:block font-weight="bold">${email?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>

    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="2"><fo:block>The thesis title and proposal were approved on </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block font-weight="bold">${declaredDate?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="2"><fo:block>Thesis progress examination passed on</fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block font-weight="bold">${thesisProgressExamination?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="2"><fo:block>I wish to take the thesis defense examination </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block font-weight="bold">${thesisDefenseExamination?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block>Room No. :</fo:block></fo:table-cell>
      <fo:table-cell><fo:block font-weight="bold">${roomNo?if_exists}</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>Building :</fo:block></fo:table-cell>
      <fo:table-cell><fo:block font-weight="bold">${building?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="4"><fo:block>I have fulfilled all Graduate Regulation and curriculum requirements ${semesterCredits?if_exists} credits with GPA ${gpa?if_exists}</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="4"><fo:block>Enclosed - ......... copies of thesis for the examination comittee and TGGS Academic Affairs</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
        <fo:table-cell><fo:block text-align="left"></fo:block></fo:table-cell>
        <fo:table-cell number-columns-spanned="3"><fo:block text-align="right">
          Student's Signature...............................................
        </fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="120px">
        <fo:table-cell><fo:block></fo:block></fo:table-cell>
        <fo:table-cell number-columns-spanned="3">
          <fo:block text-align="right">
            (${firstName?if_exists} ${middleName?if_exists} ${lastName?if_exists})
          </fo:block>
        </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
        <fo:table-cell number-columns-spanned="4">
          <fo:block text-align="center" font-weight="bold">
            Approved by Student's Advisory Comittee
          </fo:block>
        </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="2"><fo:block>1 Advisor ${asalutation?if_exists} ${afirstName?if_exists} ${amiddleName?if_exists} ${alastName?if_exists}</fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block>Signature................................................................</fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="2"><fo:block>2 Co-Advisor ${casalutation?if_exists} ${cafirstName?if_exists} ${camiddleName?if_exists} ${calastName?if_exists}</fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block>Signature................................................................</fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="2"><fo:block>3 Co-Advisor ${ca1salutation?if_exists} ${ca1firstName?if_exists} ${ca1middleName?if_exists} ${ca1lastName?if_exists}</fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block>Signature................................................................</fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block text-align="left" font-weight="bold">Remarks : </fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3">
        <fo:block text-align="left" font-weight="bold">
          1. Defense examination must be taken after the approval of thesis title at least 60 days for master degree student and at least 120 days for doctoral degree.
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell><fo:block text-align="left" font-weight="bold"></fo:block></fo:table-cell>
      <fo:table-cell number-columns-spanned="3">
        <fo:block text-align="left" font-weight="bold">
          2. Students must submit TG.54 form together with this TG.04 form
        </fo:block>
      </fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="4"><fo:block text-align="left" font-weight="bold">Consent of Program Coordinator and Head of Department</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="4"><fo:block>The thesis has been checked.</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="4"><fo:block>O For Approval</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="40px">
      <fo:table-cell number-columns-spanned="4"><fo:block>O For consideration because of........................................................................................</fo:block></fo:table-cell>
    </fo:table-row>

    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="4"><fo:block font-weight="bold">The Thesis Defense Examination Comittee and TGGS Representative are composed of</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="4"><fo:block>1. Name..............................................Academic/Admministration Position................................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="4"><fo:block>Office...............................................................................Phone No...........................................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="4"><fo:block>2. Name..............................................Academic/Admministration Position................................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="4"><fo:block>Office...............................................................................Phone No...........................................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="20px">
      <fo:table-cell number-columns-spanned="4"><fo:block>3. Name..............................................Academic/Admministration Position................................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="120px">
      <fo:table-cell number-columns-spanned="4"><fo:block>Office...............................................................................Phone No...........................................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="2"><fo:block>Department Academic Affair's Officer:</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>Program Coordinator:</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>Head of Department:</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="2"><fo:block>Signature....................</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>Signature....................</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>Signature....................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="2"><fo:block>(...........................)</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>(...........................)</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>(...........................)</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="50px">
      <fo:table-cell number-columns-spanned="2"><fo:block>Date.........................</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>Date.........................</fo:block></fo:table-cell>
      <fo:table-cell><fo:block>Date.........................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="4"><fo:block text-align="left" font-weight="bold">Memo by the TGGS Academic Affairs</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="4"><fo:block text-align="left" font-weight="bold">To TGGS Associate Dean for Academic Affairs</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block text-align="left">O The applicant is qualified to take the thesis defense examination. Please sign on the attached appointment of the thesis examination comittee.</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="right">Signature.........................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="right">(...............................)</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="right">Date..............................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="4"><fo:block text-align="left" font-weight="bold">Approval of TGGS Associate Dean for Academic Affairs</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block text-align="left">O Approved</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block text-align="left">O Approved with condition....................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block text-align="left">O Not Approved</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="3"><fo:block text-align="left">O Other......................................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="right">Signature.........................</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="right">(...............................)</fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row height="30px">
      <fo:table-cell number-columns-spanned="2"><fo:block/></fo:table-cell>
      <fo:table-cell number-columns-spanned="2"><fo:block text-align="right">Date..............................</fo:block></fo:table-cell>
    </fo:table-row>
  </fo:table-body>
</fo:table>

<fo:table table-layout="fixed" width="95%">
      <fo:table-body>

        <fo:table-row height="40px">
          <fo:table-cell number-columns-spanned="4"><fo:block font-weight="bold" text-align="center">Thesis Defense Examination</fo:block></fo:table-cell>
        </fo:table-row>

        <fo:table-row height="20px">
          <fo:table-cell>
              <fo:block>Topic</fo:block>
          </fo:table-cell>
          <fo:table-cell number-columns-spanned="3">
              <fo:block font-weight="bold">${topic?if_exists}</fo:block>
          </fo:table-cell>
        </fo:table-row>

         <fo:table-row height="20px">
          <fo:table-cell>
              <fo:block>By</fo:block>
          </fo:table-cell>
          <fo:table-cell number-columns-spanned="3">
              <fo:block font-weight="bold">${salutation?if_exists} ${firstName?if_exists} ${middleName?if_exists} ${lastName?if_exists}</fo:block>
          </fo:table-cell>
        </fo:table-row>

        <fo:table-row height="40px">
          <fo:table-cell>
              <fo:block>Program</fo:block>
          </fo:table-cell>
          <fo:table-cell>
              <fo:block font-weight="bold">${program?if_exists}</fo:block>
          </fo:table-cell>
          <fo:table-cell>
              <fo:block>Student ID.No.</fo:block>
          </fo:table-cell>
          <fo:table-cell>
              <fo:block font-weight="bold">${studentId?if_exists}</fo:block>
          </fo:table-cell>
        </fo:table-row>

        <fo:table-row height="20px">
          <fo:table-cell>
              <fo:block>Advisor</fo:block>
          </fo:table-cell>
          <fo:table-cell number-columns-spanned="3">
              <fo:block >1. ${advisor?if_exists}</fo:block>
          </fo:table-cell>
        </fo:table-row>

        <fo:table-row height="20px">
          <fo:table-cell>
              <fo:block>Co-Advisor</fo:block>
          </fo:table-cell>
          <fo:table-cell number-columns-spanned="3">
              <fo:block>2. ${coadvisor?if_exists}</fo:block>
          </fo:table-cell>
        </fo:table-row>

        <fo:table-row height="40px">
          <fo:table-cell>
              <fo:block>Co-Advisor</fo:block>
          </fo:table-cell>
          <fo:table-cell number-columns-spanned="3">
              <fo:block>3. ${coadvisor2?if_exists}</fo:block>
          </fo:table-cell>
        </fo:table-row>

        <fo:table-row height="20px">
          <fo:table-cell>
              <fo:block>Abstract</fo:block>
          </fo:table-cell>
        </fo:table-row>

        <fo:table-row height="120px" number-columns-spanned="2">
          <fo:table-cell number-columns-spanned="4">
              <fo:block text-align="justify">${studentAbstract?if_exists}</fo:block>
          </fo:table-cell>
        </fo:table-row>

        <fo:table-row height="20px">
          <fo:table-cell number-columns-spanned="2">
              <fo:block>Thesis Defense Examination</fo:block>
          </fo:table-cell>
        </fo:table-row>

        <fo:table-row height="20px" number-columns-spanned="2">
          <fo:table-cell>
              <fo:block text-align="left">Date + Time : </fo:block>
          </fo:table-cell>
          <fo:table-cell number-columns-spanned="3">
              <fo:block font-weight="bold">${thesisProgressExamination?if_exists}</fo:block>
          </fo:table-cell>
        </fo:table-row>

        <fo:table-row height="20px" number-columns-spanned="2">
          <fo:table-cell>
              <fo:block text-align="left">Room No : </fo:block>
          </fo:table-cell>
          <fo:table-cell number-columns-spanned="3">
              <fo:block font-weight="bold">${roomNo?if_exists}</fo:block>
          </fo:table-cell>
        </fo:table-row>

        <fo:table-row height="20px" number-columns-spanned="2">
          <fo:table-cell>
              <fo:block text-align="left">Building : </fo:block>
          </fo:table-cell>
          <fo:table-cell number-columns-spanned="3">
              <fo:block font-weight="bold">${building?if_exists}</fo:block>
          </fo:table-cell>
        </fo:table-row>

        <fo:table-row height="60px" >
          <fo:table-cell number-columns-spanned="4">
              <fo:block text-align="center">Advisory Signature</fo:block>
          </fo:table-cell>
        </fo:table-row>

        <fo:table-row height="20px">
          <fo:table-cell number-columns-spanned="4">
              <fo:block text-align="center">......................</fo:block>
          </fo:table-cell>
        </fo:table-row>

        <fo:table-row height="20px">
          <fo:table-cell number-columns-spanned="4">
              <fo:block text-align="center">(${advisor?default('...........................')})</fo:block>
          </fo:table-cell>
        </fo:table-row>

        <fo:table-row height="20px">
          <fo:table-cell number-columns-spanned="4">
              <fo:block text-align="center">${date?if_exists}</fo:block>
          </fo:table-cell>
        </fo:table-row>

      </fo:table-body>
  </fo:table>
</#escape>