<#--
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
-->
  <#if !mechMap.contact?has_content>
    <h1>${uiLabelMap.PartyCreateNewContact}</h1>
    <div id="mech-purpose-types">
    <table class="basic-table" cellspacing="0">
      <form method="post" action="<@ofbizUrl>createInternshipCompany</@ofbizUrl>" name="NewCompany" id="NewCompany">
  </#if>
    <tr>
      <td class="label">Company Name</td>
      <td>
        <input type="text" size="50" maxlength="100" name="COMPANY_NAME"/> *
      </td>
    </tr>
    <tr>
      <td class="label">${uiLabelMap.PartyAddressLine1}</td>
      <td>
        <input type="text" size="100" maxlength="255" name="COM_ADDRESS1"/> *
      </td>
    </tr>
    <tr>
      <td class="label">${uiLabelMap.PartyAddressLine2}</td>
      <td>
        <input type="text" size="100" maxlength="255" name="COM_ADDRESS2"/>
      </td>
    </tr>
    <tr>
      <td class="label">${uiLabelMap.PartyCity}</td>
      <td>
        <input type="text" size="50" maxlength="100" name="COM_CITY"/> *
      </td>
    </tr>
    <tr>
      <td class="label">${uiLabelMap.PartyState}</td>
      <td>
        <select name="COM_STATE" id="NewCompany_stateProvinceGeoId">
        </select>
      </td>
    </tr>
    <tr>
      <td class="label">${uiLabelMap.PartyZipCode}</td>
      <td>
        <input type="text" size="30" maxlength="60" name="COM_POSTAL_CODE"/> *
      </td>
    </tr>
    <tr>   
      <td class="label">${uiLabelMap.CommonCountry}</td>      
      <td>     
        <select name="COM_COUNTRY" id="NewCompany_countryGeoId">
          ${screens.render("component://common/widget/CommonScreens.xml#countries")}        
          <#if (mechMap.postalAddress?exists) && (mechMap.postalAddress.countryGeoId?exists)>
            <#assign defaultCountryGeoId = mechMap.postalAddress.countryGeoId>
          <#else>
           <#assign defaultCountryGeoId = Static["org.ofbiz.base.util.UtilProperties"].getPropertyValue("general.properties", "country.geo.id.default")>
          </#if>
          <option selected="selected" value="${defaultCountryGeoId}">
            <#assign countryGeo = delegator.findOne("Geo",Static["org.ofbiz.base.util.UtilMisc"].toMap("geoId",defaultCountryGeoId), false)>
            ${countryGeo.get("geoName",locale)}
          </option>
        </select>
      </td>
    </tr>
    <tr>
      <td class="label">${uiLabelMap.PartyPhoneNumber}</td>
      <td>
        <input type="text" size="4" maxlength="10" name="PHONE_CODE"/>
        -&nbsp;<input type="text" size="4" maxlength="10" name="PHONE_AREA"/>
        -&nbsp;<input type="text" size="15" maxlength="15" name="PHONE_CONTACT"/>
        &nbsp;${uiLabelMap.PartyContactExt}&nbsp;<input type="text" size="6" maxlength="10" name="PHONE_EXT"/>
      </td>
    </tr>
    <tr>
      <td class="label"></td>
      <td>[${uiLabelMap.CommonCountryCode}] [${uiLabelMap.PartyAreaCode}] [${uiLabelMap.PartyContactNumber}] [${uiLabelMap.PartyContactExt}]</td>
    </tr>
  </form>
  </table>
  </div>
  <div class="button-bar">
    <a href="<@ofbizUrl>InternshipApplication</@ofbizUrl>" class="smallSubmit">${uiLabelMap.CommonGoBack}</a>
    <a href="javascript:document.NewCompany.submit()" class="smallSubmit">${uiLabelMap.CommonSave}</a>
  </div>
