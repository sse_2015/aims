/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import org.ofbiz.aims.party.contact.ContactWorker;

partyId = parameters.partyId;
context.partyId = partyId;

Map mechMap = new HashMap();
ContactWorker.getContactAndRelated(request, partyId, mechMap);
context.mechMap = mechMap;

context.contactId = mechMap.contactId;
context.preContactTypeId = parameters.preContactTypeId;
context.paymentMethodId = parameters.paymentMethodId;

cmNewPurposeTypeId = parameters.contactPurposeTypeId;
if (cmNewPurposeTypeId) {
    contactPurposeType = delegator.findOne("ContactPurposeType", [contactPurposeTypeId : cmNewPurposeTypeId], false);
    if (contactPurposeType) {
        context.contactPurposeType = contactPurposeType;
    } else {
        cmNewPurposeTypeId = null;
    }
    context.cmNewPurposeTypeId = cmNewPurposeTypeId;
}
context.donePage = parameters.DONE_PAGE ?:"viewprofile?party_id=" + partyId + "&partyId=" + partyId;;
