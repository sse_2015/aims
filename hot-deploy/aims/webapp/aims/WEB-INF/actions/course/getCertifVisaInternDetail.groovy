NewPartyRequestAndTG94s = delegator.findList("NewPartyRequestAndTG94",null,null,null,null,false);
String reqType;
String status;
for(int i=0;i<NewPartyRequestAndTG94s.size();i++){
	if(NewPartyRequestAndTG94s[i].TG94Id == TG94Id){
		context.NewPartyRequestAndTG94s = NewPartyRequestAndTG94s[i];
	}
	if(NewPartyRequestAndTG94s[i].status == "SERVICE_PENDING"){
		status = "Pending";
	}else if(NewPartyRequestAndTG94s[i].status == "SERVICE_FINISHED"){
		status = "Approved";
	}else if(NewPartyRequestAndTG94s[i].status == "SERVICE_FAILED"){
		status = "Rejected";		
	}
	if(NewPartyRequestAndTG94s[i].requestType == "CERTIF_VISA_INTERN"){
		reqType = "Request for Student Certification for Visa Application Internship Abroad";
	}
}
context.put("reqType",reqType);
context.put("status",status);

PartyRoles = delegator.findList("PartyRole",null,null,null,null,false);
for(int i=0;i<PartyRoles.size();i++){
	if(PartyRoles[i].partyId == userLogin.partyId && PartyRoles[i].roleTypeId != "_NA_"){
		context.PartyRoles = PartyRoles[i];
	}
}