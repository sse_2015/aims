import java.util.*;
import org.ofbiz.entity.*;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.entity.condition.EntityCondition;

room = parameters.room;
time = parameters.time
semesterId = parameters.get("semesterId");
status = "Available for Student";

/*Sections = delegator.findList("Section",(EntityCondition.makeCondition(EntityCondition.makeCondition("room",(String)room),
								EntityCondition.makeCondition("time",(String)time),
								EntityCondition.makeCondition("status",(String)status),
								EntityCondition.makeCondition("semesterId",(String)semesterId)))
								,null,null,null,false);*/
								
Sections = delegator.findList("Section",null,null,null,null,false);
context.sectionEligible = "true";
for(int i=0;i<Sections.size();i++){
	if(Sections[i].room == room){
		if(Sections[i].time == time){
			if(Sections[i].semesterId == semesterId){
				if(Sections[i].status == status){
					context.sectionEligible = "false";
				}
			}
		}
	}
}

/*if(Sections){
	context.sectionEligible = "false";
}else{
	context.sectionEligible = "true";
}*/
