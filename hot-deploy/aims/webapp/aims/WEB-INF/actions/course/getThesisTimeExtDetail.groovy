NewPartyRequestAndTG99s = delegator.findList("NewPartyRequestAndTG99",null,null,null,null,false);
String reqType;
String status;
for(int i=0;i<NewPartyRequestAndTG99s.size();i++){
	if(NewPartyRequestAndTG99s[i].TG99Id == TG99Id){
		context.NewPartyRequestAndTG99s = NewPartyRequestAndTG99s[i];
	}
	
	if(NewPartyRequestAndTG99s[i].status == "SERVICE_PENDING"){
		status = "Pending";
	}else if(NewPartyRequestAndTG99s[i].status == "SERVICE_FINISHED"){
		status = "Approved";
	}else if(NewPartyRequestAndTG99s[i].status == "SERVICE_FAILED"){
		status = "Rejected";
	}
		
	if(NewPartyRequestAndTG99s[i].requestType == "THESIS_TIME_EXT"){
		reqType = "Request for Thesis Work Time Extension";
	}
}

context.put("reqType",reqType);
context.put("status",status);

PartyRoles = delegator.findList("PartyRole",null,null,null,null,false);
for(int i=0;i<PartyRoles.size();i++){
	if(PartyRoles[i].partyId == userLogin.partyId && PartyRoles[i].roleTypeId != "_NA_"){
		context.PartyRoles = PartyRoles[i];
	}
}