import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;

condition = EntityCondition.makeCondition(EntityOperator.OR,
	EntityCondition.makeCondition("studentId",parameters.studentId),
	EntityCondition.makeCondition("roleTypeIdTo","_NA_"));
Informations = delegator.findList("EnglishProficiencyAndPersonAndPartyTypeAndStatusItem",condition,null,null,null,false);

for(int i=0;i<Informations.size();i++){
	if(Informations[i].requestId == parameters.requestId){
		context.firstName = Informations[i].firstName;
		context.middleName = Informations[i].middleName;
		context.lastName = Informations[i].lastName;
		context.studentId = Informations[i].studentId;
		context.program = Informations[i].partyIdFrom;
		context.address1 = Informations[i].address1;
		context.email = Informations[i].nickname;
		context.cardId = Informations[i].cardId;
		context.homeaddress2 = Informations[i].address2;
		context.contactNumber = Informations[i].contactNumber;		
		if(Informations[i].englishTest == "TOEFL ibt")
			if (context.toeflinternet < Informations[i].score) {
				context.toeflinternet = Informations[i].score;	
			}
			
		if(Informations[i].englishTest == "TOEFL pbt")
			if (context.toeflpaper < Informations[i].score) {
				context.toeflpaper = Informations[i].score;
			}
		if(Informations[i].englishTest == "IELTS")
			if (context.ielts < Informations[i].score) {
				context.ielts = Informations[i].score;	
			}
			
		if(Informations[i].englishTest == "TOEFL cbt")
			if (context.toeflcomputer < Informations[i].score) {
				context.toeflcomputer = Informations[i].score;
			}
			
		if(Informations[i].englishTest == "CU-TEP")
			if (context.cutep < Informations[i].score) {
				context.cutep = Informations[i].score;
			}
	}
}