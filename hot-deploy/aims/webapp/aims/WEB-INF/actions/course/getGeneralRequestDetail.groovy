NewPartyRequestAndTG91s = delegator.findList("NewPartyRequestAndTG91",null,null,null,null,false);
String reqType;
String status;
for(int i=0;i<NewPartyRequestAndTG91s.size();i++){
	if(NewPartyRequestAndTG91s[i].TG91Id == TG91Id){
		context.NewPartyRequestAndTG91s = NewPartyRequestAndTG91s[i];
		if(NewPartyRequestAndTG91s[i].status == "SERVICE_PENDING"){
			status = "Pending";
		}else if(NewPartyRequestAndTG91s[i].status == "SERVICE_FINISHED"){
			status = "Approved";
		}else if(NewPartyRequestAndTG91s[i].status == "SERVICE_FAILED"){
			status = "Rejected";
		}	
		
		if(NewPartyRequestAndTG91s[i].requestType == "GEN_REQUEST"){
			reqType = "General Request";
		}
	}
}

context.put("reqType",reqType);
context.put("status",status);

PartyRoles = delegator.findList("PartyRole",null,null,null,null,false);
for(int i=0;i<PartyRoles.size();i++){
	if(PartyRoles[i].partyId == userLogin.partyId && PartyRoles[i].roleTypeId != "_NA_"){
		context.PartyRoles = PartyRoles[i];
	}
}