NewPartyRequestAndTG93s = delegator.findList("NewPartyRequestAndTG93",null,null,null,null,false);
NewPartyRequests = delegator.findList("NewPartyRequest",null,null,null,null,false);
String reqType;
String status;
for(int i=0;i<NewPartyRequestAndTG93s.size();i++){
	if(NewPartyRequestAndTG93s[i].TG93Id == TG93Id){
		context.NewPartyRequestAndTG93s = NewPartyRequestAndTG93s[i];
	}
	if(NewPartyRequestAndTG93s[i].status == "SERVICE_PENDING"){
		status = "Pending";
	}
	else if(NewPartyRequestAndTG93s[i].status == "SERVICE_FINISHED"){
		status = "Approved";
	}
	else if(NewPartyRequestAndTG93s[i].status == "SERVICE_FAILED"){
		status = "Rejected";
	}
	
	if(NewPartyRequestAndTG93s[i].requestType == "REQ_TRANSCRIPT"){
		reqType = "Request for Unofficial Transcript";
	}
}

for(int i=0;i<NewPartyRequests.size();i++){
	if(NewPartyRequests[i].TG93Id == TG93Id){
		context.NewPartyRequests = NewPartyRequests[i];
	}
}

context.put("reqType",reqType);
context.put("status",status);

PartyRoles = delegator.findList("PartyRole",null,null,null,null,false);
for(int i=0;i<PartyRoles.size();i++){
	if(PartyRoles[i].partyId == userLogin.partyId && PartyRoles[i].roleTypeId != "_NA_"){
		context.PartyRoles = PartyRoles[i];
	}
}