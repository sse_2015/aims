NewPartyRequestAndTG96s = delegator.findList("NewPartyRequestAndTG96",null,null,null,null,false);
String reqType;
String status;
for(int i=0;i<NewPartyRequestAndTG96s.size();i++){
	if(NewPartyRequestAndTG96s[i].TG96Id == TG96Id){
		context.NewPartyRequestAndTG96s = NewPartyRequestAndTG96s[i];
	}
	
	if(NewPartyRequestAndTG96s[i].status == "SERVICE_PENDING"){
			status = "Pending";
		}else if(NewPartyRequestAndTG96s[i].status == "SERVICE_FINISHED"){
			status = "Approved";
		}else if(NewPartyRequestAndTG96s[i].status == "SERVICE_FAILED"){
			status = "Rejected";
		}
	
	if(NewPartyRequestAndTG96s[i].requestType == "ENG_TEST_APP"){
		reqType = "Request Approval for English Requirement";
	}
}
context.put("reqType",reqType);
context.put("status",status);

PartyRoles = delegator.findList("PartyRole",null,null,null,null,false);
for(int i=0;i<PartyRoles.size();i++){
	if(PartyRoles[i].partyId == userLogin.partyId && PartyRoles[i].roleTypeId != "_NA_"){
		context.PartyRoles = PartyRoles[i];
	}
}