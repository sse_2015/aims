Informations = delegator.findList("NewPartyRequestAndTG98AndPartyAndPerson",null,null,null,null,false);

for(int i=0;i<Informations.size();i++){
	if(Informations[i].requestId == parameters.requestId){
		context.firstName = Informations[i].firstName;
		context.middleName = Informations[i].middleName;
		context.lastName = Informations[i].lastName;
		context.studentId = Informations[i].studentId;
		context.program = Informations[i].partyIdFrom;
		context.homeaddress1 = Informations[i].address1;
		context.homeaddress2 = Informations[i].address2;
		context.countrycode = Informations[i].countryCode;
		context.contactnumber = Informations[i].contactNumber;
		context.thesistitle = Informations[i].thesisTitle;
		context.defensepassedtest = Informations[i].defensePassedTest;
		context.period = Informations[i].period;
		context.effectivedate = Informations[i].effectiveDate;
		context.reason = Informations[i].reason;
		break;
	}
}