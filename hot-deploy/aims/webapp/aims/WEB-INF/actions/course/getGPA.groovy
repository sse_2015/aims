import java.util.*;

takenList = delegator.findList("GradeReportAndGradeAndSectionAndCourse",null,null,null,null,false);
int creditTotal=0;
double gpa=0;
for(int i=0;i<takenList.size();i++){
	if(takenList[i].studentId == userLogin.partyId && takenList[i].gradeLetter != "N/A"){
		if(takenList[i].point != -1){
			creditTotal += (int)takenList[i].kmutnbCredits;
			gpa += takenList[i].point * takenList[i].kmutnbCredits;
		}
	}
}

gpa = gpa / creditTotal;
context.put("mygpa",gpa);