Sections = delegator.findList("SectionAndSemesterYearAndTimeShiftAndCourseAndPerson",null,null,null,null,false);
OriginalSections = delegator.findList("Section",null,null,null,null,false);

for(int i=0;i<Sections.size();i++){
	if(Sections[i].sectionId == id){
		context.Sections = Sections[i];
	}
}

for(int i=0;i<OriginalSections.size();i++){
	if(OriginalSections[i].sectionId == id){
		context.OriginalSections = OriginalSections[i];
	}
}

String userLogin = userLogin.partyId;
String role = userLogin.substring(0,5);
context.put("role",role);