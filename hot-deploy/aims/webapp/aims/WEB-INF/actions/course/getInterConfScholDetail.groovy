NewPartyRequestAndTG92s = delegator.findList("NewPartyRequestAndTG92",null,null,null,null,false);
String reqType;
String status;
for(int i=0;i<NewPartyRequestAndTG92s.size();i++){
	if(NewPartyRequestAndTG92s[i].TG92Id == TG92Id){
		context.NewPartyRequestAndTG92s = NewPartyRequestAndTG92s[i];
	}
	
	if(NewPartyRequestAndTG92s[i].status == "SERVICE_PENDING"){
		status = "Pending";
	}else if(NewPartyRequestAndTG92s[i].status == "SERVICE_FINISHED"){
		status = "Approved";
	}else if(NewPartyRequestAndTG92s[i].status == "SERVICE_FAILED"){
		status = "Rejected";
	}
	
	if(NewPartyRequestAndTG92s[i].requestType == "INTER_CONF_SCHOL"){
		reqType = "Request for International Conference Scholarship";
	}
}
context.put("reqType",reqType);
context.put("status",status);

PartyRoles = delegator.findList("PartyRole",null,null,null,null,false);
for(int i=0;i<PartyRoles.size();i++){
	if(PartyRoles[i].partyId == userLogin.partyId && PartyRoles[i].roleTypeId != "_NA_"){
		context.PartyRoles = PartyRoles[i];
	}
}