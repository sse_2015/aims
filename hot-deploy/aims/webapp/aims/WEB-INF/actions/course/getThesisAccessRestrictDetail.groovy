NewPartyRequestAndTG98s = delegator.findList("NewPartyRequestAndTG98",null,null,null,null,false);
String reqType;
String status;
for(int i=0;i<NewPartyRequestAndTG98s.size();i++){
	if(NewPartyRequestAndTG98s[i].TG98Id == TG98Id){
		context.NewPartyRequestAndTG98s = NewPartyRequestAndTG98s[i];
	}
	
	if(NewPartyRequestAndTG98s[i].status == "SERVICE_PENDING"){
		status = "Pending";
	}else if(NewPartyRequestAndTG98s[i].status == "SERVICE_FINISHED"){
		status = "Approved";
	}else if(NewPartyRequestAndTG98s[i].status == "SERVICE_FAILED"){
		status = "Rejected";
	}
	
	if(NewPartyRequestAndTG98s[i].requestType == "THESIS_ACC_REST"){
		reqType = "Request for Thesis Access Restriction";
	}	
}

context.put("reqType",reqType);
context.put("status",status);

PartyRoles = delegator.findList("PartyRole",null,null,null,null,false);
for(int i=0;i<PartyRoles.size();i++){
	if(PartyRoles[i].partyId == userLogin.partyId && PartyRoles[i].roleTypeId != "_NA_"){
		context.PartyRoles = PartyRoles[i];
	}
}