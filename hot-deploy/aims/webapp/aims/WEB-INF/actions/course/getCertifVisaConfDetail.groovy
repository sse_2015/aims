NewPartyRequestAndTG95s = delegator.findList("NewPartyRequestAndTG95",null,null,null,null,false);
String reqType;
String status;
for(int i=0;i<NewPartyRequestAndTG95s.size();i++){
	if(NewPartyRequestAndTG95s[i].TG95Id == TG95Id){
		context.NewPartyRequestAndTG95s = NewPartyRequestAndTG95s[i];
	}
	if(NewPartyRequestAndTG95s[i].status == "SERVICE_PENDING"){
		status = "Pending";
	}else if(NewPartyRequestAndTG95s[i].status == "SERVICE_FINISHED"){
		status = "Approved";
	}else if(NewPartyRequestAndTG95s[i].status == "SERVICE_FAILED"){
		status = "Rejected";
	}
	
	if(NewPartyRequestAndTG95s[i].requestType == "CERTIF_VISA_CONF"){
		reqType = "Request for Student Certification for Visa Application Conference Abroad";
	}
}
context.put("reqType",reqType);
context.put("status",status);
PartyRoles = delegator.findList("PartyRole",null,null,null,null,false);
for(int i=0;i<PartyRoles.size();i++){
	if(PartyRoles[i].partyId == userLogin.partyId && PartyRoles[i].roleTypeId != "_NA_"){
		context.PartyRoles = PartyRoles[i];
	}
}