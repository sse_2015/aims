import org.ofbiz.base.util.*;
import org.ofbiz.entity.condition.*;


 PartyGroups = delegator.findList("PartyGroup",
	EntityCondition.makeCondition("partyId",(""+parameters.partyId)),null,null,null,false);

	context.groupName = PartyGroups[0].groupName;
	context.abbreviation = PartyGroups[0].abbreviation;	

try
{
	//PartyRelationships = delegator.findList("PartyRelationship",
	//EntityCondition.makeCondition("partyIdTo",(""+parameters.partyId)),null,null,null,false);
	PartyRelationships = delegator.findList("PartyRelationship",null,null,null,null,false);
	for(int i=0;i<PartyRelationships.size();i++)
	{
		if(PartyRelationships[i].partyIdTo.equalsIgnoreCase(parameters.partyId))
		{
			x = PartyRelationships[i];
			break;
		}
	}

	context.put("parent",x.partyIdFrom);
	context.parentAbbreviation = x.abbreviation;
	context.roleTypeIdFrom = x.roleTypeIdFrom;
	context.roleTypeIdTo = x.roleTypeIdTo;
	context.fromDatee = x.fromDate;		
}
catch(Exception e)
{
	context.roleTypeIdTo = delegator.findList("PartyRole",
		EntityCondition.makeCondition("partyId",(""+parameters.partyId)),null,null,null,false)[0].roleTypeId;
	 
}
