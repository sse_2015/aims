RequestVisaPersons = delegator.findList("RequestVisaPerson",null,null,null,null,false);
Persons = delegator.findList("Person",null,null,null,null,false);

int index = -1;

for(int i=0;i<RequestVisaPersons.size();i++)
{	
	if(RequestVisaPersons[i].reqPartyIdVisa == parameters.reqPartyIdVisa )
	{
		context.RequestVisaPersons = RequestVisaPersons[i];		
		index = i;
		break;
	}
}

for(int i=0;i<Persons.size();i++)
{	
	if(Persons[i].partyId == RequestVisaPersons[index].personId)
	{
		context.Persons = Persons[i];		
		break;
	}
}