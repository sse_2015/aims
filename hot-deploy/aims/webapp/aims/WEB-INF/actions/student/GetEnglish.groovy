import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityOperator;

EnglishProficiencies = delegator.findList("EnglishProficiency",
	EntityCondition.makeCondition("studentId",userLogin.partyId),null,null,null,false);

Person = delegator.findList("Person",
	EntityCondition.makeCondition("partyId",userLogin.partyId),null,null,null,false);

context.Persons = Person[0];

PartyContact = delegator.findList("PartyContact",
	EntityCondition.makeCondition("partyId",userLogin.partyId),null,null,null,false);

try
{
	PartyRoles = 
	delegator.findList("PartyRole",EntityCondition.makeCondition("partyId",userLogin.partyId),null,null,null,false);	

	
	for(int i=0;i<PartyRoles.size();i++)
	{
		try
		{
			condition = EntityCondition.makeCondition(EntityOperator.AND,
			EntityCondition.makeCondition("partyIdTo",userLogin.partyId),
			EntityCondition.makeCondition("roleTypeIdTo",PartyRoles[i].roleTypeId));

			PartyRelationship = delegator.findList("PartyRelationship",condition,null,null,null,false);

			context.put("program",PartyRelationship[0].partyIdFrom);

			context.put("groupName",delegator.findList("PartyGroup",
			EntityCondition.makeCondition("partyId",PartyRelationship[0].partyIdFrom),null,null,null,false)[0].groupName);
		
		}catch(Exception es){}
	}

}
catch(Exception e)
{
	context.put("program",e);
}

PostalAddress = null;
TelecomNumber = null;
Contact = null;


if(PartyContact.size()>0)
{
	PostalAddress = delegator.findList("PostalAddress",
		EntityCondition.makeCondition("contactId",PartyContact[0].contactId),null,null,null,false);

	TelecomNumber = delegator.findList("TelecomNumber",
		EntityCondition.makeCondition("contactId",PartyContact[0].contactId),null,null,null,false);

	Contact = delegator.findList("Contact",
		EntityCondition.makeCondition("contactId",PartyContact[0].contactId),null,null,null,false);
}

context.EnglishProficiency = EnglishProficiencies;

int toeflpaper = -1;
int toeflinternet = -1;
int toeflcomputer = -1;
int cutep = -1;
int ielts = -1;

for(int i=0;i<EnglishProficiencies.size();i++)
{
	if(EnglishProficiencies[i].englishTest.contains("TOEFL ibt"))
	{
		if(toeflinternet< 0 )
			toeflinternet = EnglishProficiencies[i].score;
	}
	else if(EnglishProficiencies[i].englishTest.contains("TOEFL pbt"))
	{
		if(toeflpaper< 0 )
			toeflpaper = EnglishProficiencies[i].score;
	}
	else if(EnglishProficiencies[i].englishTest.contains("TOEFL cbt"))
	{
		if(toeflcomputer< 0 )
			toeflcomputer = EnglishProficiencies[i].score;
	}
	else if(EnglishProficiencies[i].englishTest.contains("IELTS"))
	{
		if(ielts< 0 )
			ielts = EnglishProficiencies[i].score;
	}
	else if(EnglishProficiencies[i].englishTest.contains("CUTEP"))
	{
		if(cutep< 0 )
			cutep = EnglishProficiencies[i].score;
	}
}
context.put("toeflpaper",toeflpaper>0?toeflpaper:null);
context.put("toeflinternet",toeflinternet>0?toeflinternet:null);
context.put("toeflcomputer",toeflcomputer>0?toeflcomputer:null);
context.put("cutep",cutep>0?cutep:null);
context.put("ielts",ielts>0?ielts:null);

if(PostalAddress!=null)
{
	try{context.put("address1",PostalAddress[0].address1);}catch(Exception e){}	
	try{context.put("homeaddress",PostalAddress[0].address2);}catch(Exception e){}
}
if(Contact!=null)
{

	try{context.put("email",Contact[0].infoString);}catch(Exception e){}
}
if(TelecomNumber!=null)
{
	try{context.put("contactNumber",TelecomNumber[0].contactNumber);}catch(Exception e){}
}