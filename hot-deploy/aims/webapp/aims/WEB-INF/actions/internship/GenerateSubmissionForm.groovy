/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import org.ofbiz.aims.party.contact.ContactWorker;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

showOld = "true".equals(parameters.SHOW_OLD);

submissionId = parameters.submissionId;
submission = delegator.findOne("InternshipSubmission", [submissionId : submissionId], false);

deliveryLetters = delegator.findByAnd("InternshipDeliveryLetter", [studentId : submission.studentId], null, false);
deliveryLetter = deliveryLetters[0];

student = delegator.findOne("PartyAndPerson", [partyId : submission.studentId], false);
studentContacts = ContactWorker.getPartyContactValueMaps(delegator, submission.studentId, showOld);
for (contactMap in studentContacts) {
	if ("EMAIL_ADDRESS".equals(contactMap.contact.contactTypeId)) {
		context.studentEmail = contactMap.contact.infoString;
	}
	if ("TELECOM_NUMBER".equals(contactMap.contact.contactTypeId)) {
		context.studentTelecomNumber = contactMap.telecomNumber.contactNumber;
	}
	if ("POSTAL_ADDRESS".equals(contactMap.contact.contactTypeId)) {
		context.companyPostalAddress = contactMap.postalAddress.address1;
		if (contactMap.postalAddress.address2 != null)
			context.companyPostalAddress += ", " + contactMap.postalAddress.address2;
		context.companyPostalAddress +=  ", " + contactMap.postalAddress.city;
	}
}

lecturer = delegator.findOne("PartyAndPerson", [partyId : deliveryLetter.advisor], false);
advisorContacts = ContactWorker.getPartyContactValueMaps(delegator, deliveryLetter.advisor, showOld);
for (contactMap in advisorContacts) {
	if ("EMAIL_ADDRESS".equals(contactMap.contact.contactTypeId)) {
		context.advisorEmail = contactMap.contact.infoString;
	}
	if ("TELECOM_NUMBER".equals(contactMap.contact.contactTypeId)) {
		context.advisorTelecomNumber = contactMap.telecomNumber.contactNumber;
	}
	if ("POSTAL_ADDRESS".equals(contactMap.contact.contactTypeId)) {
		context.advisorPostalAddress = contactMap.postalAddress.city;		
	}
}

company = delegator.findOne("PartyAndGroup", [partyId : deliveryLetter.company], false);
companyContacts = ContactWorker.getPartyContactValueMaps(delegator, deliveryLetter.company, showOld);
for (contactMap in companyContacts) {
	if ("EMAIL_ADDRESS".equals(contactMap.contact.contactTypeId)) {
		context.companyEmail = contactMap.contact.infoString;
	}
	if ("TELECOM_NUMBER".equals(contactMap.contact.contactTypeId)) {
		context.companyTelecomNumber = contactMap.telecomNumber.contactNumber;
	}
	if ("POSTAL_ADDRESS".equals(contactMap.contact.contactTypeId)) {
		context.companyPostalAddress = contactMap.postalAddress.address1;
		if (contactMap.postalAddress.address2 != null)
			context.companyPostalAddress += ", " + contactMap.postalAddress.address2;
		context.companyPostalAddress +=  ", " + contactMap.postalAddress.city;		
	}
}

DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

context.dd = deliveryLetter.lastUpdatedStamp.getDate();
context.mm = deliveryLetter.lastUpdatedStamp.getMonth()+1;
context.yy = deliveryLetter.lastUpdatedStamp.getYear()+1900;

context.name = student.firstName;
if (student.middleName != null)
	context.name += " " + student.middleName;
if (student.lastName != null)
	context.name += " " + student.lastName;

context.studentId = student.cardId;
programs = delegator.findByAnd("PartyRelationship",
            [partyIdTo: deliveryLetter.studentId,
             roleTypeIdFrom: "INTERNAL_ORGANIZATIO",
             roleTypeIdTo: "_NA_"], null, false);
if (programs) {
	program = delegator.findOne("PartyAndGroup", [partyId : programs[0].partyIdFrom], false);
    context.program = program.groupName;
}
context.advisor = ""
if (lecturer.salutation != null)
	context.advisor += lecturer.salutation + " ";
if (lecturer.firstName != null)
	context.advisor += lecturer.firstName + " ";
if (lecturer.middleName != null)
	context.advisor += lecturer.middleName + " ";
if (lecturer.lastName != null)
	context.advisor += lecturer.lastName;

context.companyAdvisor = deliveryLetter.companyAdvisorName;
context.companyAdvisorTelecomNumber = deliveryLetter.companyAdvisorTelephone;
context.companyAdvisorEmail = deliveryLetter.companyAdvisorEmail;
context.companyAdvisorPosition = deliveryLetter.companyAdvisorPosition;

context.company = company.groupName;
context.projectTitle = deliveryLetter.projectTitle;
context.desiredPeriod = dateFormat.format(deliveryLetter.fromDate) + " - " + dateFormat.format(deliveryLetter.toDate);
context.projectDescription = deliveryLetter.projectDescription;
context.requestedDocuments = deliveryLetter.requestedDocuments;

