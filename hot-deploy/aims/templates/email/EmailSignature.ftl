<#--
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
-->

<html xmlns="http://www.w3.org/1999/xhtml">
    <body>
        <br /><br />
        Best regards,

        <p>
        Academic Affairs<br/>

        The Sirindhorn International Thai-German Graduate School of Engineering (TGGS) <br/>
        King Mongkut's University of Technology North Bangkok (KMUTNB) <br/>
        1518 Pibulsongkram (Pracharaj 1) Rd., <br/>
        Wongsawang Sub-District, Bangsue District <br/>
        Bangkok 10800 Thailand <br/>
        Phone: +66(0)2-555-2000 Ext. 2931 <br/>
        Fax  : +66(0)2-913 5805<br/>
        Email: info@tggs-bangkok.org
        </p>
    </body>
</html>
