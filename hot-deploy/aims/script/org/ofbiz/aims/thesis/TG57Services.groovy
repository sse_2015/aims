import java.util.*;
import org.ofbiz.entity.*;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.base.util.*;
import org.ofbiz.base.util.collections.*;
import org.ofbiz.accounting.invoice.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.math.BigDecimal;
import java.math.MathContext;
import org.ofbiz.base.util.UtilNumber;
import javolution.util.FastList;
import javolution.util.FastMap;
import org.ofbiz.entity.condition.EntityCondition;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

//get parameters
userLoginId = parameters.get("userLoginId");
partyId = parameters.get("partyId");
thesisId = parameters.get("title");
comittee = parameters.get("comittee");

//userLogin
context.userLoginId = userLoginId;

if(comittee != null && !comittee.equals("")){
	partyAndPerson = delegator.findOne("PartyAndPerson", [partyId : comittee], false);
	if(partyAndPerson){
		if(partyAndPerson.middleName != null && !(partyAndPerson.middleName).equals("")){
			context.comittee = partyAndPerson.salutation + ". " + partyAndPerson.firstName + " " + partyAndPerson.middleName + " " + partyAndPerson.lastName;
		}else{
			context.comittee = partyAndPerson.salutation + ". " + partyAndPerson.firstName + " " + partyAndPerson.lastName;
		}
	}
}

//find firstName, middleName, lastName, email, studentId
partyAndPerson = delegator.findOne("PartyAndPerson", [partyId : partyId], false);
if(partyAndPerson){
	context.salutation = partyAndPerson.salutation;
	context.studentId = partyAndPerson.cardId;
	context.firstName = partyAndPerson.firstName;
	context.middleName = partyAndPerson.middleName;
	context.lastName = partyAndPerson.lastName;

	//find program
	partyRelationship = delegator.findList("PartyRelationship", EntityCondition.makeCondition(EntityCondition.makeCondition("partyIdTo",partyId),EntityCondition.makeCondition("roleTypeIdTo","_NA_")) , null, null, null, false);
	if(partyRelationship){
		context.program = partyRelationship[0].partyIdFrom;
	}
}

//find thesis
thesis = delegator.findOne("Thesis", [title : thesisId], false);

if(thesis){
	context.title = thesis.judulSkripsi;
	context.appDate = thesis.declaredDate;

	if((thesis.educationLevel).equals("Master Degree")){
		context.kind = "Thesis";
	}else{
		context.kind = "Dissertation";
	}

	partyAndPerson = delegator.findOne("PartyAndPerson", [partyId : thesis.advisor], false);

	if(partyAndPerson){
		if(partyAndPerson.middleName !=null){
			context.advisorName = partyAndPerson.salutation + " " + partyAndPerson.firstName + " " + partyAndPerson.middleName + " " + partyAndPerson.lastName;
		}else{
			context.advisorName = partyAndPerson.salutation + " " + partyAndPerson.firstName + " " + partyAndPerson.lastName;
		}
	}
}
//date
DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
Date date = new Date();
context.date = dateFormat.format(date);

DateFormat dateFormat2 = new SimpleDateFormat("yyyy");
Date date2 = new Date();
context.year = dateFormat2.format(date2);

DateFormat dateFormat3 = new SimpleDateFormat("MM");
Date date3 = new Date();
tempMonth = dateFormat3.format(date3);

if(Integer.parseInt(tempMonth) >=8 && Integer.parseInt(tempMonth) <=12){
	tempSemester = "1";
}else{
	tempSemester = "2";
	context.year = (Integer.parseInt((String)context.year) - 1) + "";
}

context.semester = tempSemester + "-" +context.year ;