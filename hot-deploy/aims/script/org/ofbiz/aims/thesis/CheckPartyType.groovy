//check whether thesis created or not
import java.util.*;
import java.lang.String;
import org.ofbiz.entity.*;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.base.util.*;
import org.ofbiz.base.util.collections.*;
import org.ofbiz.accounting.invoice.*;
import java.math.BigDecimal;
import java.math.MathContext;
import org.ofbiz.base.util.UtilNumber;
import javolution.util.FastList;
import javolution.util.FastMap;
import org.ofbiz.entity.condition.EntityCondition;


partyId = parameters.get("partyId");
if(partyId == null){
	partyId = userLogin.partyId;
}

partyRole = delegator.findList("PartyRole", EntityCondition.makeCondition("partyId",(String)partyId) , null, null, null, false);

try{
	for(int i = 0 ; i < partyRole.size() ; i++){
		if(!(partyRole[i].roleTypeId).equals("_NA_")){
			String tempType = partyRole[0].roleTypeId;
			context.type = tempType.toLowerCase();
			break;
		}
	}
}catch(Exception e){
	context.type = "Student";
}