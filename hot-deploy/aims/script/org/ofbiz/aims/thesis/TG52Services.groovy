import java.util.*;
import org.ofbiz.entity.*;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.base.util.*;
import org.ofbiz.base.util.collections.*;
import org.ofbiz.accounting.invoice.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.math.BigDecimal;
import java.math.MathContext;
import org.ofbiz.base.util.UtilNumber;
import javolution.util.FastList;
import javolution.util.FastMap;
import org.ofbiz.entity.condition.EntityCondition;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

//get parameters
partyId = parameters.get("partyId");

//find thesis information
thesis = delegator.findList("Thesis", EntityCondition.makeCondition("studentId",(String)partyId), null, null, null, false);

if(thesis){
	partyAndPerson = delegator.findOne("PartyAndPerson", [partyId : thesis[thesis.size()-1].advisor], false);

	if(partyAndPerson){
		context.salutation = partyAndPerson.salutation;
		context.firstName = partyAndPerson.firstName;
		context.middleName = partyAndPerson.middleName;
		context.lastName = partyAndPerson.lastName;
		context.email = partyAndPerson.nickname;
	}
}

//date
DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
Date date = new Date();
context.date = dateFormat.format(date);