import java.util.*;
import org.ofbiz.entity.*;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.base.util.*;
import org.ofbiz.base.util.collections.*;
import org.ofbiz.accounting.invoice.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.math.BigDecimal;
import java.math.MathContext;
import org.ofbiz.base.util.UtilNumber;
import javolution.util.FastList;
import javolution.util.FastMap;
import org.ofbiz.entity.condition.EntityCondition;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

//get parameters
userLoginId = parameters.get("userLoginId");
partyId = parameters.get("partyId");
semesterCredits = parameters.get("semesterCredits");
//gpa = parameters.get("gpa");
newAdvisor = parameters.get("advisor");
newCoAdvisor = parameters.get("coadvisor");
newCoAdvisor1 = parameters.get("coadvisor1");

//new Advisor

if(newAdvisor != null || !(newAdvisor.equals(""))){
	partyAndPerson = delegator.findOne("PartyAndPerson", [partyId : newAdvisor], false);

	if(partyAndPerson){
		if(partyAndPerson.middleName != null){
			context.newAdvisorName = partyAndPerson.salutation + " " + partyAndPerson.firstName + " " + partyAndPerson.middleName + " " + partyAndPerson.lastName;
		}else{
			context.newAdvisorName = partyAndPerson.salutation + " " + partyAndPerson.firstName + " " + partyAndPerson.lastName;
		}
	}

	if(newCoAdvisor != null || !(newCoAdvisor.equals(""))){
		partyAndPerson = delegator.findOne("PartyAndPerson", [partyId : newCoAdvisor], false);

		if(partyAndPerson){
			if(partyAndPerson.middleName != null){
				context.newCoAdvisorName = partyAndPerson.salutation + " " + partyAndPerson.firstName + " " + partyAndPerson.middleName + " " + partyAndPerson.lastName;
			}else{
				context.newCoAdvisorName = partyAndPerson.salutation + " " + partyAndPerson.firstName + " " + partyAndPerson.lastName;
			}
		}
	}

	if(newCoAdvisor1 != null || !(newCoAdvisor1.equals(""))){
		partyAndPerson = delegator.findOne("PartyAndPerson", [partyId : newCoAdvisor1], false);

		if(partyAndPerson){
			if(partyAndPerson.middleName != null){
				context.newCoAdvisorName1 = partyAndPerson.salutation + " " + partyAndPerson.firstName + " " + partyAndPerson.middleName + " " + partyAndPerson.lastName;
			}else{
				context.newCoAdvisorName1 = partyAndPerson.salutation + " " + partyAndPerson.firstName + " " + partyAndPerson.lastName;
			}
		}
	}
}

//userLogin
context.userLoginId = userLoginId;
context.semesterCredits = semesterCredits;

takenList = delegator.findList("GradeReportAndGradeAndSectionAndCourse",null,null,null,null,false);
int creditTotal=0;
double gpa=0;
for(int i=0;i<takenList.size();i++){
	if(takenList[i].studentId == partyId && takenList[i].gradeLetter != "N/A"){
		if(takenList[i].point != -1){
			creditTotal += (int)takenList[i].kmutnbCredits;
			gpa += takenList[i].point * takenList[i].kmutnbCredits;
		}
	}
}

gpa = gpa / creditTotal;

context.gpa = gpa;

//find firstName, middleName, lastName, email, studentId

partyAndPerson = delegator.findOne("PartyAndPerson", [partyId : partyId], false);
if(partyAndPerson){
	context.salutation = partyAndPerson.salutation;
	context.studentId = partyAndPerson.cardId;
	context.firstName = partyAndPerson.firstName;
	context.middleName = partyAndPerson.middleName;
	context.lastName = partyAndPerson.lastName;
	context.email = partyAndPerson.nickname;

	//find department, program
	partyRelationship = delegator.findList("PartyRelationship", EntityCondition.makeCondition(EntityCondition.makeCondition("partyIdTo",(String)context.partyId),EntityCondition.makeCondition("roleTypeIdTo","_NA_")) , null, null, null, false);
	if(partyRelationship){
		context.program = partyRelationship[0].partyIdFrom;

		partyRelationship2 = delegator.findList("PartyRelationship", EntityCondition.makeCondition("partyIdTo",(String)context.program) , null, null, null, false);	

		if(partyRelationship2){
			context.department = partyRelationship2[0].partyIdFrom;
		}
	}
}

//find address, telephone number
partyAndContact = delegator.findList("PartyAndContact", EntityCondition.makeCondition(EntityCondition.makeCondition("partyId",(String)partyId),EntityCondition.makeCondition("partyTypeId","PERSON")) , null, null, null, false);

if(partyAndContact){
	context.address = partyAndContact[0].paAddress1 + " , " + partyAndContact[0].paCity + " " + partyAndContact[0].paPostalCode + " , " + partyAndContact[0].paCountryGeoId;
	context.telephone = "+" + partyAndContact[0].tnCountryCode + "-" + partyAndContact[0].tnContactNumber;
}

thesis = delegator.findList("Thesis", EntityCondition.makeCondition("studentId",(String)partyId), null, null, null, false);

if(thesis){
	context.title = thesis[thesis.size()-1].judulSkripsi;
	context.educationLevel = thesis[thesis.size()-1].educationLevel;
	
	partyAndPerson = delegator.findOne("PartyAndPerson", [partyId : thesis[thesis.size()-1].advisor], false);

	if(partyAndPerson){
		if(partyAndPerson.middleName !=null){
			context.advisorName = partyAndPerson.salutation + " " + partyAndPerson.firstName + " " + partyAndPerson.middleName + " " + partyAndPerson.lastName;
		}else{
			context.advisorName = partyAndPerson.salutation + " " + partyAndPerson.firstName + " " + partyAndPerson.lastName;
		}
	}

	if(thesis[thesis.size()-1].coadvisor != null || !(thesis[thesis.size()-1].coadvisor).equals("")){
		partyAndPerson = delegator.findOne("PartyAndPerson", [partyId : thesis[thesis.size()-1].coadvisor], false);

		if(partyAndPerson){
			if(partyAndPerson.middleName != null){
				context.coadvisorName = partyAndPerson.salutation + " " + partyAndPerson.firstName + " " + partyAndPerson.middleName + " " + partyAndPerson.lastName;
			}else{
				context.coadvisorName = partyAndPerson.salutation + " " + partyAndPerson.firstName + " " + partyAndPerson.lastName;
			}
		}
	}

	if(thesis[thesis.size()-1].coadvisor1 != null || !(thesis[thesis.size()-1].coadvisor1).equals("")){
		partyAndPerson = delegator.findOne("PartyAndPerson", [partyId : thesis[thesis.size()-1].coadvisor1], false);

		if(partyAndPerson){
			if(partyAndPerson.middleName != null){
				context.coadvisorName1 = partyAndPerson.salutation + " " + partyAndPerson.firstName + " " + partyAndPerson.middleName + " " + partyAndPerson.lastName;
			}else{
				context.coadvisorName1 = partyAndPerson.salutation + " " + partyAndPerson.firstName + " " + partyAndPerson.lastName;
			}
		}
	}
}

//date
DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
Date date = new Date();
context.date = dateFormat.format(date);