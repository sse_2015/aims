//check whether thesis created or not
import java.util.*;
import java.lang.String;
import org.ofbiz.entity.*;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.base.util.*;
import org.ofbiz.base.util.collections.*;
import org.ofbiz.accounting.invoice.*;
import java.math.BigDecimal;
import java.math.MathContext;
import org.ofbiz.base.util.UtilNumber;
import javolution.util.FastList;
import javolution.util.FastMap;
import org.ofbiz.entity.condition.EntityCondition;


thesisId = parameters.get("title");
if(thesisId != null){
	thesis = delegator.findOne("Thesis", [title : thesisId], false);

	if(thesis){
		context.thesisTitle = thesis.judulSkripsi;
	}
}