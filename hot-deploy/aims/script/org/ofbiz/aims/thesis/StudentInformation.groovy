import java.util.*;
import org.ofbiz.entity.*;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.base.util.*;
import org.ofbiz.base.util.collections.*;
import org.ofbiz.accounting.invoice.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.math.BigDecimal;
import java.math.MathContext;
import org.ofbiz.base.util.UtilNumber;
import javolution.util.FastList;
import javolution.util.FastMap;
import org.ofbiz.entity.condition.EntityCondition;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

//get parameters
studentId = (String) parameters.get("studentId");
thesisId = (String) parameters.get("thesisId");

//set field
context.studentId = (String)studentId;

//find firstName, middleName, lastName, email, studentId

partyAndPerson = delegator.findOne("PartyAndPerson", [partyId : studentId], false);
if(partyAndPerson){
	String tempStudentName = "";

	if((partyAndPerson.salutation != null) && !((partyAndPerson.salutation).equals(""))){
		tempStudentName = partyAndPerson.salutation + " ";
	}else{
		tempStudentName = "(Mr. / Mrs. / Ms. ) ";
	}

	if((partyAndPerson.firstName != null) && !((partyAndPerson.firstName).equals(""))){
		tempStudentName += (partyAndPerson.firstName + " ");
	}

	if((partyAndPerson.middleName != null) && !((partyAndPerson.middleName).equals(""))){
		tempStudentName += (partyAndPerson.middleName + " ");
	}

	if((partyAndPerson.lastName != null) && !((partyAndPerson.lastName).equals(""))){
		tempStudentName += (partyAndPerson.lastName + " ");
	}

	context.studentName = tempStudentName;
	
	if((partyAndPerson.cardId != null) && !((partyAndPerson.cardId).equals(""))){
		context.studentNumber = partyAndPerson.cardId;
	}else{
		context.studentNumber = "-";
	}

	if((partyAndPerson.nickname != null) && !((partyAndPerson.nickname).equals(""))){
		context.studentEmail = partyAndPerson.nickname;
	}else{
		context.studentEmail = "-";
	}
}

thesis = delegator.findOne("Thesis", [title : thesisId], false);

if(thesis){
	context.thesisTitle = thesis.judulSkripsi;
	context.thesisStatus = thesis.statusId;
	context.finalAbstract = thesis.finalAbstract;

	partyAndPerson = delegator.findOne("PartyAndPerson", [partyId : thesis.advisor], false);

	if(partyAndPerson){
		//context.advisorName = partyAndPerson.salutation + " " + partyAndPerson.firstName + " " + partyAndPerson.middleName + " " + partyAndPerson.lastName;

		String tempAdvisorName = "";

		if((partyAndPerson.salutation != null) && !((partyAndPerson.salutation).equals(""))){
			tempAdvisorName = partyAndPerson.salutation + " ";
		}else{
			tempAdvisorName = "(Mr. / Mrs. / Ms. ) ";
		}

		if((partyAndPerson.firstName != null) && !((partyAndPerson.firstName).equals(""))){
			tempAdvisorName += (partyAndPerson.firstName + " ");
		}

		if((partyAndPerson.middleName != null) && !((partyAndPerson.middleName).equals(""))){
			tempAdvisorName += (partyAndPerson.middleName + " ");
		}

		if((partyAndPerson.lastName != null) && !((partyAndPerson.lastName).equals(""))){
			tempAdvisorName += (partyAndPerson.lastName + " ");
		}

		context.advisorName = tempAdvisorName;
	}

	if((thesis.coadvisor != null) && !((thesis.coadvisor).equals(""))){
		partyAndPerson = delegator.findOne("PartyAndPerson", [partyId : thesis.coadvisor], false);

		if(partyAndPerson){
			//context.coAdvisorName = partyAndPerson.salutation + " " + partyAndPerson.firstName + " " + partyAndPerson.middleName + " " + partyAndPerson.lastName;

			String tempCoAdvisorName = "";

			if((partyAndPerson.salutation != null) && !((partyAndPerson.salutation).equals(""))){
				tempCoAdvisorName = partyAndPerson.salutation + " ";
			}else{
				tempCoAdvisorName = "(Mr. / Mrs. / Ms. ) ";
			}

			if((partyAndPerson.firstName != null) && !((partyAndPerson.firstName).equals(""))){
				tempCoAdvisorName += (partyAndPerson.firstName + " ");
			}

			if((partyAndPerson.middleName != null) && !((partyAndPerson.middleName).equals(""))){
				tempCoAdvisorName += (partyAndPerson.middleName + " ");
			}

			if((partyAndPerson.lastName != null) && !((partyAndPerson.lastName).equals(""))){
				tempCoAdvisorName += (partyAndPerson.lastName + " ");
			}

			context.coAdvisorName = tempCoAdvisorName;
		}
	}else{
		context.coAdvisorName = "-";
	}

	if((thesis.coadvisor1 != null) && !((thesis.coadvisor1).equals(""))){
		partyAndPerson = delegator.findOne("PartyAndPerson", [partyId : thesis.coadvisor1], false);

		if(partyAndPerson){
			//context.coAdvisor2Name = partyAndPerson.salutation + " " + partyAndPerson.firstName + " " + partyAndPerson.middleName + " " + partyAndPerson.lastName;

			String tempCoAdvisorName = "";

			if((partyAndPerson.salutation != null) && !((partyAndPerson.salutation).equals(""))){
				tempCoAdvisorName = partyAndPerson.salutation + " ";
			}else{
				tempCoAdvisorName = "(Mr. / Mrs. / Ms. ) ";
			}

			if((partyAndPerson.firstName != null) && !((partyAndPerson.firstName).equals(""))){
				tempCoAdvisorName += (partyAndPerson.firstName + " ");
			}

			if((partyAndPerson.middleName != null) && !((partyAndPerson.middleName).equals(""))){
				tempCoAdvisorName += (partyAndPerson.middleName + " ");
			}

			if((partyAndPerson.lastName != null) && !((partyAndPerson.lastName).equals(""))){
				tempCoAdvisorName += (partyAndPerson.lastName + " ");
			}

			context.coAdvisor2Name = tempCoAdvisorName;
		}
	}else{
		context.coAdvisor2Name = "-";
	}
}