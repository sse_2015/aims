import java.util.*;
import org.ofbiz.entity.*;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.base.util.*;
import org.ofbiz.base.util.collections.*;
import org.ofbiz.accounting.invoice.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.math.BigDecimal;
import java.math.MathContext;
import org.ofbiz.base.util.UtilNumber;
import javolution.util.FastList;
import javolution.util.FastMap;
import org.ofbiz.entity.condition.EntityCondition;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

//get parameters
userLoginId = parameters.get("userLoginId");
partyId = parameters.get("partyId");
dateAndTime = parameters.get("dateAndTime");
roomNo = parameters.get("roomNo");
building = parameters.get("building");

//userLogin
context.userLoginId = userLoginId;
context.dateAndTime = dateAndTime;
context.roomNo = roomNo;
context.building = building;

//find firstName, middleName, lastName, email, studentId
partyAndPerson = delegator.findOne("PartyAndPerson", [partyId : partyId], false);
if(partyAndPerson){
	context.salutation = partyAndPerson.salutation;
	context.studentId = partyAndPerson.cardId;
	context.firstName = partyAndPerson.firstName;
	context.middleName = partyAndPerson.middleName;
	context.lastName = partyAndPerson.lastName;

	//find program
	partyRelationship = delegator.findList("PartyRelationship", EntityCondition.makeCondition(EntityCondition.makeCondition("partyIdTo",(String)partyId),EntityCondition.makeCondition("roleTypeIdTo","_NA_")) , null, null, null, false);
	if(partyRelationship){
		context.program = partyRelationship[0].partyIdFrom;
	}
}

//find thesis information
thesis = delegator.findList("Thesis", EntityCondition.makeCondition("studentId",(String)partyId), null, null, null, false);

if(thesis){
	context.title = thesis[thesis.size()-1].judulSkripsi;

	partyAndPerson = delegator.findOne("PartyAndPerson", [partyId : thesis[thesis.size()-1].advisor], false);

	if(partyAndPerson){
		if(partyAndPerson.middleName !=null){
			context.advisorName = partyAndPerson.salutation + " " + partyAndPerson.firstName + " " + partyAndPerson.middleName + " " + partyAndPerson.lastName;
		}else{
			context.advisorName = partyAndPerson.salutation + " " + partyAndPerson.firstName + " " + partyAndPerson.lastName;
		}
	}

	if(thesis[thesis.size()-1].coadvisor != null || !(thesis[thesis.size()-1].coadvisor).equals("")){
		partyAndPerson = delegator.findOne("PartyAndPerson", [partyId : thesis[thesis.size()-1].coadvisor], false);

		if(partyAndPerson){
			if(partyAndPerson.middleName != null){
				context.coadvisorName = partyAndPerson.salutation + " " + partyAndPerson.firstName + " " + partyAndPerson.middleName + " " + partyAndPerson.lastName;
			}else{
				context.coadvisorName = partyAndPerson.salutation + " " + partyAndPerson.firstName + " " + partyAndPerson.lastName;
			}
		}
	}

	if(thesis[thesis.size()-1].coadvisor1 != null || !(thesis[thesis.size()-1].coadvisor1).equals("")){
		partyAndPerson = delegator.findOne("PartyAndPerson", [partyId : thesis[thesis.size()-1].coadvisor1], false);

		if(partyAndPerson){
			if(partyAndPerson.middleName != null){
				context.coadvisorName1 = partyAndPerson.salutation + " " + partyAndPerson.firstName + " " + partyAndPerson.middleName + " " + partyAndPerson.lastName;
			}else{
				context.coadvisorName1 = partyAndPerson.salutation + " " + partyAndPerson.firstName + " " + partyAndPerson.lastName;
			}
		}
	}
}