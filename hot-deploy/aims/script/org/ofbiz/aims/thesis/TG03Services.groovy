import java.util.*;
import org.ofbiz.entity.*;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.base.util.*;
import org.ofbiz.base.util.collections.*;
import org.ofbiz.accounting.invoice.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.math.BigDecimal;
import java.math.MathContext;
import org.ofbiz.base.util.UtilNumber;
import javolution.util.FastList;
import javolution.util.FastMap;
import org.ofbiz.entity.condition.EntityCondition;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

//get parameters
userLoginId = parameters.get("userLoginId");
partyId = parameters.get("partyId");
roomNo = parameters.get("roomNo");
building = parameters.get("building");
thesisProgressExamination = parameters.get("thesisProgressExamination");
studentAbstract = parameters.get("abstract");

//userLogin
context.userLoginId = userLoginId;

if(roomNo != null){
	context.roomNo = roomNo;
}

if(studentAbstract != null){
	context.studentAbstract = studentAbstract;
}

if(building != null){
	context.building = building;
}

if(thesisProgressExamination != null){
	context.thesisProgressExamination = thesisProgressExamination;
}
//find firstName, middleName, lastName, email, studentId

partyAndPerson = delegator.findOne("PartyAndPerson", [partyId : partyId], false);
if(partyAndPerson){
	context.salutation = partyAndPerson.salutation;
	context.studentId = partyAndPerson.cardId;
	context.firstName = partyAndPerson.firstName;
	context.middleName = partyAndPerson.middleName;
	context.lastName = partyAndPerson.lastName;
	context.email = partyAndPerson.nickname;

	//find department, program
	partyRelationship = delegator.findList("PartyRelationship", EntityCondition.makeCondition(EntityCondition.makeCondition("partyIdTo",(String)partyId),EntityCondition.makeCondition("roleTypeIdTo","_NA_")) , null, null, null, false);
	if(partyRelationship){
		context.program = partyRelationship[0].partyIdFrom;

		partyRelationship2 = delegator.findList("PartyRelationship", EntityCondition.makeCondition("partyIdTo",(String)context.program) , null, null, null, false);	

		if(partyRelationship2){
			context.department = partyRelationship2[0].partyIdFrom;
		}
	}
}

//find address, telephone number
partyAndContact = delegator.findList("PartyAndContact", EntityCondition.makeCondition(EntityCondition.makeCondition("partyId",(String)partyId),EntityCondition.makeCondition("partyTypeId","PERSON")) , null, null, null, false);

if(partyAndContact){
	context.address = partyAndContact[0].paAddress1 + " , " + partyAndContact[0].paCity + " " + partyAndContact[0].paPostalCode + " , " + partyAndContact[0].paCountryGeoId;
	context.telephone = "+" + partyAndContact[0].tnCountryCode + "-" + partyAndContact[0].tnContactNumber;
}

thesis = delegator.findList("Thesis", EntityCondition.makeCondition("studentId",(String)partyId), null, null, null, false);

if(thesis){
	context.title = thesis[thesis.size()-1].judulSkripsi;
	context.topic = thesis[thesis.size()-1].judulSkripsi;
	context.declaredDate = thesis[thesis.size()-1].declaredDate;
	context.educationLevel = thesis[thesis.size()-1].educationLevel;

	partyAndPerson = delegator.findOne("PartyAndPerson", [partyId : thesis[thesis.size()-1].advisor], false);

	if(partyAndPerson){
		context.asalutation = partyAndPerson.salutation;
		context.afirstName = partyAndPerson.firstName;
		context.amiddleName = partyAndPerson.middleName;
		context.alastName = partyAndPerson.lastName;

		if(partyAndPerson.middleName != null){
			context.advisor = partyAndPerson.salutation + " " + partyAndPerson.firstName + " " + partyAndPerson.middleName + " " + partyAndPerson.lastName;
		}else{
			context.advisor = partyAndPerson.salutation + " " + partyAndPerson.firstName + " " + partyAndPerson.lastName;
		}
	}

	if(thesis[thesis.size()-1].coadvisor != null || !(thesis[thesis.size()-1].coadvisor).equals("")){
		partyAndPerson = delegator.findOne("PartyAndPerson", [partyId : thesis[thesis.size()-1].coadvisor], false);

		if(partyAndPerson){
			context.casalutation = partyAndPerson.salutation;
			context.cafirstName = partyAndPerson.firstName;
			context.camiddleName = partyAndPerson.middleName;
			context.calastName = partyAndPerson.lastName;

			if(partyAndPerson.middleName != null){
				context.coadvisor = partyAndPerson.salutation + " " + partyAndPerson.firstName + " " + partyAndPerson.middleName + " " + partyAndPerson.lastName;
			}else{
				context.coadvisor = partyAndPerson.salutation + " " + partyAndPerson.firstName + " " + partyAndPerson.lastName;
			}
		}
	}

	if(thesis[thesis.size()-1].coadvisor1 != null || !(thesis[thesis.size()-1].coadvisor1).equals("")){
		partyAndPerson = delegator.findOne("PartyAndPerson", [partyId : thesis[thesis.size()-1].coadvisor1], false);

		if(partyAndPerson){
			context.ca1salutation = partyAndPerson.salutation;
			context.ca1firstName = partyAndPerson.firstName;
			context.ca1middleName = partyAndPerson.middleName;
			context.ca1lastName = partyAndPerson.lastName;

			if(partyAndPerson.middleName != null){
				context.coadvisor2 = partyAndPerson.salutation + " " + partyAndPerson.firstName + " " + partyAndPerson.middleName + " " + partyAndPerson.lastName;
			}else{
				context.coadvisor2 = partyAndPerson.salutation + " " + partyAndPerson.firstName + " " + partyAndPerson.lastName;
			}
		}
	}
}

//date
DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
Date date = new Date();
context.date = dateFormat.format(date);