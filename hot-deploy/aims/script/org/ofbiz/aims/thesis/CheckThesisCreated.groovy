//check whether thesis created or not
import java.util.*;
import org.ofbiz.entity.*;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.base.util.*;
import org.ofbiz.base.util.collections.*;
import org.ofbiz.accounting.invoice.*;
import java.math.BigDecimal;
import java.math.MathContext;
import org.ofbiz.base.util.UtilNumber;
import javolution.util.FastList;
import javolution.util.FastMap;
import org.ofbiz.entity.condition.EntityCondition;


partyId = parameters.get("partyId");

if(partyId == null){
	partyId = userLogin.partyId;
}

thesis = delegator.findList("Thesis", EntityCondition.makeCondition("studentId",(String)partyId), null, null, null, false);

if(thesis){
	if(thesis[thesis.size()-1].statusId.equals("THESIS_REJECTED")){
		context.thesisCreated = "false";
	}else{
		context.thesisCreated = "true";
	}

	/*if(thesis.size() < 2){
		for(int i = 0 ; i < thesis.size(); i++){
			if(thesis[i].thesisStatus.equals("THESIS_REJECTED")){
				context.thesisCreated = "false";
			}else{
				context.thesisCreated = "true";
			}
		}
	}else{
		context.thesisCreated = "true";
	}*/

	
}else{
	context.thesisCreated = "false";
}