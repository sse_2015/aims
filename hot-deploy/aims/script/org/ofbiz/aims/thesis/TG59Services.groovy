import java.util.*;
import org.ofbiz.entity.*;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.base.util.*;
import org.ofbiz.base.util.collections.*;
import org.ofbiz.accounting.invoice.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.math.BigDecimal;
import java.math.MathContext;
import org.ofbiz.base.util.UtilNumber;
import javolution.util.FastList;
import javolution.util.FastMap;
import org.ofbiz.entity.condition.EntityCondition;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

//get parameters
userLoginId = parameters.get("userLoginId");
partyId = parameters.get("partyId");
kind = parameters.get("kind");
thesisId = parameters.get("title");
comittee = parameters.get("comittee");
roomNo = parameters.get("roomNo");
building = parameters.get("building");

if(roomNo != null){
	context.roomNo = roomNo;
}

if(building != null){
	context.building = building;
}


comittee = parameters.get("comittee");
comittee1 = parameters.get("comittee1");
comittee2 = parameters.get("comittee2");

if(comittee != null && !comittee.equals("")){
	partyAndPerson = delegator.findOne("PartyAndPerson", [partyId : comittee], false);
	if(partyAndPerson){
		if(partyAndPerson.middleName != null && !(partyAndPerson.middleName).equals("")){
			context.comittee = partyAndPerson.salutation + ". " + partyAndPerson.firstName + " " + partyAndPerson.middleName + " " + partyAndPerson.lastName;
		}else{
			context.comittee = partyAndPerson.salutation + ". " + partyAndPerson.firstName + " " + partyAndPerson.lastName;
		}
	}
}

if(comittee1 != null && !comittee1.equals("")){
	partyAndPerson = delegator.findOne("PartyAndPerson", [partyId : comittee1], false);
	if(partyAndPerson){
		if(partyAndPerson.middleName != null && !(partyAndPerson.middleName).equals("")){
			context.comittee1 = partyAndPerson.salutation + ". " + partyAndPerson.firstName + " " + partyAndPerson.middleName + " " + partyAndPerson.lastName;
		}else{
			context.comittee1 = partyAndPerson.salutation + ". " + partyAndPerson.firstName + " " + partyAndPerson.lastName;
		}
	}
}

if(comittee2 != null && !comittee2.equals("")){
	partyAndPerson = delegator.findOne("PartyAndPerson", [partyId : comittee2], false);
	if(partyAndPerson){
		if(partyAndPerson.middleName != null && !(partyAndPerson.middleName).equals("")){
			context.comittee2 = partyAndPerson.salutation + ". " + partyAndPerson.firstName + " " + partyAndPerson.middleName + " " + partyAndPerson.lastName;
		}else{
			context.comittee2 = partyAndPerson.salutation + ". " + partyAndPerson.firstName + " " + partyAndPerson.lastName;
		}
	}
}

//userLogin
context.userLoginId = userLoginId;

if(comittee != null && !comittee.equals("")){
	partyAndPerson = delegator.findOne("PartyAndPerson", [partyId : comittee], false);
	if(partyAndPerson){
		if(partyAndPerson.middleName != null && !(partyAndPerson.middleName).equals("")){
			context.comittee = partyAndPerson.salutation + ". " + partyAndPerson.firstName + " " + partyAndPerson.middleName + " " + partyAndPerson.lastName;
		}else{
			context.comittee = partyAndPerson.salutation + ". " + partyAndPerson.firstName + " " + partyAndPerson.lastName;
		}
	}
}

//find firstName, middleName, lastName, email, studentId
partyAndPerson = delegator.findOne("PartyAndPerson", [partyId : partyId], false);
if(partyAndPerson){
	context.salutation = partyAndPerson.salutation;
	context.studentId = partyAndPerson.cardId;
	context.firstName = partyAndPerson.firstName;
	context.middleName = partyAndPerson.middleName;
	context.lastName = partyAndPerson.lastName;

	//find program
	partyRelationship = delegator.findList("PartyRelationship", EntityCondition.makeCondition(EntityCondition.makeCondition("partyIdTo",(String)partyId),EntityCondition.makeCondition("roleTypeIdTo","_NA_")) , null, null, null, false);
	if(partyRelationship){
		context.program = partyRelationship[0].partyIdFrom;
	}
}

//find thesis
thesis = delegator.findOne("Thesis", [title : thesisId], false);

if(thesis){
	context.title = thesis.judulSkripsi;
	context.appDate = thesis.declaredDate;
	context.defenseDate = thesis.defenseDate;

	partyAndPerson = delegator.findOne("PartyAndPerson", [partyId : thesis.advisor], false);

	if(partyAndPerson){
		context.asalutation = partyAndPerson.salutation;
		context.afirstName = partyAndPerson.firstName;
		context.amiddleName = partyAndPerson.middleName;
		context.alastName = partyAndPerson.lastName;
	}

	if(thesis.coadvisor != null || !(thesis.coadvisor).equals("")){
		partyAndPerson = delegator.findOne("PartyAndPerson", [partyId : thesis.coadvisor], false);

		if(partyAndPerson){
			context.casalutation = partyAndPerson.salutation;
			context.cafirstName = partyAndPerson.firstName;
			context.camiddleName = partyAndPerson.middleName;
			context.calastName = partyAndPerson.lastName;
		}
	}
}
//date
DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
Date date = new Date();
context.date = dateFormat.format(date);

DateFormat dateFormat2 = new SimpleDateFormat("yyyy");
Date date2 = new Date();
context.year = dateFormat2.format(date2);
