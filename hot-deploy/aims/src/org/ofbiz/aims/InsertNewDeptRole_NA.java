package org.ofbiz.aims;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.sql.Date;

import javolution.util.FastList;
import javolution.util.FastMap;

import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilGenerics;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilProperties;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityConditionList;
import org.ofbiz.entity.condition.EntityExpr;
import org.ofbiz.entity.condition.EntityFunction;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.model.DynamicViewEntity;
import org.ofbiz.entity.model.ModelKeyMap;
import org.ofbiz.entity.util.EntityFindOptions;
import org.ofbiz.entity.util.EntityListIterator;
import org.ofbiz.entity.util.EntityTypeUtil;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.GenericServiceException;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ModelService;
import org.ofbiz.service.ServiceUtil;

//for import upload
import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.base.util.string.FlexibleStringExpander;
import java.util.*;
import java.io.*;
import java.lang.Long;

public class InsertNewDeptRole_NA{
    public static final String module = InsertNewDeptRole_NA.class.getName();
    public static final String resource = "PartyUiLabels";
    public static final String resourceError = "PartyErrorUiLabels";

    public static String getPartyId(Map<String, ? extends Object> context) {
        String partyId = (String) context.get("partyId");
        if (UtilValidate.isEmpty(partyId)) {
            GenericValue userLogin = (GenericValue) context.get("userLogin");
            if (userLogin != null) {
                partyId = userLogin.getString("partyId");
            }
        }
        return partyId;
    }

     public static String createReqId(Delegator delegator, String prefix, int length) {
        final String seqName = "RequestUpdatePerson";
        String reqPartyId = prefix != null ? prefix : "";

        // generate the sequenced number and pad
        Long seq = delegator.getNextSeqIdLong(seqName);
        reqPartyId = reqPartyId + UtilFormatOut.formatPaddedNumber(seq.longValue(), (length - reqPartyId.length() - 1));

        // get the check digit
        int check = UtilValidate.getLuhnCheckDigit(reqPartyId);
        reqPartyId = reqPartyId + Integer.toString(check);

        return reqPartyId;
    }

    public static String createReqId2(String seqname, Delegator delegator, String prefix, int length) {
        final String seqName = seqname;
        String reqPartyId = prefix != null ? prefix : "";

        // generate the sequenced number and pad
        Long seq = delegator.getNextSeqIdLong(seqName);
        reqPartyId = reqPartyId + UtilFormatOut.formatPaddedNumber(seq.longValue(), (length - reqPartyId.length() - 1));

        // get the check digit
        int check = UtilValidate.getLuhnCheckDigit(reqPartyId);
        reqPartyId = reqPartyId + Integer.toString(check);

        return reqPartyId;
    }

    public static Map<String, Object> uploadFile(DispatchContext ctx, Map<String, ? extends Object> context) {
        Map<String, Object> result = FastMap.newInstance();
        Delegator delegator = ctx.getDelegator();
        LocalDispatcher dispatcher = ctx.getDispatcher();
        Locale locale = (Locale) context.get("locale");

        String partyId = (String)context.get("partyId");
        String partyIdFrom = (String)context.get("partyIdFrom");
        String roleTypeIdFrom = "_NA_";
        String roleTypeIdTo = "_NA_";

        int inPerson = 0;

        if (UtilValidate.isEmpty(partyId)) {
            return ServiceUtil.returnError(UtilProperties.getMessage(ServiceUtil.resource, 
                    "serviceUtil.party_id_missing", locale));
        }

        GenericValue party = null;       

        try{
            // newId = createReqId(delegator,"",5);
            party = delegator.makeValue("PartyRole");
            party.set("partyId",partyId);
            party.set("roleTypeId","_NA_");
            //java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy/MM/dd HH:mm:ss");            
            delegator.create(party);
         
        }catch(Exception e){
            //req party id not found]
            Debug.logWarning(e, module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, "asd "+e, new Object[] {e.getMessage() }, locale));
            
        }    

        return result;
    }
}