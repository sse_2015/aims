package org.ofbiz.aims;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import javolution.util.FastList;
import javolution.util.FastMap;

import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilGenerics;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilProperties;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityConditionList;
import org.ofbiz.entity.condition.EntityExpr;
import org.ofbiz.entity.condition.EntityFunction;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.model.DynamicViewEntity;
import org.ofbiz.entity.model.ModelKeyMap;
import org.ofbiz.entity.util.EntityFindOptions;
import org.ofbiz.entity.util.EntityListIterator;
import org.ofbiz.entity.util.EntityTypeUtil;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.GenericServiceException;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ModelService;
import org.ofbiz.service.ServiceUtil;

//for import upload
import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.base.util.string.FlexibleStringExpander;
import java.util.*;
import java.io.*;
import java.lang.Long;
import org.ofbiz.aims.upload.UploadWorker;
import org.ofbiz.aims.upload.UploadServices;

public class ThesisServices{
    public static final String module = ThesisServices.class.getName();
    public static final String resource = "PartyUiLabels";
    public static final String resourceError = "PartyErrorUiLabels";

    public static String getPartyId(Map<String, ? extends Object> context) {
        String partyId = (String) context.get("partyId");
        if (UtilValidate.isEmpty(partyId)) {
            GenericValue userLogin = (GenericValue) context.get("userLogin");
            if (userLogin != null) {
                partyId = userLogin.getString("partyId");
            }
        }
        return partyId;
    }

    public static Map<String, Object> updateThesis(DispatchContext ctx, Map<String, ? extends Object> context) throws GenericServiceException{
        Map<String, Object> result = FastMap.newInstance();
        Delegator delegator = ctx.getDelegator();
        LocalDispatcher dispatcher = ctx.getDispatcher();
        Locale locale = (Locale) context.get("locale");

        //String partyId = getPartyId(context);

        String thesisId = (String)context.get("title");
        String judulSkripsi = (String)context.get("judulSkripsi");
        String educationLevel = (String)context.get("educationLevel");
        String finalAbstract = (String)context.get("finalAbstract");
        String proposalResult = (String)context.get("proposalResult");
        String proposalReason = (String)context.get("proposalReason");
        String progressResult = (String)context.get("progressResult");
        String progressReason = (String)context.get("progressReason");
        String studentId = (String)context.get("studentId");
        String statusId = (String)context.get("statusId");
        String location = (String)context.get("location");
        String book = (String)context.get("book");
        String grade = (String)context.get("grade");
        String advisor = (String)context.get("advisor");

        Timestamp declaredDate = (Timestamp)context.get("declaredDate");
        Timestamp defenseDate = (Timestamp)context.get("defenseDate");

        if (UtilValidate.isEmpty(thesisId)) {
            return ServiceUtil.returnError(UtilProperties.getMessage(ServiceUtil.resource, 
                    "serviceUtil.party_id_missing", locale));
        }

        GenericValue thesis = null;

        try{
            thesis = delegator.findOne("Thesis", UtilMisc.toMap("title", thesisId), false);
        }catch(Exception e){
            Debug.logWarning(e, module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, "thesis.update.read_failure", new Object[] {e.getMessage() }, locale));
        }    

        if(thesis == null){
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "thesis.update.not_found", locale));
        }

        Map<String, Object> tempThesis = (Map<String, Object>)thesis.getAllFields();

        /**
        *   For Upload
        **/

        /*
            =================
            Thesis Proposal
            =================
        */

        if(tempThesis.get("location").equals("Thailand")){
            String propTG01 = (String)tempThesis.get("TG01");
            ByteBuffer fileDataTG01 = null;
            String fileNameTG01 = "";
            String contentTypeTG01 = "";
            byte[] reportDataTG01 = null;

            try{
                fileDataTG01 = (ByteBuffer) context.get("TG01");
                fileNameTG01 = (String) context.get("_TG01_fileName");
                contentTypeTG01 = (String) context.get("_TG01_contentType");
                reportDataTG01 = fileDataTG01.array();

                if(propTG01.equals("-")){
                    if(reportDataTG01 == null || reportDataTG01.length == 0){
                        thesis.set("TG01", "-");
                        thesis.set("checkTG01", "http://asap-sol.com/images/icon-minus.png");
                    }else{
                        thesis.set("TG01", UploadServices.createFileUpload(delegator, fileNameTG01, contentTypeTG01, reportDataTG01), false);
                        thesis.set("checkTG01","http://www.tipinsure.com/images/check.png");
                    }
                }else{
                    thesis.set("TG01", propTG01);
                    thesis.set("checkTG01","http://www.tipinsure.com/images/check.png");
                    if(reportDataTG01 != null && reportDataTG01.length > 0){
                        UploadServices.updateFileUpload(delegator, propTG01, fileNameTG01, contentTypeTG01, reportDataTG01);    
                    }
                }
            }catch(Exception e){
                thesis.set("TG01", propTG01);
                thesis.set("checkTG01", (String)tempThesis.get("checkTG01"));
            }

             String propTG02 = (String)tempThesis.get("TG02");
             ByteBuffer fileDataTG02 = null;
             String fileNameTG02 = "";
             String contentTypeTG02 = "";
             byte[] reportDataTG02 = null;

             try{
                fileDataTG02 = (ByteBuffer) context.get("TG02");
                fileNameTG02 = (String) context.get("_TG02_fileName");
                contentTypeTG02 = (String) context.get("_TG02_contentType");
                reportDataTG02 = fileDataTG02.array();
                if(propTG02.equals("-")){
                    if(reportDataTG02 == null || reportDataTG02.length == 0){
                        thesis.set("TG02", "-");
                        thesis.set("checkTG02", "http://asap-sol.com/images/icon-minus.png");
                    }else{
                        thesis.set("TG02", UploadServices.createFileUpload(delegator, fileNameTG02, contentTypeTG02, reportDataTG02),false);
                        thesis.set("checkTG02","http://www.tipinsure.com/images/check.png");
                    }
                }else{
                    thesis.set("TG02", propTG02);
                    thesis.set("checkTG02","http://www.tipinsure.com/images/check.png");
                    if(reportDataTG02 != null && reportDataTG02.length > 0){
                        UploadServices.updateFileUpload(delegator, propTG02, fileNameTG02, contentTypeTG02, reportDataTG02);                
                    }
                }
             }catch(Exception e){
                thesis.set("TG02", propTG02);
                thesis.set("checkTG02", (String)tempThesis.get("checkTG02"));
             }

            String propTG51 = (String)tempThesis.get("TG51");
            ByteBuffer fileDataTG51 = null;
            String fileNameTG51 = "";
            String contentTypeTG51 = "";
            byte[] reportDataTG51 = null;

            try{
                fileDataTG51 = (ByteBuffer) context.get("TG51");
                fileNameTG51 = (String) context.get("_TG51_fileName");
                contentTypeTG51 = (String) context.get("_TG51_contentType");
                reportDataTG51 = fileDataTG51.array();

                if(propTG51.equals("-")){
                    if(reportDataTG51 == null || reportDataTG51.length == 0){
                        thesis.set("TG51", "-");
                        thesis.set("checkTG51", "http://asap-sol.com/images/icon-minus.png");
                    }else{
                        thesis.set("TG51", UploadServices.createFileUpload(delegator, fileNameTG51, contentTypeTG51, reportDataTG51),false);
                        thesis.set("checkTG51","http://www.tipinsure.com/images/check.png");
                    }
                }else{
                    thesis.set("TG51", propTG51);
                    thesis.set("checkTG51","http://www.tipinsure.com/images/check.png");
                    if(reportDataTG51 != null && reportDataTG51.length > 0){
                        UploadServices.updateFileUpload(delegator, propTG51, fileNameTG51, contentTypeTG51, reportDataTG51);                
                    }
                }
            }catch(Exception e){
                thesis.set("TG51", propTG51);
                thesis.set("checkTG51", (String)tempThesis.get("checkTG51"));
            }
        }else{
            thesis.set("TG01", "-");
            thesis.set("TG02", "-");
            thesis.set("TG51", "-");
            thesis.set("TG57", "-");
            thesis.set("TG58", "-");
        }

        String propTG52 = (String)tempThesis.get("TG52");
        ByteBuffer fileDataTG52 = null;
        String fileNameTG52 = "";
        String contentTypeTG52 = "";
        byte[] reportDataTG52 = null;

        try{
            fileDataTG52 = (ByteBuffer) context.get("TG52");
            fileNameTG52 = (String) context.get("_TG52_fileName");
            contentTypeTG52 = (String) context.get("_TG52_contentType");
            reportDataTG52 = fileDataTG52.array();

            if(propTG52.equals("-")){
                if(reportDataTG52 == null || reportDataTG52.length == 0){
                    thesis.set("TG52", "-");
                    thesis.set("checkTG52", "http://asap-sol.com/images/icon-minus.png");
                }else{
                    thesis.set("TG52", UploadServices.createFileUpload(delegator, fileNameTG52, contentTypeTG52, reportDataTG52),false);
                    thesis.set("checkTG52","http://www.tipinsure.com/images/check.png");
                }
            }else{
                thesis.set("TG52", propTG52);
                thesis.set("checkTG52","http://www.tipinsure.com/images/check.png");
                if(reportDataTG52 != null && reportDataTG52.length > 0){
                    UploadServices.updateFileUpload(delegator, propTG52, fileNameTG52, contentTypeTG52, reportDataTG52);                
                }
            }
        }catch(Exception e){
            thesis.set("TG52", propTG52);
            thesis.set("checkTG52", (String)tempThesis.get("checkTG52"));
        }

        if(tempThesis.get("location").equals("Thailand")){
        	String propTG56 = (String)tempThesis.get("TG56");
            ByteBuffer fileDataTG56 = null;
            String fileNameTG56 = "";
            String contentTypeTG56 = "";
            byte[] reportDataTG56 = null;

            try{
                fileDataTG56 = (ByteBuffer) context.get("TG56");
                fileNameTG56 = (String) context.get("_TG56_fileName");
                contentTypeTG56 = (String) context.get("_TG56_contentType");
                reportDataTG56 = fileDataTG56.array();

                if(propTG56.equals("-")){
                    if(reportDataTG56 == null || reportDataTG56.length == 0){
                        thesis.set("TG56", "-");
                        thesis.set("checkTG56", "http://asap-sol.com/images/icon-minus.png");
                    }else{
                        thesis.set("TG56", UploadServices.createFileUpload(delegator, fileNameTG56, contentTypeTG56, reportDataTG56),false);
                        thesis.set("checkTG56","http://www.tipinsure.com/images/check.png");
                    }
                }else{
                    thesis.set("TG56", propTG56);
                    thesis.set("checkTG56","http://www.tipinsure.com/images/check.png");
                    if(reportDataTG56 != null && reportDataTG56.length > 0){
                        UploadServices.updateFileUpload(delegator, propTG56, fileNameTG56, contentTypeTG56, reportDataTG56);                
                    }
                }
            }catch(Exception e){
                thesis.set("TG56", propTG56);
                thesis.set("checkTG56", (String)tempThesis.get("checkTG56"));
            }
            
            String propTG57 = (String)tempThesis.get("TG57");
            ByteBuffer fileDataTG57 = null;
            String fileNameTG57 = "";
            String contentTypeTG57 = "";
            byte[] reportDataTG57 = null;

            try{
                fileDataTG57 = (ByteBuffer) context.get("TG57");
                fileNameTG57 = (String) context.get("_TG57_fileName");
                contentTypeTG57 = (String) context.get("_TG57_contentType");
                reportDataTG57 = fileDataTG57.array();

                if(propTG57.equals("-")){
                    if(reportDataTG57 == null || reportDataTG57.length == 0){
                        thesis.set("TG57", "-");
                        thesis.set("checkTG57", "http://asap-sol.com/images/icon-minus.png");
                    }else{
                        thesis.set("TG57", UploadServices.createFileUpload(delegator, fileNameTG57, contentTypeTG57, reportDataTG57),false);
                        thesis.set("checkTG57","http://www.tipinsure.com/images/check.png");
                    }
                }else{
                    thesis.set("TG57", propTG57);
                    thesis.set("checkTG57","http://www.tipinsure.com/images/check.png");
                    if(reportDataTG57 != null && reportDataTG57.length > 0){
                        UploadServices.updateFileUpload(delegator, propTG57, fileNameTG57, contentTypeTG57, reportDataTG57);                
                    }
                }
            }catch(Exception e){
                thesis.set("TG57", propTG57);
                thesis.set("checkTG57", (String)tempThesis.get("checkTG57"));
            }


            String propTG58 = (String)tempThesis.get("TG58");
            ByteBuffer fileDataTG58 = null;
            String fileNameTG58 = "";
            String contentTypeTG58 = "";
            byte[] reportDataTG58 = null;

            try{
                fileDataTG58 = (ByteBuffer) context.get("TG58");
                fileNameTG58 = (String) context.get("_TG58_fileName");
                contentTypeTG58 = (String) context.get("_TG58_contentType");
                reportDataTG58 = fileDataTG58.array();

                if(propTG58.equals("-")){
                    if(reportDataTG58 == null || reportDataTG58.length == 0){
                        thesis.set("TG58", "-");
                        thesis.set("checkTG58", "http://asap-sol.com/images/icon-minus.png");
                    }else{
                        thesis.set("TG58", UploadServices.createFileUpload(delegator, fileNameTG58, contentTypeTG58, reportDataTG58),false);
                        thesis.set("checkTG58","http://www.tipinsure.com/images/check.png");
                    }
                }else{
                    thesis.set("TG58", propTG58);
                    thesis.set("checkTG58","http://www.tipinsure.com/images/check.png");
                    if(reportDataTG58 != null && reportDataTG58.length > 0){
                        UploadServices.updateFileUpload(delegator, propTG58, fileNameTG58, contentTypeTG58, reportDataTG58);                
                    }
                }
            }catch(Exception e){
                thesis.set("TG58", propTG58);
                thesis.set("checkTG58", (String)tempThesis.get("checkTG58"));
            }
        }
        /*
            ======================
            End Of Thesis Proposal
            ======================
        */

        /*
            =================
            Thesis Progress
            =================
        */
        if(tempThesis.get("location").equals("Thailand")){
            String propTG03 = (String)tempThesis.get("TG03");
            ByteBuffer fileDataTG03 = null;
            String fileNameTG03 = "";
            String contentTypeTG03 = "";
            byte[] reportDataTG03 = null;

            try{
                fileDataTG03 = (ByteBuffer) context.get("TG03");
                fileNameTG03 = (String) context.get("_TG03_fileName");
                contentTypeTG03 = (String) context.get("_TG03_contentType");
                reportDataTG03 = fileDataTG03.array();

                if(propTG03.equals("-")){
                    if(reportDataTG03 == null || reportDataTG03.length == 0){
                        thesis.set("TG03", "-");
                        thesis.set("checkTG03", "http://asap-sol.com/images/icon-minus.png");
                    }else{
                        thesis.set("TG03", UploadServices.createFileUpload(delegator, fileNameTG03, contentTypeTG03, reportDataTG03),false);
                        thesis.set("checkTG03","http://www.tipinsure.com/images/check.png");
                    }
                }else{
                    thesis.set("TG03", propTG03);
                    thesis.set("checkTG03","http://www.tipinsure.com/images/check.png");
                    if(reportDataTG03 != null && reportDataTG03.length > 0){
                        UploadServices.updateFileUpload(delegator, propTG03, fileNameTG03, contentTypeTG03, reportDataTG03);                
                    }
                }
            }catch(Exception e){
                thesis.set("TG03", propTG03);
                thesis.set("checkTG03", (String)tempThesis.get("checkTG03"));
            }

            String propTG53 = (String)tempThesis.get("TG53");
            ByteBuffer fileDataTG53 = null;
            String fileNameTG53 = "";
            String contentTypeTG53 = "";
            byte[] reportDataTG53 = null;

            try{
                fileDataTG53 = (ByteBuffer) context.get("TG53");
                fileNameTG53 = (String) context.get("_TG53_fileName");
                contentTypeTG53 = (String) context.get("_TG53_contentType");
                reportDataTG53 = fileDataTG53.array();

                if(propTG53.equals("-")){
                    if(reportDataTG53 == null || reportDataTG53.length == 0){
                        thesis.set("TG53", "-");
                        thesis.set("checkTG53", "http://asap-sol.com/images/icon-minus.png");
                    }else{
                        thesis.set("TG53", UploadServices.createFileUpload(delegator, fileNameTG53, contentTypeTG53, reportDataTG53),false);
                        thesis.set("checkTG53","http://www.tipinsure.com/images/check.png");
                    }
                }else{
                    thesis.set("TG53", propTG53);
                    thesis.set("checkTG53","http://www.tipinsure.com/images/check.png");
                    if(reportDataTG53 != null && reportDataTG53.length > 0){
                        UploadServices.updateFileUpload(delegator, propTG53, fileNameTG53, contentTypeTG53, reportDataTG53);                
                    }
                }
            }catch(Exception e){
                thesis.set("TG53", propTG53);
                thesis.set("checkTG53", (String)tempThesis.get("checkTG53"));
            }

            String propTG60 = (String)tempThesis.get("TG60");
            ByteBuffer fileDataTG60 = null;
            String fileNameTG60 = "";
            String contentTypeTG60 = "";
            byte[] reportDataTG60 = null;

            try{
                fileDataTG60 = (ByteBuffer) context.get("TG60");
                fileNameTG60 = (String) context.get("_TG60_fileName");
                contentTypeTG60 = (String) context.get("_TG60_contentType");
                reportDataTG60 = fileDataTG60.array();

                if(propTG60.equals("-")){
                    if(reportDataTG60 == null || reportDataTG60.length == 0){
                        thesis.set("TG60", "-");
                        thesis.set("checkTG60", "http://asap-sol.com/images/icon-minus.png");
                    }else{
                        thesis.set("TG60", UploadServices.createFileUpload(delegator, fileNameTG60, contentTypeTG60, reportDataTG60),false);
                        thesis.set("checkTG60","http://www.tipinsure.com/images/check.png");
                    }
                }else{
                    thesis.set("TG60", propTG60);
                    thesis.set("checkTG60","http://www.tipinsure.com/images/check.png");
                    if(reportDataTG60 != null && reportDataTG60.length > 0){
                        UploadServices.updateFileUpload(delegator, propTG60, fileNameTG60, contentTypeTG60, reportDataTG60);                
                    }
                }
            }catch(Exception e){
                thesis.set("TG60", propTG60);
                thesis.set("checkTG60", (String)tempThesis.get("checkTG60"));
            }        
        }else{
            thesis.set("TG03", "-");
            thesis.set("TG53", "-");
            thesis.set("TG60", "-");
        }

        /*
            ======================
            End Of Thesis Progress
            ======================
        */

        /*
            ===============
            Thesis Defense
            ===============
        */

        if(tempThesis.get("location").equals("Thailand")){            
            String propTG04 = (String)tempThesis.get("TG04");
            ByteBuffer fileDataTG04 = null;
            String fileNameTG04 = "";
            String contentTypeTG04 = "";
            byte[] reportDataTG04 = null;

            try{
                fileDataTG04 = (ByteBuffer) context.get("TG04");
                fileNameTG04 = (String) context.get("_TG04_fileName");
                contentTypeTG04 = (String) context.get("_TG04_contentType");
                reportDataTG04 = fileDataTG04.array();        
                if(propTG04.equals("-")){
                    if(reportDataTG04 == null || reportDataTG04.length == 0){
                        thesis.set("TG04", "-");
                        thesis.set("checkTG04", "http://asap-sol.com/images/icon-minus.png");
                    }else{
                        thesis.set("TG04", UploadServices.createFileUpload(delegator, fileNameTG04, contentTypeTG04, reportDataTG04),false);
                        thesis.set("checkTG04","http://www.tipinsure.com/images/check.png");
                    }
                }else{
                    thesis.set("TG04", propTG04);
                    thesis.set("checkTG04","http://www.tipinsure.com/images/check.png");
                    if(reportDataTG04 != null && reportDataTG04.length > 0){
                        UploadServices.updateFileUpload(delegator, propTG04, fileNameTG04, contentTypeTG04, reportDataTG04);                
                    }
                }
            }catch(Exception e){
                thesis.set("TG04", propTG04);
                thesis.set("checkTG04", (String)tempThesis.get("checkTG04"));
            }

            String propTG54 = (String)tempThesis.get("TG54");
            ByteBuffer fileDataTG54 = null;
            String fileNameTG54 = "";
            String contentTypeTG54 = "";
            byte[] reportDataTG54 = null;

            try{
                fileDataTG54 = (ByteBuffer) context.get("TG54");
                fileNameTG54 = (String) context.get("_TG54_fileName");
                contentTypeTG54 = (String) context.get("_TG54_contentType");
                reportDataTG54 = fileDataTG54.array();
                if(propTG54.equals("-")){
                    if(reportDataTG54 == null || reportDataTG54.length == 0){
                        thesis.set("TG54", "-");
                        thesis.set("checkTG54", "http://asap-sol.com/images/icon-minus.png");
                    }else{
                        thesis.set("TG54", UploadServices.createFileUpload(delegator, fileNameTG54, contentTypeTG54, reportDataTG54),false);
                        thesis.set("checkTG54","http://www.tipinsure.com/images/check.png");
                    }
                }else{
                    thesis.set("TG54", propTG54);
                    thesis.set("checkTG54","http://www.tipinsure.com/images/check.png");
                    if(reportDataTG54 != null && reportDataTG54.length > 0){
                        UploadServices.updateFileUpload(delegator, propTG54, fileNameTG54, contentTypeTG54, reportDataTG54);                
                    }
                }
            }catch(Exception e){
                thesis.set("TG54", propTG54);
                thesis.set("checkTG54", (String)tempThesis.get("checkTG54"));
            }

            String propTG59 = (String)tempThesis.get("TG59");
            ByteBuffer fileDataTG59 = null;
            String fileNameTG59 = "";
            String contentTypeTG59 = "";
            byte[] reportDataTG59 = null;

            try{
                fileDataTG59 = (ByteBuffer) context.get("TG59");
                fileNameTG59 = (String) context.get("_TG59_fileName");
                contentTypeTG59 = (String) context.get("_TG59_contentType");
                reportDataTG59 = fileDataTG59.array();
                if(propTG59.equals("-")){
                    if(reportDataTG59 == null || reportDataTG59.length == 0){
                        thesis.set("TG59", "-");
                        thesis.set("checkTG59", "http://asap-sol.com/images/icon-minus.png");
                    }else{
                        thesis.set("TG59", UploadServices.createFileUpload(delegator, fileNameTG59, contentTypeTG59, reportDataTG59),false);
                        thesis.set("checkTG59","http://www.tipinsure.com/images/check.png");
                    }
                }else{
                    thesis.set("TG59", propTG59);
                    thesis.set("checkTG59","http://www.tipinsure.com/images/check.png");
                    if(reportDataTG59 != null && reportDataTG59.length > 0){
                        UploadServices.updateFileUpload(delegator, propTG59, fileNameTG59, contentTypeTG59, reportDataTG59);                
                    }
                }
            }catch(Exception e){
                thesis.set("TG59", propTG59);
                thesis.set("checkTG59", (String)tempThesis.get("checkTG59"));           
            }

            String propTG61 = (String)tempThesis.get("TG61");
            ByteBuffer fileDataTG61 = null;
            String fileNameTG61 = "";
            String contentTypeTG61 = "";
            byte[] reportDataTG61 = null;

            try{
                fileDataTG61 = (ByteBuffer) context.get("TG61");
                fileNameTG61 = (String) context.get("_TG61_fileName");
                contentTypeTG61 = (String) context.get("_TG61_contentType");
                reportDataTG61 = fileDataTG61.array();

                if(propTG61.equals("-")){
                    if(reportDataTG61 == null || reportDataTG61.length == 0){
                        thesis.set("TG61", "-");
                        thesis.set("checkTG61", "http://asap-sol.com/images/icon-minus.png");
                    }else{
                        thesis.set("TG61", UploadServices.createFileUpload(delegator, fileNameTG61, contentTypeTG61, reportDataTG61),false);
                        thesis.set("checkTG61","http://www.tipinsure.com/images/check.png");
                    }
                }else{
                    thesis.set("TG61", propTG61);
                    thesis.set("checkTG61","http://www.tipinsure.com/images/check.png");
                    if(reportDataTG61 != null && reportDataTG61.length > 0){
                        UploadServices.updateFileUpload(delegator, propTG61, fileNameTG61, contentTypeTG61, reportDataTG61);                
                    }
                }
            }catch(Exception e){
                thesis.set("TG61", propTG61);
                thesis.set("checkTG61", (String)tempThesis.get("checkTG61"));
            }
        }else{
            thesis.set("TG04", "-");
            thesis.set("TG54", "-");
            thesis.set("TG59", "-");
            thesis.set("TG61", "-");           
        }

        /*
            =====================
            End Of Thesis Defense
            =====================
        */
        
        /*
	        ==================
	        Thesis Book and CD
	        ==================
         */
	        String propBookLocation = (String)tempThesis.get("bookLocation");
	        ByteBuffer fileDataBookLocation = null;
	        String fileNameBookLocation = "";
	        String contentTypeBookLocation = "";
	        byte[] reportDataBookLocation = null;
	
	        try{
	            fileDataBookLocation = (ByteBuffer) context.get("bookLocation");
	            fileNameBookLocation = (String) context.get("_bookLocation_fileName");
	            contentTypeBookLocation = (String) context.get("_bookLocation_contentType");
	            reportDataBookLocation = fileDataBookLocation.array();
	
	            if(propBookLocation.equals("-")){
	                if(reportDataBookLocation == null || reportDataBookLocation.length == 0){
	                    thesis.set("bookLocation", "-");
	                    thesis.set("book", "Not Submitted");
	                }else{
	                	if(contentTypeBookLocation.equals("application/pdf")){
	                		thesis.set("bookLocation", UploadServices.createFileUpload(delegator, fileNameBookLocation, contentTypeBookLocation, reportDataBookLocation),false);
	                    	thesis.set("book","Submitted");
	                	}else{
	                		return ServiceUtil.returnError(UtilProperties.getMessage(ServiceUtil.resource,"Must be in pdf format", locale));
	                	}
	                }
	            }else{
	                thesis.set("book", "Submitted");
	                thesis.set("bookLocation", propBookLocation);
	                if(reportDataBookLocation != null && reportDataBookLocation.length > 0){
	                    if(contentTypeBookLocation.equals("application/pdf")){
	                    	UploadServices.updateFileUpload(delegator, propBookLocation, fileNameBookLocation, contentTypeBookLocation, reportDataBookLocation);
	                	}else{
	                		return ServiceUtil.returnError(UtilProperties.getMessage(ServiceUtil.resource,"Must be in pdf format", locale));
	                	}
	                }
	            }
	        }catch(Exception e){
	        	thesis.set("bookLocation", propBookLocation);
	            thesis.set("book", (String)tempThesis.get("book"));
	        }
	        
	        String propThesisSupplement = (String)tempThesis.get("thesisSupplement");
	        ByteBuffer fileDataThesisSupplement = null;
	        String fileNameThesisSupplement = "";
	        String contentTypeThesisSupplement = "";
	        byte[] reportDataThesisSupplement = null;
	
	        try{
	        	fileDataThesisSupplement = (ByteBuffer) context.get("thesisSupplement");
	        	fileNameThesisSupplement = (String) context.get("_thesisSupplement_fileName");
	        	contentTypeThesisSupplement = (String) context.get("_thesisSupplement_contentType");
	        	reportDataThesisSupplement = fileDataThesisSupplement.array();
	
	            if(propThesisSupplement.equals("-")){
	                if(reportDataThesisSupplement == null || reportDataThesisSupplement.length == 0){
	                    thesis.set("thesisSupplement", "-");
	                    thesis.set("checkThesisSupplement", "Not Submitted");
	                }else{
	                	if(contentTypeThesisSupplement.equals("application/zip")){
	                		thesis.set("thesisSupplement", UploadServices.createFileUpload(delegator, fileNameThesisSupplement, contentTypeThesisSupplement, reportDataThesisSupplement),false);
	                    	thesis.set("checkThesisSupplement","Submitted");
	                	}else{
	                		return ServiceUtil.returnError(UtilProperties.getMessage(ServiceUtil.resource,"Must be in zip format", locale));
	                	}
	                }
	            }else{
	                thesis.set("checkThesisSupplement", "Submitted");
	                thesis.set("thesisSupplement", propThesisSupplement);
	                if(reportDataThesisSupplement != null && reportDataThesisSupplement.length > 0){
	                    if(contentTypeThesisSupplement.equals("application/zip")){
	                    	UploadServices.updateFileUpload(delegator, propThesisSupplement, fileNameThesisSupplement, contentTypeThesisSupplement, reportDataThesisSupplement);
	                	}else{
	                		return ServiceUtil.returnError(UtilProperties.getMessage(ServiceUtil.resource,"Must be in zip format", locale));
	                	}
	                }
	            }
	        }catch(Exception e){
	        	thesis.set("thesisSupplement", propThesisSupplement);
	            thesis.set("checkThesisSupplement", (String)tempThesis.get("checkThesisSupplement"));
	        }
	        
	        String propThesisPresentation = (String)tempThesis.get("thesisPresentation");
	        ByteBuffer fileDataThesisPresentation = null;
	        String fileNameThesisPresentation = "";
	        String contentTypeThesisPresentation = "";
	        byte[] reportDataThesisPresentation = null;
	
	        try{
	        	fileDataThesisPresentation = (ByteBuffer) context.get("thesisPresentation");
	        	fileNameThesisPresentation = (String) context.get("_thesisPresentation_fileName");
	        	contentTypeThesisPresentation = (String) context.get("_thesisPresentation_contentType");
	        	reportDataThesisPresentation = fileDataThesisPresentation.array();
	
	            if(propThesisPresentation.equals("-")){
	                if(reportDataThesisPresentation == null || reportDataThesisPresentation.length == 0){
	                    thesis.set("thesisPresentation", "-");
	                    thesis.set("checkThesisPresentation", "Not Submitted");
	                }else{
	                	//if(contentTypeThesisPresentation.equals("application/ppt") || contentTypeThesisPresentation.equals("application/pptx")){
	                		thesis.set("thesisPresentation", UploadServices.createFileUpload(delegator, fileNameThesisPresentation, contentTypeThesisPresentation, reportDataThesisPresentation),false);
	                    	thesis.set("checkThesisPresentation","Submitted");
	                	//}else{
	                	//	return ServiceUtil.returnError(UtilProperties.getMessage(ServiceUtil.resource,"Must be in ppt or pptx format", locale));
	                	//}
	                }
	            }else{
	                thesis.set("checkThesisPresentation", "Submitted");
	                thesis.set("thesisPresentation", propThesisPresentation);
	                if(reportDataThesisPresentation != null && reportDataThesisPresentation.length > 0){
	                    //if(contentTypeThesisPresentation.equals("application/ppt") || contentTypeThesisPresentation.equals("application/pptx")){
	                    	UploadServices.updateFileUpload(delegator, propThesisPresentation, fileNameThesisPresentation, contentTypeThesisPresentation, reportDataThesisPresentation);
	                	//}else{
	                	//	return ServiceUtil.returnError(UtilProperties.getMessage(ServiceUtil.resource,"Must be in ppt or pptx format", locale));
	                	//}
	                }
	            }
	        }catch(Exception e){
	        	thesis.set("thesisPresentation", propThesisPresentation);
	            thesis.set("checkThesisPresentation", (String)tempThesis.get("checkThesisPresentation"));
	        }
        
        /*
	        =========================
	        End of Thesis Book and CD
	        =========================
	     */

        /*if(tempThesis.get("location").equals("Thailand")){
            if(!thesis.get("TG01").equals("-")){
                thesis.set("thesisStatus", "THESIS_ON_PROGRESS");
            }

            if(!thesis.get("TG03").equals("-")){
                thesis.set("thesisStatus", "ELIGIBLE_TO_HAVE_THESIS_PROGRESS_EXAMINATION");
            }

            if(!thesis.get("TG04").equals("-")){
                thesis.set("thesisStatus", "ELIGIBLE_TO_HAVE_THESIS_DEFENSE");
            }

            if(tempThesis.get("book").equals("Submitted")){
                thesis.set("thesisStatus", "THESIS_DONE");
            }
        }else{
            if(!thesis.get("TG52").equals("-")){
                thesis.set("thesisStatus", "THESIS_ON_PROGRESS");
            }

            if(tempThesis.get("book").equals("Submitted")){
                thesis.set("thesisStatus", "THESIS_DONE");
            }
        }*/

        thesis.set("judulSkripsi", judulSkripsi);
        thesis.set("studentId", studentId);

        if(statusId!= null){
            thesis.set("statusId", statusId);
        }else{
            thesis.set("statusId", tempThesis.get("statusId"));
        }

        if(location != null){
            thesis.set("location", location);
        }else{
            thesis.set("location", tempThesis.get("location"));
        }

        /*if(book != null){
            thesis.set("book", book);    
        }else{
            thesis.set("book", tempThesis.get("book"));
        }*/
        
        if(grade != null){
            thesis.set("grade", Long.parseLong(grade));
        }else{
            thesis.set("grade", tempThesis.get("grade"));
        }
        
        if(educationLevel != null){
        	thesis.set("educationLevel", educationLevel);
        }else{
        	thesis.set("educationLevel", tempThesis.get("educationLevel"));
        }
        
        if(finalAbstract != null){
        	thesis.set("finalAbstract", finalAbstract);
        }else{
        	thesis.set("finalAbstract", tempThesis.get("finalAbstract"));
        }
        
        if(progressReason != null){
        	thesis.set("progressReason", progressReason);
        }else{
        	thesis.set("progressReason", tempThesis.get("progressReason"));
        }
        
        if(progressResult != null){
        	thesis.set("progressResult", progressResult);
        }else{
        	thesis.set("progressResult", tempThesis.get("progressResult"));
        }
        
        if(proposalReason != null){
        	thesis.set("proposalReason", proposalReason);
        }else{
        	thesis.set("proposalReason", tempThesis.get("proposalReason"));
        }
        
        if(proposalResult != null){
        	thesis.set("proposalResult", proposalResult);
        }else{
        	thesis.set("proposalResult", tempThesis.get("proposalResult"));
        }

        thesis.set("advisor", advisor);
        try{
            thesis.set("declaredDate", declaredDate);
            thesis.set("defenseDate", defenseDate);
        }catch(Exception e){
            thesis.set("declaredDate", tempThesis.get("declaredDate"));
            thesis.set("defenseDate", tempThesis.get("defenseDate"));
        }

        try {
            thesis.store();
        } catch (GenericEntityException e) {
            Debug.logWarning(e.getMessage(), module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    e.getMessage(), new Object[] { e.getMessage() }, locale));
        }

        result.put(ModelService.RESPONSE_MESSAGE, ModelService.RESPOND_SUCCESS);
        result.put(ModelService.SUCCESS_MESSAGE, 
                UtilProperties.getMessage(resourceError, "thesis.update.success", locale));
        return result;
    }
}