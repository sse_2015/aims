/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.ofbiz.aims.student;

import org.ofbiz.aims.upload.UploadWorker;
import org.ofbiz.aims.upload.UploadServices;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.StringWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.Writer;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import javolution.util.FastList;
import javolution.util.FastMap;

import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.FileUtil;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilGenerics;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilProperties;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityConditionList;
import org.ofbiz.entity.condition.EntityExpr;
import org.ofbiz.entity.condition.EntityFunction;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.model.DynamicViewEntity;
import org.ofbiz.entity.model.ModelKeyMap;
import org.ofbiz.entity.util.EntityFindOptions;
import org.ofbiz.entity.util.EntityListIterator;
import org.ofbiz.entity.util.EntityTypeUtil;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.GenericServiceException;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ModelService;
import org.ofbiz.service.ServiceUtil;
import org.ofbiz.security.Security;

/**
 * Services for Internship maintenance
 */
public class InternshipServices {

    public static final String module = InternshipServices.class.getName();
    public static final String resource = "AIMSUiLabels";
    public static final String resourceError = "AIMSErrorUiLabels";

    /**
     * Find the Internship of UserLogin.
     * @param ctx The DispatchContext that this service is operating in.
     * @param context Map containing the input parameters.
     * @return Map with the result of the service, the output parameters.
     */
    public static Map<String, Object> preEditInternship(DispatchContext ctx, Map<String, ? extends Object> context) {
        Map<String, Object> result = FastMap.newInstance();
        Delegator delegator = ctx.getDelegator();
        LocalDispatcher dispatcher = ctx.getDispatcher();
        Locale locale = (Locale) context.get("locale");
        GenericValue userLogin = (GenericValue) context.get("userLogin");
        String partyId = (String) userLogin.get("partyId");

        List<GenericValue> internshipList = null;

        try {
            internshipList = delegator.findByAnd("Internship", UtilMisc.toMap("studentId", partyId), null, false);
        } catch (GenericEntityException e) {
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError,
                    "internshipservices.cannot_get_internship_entities_read",
                    UtilMisc.toMap("errMessage", e.getMessage()), locale));
        }

        if (!internshipList.isEmpty()) {
            GenericValue internship = internshipList.get(0);
            result.put("studentId", internship.get("studentId"));
            result.put("internshipId", internship.get("internshipId"));
            result.put("title", internship.get("title"));
            result.put("fromDate", internship.get("fromDate"));
            result.put("toDate", internship.get("toDate"));
            result.put("location", internship.get("location"));
            result.put("advisor", internship.get("advisor"));
            result.put("report", internship.get("report"));
            result.put("statusId", internship.get("statusId"));
            result.put("comment", internship.get("comment"));
            if (internship.get("report") != null) {
                GenericValue fileUpload = null;

                try {
                    fileUpload = delegator.findOne("FileUpload", UtilMisc.toMap("fileId", (String)internship.get("report")), false);
                } catch (GenericEntityException e) {
                    Debug.logWarning(e, module);
                    return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                        "fileUpload.update.read_failure", new Object[] { e.getMessage() }, locale));
                }

                if (fileUpload == null) {
                    return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                        "fileUpload.update.not_found", locale));
                }

                String link = "file/" + (String) fileUpload.get("name") + "?fileId=" + (String) internship.get("report");

                result.put("downloadLink", link);
                result.put("fileName", (String) fileUpload.get("name"));
            }
        }
        else {            
            result.put("studentId", partyId);
        }
        return result;
    }

    /**
     * Find an Internship.
     * @param ctx The DispatchContext that this service is operating in.
     * @param context Map containing the input parameters.
     * @return Map with the result of the service, the output parameters.
     */
    public static Map<String, Object> preViewInternship(DispatchContext ctx, Map<String, ? extends Object> context) {
        Map<String, Object> result = FastMap.newInstance();
        Delegator delegator = ctx.getDelegator();
        LocalDispatcher dispatcher = ctx.getDispatcher();
        Locale locale = (Locale) context.get("locale");
        GenericValue userLogin = (GenericValue) context.get("userLogin");
        String partyId = (String) userLogin.get("partyId");

        String internshipId = (String) context.get("internshipId");
        if (UtilValidate.isEmpty(internshipId)) {
            return ServiceUtil.returnError(UtilProperties.getMessage(ServiceUtil.resource, 
                    "serviceUtil.internship_id_missing", locale));
        }

        GenericValue internship = null;

        try {
            internship = delegator.findOne("Internship", UtilMisc.toMap("internshipId", internshipId), false);
        } catch (GenericEntityException e) {
            Debug.logWarning(e, module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "internship.update.read_failure", new Object[] { e.getMessage() }, locale));
        }

        if (internship == null) {
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "internship.update.not_found", locale));
        }

        result.put("studentId", internship.get("studentId"));
        result.put("internshipId", internship.get("internshipId"));
        result.put("title", internship.get("title"));
        result.put("fromDate", internship.get("fromDate"));
        result.put("toDate", internship.get("toDate"));
        result.put("location", internship.get("location"));
        result.put("advisor", internship.get("advisor"));
        result.put("report", internship.get("report"));
        if (internship.get("report") != null) {
            GenericValue fileUpload = null;

            try {
                fileUpload = delegator.findOne("FileUpload", UtilMisc.toMap("fileId", (String)internship.get("report")), false);
            } catch (GenericEntityException e) {
                Debug.logWarning(e, module);
                return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "fileUpload.update.read_failure", new Object[] { e.getMessage() }, locale));
            }

            if (fileUpload == null) {
                return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "fileUpload.update.not_found", locale));
            }

            String link = "file/" + (String) fileUpload.get("name") + "?fileId=" + (String) internship.get("report");

            result.put("downloadLink", link);
            result.put("fileName", (String) fileUpload.get("name"));
        }
        return result;
    }

    /**
     * Creates a Internship.
     * @param ctx The DispatchContext that this service is operating in.
     * @param context Map containing the input parameters.
     * @return Map with the result of the service, the output parameters.
     */
    public static Map<String, Object> createInternship(DispatchContext ctx, Map<String, ? extends Object> context) throws GenericServiceException {
        Map<String, Object> result = FastMap.newInstance();
        Delegator delegator = ctx.getDelegator();
        Timestamp now = UtilDateTime.nowTimestamp();
        List<GenericValue> toBeStored = FastList.newInstance();
        Locale locale = (Locale) context.get("locale");
        // in most cases userLogin will be null, but get anyway so we can keep track of that info if it is available
        GenericValue userLogin = (GenericValue) context.get("userLogin");

        String fileName = (String) context.get("_reportFile_fileName");
        String contentType = (String) context.get("_reportFile_contentType");
        ByteBuffer reportDataBuffer = (ByteBuffer) context.get("reportFile");
        byte[] reportData = reportDataBuffer.array();

		String newInternshipId = null;
        try {
            newInternshipId = delegator.getNextSeqId("Internship");
        } catch (IllegalArgumentException e) {
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "internshipservices.could_not_create_internship_id_generation_failure", locale));
        }

		GenericValue internship = delegator.makeValue("Internship", UtilMisc.toMap("internshipId", newInternshipId));

		internship.set("studentId", context.get("studentId"), false);
        internship.set("title", context.get("title"), false);
		internship.set("fromDate", context.get("fromDate"), false);
		internship.set("toDate", context.get("toDate"), false);
		internship.set("location", context.get("location"), false);
		internship.set("advisor", context.get("advisor"), false);
        internship.set("statusId", "INTERNSHIP_CREATED");
        internship.set("report", UploadServices.createFileUpload(delegator, fileName, contentType, reportData), false);


		try {
            if (delegator.findOne(internship.getEntityName(), internship.getPrimaryKey(), false) != null) {
                return ServiceUtil.returnError(UtilProperties.getMessage(resource, 
                        "InternshipAlreadyExists", locale));
            }
        } catch (GenericEntityException e) {
            Debug.logWarning(e, module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resource, 
                    "InternshipReadFailure",
                    UtilMisc.toMap("errorString", e.getMessage()),    locale));
        }

        try {
            internship.create();
        } catch (GenericEntityException e) {
            Debug.logWarning(e.getMessage(), module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resource, 
                    "InternshipWriteFailure",
                    UtilMisc.toMap("errorString", e.getMessage()),    locale));
        }

        result.put("internshipId", newInternshipId);
        result.put(ModelService.RESPONSE_MESSAGE, ModelService.RESPOND_SUCCESS);
        result.put(ModelService.SUCCESS_MESSAGE, 
                UtilProperties.getMessage(resourceError, "internship.create.success_and_wait_for_approving", locale));
        return result;
    }

    /**
     * Creates a Internship Weekly Report.
     * @param ctx The DispatchContext that this service is operating in.
     * @param context Map containing the input parameters.
     * @return Map with the result of the service, the output parameters.
     */
    public static Map<String, Object> createInternshipReport(DispatchContext ctx, Map<String, ? extends Object> context) throws GenericServiceException {
        Map<String, Object> result = FastMap.newInstance();
        Delegator delegator = ctx.getDelegator();
        Timestamp now = UtilDateTime.nowTimestamp();
        List<GenericValue> toBeStored = FastList.newInstance();
        Locale locale = (Locale) context.get("locale");
        // in most cases userLogin will be null, but get anyway so we can keep track of that info if it is available
        GenericValue userLogin = (GenericValue) context.get("userLogin");

        String fileName = (String) context.get("_weeklyReportFile_fileName");
        String contentType = (String) context.get("_weeklyReportFile_contentType");
        ByteBuffer reportDataBuffer = (ByteBuffer) context.get("weeklyReportFile");

        String newInternshipReportId = null;
        try {
            newInternshipReportId = delegator.getNextSeqId("InternshipWeeklyReport");
        } catch (IllegalArgumentException e) {
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "internshipservices.could_not_create_internship_id_generation_failure", locale));
        }

        GenericValue internshipReport = delegator.makeValue("InternshipWeeklyReport", UtilMisc.toMap("reportId", newInternshipReportId));

        internshipReport.set("internshipId", context.get("internshipId"), false);
        internshipReport.set("description", context.get("description"), false);
        internshipReport.set("reportedDate", UtilDateTime.nowTimestamp(), false);

        if (fileName != null && !fileName.isEmpty()) {
            internshipReport.set("report", UploadServices.createFileUpload(delegator, fileName, contentType, reportDataBuffer.array()), false);
        }

        try {
            if (delegator.findOne(internshipReport.getEntityName(), internshipReport.getPrimaryKey(), false) != null) {
                return ServiceUtil.returnError(UtilProperties.getMessage(resource, 
                        "InternshipAlreadyExists", locale));
            }
        } catch (GenericEntityException e) {
            Debug.logWarning(e, module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resource, 
                    "InternshipReadFailure",
                    UtilMisc.toMap("errorString", e.getMessage()),    locale));
        }

        try {
            internshipReport.create();
        } catch (GenericEntityException e) {
            Debug.logWarning(e.getMessage(), module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resource, 
                    "InternshipWriteFailure",
                    UtilMisc.toMap("errorString", e.getMessage()),    locale));
        }

        result.put("internshipId", context.get("internshipId"));
        result.put("description", "");

        result.put(ModelService.RESPONSE_MESSAGE, ModelService.RESPOND_SUCCESS);
        return result;
    }

    /**
     * Updates a Internship.
     * @param ctx The DispatchContext that this service is operating in.
     * @param context Map containing the input parameters.
     * @return Map with the result of the service, the output parameters.
     */
    public static Map<String, Object> updateInternship(DispatchContext ctx, Map<String, ? extends Object> context) throws GenericServiceException {
        Map<String, Object> result = FastMap.newInstance();
        Delegator delegator = ctx.getDelegator();
        LocalDispatcher dispatcher = ctx.getDispatcher();
        Locale locale = (Locale) context.get("locale");

        String internshipId = (String) context.get("internshipId");
        if (UtilValidate.isEmpty(internshipId)) {
            return ServiceUtil.returnError(UtilProperties.getMessage(ServiceUtil.resource, 
                    "serviceUtil.internship_id_missing", locale));
        }

        GenericValue internship = null;

        try {
            internship = delegator.findOne("Internship", UtilMisc.toMap("internshipId", internshipId), false);
        } catch (GenericEntityException e) {
            Debug.logWarning(e, module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "internship.update.read_failure", new Object[] { e.getMessage() }, locale));
        }

        if (internship == null) {
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "internship.update.not_found", locale));
        }

        internship.setNonPKFields(context);

        String fileName = (String) context.get("_reportFile_fileName");
        String contentType = (String) context.get("_reportFile_contentType");
        ByteBuffer reportDataBuffer = (ByteBuffer) context.get("reportFile");

        if (fileName != null && !fileName.isEmpty()) {
            String fileId = (String) internship.get("report");
            if (fileId == null || fileId.isEmpty()) {
                internship.set("report", UploadServices.createFileUpload(delegator, fileName, contentType, reportDataBuffer.array()), false);
            }
            else {
                UploadServices.updateFileUpload(delegator, fileId, fileName, contentType, reportDataBuffer.array());
            }
        }

        try {
            internship.store();
        } catch (GenericEntityException e) {
            Debug.logWarning(e.getMessage(), module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "internship.update.write_failure", new Object[] { e.getMessage() }, locale));
        }

        if (internship.get("report") != null) {
            GenericValue fileUpload = null;

            try {
                fileUpload = delegator.findOne("FileUpload", UtilMisc.toMap("fileId", (String)internship.get("report")), false);
            } catch (GenericEntityException e) {
                Debug.logWarning(e, module);
                return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "fileUpload.update.read_failure", new Object[] { e.getMessage() }, locale));
            }

            if (fileUpload == null) {
                return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "fileUpload.update.not_found", locale));
            }

            String link = "file/" + (String) fileUpload.get("name") + "?fileId=" + (String) internship.get("report");

            result.put("downloadLink", link);
            result.put("fileName", (String) fileUpload.get("name"));
        }

        result.put(ModelService.RESPONSE_MESSAGE, ModelService.RESPOND_SUCCESS);
        result.put(ModelService.SUCCESS_MESSAGE, 
                UtilProperties.getMessage(resourceError, "internship.update.success", locale));
        return result;
    }

    /**
     * Assess a Internship.
     * @param ctx The DispatchContext that this service is operating in.
     * @param context Map containing the input parameters.
     * @return Map with the result of the service, the output parameters.
     */
    public static Map<String, Object> assessInternship(DispatchContext ctx, Map<String, ? extends Object> context) {
        Map<String, Object> result = FastMap.newInstance();
        Delegator delegator = ctx.getDelegator();
        LocalDispatcher dispatcher = ctx.getDispatcher();
        Locale locale = (Locale) context.get("locale");

        String internshipId = (String) context.get("internshipId");
        if (UtilValidate.isEmpty(internshipId)) {
            return ServiceUtil.returnError(UtilProperties.getMessage(ServiceUtil.resource, 
                    "serviceUtil.internship_id_missing", locale));
        }

        GenericValue internship = null;

        try {
            internship = delegator.findOne("Internship", UtilMisc.toMap("internshipId", internshipId), false);
        } catch (GenericEntityException e) {
            Debug.logWarning(e, module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "internship.update.read_failure", new Object[] { e.getMessage() }, locale));
        }

        if (internship == null) {
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "internship.update.not_found", locale));
        }

        try {
            String grade = (String) context.get("grade");
            internship.set("grade", Double.parseDouble(grade));
        } catch (NumberFormatException e) {
            Debug.logWarning(e, module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "internship.update.grade_is_not_a_number", new Object[] { e.getMessage() }, locale));
        }

        try {
            internship.store();
        } catch (GenericEntityException e) {
            Debug.logWarning(e.getMessage(), module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "internship.update.write_failure", new Object[] { e.getMessage() }, locale));
        }

        result.put(ModelService.RESPONSE_MESSAGE, ModelService.RESPOND_SUCCESS);
        result.put(ModelService.SUCCESS_MESSAGE, 
                UtilProperties.getMessage(resourceError, "internship.update.success", locale));
        return result;
    }

    /**
     * Approve a Internship.
     * @param ctx The DispatchContext that this service is operating in.
     * @param context Map containing the input parameters.
     * @return Map with the result of the service, the output parameters.
     */
    public static Map<String, Object> approveInternship(DispatchContext ctx, Map<String, ? extends Object> context) {
        Map<String, Object> result = FastMap.newInstance();
        Delegator delegator = ctx.getDelegator();
        LocalDispatcher dispatcher = ctx.getDispatcher();
        Locale locale = (Locale) context.get("locale");

        String internshipId = (String) context.get("internshipId");
        if (UtilValidate.isEmpty(internshipId)) {
            return ServiceUtil.returnError(UtilProperties.getMessage(ServiceUtil.resource, 
                    "serviceUtil.internship_id_missing", locale));
        }

        GenericValue internship = null;

        try {
            internship = delegator.findOne("Internship", UtilMisc.toMap("internshipId", internshipId), false);
        } catch (GenericEntityException e) {
            Debug.logWarning(e, module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "internship.update.read_failure", new Object[] { e.getMessage() }, locale));
        }

        if (internship == null) {
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "internship.update.not_found", locale));
        }

        String statusId = (String) context.get("statusId");
        internship.set("statusId", "INTERNSHIP_APPROVED");
        internship.set("comment", (String) context.get("comment"));

        try {
            internship.store();
        } catch (GenericEntityException e) {
            Debug.logWarning(e.getMessage(), module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "internship.update.write_failure", new Object[] { e.getMessage() }, locale));
        }

        result.put(ModelService.RESPONSE_MESSAGE, ModelService.RESPOND_SUCCESS);
        result.put(ModelService.SUCCESS_MESSAGE, 
                UtilProperties.getMessage(resourceError, "internship.update.success", locale));
        return result;
    }

    /**
     * Updates a Internship Weekly Report.
     * @param ctx The DispatchContext that this service is operating in.
     * @param context Map containing the input parameters.
     * @return Map with the result of the service, the output parameters.
     */
    public static Map<String, Object> updateInternshipReport(DispatchContext ctx, Map<String, ? extends Object> context) throws GenericServiceException {
        Map<String, Object> result = FastMap.newInstance();
        Delegator delegator = ctx.getDelegator();
        LocalDispatcher dispatcher = ctx.getDispatcher();
        Locale locale = (Locale) context.get("locale");

        String reportId = (String) context.get("reportId");
        if (UtilValidate.isEmpty(reportId)) {
            return ServiceUtil.returnError(UtilProperties.getMessage(ServiceUtil.resource, 
                    "serviceUtil.report_id_missing", locale));
        }

        GenericValue internshipReport = null;

        try {
            internshipReport = delegator.findOne("InternshipWeeklyReport", UtilMisc.toMap("reportId", reportId), false);
        } catch (GenericEntityException e) {
            Debug.logWarning(e, module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "internshipWeeklyReport.update.read_failure", new Object[] { e.getMessage() }, locale));
        }

        if (internshipReport == null) {
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "internshipWeeklyReport.update.not_found", locale));
        }

        internshipReport.setNonPKFields(context);
        internshipReport.set("reportedDate", UtilDateTime.nowTimestamp(), false);

        String fileName = (String) context.get("_weeklyReportFile_fileName");
        String contentType = (String) context.get("_weeklyReportFile_contentType");
        ByteBuffer reportDataBuffer = (ByteBuffer) context.get("weeklyReportFile");

        if (fileName != null && !fileName.isEmpty()) {
            String fileId = (String) internshipReport.get("report");
            if (fileId == null || fileId.isEmpty()) {
                internshipReport.set("report", UploadServices.createFileUpload(delegator, fileName, contentType, reportDataBuffer.array()), false);
            }
            else {
                UploadServices.updateFileUpload(delegator, fileId, fileName, contentType, reportDataBuffer.array());
            }
        }

        try {
            internshipReport.store();
        } catch (GenericEntityException e) {
            Debug.logWarning(e.getMessage(), module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "internshipWeeklyReport.update.write_failure", new Object[] { e.getMessage() }, locale));
        }

        result.put("description", "");
        result.put(ModelService.RESPONSE_MESSAGE, ModelService.RESPOND_SUCCESS);
        result.put(ModelService.SUCCESS_MESSAGE, 
                UtilProperties.getMessage(resourceError, "internshipWeeklyReport.update.success", locale));
        return result;
    }

    /**
     * Find an InternshipReport.
     * @param ctx The DispatchContext that this service is operating in.
     * @param context Map containing the input parameters.
     * @return Map with the result of the service, the output parameters.
     */
    public static Map<String, Object> preUpdateInternshipReport(DispatchContext ctx, Map<String, ? extends Object> context) {
        Map<String, Object> result = FastMap.newInstance();
        Delegator delegator = ctx.getDelegator();
        LocalDispatcher dispatcher = ctx.getDispatcher();
        Locale locale = (Locale) context.get("locale");
        GenericValue userLogin = (GenericValue) context.get("userLogin");

        String reportId = (String) context.get("reportId");
        if (UtilValidate.isEmpty(reportId)) {
            return ServiceUtil.returnError(UtilProperties.getMessage(ServiceUtil.resource, 
                    "serviceUtil.internshipReport_id_missing", locale));
        }

        GenericValue internshipReport = null;

        try {
            internshipReport = delegator.findOne("InternshipWeeklyReport", UtilMisc.toMap("reportId", reportId), false);
        } catch (GenericEntityException e) {
            Debug.logWarning(e, module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "internshipWeeklyReport.update.read_failure", new Object[] { e.getMessage() }, locale));
        }

        if (internshipReport == null) {
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "internshipWeeklyReport.update.not_found", locale));
        }

        result.put("reportId", internshipReport.get("reportId"));
        result.put("internshipId", internshipReport.get("internshipId"));
        result.put("description", internshipReport.get("description"));
        result.put("downloadLink", context.get("downloadLink"));
        result.put("fileName", context.get("fileName"));
        if (internshipReport.get("report") != null) {
            GenericValue fileUpload = null;

            try {
                fileUpload = delegator.findOne("FileUpload", UtilMisc.toMap("fileId", (String)internshipReport.get("report")), false);
            } catch (GenericEntityException e) {
                Debug.logWarning(e, module);
                return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "fileUpload.update.read_failure", new Object[] { e.getMessage() }, locale));
            }

            if (fileUpload == null) {
                return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "fileUpload.update.not_found", locale));
            }

            String link = "file/" + (String) fileUpload.get("name") + "?fileId=" + (String) internshipReport.get("report");

            result.put("downloadLinkWeekly", link);
            result.put("fileNameWeekly", (String) fileUpload.get("name"));
        }
        return result;
    }

    public static Map<String, Object> findInternship(DispatchContext dctx, Map<String, ? extends Object> context) {
        Map<String, Object> result = ServiceUtil.returnSuccess();
        Delegator delegator = dctx.getDelegator();
        GenericValue userLogin = (GenericValue) context.get("userLogin");
        Locale locale = (Locale) context.get("locale");
        
        // set the page parameters
        int viewIndex = 0;
        try {
            viewIndex = Integer.parseInt((String) context.get("VIEW_INDEX"));
        } catch (Exception e) {
            viewIndex = 0;
        }
        result.put("viewIndex", Integer.valueOf(viewIndex));

        int viewSize = 20;
        try {
            viewSize = Integer.parseInt((String) context.get("VIEW_SIZE"));
        } catch (Exception e) {
            viewSize = 20;
        }
        result.put("viewSize", Integer.valueOf(viewSize));

        // get the lookup flag
        String lookupFlag = (String) context.get("lookupFlag");

		Debug.logInfo("In findInternship lookupFlag = " + lookupFlag, module);

        // blank param list
        String paramList = "";

        List<GenericValue> internshipList = null;
        int internshipListSize = 0;
        int lowIndex = 0;
        int highIndex = 0;

        if ("Y".equals(lookupFlag)) {
            String showAll = (context.get("showAll") != null ? (String) context.get("showAll") : "N");
            paramList = paramList + "&lookupFlag=" + lookupFlag + "&showAll=" + showAll;

            // create the dynamic view entity
            DynamicViewEntity dynamicView = new DynamicViewEntity();

            // default view settings
            dynamicView.addMemberEntity("PU", "Internship");
            dynamicView.addAlias("PU", "internshipId");
            dynamicView.addAlias("PU", "title");
            dynamicView.addAlias("PU", "fromDate");
            dynamicView.addAlias("PU", "toDate");
            dynamicView.addAlias("PU", "location");
            //dynamicView.addRelation("one-nofk", "", "PartyType", ModelKeyMap.makeKeyMapList("partyTypeId"));
            //dynamicView.addRelation("many", "", "UserLogin", ModelKeyMap.makeKeyMapList("partyId"));

            // define the main condition & expression list
            List<EntityCondition> andExprs = FastList.newInstance();
            EntityCondition mainCond = null;

            List<String> orderBy = FastList.newInstance();
            List<String> fieldsToSelect = FastList.newInstance();
            // fields we need to select; will be used to set distinct
            fieldsToSelect.add("internshipId");
            fieldsToSelect.add("title");
            fieldsToSelect.add("fromDate");
            fieldsToSelect.add("toDate");
            fieldsToSelect.add("location");

            // get the params
            String title = (String) context.get("title");

            if (!"Y".equals(showAll)) {
                // check for a title
                if (UtilValidate.isNotEmpty(title)) {
                    paramList = paramList + "&title=" + title;
                    andExprs.add(EntityCondition.makeCondition(EntityFunction.UPPER_FIELD("title"), EntityOperator.LIKE, EntityFunction.UPPER("%"+title+"%")));
                }
                // ---- End of Dynamic View Creation

                // build the main condition
                if (andExprs.size() > 0) mainCond = EntityCondition.makeCondition(andExprs, EntityOperator.AND);
            }

            Debug.logInfo("In findInternship mainCond=" + mainCond, module);

            String sortField = (String) context.get("sortField");
            if(UtilValidate.isNotEmpty(sortField)){
                orderBy.add(sortField);
            }
            
            // do the lookup
            if (mainCond != null || "Y".equals(showAll)) {
                try {
                    // get the indexes for the partial list
                    lowIndex = viewIndex * viewSize + 1;
                    highIndex = (viewIndex + 1) * viewSize;

                    // set distinct on so we only get one row per order
                    EntityFindOptions findOpts = new EntityFindOptions(true, EntityFindOptions.TYPE_SCROLL_INSENSITIVE, EntityFindOptions.CONCUR_READ_ONLY, -1, highIndex, true);
                    // using list iterator
                    EntityListIterator pli = delegator.findListIteratorByCondition(dynamicView, mainCond, null, fieldsToSelect, orderBy, findOpts);

                    // get the partial list for this page
                    internshipList = pli.getPartialList(lowIndex, viewSize);

                    // attempt to get the full size
                    internshipListSize = pli.getResultsSizeAfterPartialList();
                    if (highIndex > internshipListSize) {
                        highIndex = internshipListSize;
                    }

                    // close the list iterator
                    pli.close();
                } catch (GenericEntityException e) {
                    String errMsg = "Failure in internship find operation, rolling back transaction: " + e.toString();
                    Debug.logError(e, errMsg, module);
                    return ServiceUtil.returnError(UtilProperties.getMessage(resource,
                            "AIMSLookupInternshipError",
                            UtilMisc.toMap("errMessage", e.toString()), locale));
                }
            } else {
                internshipListSize = 0;
            }
        }

        if (internshipList == null) internshipList = FastList.newInstance();
        result.put("internshipList", internshipList);
        result.put("internshipListSize", Integer.valueOf(internshipListSize));
        result.put("paramList", paramList);
        result.put("highIndex", Integer.valueOf(highIndex));
        result.put("lowIndex", Integer.valueOf(lowIndex));

        return result;
    }

    /**
     * Search Internship
     * @param ctx The DispatchContext that this service is operating in.
     * @param context Map containing the input parameters.
     * @return Map with the result of the service, the output parameters.
     */
    public static Map<String, Object> searchInternship(DispatchContext ctx, Map<String, ? extends Object> context) {
        Map<String, Object> result = FastMap.newInstance();
        Delegator delegator = ctx.getDelegator();
        Security security = ctx.getSecurity();
        LocalDispatcher dispatcher = ctx.getDispatcher();
        Locale locale = (Locale) context.get("locale");
        GenericValue userLogin = (GenericValue) context.get("userLogin");
        String partyId = (String) userLogin.get("partyId");

        String studentId = (String) context.get("studentId");
        String title = (String) context.get("title");
        
        // set the page parameters
        int viewIndex = 0;
        try {
            viewIndex = Integer.parseInt((String) context.get("VIEW_INDEX"));
        } catch (Exception e) {
            viewIndex = 0;
        }
        result.put("viewIndex", Integer.valueOf(viewIndex));

        int viewSize = 20;
        try {
            viewSize = Integer.parseInt((String) context.get("VIEW_SIZE"));
        } catch (Exception e) {
            viewSize = 20;
        }
        result.put("viewSize", Integer.valueOf(viewSize));

        // get the lookup flag
        String lookupFlag = (String) context.get("lookupFlag");

        Debug.logInfo("In findInternship lookupFlag = " + lookupFlag, module);

        // blank param list
        String paramList = "";

        List<GenericValue> internshipList = null;
        int internshipListSize = 0;
        int lowIndex = 0;
        int highIndex = 0;

        try {
            if (security.hasPermission("INTERNSHIP_VIEW", userLogin)) {
                internshipList = delegator.findByAnd("Internship", UtilMisc.toMap("studentId", studentId, "title", title), null, false);
            }
            else {
                internshipList = delegator.findByAnd("Internship", UtilMisc.toMap("studentId", studentId, "title", title, "advisor", partyId), null, false);
            }

        } catch (GenericEntityException e) {
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError,
                    "internshipservices.cannot_get_internship_entities_read",
                    UtilMisc.toMap("errMessage", e.getMessage()), locale));
        }

        if (internshipList == null)
            internshipList = FastList.newInstance();
        result.put("internshipList", internshipList);
        result.put("internshipListSize", Integer.valueOf(internshipListSize));
        result.put("paramList", paramList);
        result.put("highIndex", Integer.valueOf(highIndex));
        result.put("lowIndex", Integer.valueOf(lowIndex));

        return result;
    }

    /**
     * Approve Internship Application.
     * @param ctx The DispatchContext that this service is operating in.
     * @param context Map containing the input parameters.
     * @return Map with the result of the service, the output parameters.
     */
    public static Map<String, Object> approveApplication(DispatchContext ctx, Map<String, ? extends Object> context) throws GenericServiceException {
        Map<String, Object> result = FastMap.newInstance();
        Delegator delegator = ctx.getDelegator();
        LocalDispatcher dispatcher = ctx.getDispatcher();
        Locale locale = (Locale) context.get("locale");

        String applicationId = (String) context.get("applicationId");
        if (UtilValidate.isEmpty(applicationId)) {
            return ServiceUtil.returnError(UtilProperties.getMessage(ServiceUtil.resource, 
                    "serviceUtil.application_id_missing", locale));
        }

        GenericValue application = null;

        try {
            application = delegator.findOne("InternshipApplication", UtilMisc.toMap("applicationId", applicationId), false);
        } catch (GenericEntityException e) {
            Debug.logWarning(e, module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "application.approve.read_failure", new Object[] { e.getMessage() }, locale));
        }

        if (application == null) {
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "application.approve.not_found", locale));
        }

		String statusId = (String) context.get("statusId");
        application.set("statusId", statusId);
        application.set("comment", (String) context.get("comment"));

		if ("INTERNSHIP_APPROVED".equals(statusId)) {
            String deliveryLetterId = null;
            try {
                deliveryLetterId = delegator.getNextSeqId("InternshipDeliveryLetter");
            } catch (IllegalArgumentException e) {
                return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                        "InternshipDeliveryLetter.could_not_create_id_generation_failure", locale));
            }

            GenericValue deliveryLetter = delegator.makeValue("InternshipDeliveryLetter", UtilMisc.toMap("deliveryLetterId", deliveryLetterId));

    		deliveryLetter.set("studentId", application.get("studentId"), false);
    		deliveryLetter.set("fromDate", application.get("fromDate"), false);
    		deliveryLetter.set("toDate", application.get("toDate"), false);
    		deliveryLetter.set("company", application.get("company"), false);
    		deliveryLetter.set("advisor", application.get("advisor"), false);
            deliveryLetter.set("statusId", "INTERNSHIP_CREATED");


    		try {
                if (delegator.findOne(deliveryLetter.getEntityName(), deliveryLetter.getPrimaryKey(), false) != null) {
                    return ServiceUtil.returnError(UtilProperties.getMessage(resource, 
                            "DeliveryLetterAlreadyExists", locale));
                }
            } catch (GenericEntityException e) {
                Debug.logWarning(e, module);
                return ServiceUtil.returnError(UtilProperties.getMessage(resource, 
                        "DeliveryLetterReadFailure",
                        UtilMisc.toMap("errorString", e.getMessage()),    locale));
            }

            try {
                deliveryLetter.create();
            } catch (GenericEntityException e) {
                Debug.logWarning(e.getMessage(), module);
                return ServiceUtil.returnError(UtilProperties.getMessage(resource, 
                        "InternshipDeliveryLetterWriteFailure",
                        UtilMisc.toMap("errorString", e.getMessage()),    locale));
            }
        }

        try {
            application.store();
        } catch (GenericEntityException e) {
            Debug.logWarning(e.getMessage(), module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "application.approve.write_failure", new Object[] { e.getMessage() }, locale));
        }
        return result;
    }

    /**
     * Approve Internship Delivery Letter.
     * @param ctx The DispatchContext that this service is operating in.
     * @param context Map containing the input parameters.
     * @return Map with the result of the service, the output parameters.
     */
    public static Map<String, Object> approveDeliveryLetter(DispatchContext ctx, Map<String, ? extends Object> context) throws GenericServiceException {
        Map<String, Object> result = FastMap.newInstance();
        Delegator delegator = ctx.getDelegator();
        LocalDispatcher dispatcher = ctx.getDispatcher();
        Locale locale = (Locale) context.get("locale");

        String deliveryLetterId = (String) context.get("deliveryLetterId");
        if (UtilValidate.isEmpty(deliveryLetterId)) {
            return ServiceUtil.returnError(UtilProperties.getMessage(ServiceUtil.resource, 
                    "serviceUtil.deliveryLetterId_id_missing", locale));
        }

        GenericValue deliveryLetter = null;

        try {
            deliveryLetter = delegator.findOne("InternshipDeliveryLetter", UtilMisc.toMap("deliveryLetterId", deliveryLetterId), false);
        } catch (GenericEntityException e) {
            Debug.logWarning(e, module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "deliveryLetterId.approve.read_failure", new Object[] { e.getMessage() }, locale));
        }

        if (deliveryLetter == null) {
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "deliveryLetter.approve.not_found", locale));
        }

		String statusId = (String) context.get("statusId");
        deliveryLetter.set("statusId", statusId);
        deliveryLetter.set("comment", (String) context.get("comment"));

		if ("INTERNSHIP_APPROVED".equals(statusId)) {
            String submissionId = null;
            try {
                submissionId = delegator.getNextSeqId("InternshipSubmission");
            } catch (IllegalArgumentException e) {
                return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                        "InternshipSubmission.could_not_create_id_generation_failure", locale));
            }

            GenericValue submission = delegator.makeValue("InternshipSubmission", UtilMisc.toMap("submissionId", submissionId));

    		submission.set("studentId", deliveryLetter.get("studentId"), false);
            submission.set("statusId", "INTERNSHIP_CREATED");


    		try {
                if (delegator.findOne(submission.getEntityName(), submission.getPrimaryKey(), false) != null) {
                    return ServiceUtil.returnError(UtilProperties.getMessage(resource, 
                            "SubmissionAlreadyExists", locale));
                }
            } catch (GenericEntityException e) {
                Debug.logWarning(e, module);
                return ServiceUtil.returnError(UtilProperties.getMessage(resource, 
                        "SubmissionReadFailure",
                        UtilMisc.toMap("errorString", e.getMessage()),    locale));
            }

            try {
                submission.create();
            } catch (GenericEntityException e) {
                Debug.logWarning(e.getMessage(), module);
                return ServiceUtil.returnError(UtilProperties.getMessage(resource, 
                        "InternshipSubmissionWriteFailure",
                        UtilMisc.toMap("errorString", e.getMessage()), locale));
            }
        }

        try {
            deliveryLetter.store();
        } catch (GenericEntityException e) {
            Debug.logWarning(e.getMessage(), module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "deliveryLetter.approve.write_failure", new Object[] { e.getMessage() }, locale));
        }
        return result;
    }

    /**
     * Submit a Report.
     * @param ctx The DispatchContext that this service is operating in.
     * @param context Map containing the input parameters.
     * @return Map with the result of the service, the output parameters.
     */
    public static Map<String, Object> submitReport(DispatchContext ctx, Map<String, ? extends Object> context) throws GenericServiceException {
        Map<String, Object> result = FastMap.newInstance();
        Delegator delegator = ctx.getDelegator();
        Timestamp now = UtilDateTime.nowTimestamp();
        List<GenericValue> toBeStored = FastList.newInstance();
        Locale locale = (Locale) context.get("locale");
        // in most cases userLogin will be null, but get anyway so we can keep track of that info if it is available
        GenericValue userLogin = (GenericValue) context.get("userLogin");

        String fileName = (String) context.get("_reportFile_fileName");
        String contentType = (String) context.get("_reportFile_contentType");
        ByteBuffer reportDataBuffer = (ByteBuffer) context.get("reportFile");

        String fileId = null;
        try {
            fileId = delegator.getNextSeqId("InternshipSubmissionFile");
        } catch (IllegalArgumentException e) {
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "internshipservices.could_not_create_submission_file_id_generation_failure", locale));
        }

        GenericValue submissionFile = delegator.makeValue("InternshipSubmissionFile", UtilMisc.toMap("fileId", fileId));

        submissionFile.set("submissionId", context.get("submissionId"), false);
        submissionFile.set("description", context.get("description"), false);

        if (fileName != null && !fileName.isEmpty()) {
			String filePath = UploadWorker.getUploadPath() + "/" + fileName;
            File file = FileUtil.getFile(filePath);

            try {
                FileOutputStream out = new FileOutputStream(file);
                out.write(reportDataBuffer.array());
                if (Debug.infoOn()) {
                    Debug.logInfo("in createBinaryFileMethod, length:" + file.length(), module);
                }
                out.close();
            } catch (IOException e) {
                Debug.logWarning(e, module);
                throw new GenericServiceException(e.getMessage());
            }
			submissionFile.set("name", fileName, false);
		    submissionFile.set("mimeType", contentType, false);
			submissionFile.set("location", filePath, false);
        }

        try {
            if (delegator.findOne(submissionFile.getEntityName(), submissionFile.getPrimaryKey(), false) != null) {
                return ServiceUtil.returnError(UtilProperties.getMessage(resource, 
                        "InternshipSubmissionFileAlreadyExists", locale));
            }
        } catch (GenericEntityException e) {
            Debug.logWarning(e, module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resource, 
                    "InternshipSubmissionFileReadFailure",
                    UtilMisc.toMap("errorString", e.getMessage()),    locale));
        }

        try {
            submissionFile.create();
        } catch (GenericEntityException e) {
            Debug.logWarning(e.getMessage(), module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resource, 
                    "InternshipWriteFailure",
                    UtilMisc.toMap("errorString", e.getMessage()),    locale));
        }

        result.put(ModelService.RESPONSE_MESSAGE, ModelService.RESPOND_SUCCESS);
        return result;
    }
}
