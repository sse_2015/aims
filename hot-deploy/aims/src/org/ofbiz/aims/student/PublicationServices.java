/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.ofbiz.aims.student;

import org.ofbiz.aims.upload.UploadWorker;
import org.ofbiz.aims.upload.UploadServices;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.StringWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.Writer;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import javolution.util.FastList;
import javolution.util.FastMap;

import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.FileUtil;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilGenerics;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilProperties;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityConditionList;
import org.ofbiz.entity.condition.EntityExpr;
import org.ofbiz.entity.condition.EntityFunction;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.model.DynamicViewEntity;
import org.ofbiz.entity.model.ModelKeyMap;
import org.ofbiz.entity.util.EntityFindOptions;
import org.ofbiz.entity.util.EntityListIterator;
import org.ofbiz.entity.util.EntityTypeUtil;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.GenericServiceException;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ModelService;
import org.ofbiz.service.ServiceUtil;

/**
 * Services for Publication maintenance
 */
public class PublicationServices {

    public static final String module = PublicationServices.class.getName();
    public static final String resource = "AIMSUiLabels";
    public static final String resourceError = "AIMSErrorUiLabels";

    /**
     * Creates a Publication.
     * @param ctx The DispatchContext that this service is operating in.
     * @param context Map containing the input parameters.
     * @return Map with the result of the service, the output parameters.
     */
    public static Map<String, Object> createPublication(DispatchContext ctx, Map<String, ? extends Object> context) throws GenericServiceException {
        Map<String, Object> result = FastMap.newInstance();
        Delegator delegator = ctx.getDelegator();
        Timestamp now = UtilDateTime.nowTimestamp();
        List<GenericValue> toBeStored = FastList.newInstance();
        Locale locale = (Locale) context.get("locale");

		String newPublicationId = null;
        try {
            newPublicationId = delegator.getNextSeqId("Publication");
        } catch (IllegalArgumentException e) {
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "publicationservices.could_not_create_publication_id_generation_failure", locale));
        }

        GenericValue publication = delegator.makeValue("Publication", UtilMisc.toMap("publicationId", newPublicationId));

        publication.set("studentId", context.get("studentId"), false);
        publication.set("title", context.get("title"), false);
		publication.set("conference", context.get("conference"), false);
		publication.set("date", context.get("date"), false);
		publication.set("location", context.get("location"), false);
        publication.set("statusId", "PUBLICATION_CREATED");

        String fileName = (String) context.get("_imageData_fileName");
        String contentType = (String) context.get("_imageData_contentType");

      	ByteBuffer imageDataBuffer = (ByteBuffer) context.get("imageData");

		byte[] imageData = imageDataBuffer.array();
        
        if (imageData != null && imageData.length > 0) {
            String picture = "/images/" + System.currentTimeMillis() + "_" + fileName;
            String picturePath = System.getProperty("ofbiz.home") + "/framework/images/webapp" + picture;

            File file = FileUtil.getFile(picturePath);
            try {
                FileOutputStream out = new FileOutputStream(file);
                out.write(imageData);
                if (Debug.infoOn()) {
                    Debug.logInfo("in createBinaryFileMethod, length:" + file.length(), module);
                }
                out.close();
            } catch (IOException e) {
                Debug.logWarning(e, module);
                throw new GenericServiceException(e.getMessage());
            }

            publication.set("picture", picture, false);
        }

		String articleName = (String) context.get("_articleFile_fileName");
        String articleContentType = (String) context.get("_articleFile_contentType");
        ByteBuffer articleDataBuffer = (ByteBuffer) context.get("articleFile");
		publication.set("article", UploadServices.createFileUpload(delegator, articleName, articleContentType, articleDataBuffer.array()), false);

		try {
            if (delegator.findOne(publication.getEntityName(), publication.getPrimaryKey(), false) != null) {
                return ServiceUtil.returnError(UtilProperties.getMessage(resource, 
                        "PublicationAlreadyExists", locale));
            }
        } catch (GenericEntityException e) {
            Debug.logWarning(e, module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resource, 
                    "PublicationReadFailure",
                    UtilMisc.toMap("errorString", e.getMessage()),    locale));
        }

        try {
            publication.create();
        } catch (GenericEntityException e) {
            Debug.logWarning(e.getMessage(), module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resource, 
                    "PublicationWriteFailure",
                    UtilMisc.toMap("errorString", e.getMessage()),    locale));
        }

        result.put(ModelService.RESPONSE_MESSAGE, ModelService.RESPOND_SUCCESS);
        return result;
    }

    /**
     * Prepare for Updates a Publication.
     * @param ctx The DispatchContext that this service is operating in.
     * @param context Map containing the input parameters.
     * @return Map with the result of the service, the output parameters.
     */
    public static Map<String, Object> preUpdatePublication(DispatchContext ctx, Map<String, ? extends Object> context) {
        Map<String, Object> result = FastMap.newInstance();
        Delegator delegator = ctx.getDelegator();
        LocalDispatcher dispatcher = ctx.getDispatcher();
        Locale locale = (Locale) context.get("locale");

        String publicationId = (String) context.get("publicationId");
        if (UtilValidate.isEmpty(publicationId)) {
            return ServiceUtil.returnError(UtilProperties.getMessage(ServiceUtil.resource, 
                    "serviceUtil.publication_id_missing", locale));
        }

        GenericValue publication = null;

        try {
            publication = delegator.findOne("Publication", UtilMisc.toMap("publicationId", publicationId), false);
        } catch (GenericEntityException e) {
            Debug.logWarning(e, module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "publication.update.read_failure", new Object[] { e.getMessage() }, locale));
        }

        if (publication == null) {
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "publication.update.not_found", locale));
        }

        result.put("publicationId", publication.get("publicationId"));
        result.put("studentId", publication.get("studentId"));
        result.put("title", publication.get("title"));
        result.put("conference", publication.get("conference"));
        result.put("date", publication.get("date"));
        result.put("location", publication.get("location"));
        result.put("picture", publication.get("picture"));
        result.put("statusId", publication.get("statusId"));
        if (publication.get("article") != null) {
            GenericValue fileUpload = null;

            try {
                fileUpload = delegator.findOne("FileUpload", UtilMisc.toMap("fileId", (String)publication.get("article")), false);
            } catch (GenericEntityException e) {
                Debug.logWarning(e, module);
                return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "fileUpload.update.read_failure", new Object[] { e.getMessage() }, locale));
            }

            if (fileUpload == null) {
                return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "fileUpload.update.not_found", locale));
            }

            String link = "file/" + (String) fileUpload.get("name") + "?fileId=" + (String) publication.get("article");

			Debug.logInfo("BaLX: " + link, module);

            result.put("downloadLink", link);
            result.put("fileName", (String) fileUpload.get("name"));
        }
        return result;
    }

    /**
     * Updates a Publication.
     * @param ctx The DispatchContext that this service is operating in.
     * @param context Map containing the input parameters.
     * @return Map with the result of the service, the output parameters.
     */
    public static Map<String, Object> updatePublication(DispatchContext ctx, Map<String, ? extends Object> context) throws GenericServiceException {
        Map<String, Object> result = FastMap.newInstance();
        Delegator delegator = ctx.getDelegator();
        LocalDispatcher dispatcher = ctx.getDispatcher();
        Locale locale = (Locale) context.get("locale");

        String publicationId = (String) context.get("publicationId");
        if (UtilValidate.isEmpty(publicationId)) {
            return ServiceUtil.returnError(UtilProperties.getMessage(ServiceUtil.resource, 
                    "serviceUtil.publication_id_missing", locale));
        }

        GenericValue publication = null;

        try {
            publication = delegator.findOne("Publication", UtilMisc.toMap("publicationId", publicationId), false);
        } catch (GenericEntityException e) {
            Debug.logWarning(e, module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "publication.update.read_failure", new Object[] { e.getMessage() }, locale));
        }

        if (publication == null) {
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "publication.update.not_found", locale));
        }

        publication.setNonPKFields(context);

        String fileName = (String) context.get("_imageData_fileName");
        String contentType = (String) context.get("_imageData_contentType");

        ByteBuffer imageDataBuffer = (ByteBuffer) context.get("imageData");

        byte[] imageData = imageDataBuffer.array();

        if (imageData != null && imageData.length > 0) {
            String picture = "/images/" + System.currentTimeMillis() + "_" + fileName;
            String picturePath = System.getProperty("ofbiz.home") + "/framework/images/webapp" + picture;
            
            File file = FileUtil.getFile(picturePath);
            try {
                FileOutputStream out = new FileOutputStream(file);
                out.write(imageData);
                if (Debug.infoOn()) {
                    Debug.logInfo("in createBinaryFileMethod, length:" + file.length(), module);
                }
                out.close();
            } catch (IOException e) {
                Debug.logWarning(e, module);
                throw new GenericServiceException(e.getMessage());
            }

            publication.set("picture", picture, false);
        }

		String articleFileName = (String) context.get("_articleFile_fileName");
        String articleContentType = (String) context.get("_articleFile_contentType");
        ByteBuffer articleDataBuffer = (ByteBuffer) context.get("articleFile");

        if (articleFileName != null && !articleFileName.isEmpty()) {
            String fileId = (String) publication.get("article");
            if (fileId == null || fileId.isEmpty()) {
                publication.set("article", UploadServices.createFileUpload(delegator, articleFileName, articleContentType, articleDataBuffer.array()), false);
            }
            else {
                UploadServices.updateFileUpload(delegator, fileId, articleFileName, articleContentType, articleDataBuffer.array());
            }
        }

        try {
            publication.store();
        } catch (GenericEntityException e) {
            Debug.logWarning(e.getMessage(), module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "publication.update.write_failure", new Object[] { e.getMessage() }, locale));
        }


        if (publication.get("article") != null) {
            GenericValue fileUpload = null;

            try {
                fileUpload = delegator.findOne("FileUpload", UtilMisc.toMap("fileId", (String)publication.get("article")), false);
            } catch (GenericEntityException e) {
                Debug.logWarning(e, module);
                return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "fileUpload.update.read_failure", new Object[] { e.getMessage() }, locale));
            }

            if (fileUpload == null) {
                return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "fileUpload.update.not_found", locale));
            }

            String link = "file/" + (String) fileUpload.get("name") + "?fileId=" + (String) publication.get("article");

			Debug.logInfo("BaLX: " + link, module);

            result.put("downloadLink", link);
            result.put("fileName", (String) fileUpload.get("name"));
        }
        
        result.put(ModelService.RESPONSE_MESSAGE, ModelService.RESPOND_SUCCESS);
        result.put(ModelService.SUCCESS_MESSAGE, 
                UtilProperties.getMessage(resourceError, "publication.update.success", locale));
        return result;
    }

    /**
     * Approves a Publication.
     * @param ctx The DispatchContext that this service is operating in.
     * @param context Map containing the input parameters.
     * @return Map with the result of the service, the output parameters.
     */
    public static Map<String, Object> approvePublication(DispatchContext ctx, Map<String, ? extends Object> context) throws GenericServiceException {
        Map<String, Object> result = FastMap.newInstance();
        Delegator delegator = ctx.getDelegator();
        LocalDispatcher dispatcher = ctx.getDispatcher();
        Locale locale = (Locale) context.get("locale");

        String publicationId = (String) context.get("publicationId");
        if (UtilValidate.isEmpty(publicationId)) {
            return ServiceUtil.returnError(UtilProperties.getMessage(ServiceUtil.resource, 
                    "serviceUtil.publication_id_missing", locale));
        }

        GenericValue publication = null;

        try {
            publication = delegator.findOne("Publication", UtilMisc.toMap("publicationId", publicationId), false);
        } catch (GenericEntityException e) {
            Debug.logWarning(e, module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "publication.update.read_failure", new Object[] { e.getMessage() }, locale));
        }

        if (publication == null) {
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "publication.update.not_found", locale));
        }

        String statusId = (String) context.get("statusId");
        publication.set("statusId", "PUBLICATION_APPROVED");
        publication.set("comment", (String) context.get("comment"));

        try {
            publication.store();
        } catch (GenericEntityException e) {
            Debug.logWarning(e.getMessage(), module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "publication.update.write_failure", new Object[] { e.getMessage() }, locale));
        }
        
        result.put(ModelService.RESPONSE_MESSAGE, ModelService.RESPOND_SUCCESS);
        result.put(ModelService.SUCCESS_MESSAGE, 
                UtilProperties.getMessage(resourceError, "publication.update.success", locale));
        return result;
    }
}
