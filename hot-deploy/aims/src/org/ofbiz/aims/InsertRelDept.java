package org.ofbiz.aims;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.sql.Date;

import javolution.util.FastList;
import javolution.util.FastMap;

import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilGenerics;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilProperties;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityConditionList;
import org.ofbiz.entity.condition.EntityExpr;
import org.ofbiz.entity.condition.EntityFunction;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.model.DynamicViewEntity;
import org.ofbiz.entity.model.ModelKeyMap;
import org.ofbiz.entity.util.EntityFindOptions;
import org.ofbiz.entity.util.EntityListIterator;
import org.ofbiz.entity.util.EntityTypeUtil;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.GenericServiceException;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ModelService;
import org.ofbiz.service.ServiceUtil;

//for import upload
import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.base.util.string.FlexibleStringExpander;
import java.util.*;
import java.io.*;
import java.lang.Long;


public class InsertRelDept{
    public static final String module = InsertRelDept.class.getName();
    public static final String resource = "PartyUiLabels";
    public static final String resourceError = "PartyErrorUiLabels";

    public static String getPartyId(Map<String, ? extends Object> context) {
        String partyId = (String) context.get("partyId");
        if (UtilValidate.isEmpty(partyId)) {
            GenericValue userLogin = (GenericValue) context.get("userLogin");
            if (userLogin != null) {
                partyId = userLogin.getString("partyId");
            }
        }
        return partyId;
    }

     public static String createReqId(Delegator delegator, String prefix, int length) {
        final String seqName = "RequestUpdatePerson";
        String reqPartyId = prefix != null ? prefix : "";

        // generate the sequenced number and pad
        Long seq = delegator.getNextSeqIdLong(seqName);
        reqPartyId = reqPartyId + UtilFormatOut.formatPaddedNumber(seq.longValue(), (length - reqPartyId.length() - 1));

        // get the check digit
        int check = UtilValidate.getLuhnCheckDigit(reqPartyId);
        reqPartyId = reqPartyId + Integer.toString(check);

        return reqPartyId;
    }

    public static String createReqId2(String seqname, Delegator delegator, String prefix, int length) {
        final String seqName = seqname;
        String reqPartyId = prefix != null ? prefix : "";

        // generate the sequenced number and pad
        Long seq = delegator.getNextSeqIdLong(seqName);
        reqPartyId = reqPartyId + UtilFormatOut.formatPaddedNumber(seq.longValue(), (length - reqPartyId.length() - 1));

        // get the check digit
        int check = UtilValidate.getLuhnCheckDigit(reqPartyId);
        reqPartyId = reqPartyId + Integer.toString(check);

        return reqPartyId;
    }

    public static Map<String, Object> uploadFile(DispatchContext ctx, Map<String, ? extends Object> context) {
        Map<String, Object> result = FastMap.newInstance();
        Delegator delegator = ctx.getDelegator();
        LocalDispatcher dispatcher = ctx.getDispatcher();
        Locale locale = (Locale) context.get("locale");
        
        //""+delegator.getNextSeqId("NewParty");
        String deptName = (String)context.get("abbreviation");
        String partyId = delegator.getNextSeqId("NewParty");
        String groupName = (String)context.get("groupName");
        String partyTypeId = (String)context.get("partyTypeId"); 
        String partyIdFrom = (String)context.get("partyIdFrom");
        String roleTypeIdTo = (String)context.get("roleTypeIdTo");
        String roleTypeIdFrom = "";// (String)context.get("roleTypeIdTo");

        int inPerson = 0;

        if (UtilValidate.isEmpty(partyId)) {
            return ServiceUtil.returnError(UtilProperties.getMessage(ServiceUtil.resource, 
                    "serviceUtil.party_id_missing", locale));
        }

        GenericValue party = null;       
        GenericValue party1 = null;       
        GenericValue partyGroup = null; 
        GenericValue partyRole = null;
        GenericValue partyRole2 = null;
        List<GenericValue> pr = null;

        int count = 0;
        try{


            if(partyIdFrom!=null)
            {
                pr = delegator.findList("PartyRole", null,null,null,null, false);
                for(int i=0;i<pr.size();i++)
                {                       
                    if((pr.get(i).get("partyId")+"").equalsIgnoreCase((""+partyIdFrom)))
                    {                        
                        roleTypeIdFrom = (String)pr.get(i).get("roleTypeId");
                        break;
                    }
                }
            }

            party1 = delegator.makeValue("Party");
            party1.set("partyId",partyId);
            party1.set("partyTypeId",partyTypeId);  
            delegator.create(party1);          
            
            partyGroup = delegator.makeValue("PartyGroup");
            partyGroup.set("partyId",partyId);
            partyGroup.set("abbreviation",deptName);            
            partyGroup.set("groupName",groupName);
            delegator.create(partyGroup);


            partyRole = delegator.makeValue("PartyRole");
            partyRole.set("partyId",partyId);
            partyRole.set("roleTypeId",roleTypeIdTo);
            delegator.create(partyRole);

            // partyRole2 = delegator.makeValue("PartyRole");
            // partyRole2.set("partyId",partyId);
            // partyRole2.set("roleTypeId","INTERNAL_ORGANIZATIO");
            // delegator.create(partyRole2);

            // newId = createReqId(delegator,"",5);
            if(partyIdFrom!=null)
            {
                party = delegator.makeValue("PartyRelationship");
                party.set("partyIdTo",partyId);
                party.set("partyIdFrom",partyIdFrom);
                party.set("roleTypeIdFrom",roleTypeIdFrom);
                party.set("roleTypeIdTo",roleTypeIdTo);
                //java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                party.set("fromDate",UtilDateTime.nowTimestamp());
                delegator.create(party);
            }
         
        }catch(Exception e){
            //req party id not found]
            Debug.logWarning(e, module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, partyIdFrom+" "+" "+pr.get(5).get("partyId")+" "+e, new Object[] {e.getMessage() }, locale));
            
        }    

        return result;
    }
}