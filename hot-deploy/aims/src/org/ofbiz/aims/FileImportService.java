package org.ofbiz.aims;

import java.nio.ByteBuffer;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.util.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.text.DateFormat;

import org.ofbiz.entity.*;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.ofbiz.base.util.Debug;
import org.ofbiz.entity.GenericDelegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.ServiceUtil;

import org.ofbiz.service.LocalDispatcher;
import java.util.Locale;
import java.text.SimpleDateFormat;
import java.util.Locale;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilGenerics;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilProperties;
import org.ofbiz.base.util.UtilValidate;

import javolution.util.FastList;
import javolution.util.FastMap;

import java.io.*;

public class FileImportService {

    public static String module = FileImportService.class.getName();
    public static final String resourceError = "PartyErrorUiLabels";

    /**
     * This method is responsible to import spreadsheet data into "Product" and
     * "InventoryItem" entities into database. The method uses the
     * FileImportHelper class to perform its opertaion. The method uses "Apache
     * POI" api for importing spreadsheet(xls files) data.
     * 
     * Note : Create the spreadsheet directory in the ofbiz home folder and keep
     * your xls files in this folder only.
     * 
     * @param dctx
     * @param context
     * @return
     */
    public static Map<String, Object> productImport(DispatchContext dctx, Map<String, ? extends Object> context) {
        Delegator delegator = dctx.getDelegator();
        
        Map<String, Object> responseMsgs = FastMap.newInstance();
        LocalDispatcher dispatcher = dctx.getDispatcher();
        Locale locale = (Locale) context.get("locale");
        // System.getProperty("user.dir") returns the path upto ofbiz home
        // directory

        File importDir = null;
        ByteBuffer fileDataProfile = null;
        String fileeName = "";
        String path = "";
        try
        {
            
            fileDataProfile = (ByteBuffer) context.get("filee");
            fileeName = (String) context.get("_filee_fileName");

            importDir = new File(System.getProperty("user.dir")+"\\hot-deploy\\aims\\webapp\\aims\\spreadsheet\\" + fileeName);
            path = System.getProperty("user.dir")+"\\hot-deploy\\aims\\webapp\\aims\\spreadsheet\\" + fileeName;

            try {
                RandomAccessFile out = new RandomAccessFile(importDir, "rw");
                out.write(fileDataProfile.array());
                out.close();
            } catch (FileNotFoundException e) {
                Debug.logError(e, "");
            } catch (IOException e) {
                Debug.logError(e, "");
            }
        }catch(Exception e)
        {
            return ServiceUtil.returnError(fileeName+" a "+e);
        }


        //String path = System.getProperty("user.dir")+"\\hot-deploy\\aims\\webapp\\aims\\spreadsheet\\"+"test.xls";
        List fileItems = new ArrayList();

        // if (path != null && path.length() > 0) {
            //File importDir = new File(path);
        //     if (importDir.isDirectory() && importDir.canRead()) {
        //         File[] files = importDir.listFiles();
        //         // loop for all the containing xls file in the spreadsheet
        //         // directory
        //         for (int i = 0; i < files.length; i++) {
        //             if (files[i].getName().toUpperCase().endsWith("XLS")) {
                        //fileItems.add(files[i]);
            fileItems.add(importDir);
                //     }
                // }
        //     } else {
        //         Debug.logWarning("Directory not found or can't be read", module);
        //         //return responseMsgs;
        //         return ServiceUtil.returnError("Cannot parse Directory "+path);
        //     }
        // } else {
        //     Debug.logWarning("No path specified, doing nothing", module);
        //     return ServiceUtil.returnError("Cannot parse path");
        // }

        if (fileItems.size() < 1) {
            Debug.logWarning("No spreadsheet exists in " + path, module);
            return ServiceUtil.returnError("Cannot parse no sp exist ");
        }

        for (int i = 0; i < fileItems.size(); i++) {
            // read all xls file and create workbook one by one.
            File item = (File) fileItems.get(i);
            List products = new ArrayList();
            List inventoryItems = new ArrayList();
            POIFSFileSystem fs = null;
            HSSFWorkbook wb = null;
            try {
                fs = new POIFSFileSystem(new FileInputStream(item));
                wb = new HSSFWorkbook(fs);
            } catch (IOException e) {
                Debug.logError("Unable to read or create workbook from file", module);
                return ServiceUtil.returnError("Unable to read or create workbook from file");
            }


            // get first sheet
            HSSFSheet sheet = wb.getSheetAt(0);
            int sheetLastRowNumber = sheet.getLastRowNum();
            if(sheetLastRowNumber == 0)
                return ServiceUtil.returnError("sheetLastRowNumber + "+sheetLastRowNumber);
            for (int j = 1; j <= sheetLastRowNumber; j++) {
                HSSFRow row = sheet.getRow(j);
                if (row != null) {
                    // read productId from first column "sheet column index
                    // starts from 0"
                    HSSFCell cell1 = row.getCell((int) 0);
                    cell1.setCellType(HSSFCell.CELL_TYPE_STRING);
                    String firstName = cell1.getStringCellValue();

                    HSSFCell cell2 = row.getCell((int) 1);
                    cell2.setCellType(HSSFCell.CELL_TYPE_STRING);
                    String lastName = cell2.getStringCellValue();

                    HSSFCell cell3 = row.getCell((int) 2);
                    cell3.setCellType(HSSFCell.CELL_TYPE_STRING);
                    String infoString = cell3.getStringCellValue();

                    HSSFCell cell4 = row.getCell((int) 3);
                    cell4.setCellType(HSSFCell.CELL_TYPE_STRING);
                    String birthDate1 = cell4.getStringCellValue();
                    DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                    Date birthDate  = null;
                    java.sql.Date sqlDate = null;
                    try
                    {
                        birthDate = format.parse(birthDate1);    
                        sqlDate = new java.sql.Date(birthDate.getTime()); 
                    }catch(Exception dd)
                    {
                        return ServiceUtil.returnError("Cannot parse birthdate "+birthDate1+" "+dd);
                    }
                    

                    HSSFCell cell5 = row.getCell((int) 4);
                    cell5.setCellType(HSSFCell.CELL_TYPE_STRING);
                    String passportNumber = cell5.getStringCellValue();

                    HSSFCell cell6 = row.getCell((int) 5);
                    cell6.setCellType(HSSFCell.CELL_TYPE_STRING);
                    String cardId = cell6.getStringCellValue();

                    HSSFCell cell7 = row.getCell((int) 6);
                    cell7.setCellType(HSSFCell.CELL_TYPE_STRING);
                    String userLoginId = cell7.getStringCellValue();

                    HSSFCell cell8 = row.getCell((int) 7);
                    cell8.setCellType(HSSFCell.CELL_TYPE_STRING);
                    String roleTypeId = cell8.getStringCellValue();
                    // read QOH from ninth column
                    // HSSFCell cell8 = row.getCell((int) 8);
                    // double quantityOnHand = 0.0;
                    // if (cell8 != null && cell8.getCellType() == HSSFCell.CELL_TYPE_NUMERIC)
                    //     quantityOnHand = cell8.getNumericCellValue();

                    // check productId if null then skip creating inventory item
                    // too.
                    try
                    {
                        GenericValue party = delegator.makeValue("Party", (Map) FileImportHelper.prepareParty(delegator));
                        delegator.create(party);
                    }catch(Exception esd){ return ServiceUtil.returnError("Cannot store party");}
                    
                    try
                    {
                        GenericValue person = delegator.makeValue("Person", (Map) FileImportHelper.preparePerson
                            (
                                firstName,lastName,sqlDate,cardId,passportNumber
                            ));
                        delegator.create(person);
                    }catch(Exception esd){ return ServiceUtil.returnError("Cannot store person "+esd);}

                    try
                    {
                        GenericValue contact = delegator.makeValue("Contact", (Map) FileImportHelper.prepareContact(infoString,delegator));
                        delegator.create(contact);
                    }catch(Exception esd){ return ServiceUtil.returnError("Cannot store contact");}

                    try
                    {
                        GenericValue partycontact = delegator.makeValue("PartyContact", (Map) FileImportHelper.preparePartyContact(delegator));
                        delegator.create(partycontact);
                    }catch(Exception esd){ return ServiceUtil.returnError("Cannot store partycontact "+esd);}

                    try
                    {
                        GenericValue partyrole = delegator.makeValue("PartyRole", (Map) FileImportHelper.preparePartyRole(roleTypeId));
                        delegator.create(partyrole);
                    }catch(Exception esd){ return ServiceUtil.returnError("Cannot store partyrole "+esd);}

                    try
                    {
                        GenericValue userlogin = delegator.makeValue("UserLogin", (Map) FileImportHelper.prepareUserLogin(userLoginId,locale));
                        delegator.create(userlogin);
                    }catch(Exception esd){ return ServiceUtil.returnError("Cannot store userlogin "+esd);}


                    // boolean productExists = FileImportHelper.checkProductExists(productId, delegator);

                    // if (productId != null && !productId.trim().equalsIgnoreCase("") && !productExists) {
                    //     products.add(FileImportHelper.prepareParty());
                    //     if (quantityOnHand >= 0.0)
                    //         inventoryItems.add(FileImportHelper.prepareInventoryItem(productId, quantityOnHand,
                    //                 delegator.getNextSeqId("InventoryItem")));
                    //     else
                    //         inventoryItems.add(FileImportHelper.prepareInventoryItem(productId, 0.0, delegator
                    //                 .getNextSeqId("InventoryItem")));
                    // }
                    // int rowNum = row.getRowNum() + 1;
                    // if (row.toString() != null && !row.toString().trim().equalsIgnoreCase("") && products.size() > 0
                    //         && !productExists) {
                    //     Debug.logWarning("Row number " + rowNum + " not imported from " + item.getName(), module);
                    // }
                }
            }
            // // create and store values in "Product" and "InventoryItem" entity
            // // in database
            // for (int j = 0; j < products.size(); j++) {
            //     GenericValue party = delegator.makeValue("Party", (Map) products.get(j));
            //     GenericValue person = delegator.makeValue("Person", (Map) inventoryItems.get(j));
            //     if (!FileImportHelper.checkProductExists(party.getString("productId"), delegator)) {
            //         try {
            //             delegator.create(party);
            //             delegator.create(person);
            //         } catch (GenericEntityException e) {
            //             Debug.logError("Cannot store product", module);
            //             return ServiceUtil.returnError("Cannot store product");
            //         }
            //     }
            // }
            // int uploadedProducts = products.size() + 1;
            // if (products.size() > 0)
            //     Debug.logInfo("Uploaded " + uploadedProducts + " products from file " + item.getName(), module);
        }
        return responseMsgs;
    }
}