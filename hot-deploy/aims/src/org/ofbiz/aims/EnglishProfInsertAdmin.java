package org.ofbiz.aims;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.sql.Date;

import javolution.util.FastList;
import javolution.util.FastMap;

import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilGenerics;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilProperties;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityConditionList;
import org.ofbiz.entity.condition.EntityExpr;
import org.ofbiz.entity.condition.EntityFunction;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.model.DynamicViewEntity;
import org.ofbiz.entity.model.ModelKeyMap;
import org.ofbiz.entity.util.EntityFindOptions;
import org.ofbiz.entity.util.EntityListIterator;
import org.ofbiz.entity.util.EntityTypeUtil;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.GenericServiceException;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ModelService;
import org.ofbiz.service.ServiceUtil;

//for import upload
import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.base.util.string.FlexibleStringExpander;
import java.util.*;
import java.io.*;
import java.lang.Long;

public class EnglishProfInsertAdmin{
    public static final String module = EnglishProfInsertAdmin.class.getName();
    public static final String resource = "PartyUiLabels";
    public static final String resourceError = "PartyErrorUiLabels";

    public static String getPartyId(Map<String, ? extends Object> context) {
        String partyId = (String) context.get("partyId");
        if (UtilValidate.isEmpty(partyId)) {
            GenericValue userLogin = (GenericValue) context.get("userLogin");
            if (userLogin != null) {
                partyId = userLogin.getString("partyId");
            }
        }
        return partyId;
    }

     public static String createReqId(Delegator delegator, String prefix, int length) {
        final String seqName = "RequestENglishProficiency";
        String reqPartyId = prefix != null ? prefix : "";

        // generate the sequenced number and pad
        Long seq = delegator.getNextSeqIdLong(seqName);
        reqPartyId = reqPartyId + UtilFormatOut.formatPaddedNumber(seq.longValue(), (length - reqPartyId.length() - 1));

        // get the check digit
        int check = UtilValidate.getLuhnCheckDigit(reqPartyId);
        reqPartyId = reqPartyId + Integer.toString(check);

        return reqPartyId;
    }

    public static Map<String, Object> uploadFile(DispatchContext ctx, Map<String, ? extends Object> context) {
        Map<String, Object> result = FastMap.newInstance();
        Delegator delegator = ctx.getDelegator();
        LocalDispatcher dispatcher = ctx.getDispatcher();
        Locale locale = (Locale) context.get("locale");

        String studentId = (String)context.get("studentId");
        String statusId = (String)context.get("statusId");
        String status = (String)context.get("status");
        String englishTest = (String)context.get("englishTest");
        Date testdate = (Date)context.get("testdate");
        Long score = new Long(""+context.get("score"));
        String newId = "";

        if (UtilValidate.isEmpty(studentId)) {
            return ServiceUtil.returnError(UtilProperties.getMessage(ServiceUtil.resource, 
                    "serviceUtil.party_id_missing", locale));
        }


        GenericValue profile = null;
        GenericValue newpartyrequest = null;
        try{
            newId = createReqId(delegator,"",5);
            profile = delegator.makeValue("EnglishProficiency");
            newpartyrequest = delegator.makeValue("NewPartyRequest");
            // delegator.setNextSubSeqId(profile, "reqPartyId", 5, 1);
            // profile = delegator.findOne("RequestUpdatePerson", UtilMisc.toMap("partyId", partyId), false);
            // profile = delegator.findOne("RequestUpdatePerson", UtilMisc.toMap("reqPartyId", newId), false);
        }catch(Exception e){
            //req party id not found]
            Debug.logWarning(e, module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, "thesis.update.read_failure", new Object[] {e.getMessage() }, locale));
        }    

        if(profile == null){
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "thesis.update.not_found", locale));
        }

        // Map<String, Object> tempThesis = (Map<String, Object>)thesis.getAllFields();

        /*
            for upload
        */

        //for Thesis Proposal
        //===================================================================================================================
        ByteBuffer fileDataProfile = null;
        String fileNameDataProfile = "";
        String contentTypeDataProfile = "";
        File file = null;        

        try{
            fileDataProfile = (ByteBuffer) context.get("photo");
            fileNameDataProfile = (String) context.get("_photo_fileName");
            contentTypeDataProfile = (String) context.get("_photo_contentType");

            if (UtilValidate.isNotEmpty(fileNameDataProfile)) {
                file = new File(System.getProperty("user.dir")+ "/hot-deploy/aims/webapp/aims/images/" + fileNameDataProfile);
                Debug.logInfo("upload file to " + file.getAbsolutePath(), "");

                try {
                    RandomAccessFile out = new RandomAccessFile(file, "rw");
                    out.write(fileDataProfile.array());
                    out.close();
                } catch (FileNotFoundException e) {
                    Debug.logError(e, "");
                } catch (IOException e) {
                    Debug.logError(e, "");
                }
            }else{
                fileNameDataProfile = "noimage.jpg";
            }
        }catch(Exception e){
            fileNameDataProfile = "noimage.jpg"; 
        }

        fileNameDataProfile = "../images/" + fileNameDataProfile;
        //===================================================================================================================
        //end

        //update


        //for thesis proposal

        // if(file != null){
        //     profile.set("photo",fileNameDataProfile);
        profile.set("epId",newId);
        profile.set("studentId",studentId);
        profile.set("statusId",statusId);        
        profile.set("testdate",testdate);
        profile.set("photo",fileNameDataProfile);
        profile.set("englishTest",englishTest);
        profile.set("score",score);
        
        newpartyrequest.set("requestId",delegator.getNextSeqId("NewPartyRequest"));
        newpartyrequest.set("studentId",studentId);
        newpartyrequest.set("partyToId","AcademicAffairs");
        newpartyrequest.set("status","SERVICE_FINISHED");
        newpartyrequest.set("requestType","ENG_TEST_APP");
        newpartyrequest.set("TG96Id",newId);
        // }else{
        //     profile.set("photo", "awdawdawd");
        // }
        
        try {
            // EntityFindOptions findOpts = new EntityFindOptions(true,
            //         EntityFindOptions.TYPE_SCROLL_INSENSITIVE,
            //         EntityFindOptions.CONCUR_UPDATABLE, true);
            // EntityListIterator el = ((EntityListIterator)delegator.find("RequestUpdatePerson",null,null,null,null,null));            
            //el.add(profile);
            // el.getCompleteList().add(profile);
            delegator.create(profile);
            delegator.create(newpartyrequest);
            // profile.store();
            //((GenericValue)delegator.findOne("RequestUpdatePerson")).create(profile);

        } catch (GenericEntityException e) {
            Debug.logWarning(e.getMessage(), module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                   "error : "+e, new Object[] { e.getMessage() }, locale));
        }

        result.put(ModelService.RESPONSE_MESSAGE, ModelService.RESPOND_SUCCESS);
        result.put(ModelService.SUCCESS_MESSAGE, 
                UtilProperties.getMessage(resourceError, fileNameDataProfile, locale));
        return result;
    }
}