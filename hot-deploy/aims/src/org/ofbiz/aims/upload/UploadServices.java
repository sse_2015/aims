/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *******************************************************************************/
package org.ofbiz.aims.upload;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.StringWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.Writer;
import java.nio.ByteBuffer;
import java.sql.Timestamp;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javolution.util.FastMap;

import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.FileUtil;
import org.ofbiz.base.util.GeneralException;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilGenerics;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilProperties;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.GenericServiceException;
import org.ofbiz.service.ModelService;
import org.ofbiz.service.ServiceUtil;

/**
 * UploadServices Class
 */
public class UploadServices {

    public static final String module = UploadServices.class.getName();
    public static final String resource = "AIMSUiLabels";

    public static String createFileUpload(Delegator delegator, String fileName, String fileType, byte[] data)throws GenericServiceException {
    	Debug.logInfo("BALX: start = " + fileName, module);
    	String fileId = null;
    	if (data != null && data.length > 0) {
            String filePath = UploadWorker.getUploadPath() + "/" + fileName;
            File file = FileUtil.getFile(filePath);

	        Debug.logInfo("BALX: create File", module);
            try {
                FileOutputStream out = new FileOutputStream(file);
                out.write(data);
                if (Debug.infoOn()) {
                    Debug.logInfo("in createBinaryFileMethod, length:" + file.length(), module);
                }
                out.close();
            } catch (IOException e) {
                Debug.logWarning(e, module);
                throw new GenericServiceException(e.getMessage());
            }

            Debug.logInfo("BALX: Store", module);

	        try {
	            fileId = delegator.getNextSeqId("FileUpload");
	        } catch (IllegalArgumentException e) {
	            throw new GenericServiceException(e.getMessage());
	        }

			GenericValue fileUpload = delegator.makeValue("FileUpload", UtilMisc.toMap("fileId", fileId));

			fileUpload.set("name", fileName, false);
	        fileUpload.set("mimeType", fileType, false);
			fileUpload.set("location", filePath, false);

	        try {
	            fileUpload.create();
	        } catch (GenericEntityException e) {
	            Debug.logWarning(e.getMessage(), module);
	            throw new GenericServiceException(e.getMessage());
	        }
        }
        return fileId;
    }

    public static boolean updateFileUpload(Delegator delegator, String fileId, String fileName, String fileType, byte[] data)throws GenericServiceException {
    	if (data != null && data.length > 0) {
            String filePath = UploadWorker.getUploadPath() + "/" +  fileName;

            File file = FileUtil.getFile(filePath);

            try {
                FileOutputStream out = new FileOutputStream(file);
                out.write(data);
                if (Debug.infoOn()) {
                    Debug.logInfo("in createBinaryFileMethod, length:" + file.length(), module);
                }
                out.close();
            } catch (IOException e) {
                Debug.logWarning(e, module);
                throw new GenericServiceException(e.getMessage());
            }

            GenericValue fileUpload = null;

	        try {
	            fileUpload = delegator.findOne("FileUpload", UtilMisc.toMap("fileId", fileId), false);
	        } catch (GenericEntityException e) {
	            Debug.logWarning(e, module);
	            return false;
	        }

	        if (fileUpload == null) {
	            return false;
	        }

			fileUpload.set("name", fileName, false);
	        fileUpload.set("mimeType", fileType, false);
			fileUpload.set("location", filePath, false);

	        try {
	            fileUpload.store();
	        } catch (GenericEntityException e) {
	            Debug.logWarning(e.getMessage(), module);
	            return false;
	        }
        }
        return true;
    }

}
