package org.ofbiz.aims;

import java.security.MessageDigest;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.sql.Date;

import javolution.util.FastList;
import javolution.util.FastMap;

import org.ofbiz.base.util.Debug;

import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilGenerics;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilProperties;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityConditionList;
import org.ofbiz.entity.condition.EntityExpr;
import org.ofbiz.entity.condition.EntityFunction;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.model.DynamicViewEntity;
import org.ofbiz.entity.model.ModelKeyMap;
import org.ofbiz.entity.util.EntityFindOptions;
import org.ofbiz.entity.util.EntityListIterator;
import org.ofbiz.entity.util.EntityTypeUtil;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.GenericServiceException;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ModelService;
import org.ofbiz.service.ServiceUtil;

//for import upload
import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.base.util.string.FlexibleStringExpander;
import java.util.*;
import java.io.*;
import java.lang.Long;

public class InsertNewParty{
    public static final String module = InsertNewParty.class.getName();
    public static final String resource = "PartyUiLabels";
    public static final String resourceError = "PartyErrorUiLabels";

    public static String getPartyId(Map<String, ? extends Object> context) {
        String partyId = (String) context.get("partyId");
        if (UtilValidate.isEmpty(partyId)) {
            GenericValue userLogin = (GenericValue) context.get("userLogin");
            if (userLogin != null) {
                partyId = userLogin.getString("partyId");
            }
        }
        return partyId;
    }

     public static String createReqId(Delegator delegator, String prefix, int length) {
        final String seqName = "RequestUpdatePerson";
        String reqPartyId = prefix != null ? prefix : "";

        // generate the sequenced number and pad
        Long seq = delegator.getNextSeqIdLong(seqName);
        reqPartyId = reqPartyId + UtilFormatOut.formatPaddedNumber(seq.longValue(), (length - reqPartyId.length() - 1));

        // get the check digit
        int check = UtilValidate.getLuhnCheckDigit(reqPartyId);
        reqPartyId = reqPartyId + Integer.toString(check);

        return reqPartyId;
    }

    public static String createReqId2(String seqname, Delegator delegator, String prefix, int length) {
        final String seqName = seqname;
        String reqPartyId = prefix != null ? prefix : "";

        // generate the sequenced number and pad
        Long seq = delegator.getNextSeqIdLong(seqName);
        reqPartyId = reqPartyId + UtilFormatOut.formatPaddedNumber(seq.longValue(), (length - reqPartyId.length() - 1));

        // get the check digit
        int check = UtilValidate.getLuhnCheckDigit(reqPartyId);
        reqPartyId = reqPartyId + Integer.toString(check);

        return reqPartyId;
    }

    public static Map<String, Object> uploadFile(DispatchContext ctx, Map<String, ? extends Object> context) {
        Map<String, Object> result = FastMap.newInstance();
        Delegator delegator = ctx.getDelegator();
        LocalDispatcher dispatcher = ctx.getDispatcher();
        Locale locale = (Locale) context.get("locale");

        String contactId = (String)delegator.getNextSeqId("Contact");
        String requestId = (String)context.get("requestId");
        String roleTypeId = (String)context.get("roleTypeId");
        String partyTypeId = (String)context.get("partyTypeId");        
        String infoString = (String)context.get("infoString");
        String partyId = (String)context.get("partyId");
        String firstName = (String)context.get("firstName");
        String middleName = (String)context.get("middleName");
        String lastName = (String)context.get("lastName");
        String personalTitle = (String)context.get("personalTitle");
        String suffix = (String)context.get("suffix");
        String nickname = (String)context.get("nickname");
        String gender = (String)context.get("gender");
        Date birthDate = (Date)context.get("birthDate");
        Date deceasedDate = (Date)context.get("deceasedDate");
        Double height = (Double)context.get("height");
        Double weight = (Double)context.get("weight");
        String mothersMaidenName = (String)context.get("mothersMaidenName");
        String maritalStatus = (String)context.get("maritalStatus");
        String socialSecurityNumber = (String)context.get("socialSecurityNumber");
        String cardId = (String)context.get("cardId");
        String comments = (String)context.get("comments");
        String newId = "";
        String newCid = "";

        String username = (String)context.get("userLoginId");

        Timestamp fromDate = (Timestamp)context.get("fromDate");
        String passportNumber = (String)context.get("passportNumber");        

        int inPerson = 0;

        if (UtilValidate.isEmpty(partyId)) {
            return ServiceUtil.returnError(UtilProperties.getMessage(ServiceUtil.resource, 
                    "serviceUtil.party_id_missing", locale));
        }

        GenericValue party = null;
        GenericValue partyRole = null;
        GenericValue profile = null;
        GenericValue partycontact = null;
        GenericValue telecomnumber = null;
        GenericValue contact = null;
        GenericValue contactDetail = null;
        GenericValue partyrequestt = null;
        GenericValue postaladdress = null;
        GenericValue userlogin = null;

        try{
            // newId = createReqId(delegator,"",5);
            party = delegator.makeValue("Party");
            party.set("partyId",partyId);
            party.set("partyTypeId",partyTypeId);
            delegator.create(party);

            partyRole = delegator.makeValue("PartyRole");
            partyRole.set("partyId",partyId);
            partyRole.set("roleTypeId",roleTypeId);
            delegator.create(partyRole);

            // delegator.setNextSubSeqId(profile, "reqPartyId", 5, 1);
            inPerson = 1;
            profile = delegator.makeValue("Person");
            partyrequestt = delegator.findOne("RequestNewUser", UtilMisc.toMap("requestId", requestId), false);

            if(username!=null)
            {
                userlogin = delegator.makeValue("PartyRole");

            }
			/*partyContact = delegator.findOne("PartyContact", UtilMisc.toMap("partyId", partyId), false);
			if(partyContact == null)
				partyContact = delegator.makeValue("PartyContact");*/
				
            // profile = delegator.findOne("RequestUpdatePerson", UtilMisc.toMap("reqPartyId", newId), false);
        }catch(Exception e){
            //req party id not found]
            Debug.logWarning(e, module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, ""+e, new Object[] {e.getMessage() }, locale));
            
        }    

        try
        {            
            partycontact = delegator.makeValue("PartyContact"); 
            //contactId = createReqId2("Contact",delegator,"",5);
            contact = delegator.makeValue("Contact");
            contact.set("contactId",contactId);
            contact.set("infoString",infoString);

            try
            {
                delegator.create(contact);
            }
            catch(Exception es)
            {
                Debug.logWarning(es, module);
                return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    " c : "+contactId+" contact error + party id : "+partyId+" "+es, new Object[] {es.getMessage() }, locale));
            }

            partycontact.set("partyId",partyId);
            partycontact.set("contactId",contactId);
            partycontact.set("fromDate",fromDate);
            try
            {
                delegator.create(partycontact);    
            }
            catch(Exception es)
            {
                Debug.logWarning(es, module);
                return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    " c : "+contactId+" party contact error "+es, new Object[] {es.getMessage() }, locale));

            }

            userlogin = delegator.makeValue("UserLogin");
            userlogin.set("userLoginId",username);

            String pass = ""+partyId+""+contactId;
            if(cardId!=null)
                pass = pass+""+cardId;
            if(passportNumber!=null)
                pass = pass+""+passportNumber;
            MessageDigest md = MessageDigest.getInstance("SHA-1");            

            md.update(pass.getBytes("UTF-8")); // Change this to "UTF-16" if needed
            byte[] digest = md.digest();

            userlogin.set("currentPassword",
                "{SHA}"+String.format("%x", new java.math.BigInteger(1, digest))
                );
            userlogin.set("partyId",partyId);
            userlogin.set("enabled","Y");
            Timestamp timestamp = null;
            try
            {
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
                java.util.Date parsedDate = dateFormat.parse("1990-01-01 10:10:10.0");
                timestamp = new java.sql.Timestamp(parsedDate.getTime());
            }catch(Exception asd)
            {
                return ServiceUtil.returnError(UtilProperties.getMessage(resourceError,
                    ""+asd, new Object[] {asd.getMessage() }, locale));
            }

            userlogin.set("disabledDateTime",timestamp);
            try
            {
                delegator.create(userlogin);    
            }
            catch(Exception es)
            {
                Debug.logWarning(es, module);
                return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    ""+es, new Object[] {es.getMessage() }, locale));

            }
            
            // // try{
            // //     contactDetail = delegator.findOne("PartyAndContact",
            // //      UtilMisc.toMap("partyId", partyId,"contactId",contactId,"fromDate",fromDate,"paContactId",contactId,"tnContactId",contactId), false);
            // //     contactDetail.set("infoString",infoString);
            // //     contactDetail.store();
            // // }
            // // catch(Exception es)
            // // {
            // //     return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
            // //         " c : "+contactId+" party contact detail error "+es, new Object[] {es.getMessage() }, locale));
            // // }
            
            // postaladdress = delegator.makeValue("PostalAddress");
            // postaladdress.set("contactId",contactId);
            // postaladdress.set("address1",address1);
            // postaladdress.set("city",city);
            // postaladdress.set("postalCode",postalCode);

            // try
            // {
            //     delegator.create(postaladdress);
            // }
            // catch(Exception es)
            // {
            //     Debug.logWarning(es, module);
            //     return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
            //         "create postal contact error "+contactId+" "+es, new Object[] {es.getMessage() }, locale));

            // }
            // telecomnumber = delegator.makeValue("TelecomNumber");
            // telecomnumber.set("contactId",contactId);
            // telecomnumber.set("contactNumber",contactNumber);
            // try
            // {
            //     delegator.create(telecomnumber);            
            // }
            // catch(Exception es)
            // {
            //     Debug.logWarning(es, module);
            //     return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
            //         "create tele contact error", new Object[] {es.getMessage() }, locale));

            // }
        }catch(Exception e)
        {
            Debug.logWarning(e, module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                ""+contactId+" "+partyId+" "+fromDate+" "+e, new Object[] {e.getMessage() }, locale));
            
        }

        if(profile == null){
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "thesis.update.not_found", locale));
        }

        // Map<String, Object> tempThesis = (Map<String, Object>)thesis.getAllFields();

        /*
            for upload
        */

        //for Thesis Proposal
        //===================================================================================================================
        // ByteBuffer fileDataProfile = null;
        // String fileNameDataProfile = "";
        // String contentTypeDataProfile = "";
        // File file = null;        
        // boolean f = true;
        // try{
        //     fileDataProfile = (ByteBuffer) context.get("photo");
        //     fileNameDataProfile = (String) context.get("_photo_fileName");
        //     contentTypeDataProfile = (String) context.get("_photo_contentType");

        //     if (UtilValidate.isNotEmpty(fileNameDataProfile)) {
        //         file = new File(System.getProperty("user.dir")+ "/hot-deploy/aims/webapp/aims/images/" + fileNameDataProfile);
        //         Debug.logInfo("upload file to " + file.getAbsolutePath(), "");

        //         try {
        //             RandomAccessFile out = new RandomAccessFile(file, "rw");
        //             out.write(fileDataProfile.array());
        //             out.close();
        //         } catch (FileNotFoundException e) {
        //             Debug.logError(e, "");
        //         } catch (IOException e) {
        //             Debug.logError(e, "");
        //         }
        //     }else{
        //         f = false;
        //         fileNameDataProfile = "noimage.jpg";
        //         GenericValue temp = null;
        //         temp = delegator.findOne("Person", UtilMisc.toMap("partyId", partyId), false);
        //         fileNameDataProfile = (String)temp.get("photo");
        //     }
        // }catch(Exception e){
        //     fileNameDataProfile = "noimage.jpg"; 
        // }

        // if(f)
        //     fileNameDataProfile = "../images/" + fileNameDataProfile;
        //===================================================================================================================
        //end

        //update


        //for thesis proposal

        // if(file != null){
        //     profile.set("photo",fileNameDataProfile);
        profile.set("partyId",partyId);        
        profile.set("firstName",firstName);    
        profile.set("lastName",lastName);
        profile.set("birthDate",birthDate);
        if(passportNumber!=null)
            profile.set("passportNumber",passportNumber);
        if(cardId!=null)
            profile.set("cardId",cardId);

        partyrequestt.set("statusId","PARTY_APPROVE");
        
        //profile.set("comments",comments);        
        // }else{
        //     profile.set("photo", "awdawdawd");
        // }
        
        try {
            // EntityFindOptions findOpts = new EntityFindOptions(true,
            //         EntityFindOptions.TYPE_SCROLL_INSENSITIVE,
            //         EntityFindOptions.CONCUR_UPDATABLE, true);
            // EntityListIterator el = ((EntityListIterator)delegator.find("RequestUpdatePerson",null,null,null,null,null));            
            //el.add(profile);
            // el.getCompleteList().add(profile);
            //delegator.create(profile);
            if(inPerson == 0)
                profile.store();
            else
                delegator.create(profile);
            partyrequestt.store();
            //((GenericValue)delegator.findOne("RequestUpdatePerson")).create(profile);

        } catch (GenericEntityException e) {
            Debug.logWarning(e.getMessage(), module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    e.getMessage(), new Object[] { e.getMessage() }, locale));
        }

        // result.put(ModelService.RESPONSE_MESSAGE, ModelService.RESPOND_SUCCESS);
        // result.put(ModelService.SUCCESS_MESSAGE, 
        //         UtilProperties.getMessage(resourceError, fileNameDataProfile, locale));
        return result;
    }
}
