package org.ofbiz.aims;

import java.util.HashMap;
import java.util.Map;

import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.entity.GenericDelegator;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import java.security.MessageDigest;

import org.ofbiz.service.LocalDispatcher;
import java.util.Locale;
import java.text.SimpleDateFormat;
import java.util.Locale;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilGenerics;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilProperties;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.service.ServiceUtil;

public class FileImportHelper {

    static String module = FileImportHelper.class.getName();
    public static final String resource = "PartyUiLabels";
    public static final String resourceError = "PartyErrorUiLabels";
    static String nowpartyid = "";
    static String nowcontactid = "";
    static String passportNumber = "";
    static String cardId = "";


    // prepare the product map
    public static Map prepareParty(Delegator delegator) {
        Map fields = new HashMap();
        nowpartyid = delegator.getNextSeqId("NewParty");
        fields.put("partyId", nowpartyid);
        fields.put("partyTypeId", "PERSON");        
        return fields;
    }

    // prepare the inventoryItem map
    public static Map preparePerson(String firstName,String lastName,java.sql.Date birthDate,String cardId2,String passportNumber) {
        //String productId,double quantityOnHand, String inventoryItemId - parameter commented
        Map fields = new HashMap();
        fields.put("partyId", nowpartyid);
        fields.put("firstName",firstName);
        fields.put("lastName",lastName);
        fields.put("birthDate",birthDate);
        fields.put("cardId",cardId2);
        cardId = cardId2;
        //fields.put("userLoginId",userLoginId);
        //fields.put("infoString",infoString);
        //fields.put("roleTypeId",roleTypeId);
        fields.put("passportNumber",passportNumber);
        passportNumber = passportNumber;
        
        return fields;
    }

    public static Map prepareContact(String infoString,Delegator delegator) {
        //String productId,double quantityOnHand, String inventoryItemId - parameter commented
        Map fields = new HashMap();        
        nowcontactid =  delegator.getNextSeqId("Contact");
        fields.put("contactId",nowcontactid);
        fields.put("infoString",infoString);
        
        return fields;
    }

    public static Map preparePartyContact(Delegator delegator) {
        //String productId,double quantityOnHand, String inventoryItemId - parameter commented
        Map fields = new HashMap();
        fields.put("partyId", nowpartyid);
        fields.put("contactId",nowcontactid);        
        fields.put("fromDate",UtilDateTime.nowTimestamp()); 
        
        return fields;
    }

    public static Map preparePartyRole(String roleTypeId) {
        //String productId,double quantityOnHand, String inventoryItemId - parameter commented
        Map fields = new HashMap();
        fields.put("partyId", nowpartyid);
        fields.put("roleTypeId",roleTypeId);                
        return fields;
    }

    public static Map prepareUserLogin(String userLoginId, Locale locale) {
        //String productId,double quantityOnHand, String inventoryItemId - parameter commented
        Map fields = new HashMap();
        fields.put("userLoginId", userLoginId);
        fields.put("partyId", nowpartyid);
        java.sql.Timestamp timestamp = null;
        try
        {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
            java.util.Date parsedDate = dateFormat.parse("1990-01-01 10:10:10.0");
            timestamp = new java.sql.Timestamp(parsedDate.getTime());
        }catch(Exception asd)
        {
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError,
                ""+asd, new Object[] {asd.getMessage() }, locale));
        }
        fields.put("disabledDateTime",timestamp);
        String pass = ""+nowpartyid+""+nowcontactid;
        if(cardId!=null)
            pass = pass+""+cardId;
        if(passportNumber!=null)
            pass = pass+""+passportNumber;
        MessageDigest md = null;

        try
        {
            md = MessageDigest.getInstance("SHA-1");            
        }catch(Exception jh)
        {
            return ServiceUtil.returnError(UtilProperties.getMessage("Error",
                "jh1 "+jh, new Object[] {jh.getMessage() }, locale));
        }

        byte digest[] = null;
        try
        {
            md.update(pass.getBytes("UTF-8")); // Change this to "UTF-16" if needed
            digest = md.digest();
        }catch(Exception jh)
        {
            return ServiceUtil.returnError(UtilProperties.getMessage("Error",
                "jh 2 "+jh, new Object[] {jh.getMessage() }, locale));
        }
        

        fields.put("currentPassword","{SHA}"+String.format("%x", new java.math.BigInteger(1, digest)));
        fields.put("enabled","Y");

        return fields;
    }

    // check if product already exists in database
    public static boolean checkProductExists(String productId,
            Delegator delegator) {
        GenericValue tmpProductGV;
        boolean productExists = false;
        try {
            tmpProductGV = delegator.findOne("Product", UtilMisc
                .toMap("productId", productId),true);
            if (tmpProductGV != null
                    && tmpProductGV.getString("productId") == productId)
                productExists = true;
        } catch (GenericEntityException e) {
            Debug.logError("Problem in reading data of product", module);
        }
        return productExists;
    }
}