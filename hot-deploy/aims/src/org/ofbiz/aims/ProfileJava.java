package org.ofbiz.aims;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.sql.Date;

import javolution.util.FastList;
import javolution.util.FastMap;

import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilGenerics;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilProperties;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityConditionList;
import org.ofbiz.entity.condition.EntityExpr;
import org.ofbiz.entity.condition.EntityFunction;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.model.DynamicViewEntity;
import org.ofbiz.entity.model.ModelKeyMap;
import org.ofbiz.entity.util.EntityFindOptions;
import org.ofbiz.entity.util.EntityListIterator;
import org.ofbiz.entity.util.EntityTypeUtil;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.GenericServiceException;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ModelService;
import org.ofbiz.service.ServiceUtil;

//for import upload
import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.base.util.string.FlexibleStringExpander;
import java.util.*;
import java.io.*;
import java.lang.Long;

public class ProfileJava{
    public static final String module = ProfileJava.class.getName();
    public static final String resource = "PartyUiLabels";
    public static final String resourceError = "PartyErrorUiLabels";

    public static String getPartyId(Map<String, ? extends Object> context) {
        String partyId = (String) context.get("partyId");
        if (UtilValidate.isEmpty(partyId)) {
            GenericValue userLogin = (GenericValue) context.get("userLogin");
            if (userLogin != null) {
                partyId = userLogin.getString("partyId");
            }
        }
        return partyId;
    }

     public static String createReqId(Delegator delegator, String prefix, int length, String seqNamee) {
        final String seqName = seqNamee;
        String reqPartyId = prefix != null ? prefix : "";

        // generate the sequenced number and pad
        Long seq = delegator.getNextSeqIdLong(seqName);
        reqPartyId = reqPartyId + UtilFormatOut.formatPaddedNumber(seq.longValue(), (length - reqPartyId.length() - 1));

        // get the check digit
        int check = UtilValidate.getLuhnCheckDigit(reqPartyId);
        reqPartyId = reqPartyId + Integer.toString(check);

        return reqPartyId;
    }

    public static Map<String, Object> uploadFile(DispatchContext ctx, Map<String, ? extends Object> context) {
        Map<String, Object> result = FastMap.newInstance();
        Delegator delegator = ctx.getDelegator();
        LocalDispatcher dispatcher = ctx.getDispatcher();
        Locale locale = (Locale) context.get("locale");

        String partyId = getPartyId(context);        
        String firstName = (String)context.get("firstName");
        Timestamp fromDate = Timestamp.valueOf(""+context.get("fromDate"));
        String middleName = (String)context.get("middleName");
        String lastName = (String)context.get("lastName");
        String personalTitle = (String)context.get("personalTitle");
        String suffix = (String)context.get("suffix");
        String nickname = (String)context.get("nickname");
        String gender = (String)context.get("gender");
        Date birthDate = (Date)context.get("birthDate");
        Double height = (Double)context.get("height");
        Double weight = (Double)context.get("weight");
        String mothersMaidenName = (String)context.get("book");
        String maritalStatus = (String)context.get("maritalStatus");
        String socialSecurityNumber = (String)context.get("socialSecurityNumber");
        String comments = (String)context.get("comments");

        String address1 = (String)context.get("address1");
        String city = (String)context.get("city");
        String postalCode = (String)context.get("postalCode");
        String contactNumber = (String)context.get("contactNumber");
        
        String personId = partyId;
        String visaNumber = (String)context.get("visaNumber");
        Date expireDate = (Date)context.get("expireDate");

        String newId = "";
        String newCid = "";
        String newRequestVisaId = "";
        String contactId = (String)context.get("contactId");

        if (UtilValidate.isEmpty(partyId)) {
            return ServiceUtil.returnError(UtilProperties.getMessage(ServiceUtil.resource, 
                    "serviceUtil.party_id_missing", locale));
        }        

        GenericValue profile = null;
        GenericValue requestVisa = null;
        // GenericValue telNum = null;
        // GenericValue postalAdd = null;
        // GenericValue partyContact = null;
        try{
            newId = createReqId(delegator,"",5,"RequestUpdatePerson");
            if(contactId == "" || contactId == null)
                newCid = createReqId(delegator,"",5,"PartyContact");
            else
                newCid = contactId;

            newRequestVisaId = createReqId(delegator,"",5,"RequestVisa");

            profile = delegator.makeValue("RequestUpdatePerson");
            requestVisa = delegator.makeValue("RequestVisa");

            // telNum = delegator.makeValue("TelecomNumber");
            // postalAdd = delegator.makeValue("PostalAddress");
            // partyContact = delegator.makeValue("PartyContact");
            // delegator.setNextSubSeqId(profile, "reqPartyId", 5, 1);
            // profile = delegator.findOne("RequestUpdatePerson", UtilMisc.toMap("partyId", partyId), false);
            // profile = delegator.findOne("RequestUpdatePerson", UtilMisc.toMap("reqPartyId", newId), false);
        }catch(Exception e){
            //req party id not found]
            Debug.logWarning(e, module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, "thesis.update.read_failure", new Object[] {e.getMessage() }, locale));
        }    

        if(profile == null){
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "thesis.update.not_found", locale));
        }

        // Map<String, Object> tempThesis = (Map<String, Object>)thesis.getAllFields();

        /*
            for upload
        */

        //for Thesis Proposal
        //===================================================================================================================
        ByteBuffer fileDataProfile = null;
        String fileNameDataProfile = "";
        String contentTypeDataProfile = "";
        File file = null;        
        boolean f = true;
        try{
            fileDataProfile = (ByteBuffer) context.get("photo");
            fileNameDataProfile = (String) context.get("_photo_fileName");
            contentTypeDataProfile = (String) context.get("_photo_contentType");

            if (UtilValidate.isNotEmpty(fileNameDataProfile)) {
                file = new File(System.getProperty("user.dir")+ "/hot-deploy/aims/webapp/aims/images/" + fileNameDataProfile);
                Debug.logInfo("upload file to " + file.getAbsolutePath(), "");

                try {
                    RandomAccessFile out = new RandomAccessFile(file, "rw");
                    out.write(fileDataProfile.array());
                    out.close();
                } catch (FileNotFoundException e) {
                    Debug.logError(e, "");
                } catch (IOException e) {
                    Debug.logError(e, "");
                }
            }else{
                //fileNameDataProfile = "noimage.jpg";
                f = false;
                fileNameDataProfile = "noimage.jpg";
                GenericValue temp = null;
                temp = delegator.findOne("Person", UtilMisc.toMap("partyId", partyId), false);
                fileNameDataProfile = (String)temp.get("photo");
            }
        }catch(Exception e){
            fileNameDataProfile = "noimage.jpg"; 
        }
        if(f)
            fileNameDataProfile = "../images/" + fileNameDataProfile;
        //===================================================================================================================
        //end

        //update


        //for thesis proposal

        // if(file != null){
        //     profile.set("photo",fileNameDataProfile);
        profile.set("partyId",partyId);
        profile.set("reqPartyId",newId);
        profile.set("photo",fileNameDataProfile);
        profile.set("firstName",firstName);
        profile.set("middleName",middleName);
        profile.set("lastName",lastName);
        profile.set("personalTitle",personalTitle);
        profile.set("suffix",suffix);
        profile.set("nickname",nickname);
        profile.set("fromDate",fromDate);
        profile.set("gender",gender);
        profile.set("birthDate",birthDate);
        profile.set("height",height);
        profile.set("weight",weight);
        profile.set("mothersMaidenName",mothersMaidenName);
        profile.set("maritalStatus",maritalStatus);
        profile.set("socialSecurityNumber",socialSecurityNumber);
        profile.set("comments",comments);        
        profile.set("contactId",newCid);
        profile.set("contactNumber",contactNumber);
        profile.set("postalCode",postalCode);
        profile.set("city",city);
        profile.set("address1",address1);

        requestVisa.set("reqPartyIdVisa",newRequestVisaId);
        requestVisa.set("personId",partyId);
        requestVisa.set("status",new Long(0));
        requestVisa.set("visaNumber",visaNumber);
        requestVisa.set("expireDate",expireDate);
        // partyContact.set("partyId",partyId);
        // partyContact.set("contactId",newCid);

        // telNum.set("contactId",newCid);
        // telNum.set("contactId",contactNumber);

        // postalAdd.set("contactId",newCid);
        // postalAdd.set("address1",address1);


        // }else{
        //     profile.set("photo", "awdawdawd");
        // }
        
        try {
            // EntityFindOptions findOpts = new EntityFindOptions(true,
            //         EntityFindOptions.TYPE_SCROLL_INSENSITIVE,
            //         EntityFindOptions.CONCUR_UPDATABLE, true);
            // EntityListIterator el = ((EntityListIterator)delegator.find("RequestUpdatePerson",null,null,null,null,null));            
            //el.add(profile);
            // el.getCompleteList().add(profile);
            delegator.create(profile);
            delegator.create(requestVisa);

            // delegator.create(partyContact);
            // delegator.create(telNum);
            // delegator.create(postalAdd);
            // profile.store();
            //((GenericValue)delegator.findOne("RequestUpdatePerson")).create(profile);

        } catch (GenericEntityException e) {
            Debug.logWarning(e.getMessage(), module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    e.getMessage(), new Object[] { e.getMessage() }, locale));
        }

        result.put(ModelService.RESPONSE_MESSAGE, ModelService.RESPOND_SUCCESS);
        result.put(ModelService.SUCCESS_MESSAGE, 
                UtilProperties.getMessage(resourceError, fileNameDataProfile, locale));
        return result;
    }
}