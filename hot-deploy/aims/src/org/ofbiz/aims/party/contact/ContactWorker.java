/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *******************************************************************************/

package org.ofbiz.aims.party.contact;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletRequest;

import org.ofbiz.base.util.Assert;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.util.EntityQuery;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.entity.util.EntityUtilProperties;

/**
 * Worker methods for Contact anisms
 */
public class ContactWorker {

    public static final String module = ContactWorker.class.getName();

    private ContactWorker() {}

    public static List<Map<String, Object>> getPartyContactValueMaps(Delegator delegator, String partyId, boolean showOld) {
       return getPartyContactValueMaps(delegator, partyId, showOld, null);
    }

    public static List<Map<String, Object>> getPartyContactValueMaps(Delegator delegator, String partyId, boolean showOld, String contactTypeId) {
        List<Map<String, Object>> partyContactValueMaps = new LinkedList<Map<String,Object>>();

        List<GenericValue> allPartyContacts = null;

        try {
            List<GenericValue> tempCol = EntityQuery.use(delegator).from("PartyContact").where("partyId", partyId).queryList();
            if (contactTypeId != null) {
                List<GenericValue> tempColTemp = new LinkedList<GenericValue>();
                for (GenericValue partyContact: tempCol) {
                    GenericValue contact = delegator.getRelatedOne("Contact", partyContact, false);
                    if (contact != null && contactTypeId.equals(contact.getString("contactTypeId"))) {
                        tempColTemp.add(partyContact);
                    }

                }
                tempCol = tempColTemp;
            }
            if (!showOld) tempCol = EntityUtil.filterByDate(tempCol, true);
            allPartyContacts = tempCol;
        } catch (GenericEntityException e) {
            Debug.logWarning(e, module);
        }

        if (allPartyContacts == null) return partyContactValueMaps;

        for (GenericValue partyContact: allPartyContacts) {
            GenericValue contact = null;

            try {
                contact = partyContact.getRelatedOne("Contact", false);
            } catch (GenericEntityException e) {
                Debug.logWarning(e, module);
            }
            if (contact != null) {
                Map<String, Object> partyContactValueMap = new HashMap<String, Object>();

                partyContactValueMaps.add(partyContactValueMap);
                partyContactValueMap.put("contact", contact);
                partyContactValueMap.put("partyContact", partyContact);

                try {
                    partyContactValueMap.put("contactType", contact.getRelatedOne("ContactType", true));
                } catch (GenericEntityException e) {
                    Debug.logWarning(e, module);
                }

                try {
                    List<GenericValue> partyContactPurposes = partyContact.getRelated("PartyContactPurpose", null, null, false);

                    if (!showOld) partyContactPurposes = EntityUtil.filterByDate(partyContactPurposes, true);
                    partyContactValueMap.put("partyContactPurposes", partyContactPurposes);
                } catch (GenericEntityException e) {
                    Debug.logWarning(e, module);
                }

                try {
                    if ("POSTAL_ADDRESS".equals(contact.getString("contactTypeId"))) {
                        partyContactValueMap.put("postalAddress", contact.getRelatedOne("PostalAddress", false));
                    } else if ("TELECOM_NUMBER".equals(contact.getString("contactTypeId"))) {
                        partyContactValueMap.put("telecomNumber", contact.getRelatedOne("TelecomNumber", false));
                    }
                } catch (GenericEntityException e) {
                    Debug.logWarning(e, module);
                }
            }
        }

        return partyContactValueMaps;
    }

    public static List<Map<String, Object>> getFacilityContactValueMaps(Delegator delegator, String facilityId, boolean showOld, String contactTypeId) {
        List<Map<String, Object>> facilityContactValueMaps = new LinkedList<Map<String,Object>>();

        List<GenericValue> allFacilityContacts = null;

        try {
            List<GenericValue> tempCol = EntityQuery.use(delegator).from("FacilityContact").where("facilityId", facilityId).queryList();
            if (contactTypeId != null) {
                List<GenericValue> tempColTemp = new LinkedList<GenericValue>();
                for (GenericValue partyContact: tempCol) {
                    GenericValue contact = delegator.getRelatedOne("Contact", partyContact, false);
                    if (contact != null && contactTypeId.equals(contact.getString("contactTypeId"))) {
                        tempColTemp.add(partyContact);
                    }

                }
                tempCol = tempColTemp;
            }
            if (!showOld) tempCol = EntityUtil.filterByDate(tempCol, true);
            allFacilityContacts = tempCol;
        } catch (GenericEntityException e) {
            Debug.logWarning(e, module);
        }

        if (allFacilityContacts == null) return facilityContactValueMaps;

        for (GenericValue facilityContact: allFacilityContacts) {
            GenericValue contact = null;

            try {
                contact = facilityContact.getRelatedOne("Contact", false);
            } catch (GenericEntityException e) {
                Debug.logWarning(e, module);
            }
            if (contact != null) {
                Map<String, Object> facilityContactValueMap = new HashMap<String, Object>();

                facilityContactValueMaps.add(facilityContactValueMap);
                facilityContactValueMap.put("contact", contact);
                facilityContactValueMap.put("facilityContact", facilityContact);

                try {
                    facilityContactValueMap.put("contactType", contact.getRelatedOne("ContactType", true));
                } catch (GenericEntityException e) {
                    Debug.logWarning(e, module);
                }

                try {
                    List<GenericValue> facilityContactPurposes = facilityContact.getRelated("FacilityContactPurpose", null, null, false);

                    if (!showOld) facilityContactPurposes = EntityUtil.filterByDate(facilityContactPurposes, true);
                    facilityContactValueMap.put("facilityContactPurposes", facilityContactPurposes);
                } catch (GenericEntityException e) {
                    Debug.logWarning(e, module);
                }

                try {
                    if ("POSTAL_ADDRESS".equals(contact.getString("contactTypeId"))) {
                        facilityContactValueMap.put("postalAddress", contact.getRelatedOne("PostalAddress", false));
                    } else if ("TELECOM_NUMBER".equals(contact.getString("contactTypeId"))) {
                        facilityContactValueMap.put("telecomNumber", contact.getRelatedOne("TelecomNumber", false));
                    }
                } catch (GenericEntityException e) {
                    Debug.logWarning(e, module);
                }
            }
        }

        return facilityContactValueMaps;
    }


    public static List<Map<String, GenericValue>> getOrderContactValueMaps(Delegator delegator, String orderId) {
        List<Map<String, GenericValue>> orderContactValueMaps = new LinkedList<Map<String,GenericValue>>();

        List<GenericValue> allOrderContacts = null;

        try {
            allOrderContacts = EntityQuery.use(delegator).from("OrderContact")
                    .where("orderId", orderId)
                    .orderBy("contactPurposeTypeId")
                    .queryList();
        } catch (GenericEntityException e) {
            Debug.logWarning(e, module);
        }

        if (allOrderContacts == null) return orderContactValueMaps;

        for (GenericValue orderContact: allOrderContacts) {
            GenericValue contact = null;

            try {
                contact = orderContact.getRelatedOne("Contact", false);
            } catch (GenericEntityException e) {
                Debug.logWarning(e, module);
            }
            if (contact != null) {
                Map<String, GenericValue> orderContactValueMap = new HashMap<String, GenericValue>();

                orderContactValueMaps.add(orderContactValueMap);
                orderContactValueMap.put("contact", contact);
                orderContactValueMap.put("orderContact", orderContact);

                try {
                    orderContactValueMap.put("contactType", contact.getRelatedOne("ContactType", true));
                } catch (GenericEntityException e) {
                    Debug.logWarning(e, module);
                }

                try {
                    GenericValue contactPurposeType = orderContact.getRelatedOne("ContactPurposeType", false);

                    orderContactValueMap.put("contactPurposeType", contactPurposeType);
                } catch (GenericEntityException e) {
                    Debug.logWarning(e, module);
                }

                try {
                    if ("POSTAL_ADDRESS".equals(contact.getString("contactTypeId"))) {
                        orderContactValueMap.put("postalAddress", contact.getRelatedOne("PostalAddress", false));
                    } else if ("TELECOM_NUMBER".equals(contact.getString("contactTypeId"))) {
                        orderContactValueMap.put("telecomNumber", contact.getRelatedOne("TelecomNumber", false));
                    }
                } catch (GenericEntityException e) {
                    Debug.logWarning(e, module);
                }
            }
        }

        return orderContactValueMaps;
    }

    public static Collection<Map<String, GenericValue>> getWorkEffortContactValueMaps(Delegator delegator, String workEffortId) {
        Collection<Map<String, GenericValue>> workEffortContactValueMaps = new LinkedList<Map<String,GenericValue>>();

        List<GenericValue> allWorkEffortContacts = null;

        try {
            allWorkEffortContacts = EntityQuery.use(delegator).from("WorkEffortContact")
                    .where("workEffortId", workEffortId)
                    .filterByDate()
                    .queryList();
        } catch (GenericEntityException e) {
            Debug.logWarning(e, module);
        }

        if (allWorkEffortContacts == null) return null;

        for (GenericValue workEffortContact: allWorkEffortContacts) {
            GenericValue contact = null;

            try {
                contact = workEffortContact.getRelatedOne("Contact", false);
            } catch (GenericEntityException e) {
                Debug.logWarning(e, module);
            }
            if (contact != null) {
                Map<String, GenericValue> workEffortContactValueMap = new HashMap<String, GenericValue>();

                workEffortContactValueMaps.add(workEffortContactValueMap);
                workEffortContactValueMap.put("contact", contact);
                workEffortContactValueMap.put("workEffortContact", workEffortContact);

                try {
                    workEffortContactValueMap.put("contactType", contact.getRelatedOne("ContactType", true));
                } catch (GenericEntityException e) {
                    Debug.logWarning(e, module);
                }

                try {
                    if ("POSTAL_ADDRESS".equals(contact.getString("contactTypeId"))) {
                        workEffortContactValueMap.put("postalAddress", contact.getRelatedOne("PostalAddress", false));
                    } else if ("TELECOM_NUMBER".equals(contact.getString("contactTypeId"))) {
                        workEffortContactValueMap.put("telecomNumber", contact.getRelatedOne("TelecomNumber", false));
                    }
                } catch (GenericEntityException e) {
                    Debug.logWarning(e, module);
                }
            }
        }

        return workEffortContactValueMaps.size() > 0 ? workEffortContactValueMaps : null;
    }

    public static void getContactAndRelated(ServletRequest request, String partyId, Map<String, Object> target) {
        Delegator delegator = (Delegator) request.getAttribute("delegator");

        boolean tryEntity = true;
        if (request.getAttribute("_ERROR_MESSAGE_") != null) tryEntity = false;
        if ("true".equals(request.getParameter("tryEntity"))) tryEntity = true;

        String donePage = request.getParameter("DONE_PAGE");
        if (donePage == null) donePage = (String) request.getAttribute("DONE_PAGE");
        if (donePage == null || donePage.length() <= 0) donePage = "viewprofile";
        target.put("donePage", donePage);

        String contactTypeId = request.getParameter("preContactTypeId");

        if (contactTypeId == null) contactTypeId = (String) request.getAttribute("preContactTypeId");
        if (contactTypeId != null)
            tryEntity = false;

        String contactId = request.getParameter("contactId");

        if (request.getAttribute("contactId") != null)
            contactId = (String) request.getAttribute("contactId");

        GenericValue contact = null;

        if (contactId != null) {
            target.put("contactId", contactId);

            // try to find a PartyContact with a valid date range
            List<GenericValue> partyContacts = null;

            try {
                partyContacts = EntityQuery.use(delegator).from("PartyContact")
                        .where("partyId", partyId, "contactId", contactId)
                        .filterByDate()
                        .queryList();
            } catch (GenericEntityException e) {
                Debug.logWarning(e, module);
            }

            GenericValue partyContact = EntityUtil.getFirst(partyContacts);

            if (partyContact != null) {
                target.put("partyContact", partyContact);

                Collection<GenericValue> partyContactPurposes = null;

                try {
                    partyContactPurposes = EntityUtil.filterByDate(partyContact.getRelated("PartyContactPurpose", null, null, false), true);
                } catch (GenericEntityException e) {
                    Debug.logWarning(e, module);
                }
                if (UtilValidate.isNotEmpty(partyContactPurposes))
                    target.put("partyContactPurposes", partyContactPurposes);
            }

            try {
                contact = EntityQuery.use(delegator).from("Contact").where("contactId", contactId).queryOne();
            } catch (GenericEntityException e) {
                Debug.logWarning(e, module);
            }

            if (contact != null) {
                target.put("contact", contact);
                contactTypeId = contact.getString("contactTypeId");
            }
        }

        if (contactTypeId != null) {
            target.put("contactTypeId", contactTypeId);

            try {
                GenericValue contactType = EntityQuery.use(delegator).from("ContactType").where("contactTypeId", contactTypeId).queryOne();

                if (contactType != null)
                    target.put("contactType", contactType);
            } catch (GenericEntityException e) {
                Debug.logWarning(e, module);
            }

            Collection<GenericValue> purposeTypes = new LinkedList<GenericValue>();
            Iterator<GenericValue> typePurposes = null;

            try {
                typePurposes = UtilMisc.toIterator(EntityQuery.use(delegator).from("ContactTypePurpose")
                        .where("contactTypeId", contactTypeId)
                        .queryList());
            } catch (GenericEntityException e) {
                Debug.logWarning(e, module);
            }
            while (typePurposes != null && typePurposes.hasNext()) {
                GenericValue contactTypePurpose = typePurposes.next();
                GenericValue contactPurposeType = null;

                try {
                    contactPurposeType = contactTypePurpose.getRelatedOne("ContactPurposeType", false);
                } catch (GenericEntityException e) {
                    Debug.logWarning(e, module);
                }
                if (contactPurposeType != null) {
                    purposeTypes.add(contactPurposeType);
                }
            }
            if (purposeTypes.size() > 0)
                target.put("purposeTypes", purposeTypes);
        }

        String requestName;

        if (contact == null) {
            // create
            if ("POSTAL_ADDRESS".equals(contactTypeId)) {
                if (request.getParameter("contactPurposeTypeId") != null || request.getAttribute("contactPurposeTypeId") != null) {
                    requestName = "createPostalAddressAndPurpose";
                } else {
                    requestName = "createPostalAddress";
                }
            } else if ("TELECOM_NUMBER".equals(contactTypeId)) {
                requestName = "createTelecomNumber";
            } else if ("EMAIL_ADDRESS".equals(contactTypeId)) {
                requestName = "createEmailAddress";
            } else {
                requestName = "createContact";
            }
        } else {
            // update
            if ("POSTAL_ADDRESS".equals(contactTypeId)) {
                requestName = "updatePostalAddress";
            } else if ("TELECOM_NUMBER".equals(contactTypeId)) {
                requestName = "updateTelecomNumber";
            } else if ("EMAIL_ADDRESS".equals(contactTypeId)) {
                requestName = "updateEmailAddress";
            } else {
                requestName = "updateContact";
            }
        }
        target.put("requestName", requestName);

        if ("POSTAL_ADDRESS".equals(contactTypeId)) {
            GenericValue postalAddress = null;

            try {
                if (contact != null) postalAddress = contact.getRelatedOne("PostalAddress", false);
            } catch (GenericEntityException e) {
                Debug.logWarning(e, module);
            }
            if (postalAddress != null) target.put("postalAddress", postalAddress);
        } else if ("TELECOM_NUMBER".equals(contactTypeId)) {
            GenericValue telecomNumber = null;

            try {
                if (contact != null) telecomNumber = contact.getRelatedOne("TelecomNumber", false);
            } catch (GenericEntityException e) {
                Debug.logWarning(e, module);
            }
            if (telecomNumber != null) target.put("telecomNumber", telecomNumber);
        }

        if ("true".equals(request.getParameter("useValues"))) tryEntity = true;
        target.put("tryEntity", Boolean.valueOf(tryEntity));

        try {
            Collection<GenericValue> contactTypes = EntityQuery.use(delegator).from("ContactType").cache(true).queryList();

            if (contactTypes != null) {
                target.put("contactTypes", contactTypes);
            }
        } catch (GenericEntityException e) {
            Debug.logWarning(e, module);
        }
    }

    /** Returns the first valid FacilityContact found based on the given facilityId and a prioritized list of purposes
     * @param delegator the delegator
     * @param facilityId the facility id
     * @param purposeTypes a List of ContactPurposeType ids which will be checked one at a time until a valid contact  is found
     * @return returns the first valid FacilityContact found based on the given facilityId and a prioritized list of purposes
     */
    public static GenericValue getFacilityContactByPurpose(Delegator delegator, String facilityId, List<String> purposeTypes) {
        if (UtilValidate.isEmpty(facilityId)) return null;
        if (UtilValidate.isEmpty(purposeTypes)) return null;

        for (String purposeType: purposeTypes) {
            List<GenericValue> facilityContactPurposes = null;
            List<EntityCondition> conditionList = new LinkedList<EntityCondition>();
            conditionList.add(EntityCondition.makeCondition("facilityId", facilityId));
            conditionList.add(EntityCondition.makeCondition("contactPurposeTypeId", purposeType));
            EntityCondition entityCondition = EntityCondition.makeCondition(conditionList);
            try {
                facilityContactPurposes = EntityQuery.use(delegator).from("FacilityContactPurpose")
                        .where(entityCondition)
                        .orderBy("-fromDate")
                        .cache(true)
                        .filterByDate()
                        .queryList();
            } catch (GenericEntityException e) {
                Debug.logWarning(e, module);
                continue;
            }
            for (GenericValue facilityContactPurpose: facilityContactPurposes) {
                String contactId = facilityContactPurpose.getString("contactId");
                List<GenericValue> facilityContacts = null;
                conditionList = new LinkedList<EntityCondition>();
                conditionList.add(EntityCondition.makeCondition("facilityId", facilityId));
                conditionList.add(EntityCondition.makeCondition("contactId", contactId));
                entityCondition = EntityCondition.makeCondition(conditionList);
                try {
                    facilityContacts = EntityQuery.use(delegator).from("FacilityContact")
                            .where(entityCondition)
                            .orderBy("-fromDate")
                            .cache(true)
                            .filterByDate()
                            .queryList();
                } catch (GenericEntityException e) {
                    Debug.logWarning(e, module);
                }
                if (UtilValidate.isNotEmpty(facilityContacts)) {
                    return EntityUtil.getFirst(facilityContacts);
                }
            }

        }
        return null;
    }

    public static void getFacilityContactAndRelated(ServletRequest request, String facilityId, Map<String, Object> target) {
        Delegator delegator = (Delegator) request.getAttribute("delegator");

        boolean tryEntity = true;
        if (request.getAttribute("_ERROR_MESSAGE") != null) tryEntity = false;
        if ("true".equals(request.getParameter("tryEntity"))) tryEntity = true;

        String donePage = request.getParameter("DONE_PAGE");
        if (donePage == null) donePage = (String) request.getAttribute("DONE_PAGE");
        if (donePage == null || donePage.length() <= 0) donePage = "viewprofile";
        target.put("donePage", donePage);

        String contactTypeId = request.getParameter("preContactTypeId");

        if (contactTypeId == null) contactTypeId = (String) request.getAttribute("preContactTypeId");
        if (contactTypeId != null)
            tryEntity = false;

        String contactId = request.getParameter("contactId");

        if (request.getAttribute("contactId") != null)
            contactId = (String) request.getAttribute("contactId");

        GenericValue contact = null;

        if (contactId != null) {
            target.put("contactId", contactId);

            // try to find a PartyContact with a valid date range
            List<GenericValue> facilityContacts = null;

            try {
                facilityContacts = EntityQuery.use(delegator).from("FacilityContact")
                        .where("facilityId", facilityId, "contactId", contactId)
                        .filterByDate()
                        .queryList();
            } catch (GenericEntityException e) {
                Debug.logWarning(e, module);
            }

            GenericValue facilityContact = EntityUtil.getFirst(facilityContacts);

            if (facilityContact != null) {
                target.put("facilityContact", facilityContact);

                Collection<GenericValue> facilityContactPurposes = null;

                try {
                    facilityContactPurposes = EntityUtil.filterByDate(facilityContact.getRelated("FacilityContactPurpose", null, null, false), true);
                } catch (GenericEntityException e) {
                    Debug.logWarning(e, module);
                }
                if (UtilValidate.isNotEmpty(facilityContactPurposes))
                    target.put("facilityContactPurposes", facilityContactPurposes);
            }

            try {
                contact = EntityQuery.use(delegator).from("Contact").where("contactId", contactId).queryOne();
            } catch (GenericEntityException e) {
                Debug.logWarning(e, module);
            }

            if (contact != null) {
                target.put("contact", contact);
                contactTypeId = contact.getString("contactTypeId");
            }
        }

        if (contactTypeId != null) {
            target.put("contactTypeId", contactTypeId);

            try {
                GenericValue contactType = EntityQuery.use(delegator).from("ContactType").where("contactTypeId", contactTypeId).queryOne();

                if (contactType != null)
                    target.put("contactType", contactType);
            } catch (GenericEntityException e) {
                Debug.logWarning(e, module);
            }

            Collection<GenericValue> purposeTypes = new LinkedList<GenericValue>();
            Iterator<GenericValue> typePurposes = null;

            try {
                typePurposes = UtilMisc.toIterator(EntityQuery.use(delegator).from("ContactTypePurpose")
                        .where("contactTypeId", contactTypeId)
                        .queryList());
            } catch (GenericEntityException e) {
                Debug.logWarning(e, module);
            }
            while (typePurposes != null && typePurposes.hasNext()) {
                GenericValue contactTypePurpose = typePurposes.next();
                GenericValue contactPurposeType = null;

                try {
                    contactPurposeType = contactTypePurpose.getRelatedOne("ContactPurposeType", false);
                } catch (GenericEntityException e) {
                    Debug.logWarning(e, module);
                }
                if (contactPurposeType != null) {
                    purposeTypes.add(contactPurposeType);
                }
            }
            if (purposeTypes.size() > 0)
                target.put("purposeTypes", purposeTypes);
        }

        String requestName;

        if (contact == null) {
            // create
            if ("POSTAL_ADDRESS".equals(contactTypeId)) {
                if (request.getParameter("contactPurposeTypeId") != null || request.getAttribute("contactPurposeTypeId") != null) {
                    requestName = "createPostalAddressAndPurpose";
                } else {
                    requestName = "createPostalAddress";
                }
            } else if ("TELECOM_NUMBER".equals(contactTypeId)) {
                requestName = "createTelecomNumber";
            } else if ("EMAIL_ADDRESS".equals(contactTypeId)) {
                requestName = "createEmailAddress";
            } else {
                requestName = "createContact";
            }
        } else {
            // update
            if ("POSTAL_ADDRESS".equals(contactTypeId)) {
                requestName = "updatePostalAddress";
            } else if ("TELECOM_NUMBER".equals(contactTypeId)) {
                requestName = "updateTelecomNumber";
            } else if ("EMAIL_ADDRESS".equals(contactTypeId)) {
                requestName = "updateEmailAddress";
            } else {
                requestName = "updateContact";
            }
        }
        target.put("requestName", requestName);

        if ("POSTAL_ADDRESS".equals(contactTypeId)) {
            GenericValue postalAddress = null;

            try {
                if (contact != null) postalAddress = contact.getRelatedOne("PostalAddress", false);
            } catch (GenericEntityException e) {
                Debug.logWarning(e, module);
            }
            if (postalAddress != null) target.put("postalAddress", postalAddress);
        } else if ("TELECOM_NUMBER".equals(contactTypeId)) {
            GenericValue telecomNumber = null;

            try {
                if (contact != null) telecomNumber = contact.getRelatedOne("TelecomNumber", false);
            } catch (GenericEntityException e) {
                Debug.logWarning(e, module);
            }
            if (telecomNumber != null) target.put("telecomNumber", telecomNumber);
        }

        if ("true".equals(request.getParameter("useValues"))) tryEntity = true;
        target.put("tryEntity", Boolean.valueOf(tryEntity));

        try {
            Collection<GenericValue> contactTypes = EntityQuery.use(delegator).from("ContactType").cache(true).queryList();

            if (contactTypes != null) {
                target.put("contactTypes", contactTypes);
            }
        } catch (GenericEntityException e) {
            Debug.logWarning(e, module);
        }
    }

    public static List<Map<String, Object>> getPartyPostalAddresses(ServletRequest request, String partyId, String curContactId) {
        Delegator delegator = (Delegator) request.getAttribute("delegator");
        List<Map<String, Object>> postalAddressInfos = new LinkedList<Map<String,Object>>();

        List<GenericValue> allPartyContacts = null;

        try {
            allPartyContacts = EntityQuery.use(delegator).from("PartyContact").where("partyId", partyId).filterByDate().queryList();
        } catch (GenericEntityException e) {
            Debug.logWarning(e, module);
        }

        if (allPartyContacts == null) return postalAddressInfos;

        for (GenericValue partyContact: allPartyContacts) {
            GenericValue contact = null;

            try {
                contact = partyContact.getRelatedOne("Contact", false);
            } catch (GenericEntityException e) {
                Debug.logWarning(e, module);
            }
            if (contact != null && "POSTAL_ADDRESS".equals(contact.getString("contactTypeId")) && !contact.getString("contactId").equals(curContactId)) {
                Map<String, Object> postalAddressInfo = new HashMap<String, Object>();

                postalAddressInfos.add(postalAddressInfo);
                postalAddressInfo.put("contact", contact);
                postalAddressInfo.put("partyContact", partyContact);

                try {
                    GenericValue postalAddress = contact.getRelatedOne("PostalAddress", false);
                    postalAddressInfo.put("postalAddress", postalAddress);
                } catch (GenericEntityException e) {
                    Debug.logWarning(e, module);
                }

                try {
                    List<GenericValue> partyContactPurposes = EntityUtil.filterByDate(partyContact.getRelated("PartyContactPurpose", null, null, false), true);
                    postalAddressInfo.put("partyContactPurposes", partyContactPurposes);
                } catch (GenericEntityException e) {
                    Debug.logWarning(e, module);
                }
            }
        }

        return postalAddressInfos;
    }

    public static Map<String, Object> getCurrentPostalAddress(ServletRequest request, String partyId, String curContactId) {
        Delegator delegator = (Delegator) request.getAttribute("delegator");
        Map<String, Object> results = new HashMap<String, Object>();

        if (curContactId != null) {
            List<GenericValue> partyContacts = null;

            try {
                partyContacts = EntityQuery.use(delegator).from("PartyContact")
                        .where("partyId", partyId, "contactId", curContactId)
                        .filterByDate()
                        .queryList();
            } catch (GenericEntityException e) {
                Debug.logWarning(e, module);
            }
            GenericValue curPartyContact = EntityUtil.getFirst(partyContacts);
            results.put("curPartyContact", curPartyContact);

            GenericValue curContact = null;
            if (curPartyContact != null) {
                try {
                    curContact = curPartyContact.getRelatedOne("Contact", false);
                } catch (GenericEntityException e) {
                    Debug.logWarning(e, module);
                }

                Collection<GenericValue> curPartyContactPurposes = null;
                try {
                    curPartyContactPurposes = EntityUtil.filterByDate(curPartyContact.getRelated("PartyContactPurpose", null, null, false), true);
                } catch (GenericEntityException e) {
                    Debug.logWarning(e, module);
                }
                results.put("curPartyContactPurposes", curPartyContactPurposes);
            }
            results.put("curContact", curContact);

            GenericValue curPostalAddress = null;
            if (curContact != null) {
                try {
                    curPostalAddress = curContact.getRelatedOne("PostalAddress", false);
                } catch (GenericEntityException e) {
                    Debug.logWarning(e, module);
                }
            }

            results.put("curPostalAddress", curPostalAddress);
        }
        return results;
    }

    public static boolean isUspsAddress(GenericValue postalAddress) {
        if (postalAddress == null) {
            // null postal address is not a USPS address
            return false;
        }
        if (!"PostalAddress".equals(postalAddress.getEntityName())) {
            // not a postal address not a USPS address
            return false;
        }

        // get and clean the address strings
        String addr1 = postalAddress.getString("address1");
        String addr2 = postalAddress.getString("address2");

        // get the matching string from general.properties
        String matcher = EntityUtilProperties.getPropertyValue("general.properties", "usps.address.match", postalAddress.getDelegator());
        if (UtilValidate.isNotEmpty(matcher)) {
            if (addr1 != null && addr1.toLowerCase().matches(matcher)) {
                return true;
            }
            if (addr2 != null && addr2.toLowerCase().matches(matcher)) {
                return true;
            }
        }

        return false;
    }

    public static boolean isCompanyAddress(GenericValue postalAddress, String companyPartyId) {
        if (postalAddress == null) {
            // null postal address is not an internal address
            return false;
        }
        if (!"PostalAddress".equals(postalAddress.getEntityName())) {
            // not a postal address not an internal address
            return false;
        }
        if (companyPartyId == null) {
            // no partyId not an internal address
            return false;
        }

        String state = postalAddress.getString("stateProvinceGeoId");
        String addr1 = postalAddress.getString("address1");
        String addr2 = postalAddress.getString("address2");
        if (state != null) {
            state = state.replaceAll("\\W", "").toLowerCase();
        } else {
            state = "";
        }
        if (addr1 != null) {
            addr1 = addr1.replaceAll("\\W", "").toLowerCase();
        } else {
            addr1 = "";
        }
        if (addr2 != null) {
            addr2 = addr2.replaceAll("\\W", "").toLowerCase();
        } else {
            addr2 = "";
        }

        // get all company addresses
        Delegator delegator = postalAddress.getDelegator();
        List<GenericValue> postalAddresses = new LinkedList<GenericValue>();
        try {
            List<GenericValue> partyContacts = EntityQuery.use(delegator).from("PartyContact")
                    .where("partyId", companyPartyId)
                    .filterByDate()
                    .queryList();
            if (partyContacts != null) {
                for (GenericValue pcm: partyContacts) {
                    GenericValue addr = pcm.getRelatedOne("PostalAddress", false);
                    if (addr != null) {
                        postalAddresses.add(addr);
                    }
                }
            }
        } catch (GenericEntityException e) {
            Debug.logError(e, "Unable to get party postal addresses", module);
        }

        if (postalAddresses != null) {
            for (GenericValue addr: postalAddresses) {
                String thisAddr1 = addr.getString("address1");
                String thisAddr2 = addr.getString("address2");
                String thisState = addr.getString("stateProvinceGeoId");
                if (thisState != null) {
                    thisState = thisState.replaceAll("\\W", "").toLowerCase();
                } else {
                    thisState = "";
                }
                if (thisAddr1 != null) {
                    thisAddr1 = thisAddr1.replaceAll("\\W", "").toLowerCase();
                } else {
                    thisAddr1 = "";
                }
                if (thisAddr2 != null) {
                    thisAddr2 = thisAddr2.replaceAll("\\W", "").toLowerCase();
                } else {
                    thisAddr2 = "";
                }
                if (thisAddr1.equals(addr1) && thisAddr2.equals(addr2) && thisState.equals(state)) {
                    return true;
                }
            }
        }

        return false;
    }

    public static String getContactAttribute(Delegator delegator, String contactId, String attrName) {
        GenericValue attr = null;
        try {
            attr = EntityQuery.use(delegator).from("ContactAttribute").where("contactId", contactId, "attrName", attrName).queryOne();
        } catch (GenericEntityException e) {
            Debug.logError(e, module);
        }
        if (attr == null) {
            return null;
        } else {
            return attr.getString("attrValue");
        }
    }

    public static String getPostalAddressPostalCodeGeoId(GenericValue postalAddress, Delegator delegator) throws GenericEntityException {
        // if postalCodeGeoId not empty use that
        if (UtilValidate.isNotEmpty(postalAddress.getString("postalCodeGeoId"))) {
            return postalAddress.getString("postalCodeGeoId");
        }

        // no postalCodeGeoId, see if there is a Geo record matching the countryGeoId and postalCode fields
        if (UtilValidate.isNotEmpty(postalAddress.getString("countryGeoId")) && UtilValidate.isNotEmpty(postalAddress.getString("postalCode"))) {
            // first try the shortcut with the geoId convention for "{countryGeoId}-{postalCode}"
            GenericValue geo = EntityQuery.use(delegator).from("Geo").where("geoId", postalAddress.getString("countryGeoId") + "-" + postalAddress.getString("postalCode")).cache().queryOne();
            if (geo != null) {
                // save the value to the database for quicker future reference
                if (postalAddress.isMutable()) {
                    postalAddress.set("postalCodeGeoId", geo.getString("geoId"));
                    postalAddress.store();
                } else {
                    GenericValue mutablePostalAddress = EntityQuery.use(delegator).from("PostalAddress").where("contactId", postalAddress.getString("contactId")).queryOne();
                    mutablePostalAddress.set("postalCodeGeoId", geo.getString("geoId"));
                    mutablePostalAddress.store();
                }

                return geo.getString("geoId");
            }

            // no shortcut, try the longcut to see if there is something with a geoCode associated to the countryGeoId
            GenericValue geoAssocAndGeoTo = EntityQuery.use(delegator).from("GeoAssocAndGeoTo")
                    .where("geoIdFrom", postalAddress.getString("countryGeoId"), "geoCode", postalAddress.getString("postalCode"), "geoAssocTypeId", "REGIONS")
                    .cache(true)
                    .queryFirst();
            if (geoAssocAndGeoTo != null) {
                // save the value to the database for quicker future reference
                if (postalAddress.isMutable()) {
                    postalAddress.set("postalCodeGeoId", geoAssocAndGeoTo.getString("geoId"));
                    postalAddress.store();
                } else {
                    GenericValue mutablePostalAddress = EntityQuery.use(delegator).from("PostalAddress").where("contactId", postalAddress.getString("contactId")).queryOne();
                    mutablePostalAddress.set("postalCodeGeoId", geoAssocAndGeoTo.getString("geoId"));
                    mutablePostalAddress.store();
                }

                return geoAssocAndGeoTo.getString("geoId");
            }
        }

        // nothing found, return null
        return null;
    }

    /**
     * Returns a <b>PostalAddress</b> <code>GenericValue</code> as a URL encoded <code>String</code>.
     * 
     * @param postalAddress A <b>PostalAddress</b> <code>GenericValue</code>.
     * @return A URL encoded <code>String</code>.
     * @throws GenericEntityException
     * @throws UnsupportedEncodingException
     */
    public static String urlEncodePostalAddress(GenericValue postalAddress) throws GenericEntityException, UnsupportedEncodingException {
        Assert.notNull("postalAddress", postalAddress);
        if (!"PostalAddress".equals(postalAddress.getEntityName())) {
            throw new IllegalArgumentException("postalAddress argument is not a PostalAddress entity");
        }
        StringBuilder sb = new StringBuilder();
        if (postalAddress.get("address1") != null) {
            sb.append(postalAddress.get("address1"));
        }
        if (postalAddress.get("address2") != null) {
            sb.append(", ").append(postalAddress.get("address2"));
        }
        if (postalAddress.get("city") != null) {
            sb.append(", ").append(postalAddress.get("city"));
        }
        if (postalAddress.get("stateProvinceGeoId") != null) {
            GenericValue geoValue = postalAddress.getRelatedOne("StateProvinceGeo", false);
            if (geoValue != null) {
                sb.append(", ").append(geoValue.get("geoName"));
            }
        } else if (postalAddress.get("countyGeoId") != null) {
            GenericValue geoValue = postalAddress.getRelatedOne("CountyGeo", false);
            if (geoValue != null) {
                sb.append(", ").append(geoValue.get("geoName"));
            }
        }
        if (postalAddress.get("postalCode") != null) {
            sb.append(", ").append(postalAddress.get("postalCode"));
        }
        if (postalAddress.get("countryGeoId") != null) {
            GenericValue geoValue = postalAddress.getRelatedOne("CountryGeo", false);
            if (geoValue != null) {
                sb.append(", ").append(geoValue.get("geoName"));
            }
        }
        String postalAddressString = sb.toString().trim();
        while (postalAddressString.contains("  ")) {
            postalAddressString = postalAddressString.replace("  ", " ");
        }
        return URLEncoder.encode(postalAddressString, "UTF-8");
    }
}
