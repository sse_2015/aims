/*******************************************************************************
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *******************************************************************************/

package org.ofbiz.aims.party.contact;

import java.security.SecureRandom;
import java.sql.Timestamp;

import com.ibm.icu.util.Calendar;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Locale;

import org.ofbiz.base.crypto.HashCrypt;
import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilGenerics;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilProperties;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.util.EntityQuery;
import org.ofbiz.entity.util.EntityUtilProperties;
import org.ofbiz.security.Security;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.ModelService;
import org.ofbiz.service.ServiceUtil;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.GenericServiceException;


/**
 * Services for Contact maintenance
 */
public class ContactServices {

    public static final String module = ContactServices.class.getName();
    public static final String resource = "PartyUiLabels";
    public static final String resourceError = "PartyErrorUiLabels";

    /**
     * Creates a Contact
     * <b>security check</b>: userLogin partyId must equal partyId, or must have PARTYMGR_CREATE permission
     *@param ctx The DispatchContext that this service is operating in
     *@param context Map containing the input parameters
     *@return Map with the result of the service, the output parameters
     */
    public static Map<String, Object> createContact(DispatchContext ctx, Map<String, ? extends Object> context) {
        Map<String, Object> result = new HashMap<String, Object>();
        Delegator delegator = ctx.getDelegator();
        Security security = ctx.getSecurity();
        GenericValue userLogin = (GenericValue) context.get("userLogin");
        Locale locale = (Locale) context.get("locale");
        Timestamp now = UtilDateTime.nowTimestamp();
        List<GenericValue> toBeStored = new LinkedList<GenericValue>();

        String partyId = ServiceUtil.getPartyIdCheckSecurity(userLogin, security, context, result, "PARTYMGR", "_PCM_CREATE");

        if (result.size() > 0)
            return result;

        String contactTypeId = (String) context.get("contactTypeId");

        String newCmId = null;
        try {
            newCmId = delegator.getNextSeqId("Contact");
        } catch (IllegalArgumentException e) {
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "contactservices.could_not_create_contact_info_id_generation_failure", locale));
        }

        GenericValue tempContact = delegator.makeValue("Contact", UtilMisc.toMap("contactId", newCmId, "contactTypeId", contactTypeId));
        toBeStored.add(tempContact);

        if (!partyId.equals("_NA_")) {
            toBeStored.add(delegator.makeValue("PartyContact", UtilMisc.toMap("partyId", partyId, "contactId", newCmId,
                    "fromDate", now, "roleTypeId", context.get("roleTypeId"), "allowSolicitation", context.get("allowSolicitation"), "extension", context.get("extension"))));
        }

        if ("POSTAL_ADDRESS".equals(contactTypeId)) {
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError,
                    "contactservices.service_createContact_not_be_used_for_POSTAL_ADDRESS", locale));
        } else if ("TELECOM_NUMBER".equals(contactTypeId)) {
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError,
                    "contactservices.service_createContact_not_be_used_for_TELECOM_NUMBER", locale));
        } else {
            tempContact.set("infoString", context.get("infoString"));
        }

        try {
            delegator.storeAll(toBeStored);
        } catch (GenericEntityException e) {
            Debug.logWarning(e.toString(), module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError,
                    "contactservices.could_not_create_contact_info_write",
                    UtilMisc.toMap("errMessage", e.getMessage()), locale));
        }

        result.put("contactId", newCmId);
        result.put(ModelService.RESPONSE_MESSAGE, ModelService.RESPOND_SUCCESS);
        return result;
    }

    /**
     * Updates a Contact
     * <b>security check</b>: userLogin partyId must equal partyId, or must have PARTYMGR_UPDATE permission
     *@param ctx The DispatchContext that this service is operating in
     *@param context Map containing the input parameters
     *@return Map with the result of the service, the output parameters
     */
    public static Map<String, Object> updateContact(DispatchContext ctx, Map<String, ? extends Object> context) {
        Map<String, Object> result = new HashMap<String, Object>();
        Delegator delegator = ctx.getDelegator();
        Security security = ctx.getSecurity();
        GenericValue userLogin = (GenericValue) context.get("userLogin");
        Locale locale = (Locale) context.get("locale");
        Timestamp now = UtilDateTime.nowTimestamp();
        List<GenericValue> toBeStored = new LinkedList<GenericValue>();
        boolean isModified = false;

        String partyId = ServiceUtil.getPartyIdCheckSecurity(userLogin, security, context, result, "PARTYMGR", "_PCM_UPDATE");
        
        if (result.size() > 0)
            return result;

        String newCmId = null;
        try {
            newCmId = delegator.getNextSeqId("Contact");
        } catch (IllegalArgumentException e) {
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError,
                    "contactservices.could_not_change_contact_info_id_generation_failure", locale));
        }

        String contactId = (String) context.get("contactId");
        GenericValue contact = null;
        GenericValue partyContact = null;

        try {
            contact = EntityQuery.use(delegator).from("Contact").where("contactId", contactId).queryOne();
        } catch (GenericEntityException e) {
            Debug.logWarning(e.getMessage(), module);
            contact = null;
        }

        if (!partyId.equals("_NA_")) {
            // try to find a PartyContact with a valid date range
            try {
                partyContact = EntityQuery.use(delegator).from("PartyContact")
                        .where("partyId", partyId, "contactId", contactId)
                        .orderBy("fromDate")
                        .filterByDate()
                        .queryFirst();
                if (partyContact == null) {
                    return ServiceUtil.returnError(UtilProperties.getMessage(resourceError,
                            "contactservices.cannot_update_specified_contact_info_not_corresponds", locale));
                } else {
                    toBeStored.add(partyContact);
                }
            } catch (GenericEntityException e) {
                Debug.logWarning(e.getMessage(), module);
                contact = null;
            }
        }
        if (contact == null) {
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError,
                    "contactservices.could_not_find_specified_contact_info_read", locale));
        }

        String contactTypeId = contact.getString("contactTypeId");

        // never change a contact , just create a new one with the changes
        GenericValue newContact = GenericValue.create(contact);
        GenericValue newPartyContact = GenericValue.create(partyContact);

        if ("POSTAL_ADDRESS".equals(contactTypeId)) {
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError,
                    "contactservices.service_updateContact_not_be_used_for_POSTAL_ADDRESS", locale));
        } else if ("TELECOM_NUMBER".equals(contactTypeId)) {
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError,
                    "contactservices.service_updateContact_not_be_used_for_TELECOM_NUMBER", locale));
        } else {
            newContact.set("infoString", context.get("infoString"));
        }

        newPartyContact.set("roleTypeId", context.get("roleTypeId"));
        newPartyContact.set("allowSolicitation", context.get("allowSolicitation"));

        if (!newContact.equals(contact)) isModified = true;
        if (!newPartyContact.equals(partyContact)) isModified = true;

        toBeStored.add(newContact);
        toBeStored.add(newPartyContact);

        if (isModified) {
            newContact.set("contactId", newCmId);
            newPartyContact.set("contactId", newCmId);
            newPartyContact.set("fromDate", now);
            newPartyContact.set("thruDate", null);

            try {
                Iterator<GenericValue> partyContactPurposes = UtilMisc.toIterator(partyContact.getRelated("PartyContactPurpose", null, null, false));

                while (partyContactPurposes != null && partyContactPurposes.hasNext()) {
                    GenericValue tempVal = GenericValue.create(partyContactPurposes.next());

                    tempVal.set("contactId", newCmId);
                    toBeStored.add(tempVal);
                }
            } catch (GenericEntityException e) {
                Debug.logWarning(e.toString(), module);
                return ServiceUtil.returnError(UtilProperties.getMessage(resourceError,
                        "contactservices.could_not_change_contact_info_read",
                        UtilMisc.toMap("errMessage", e.getMessage()), locale));
            }

            partyContact.set("thruDate", now);
            try {
                delegator.storeAll(toBeStored);
            } catch (GenericEntityException e) {
                Debug.logWarning(e.toString(), module);
                return ServiceUtil.returnError(UtilProperties.getMessage(resourceError,
                        "contactservices.could_not_change_contact_info_write", 
                        UtilMisc.toMap("errMessage", e.getMessage()), locale));
            }
        } else {
            result.put("newContactId", contactId);
            result.put(ModelService.RESPONSE_MESSAGE, ModelService.RESPOND_SUCCESS);
            result.put(ModelService.SUCCESS_MESSAGE, UtilProperties.getMessage(resourceError,
                       "contactservices.no_changes_made_not_updating", locale));
            return result;
        }

        result.put("newContactId", newCmId);
        result.put(ModelService.RESPONSE_MESSAGE, ModelService.RESPOND_SUCCESS);
        return result;
    }

    /**
     * Deletes a Contact
     * <b>security check</b>: userLogin partyId must equal partyId, or must have PARTYMGR_DELETE permission
     *@param ctx The DispatchContext that this service is operating in
     *@param context Map containing the input parameters
     *@return Map with the result of the service, the output parameters
     */
    public static Map<String, Object> deleteContact(DispatchContext ctx, Map<String, ? extends Object> context) {
        Map<String, Object> result = new HashMap<String, Object>();
        Delegator delegator = ctx.getDelegator();
        Security security = ctx.getSecurity();
        GenericValue userLogin = (GenericValue) context.get("userLogin");
        Locale locale = (Locale) context.get("locale");
        
        String partyId = ServiceUtil.getPartyIdCheckSecurity(userLogin, security, context, result, "PARTYMGR", "_PCM_DELETE");
        
        if (result.size() > 0)
            return result;

        // never delete a contact anism, just put a to date on the link to the party
        String contactId = (String) context.get("contactId");
        GenericValue partyContact = null;

        try {
            // try to find a PartyContact with a valid date range
            partyContact = EntityQuery.use(delegator).from("PartyContact")
                    .where("partyId", partyId, "contactId", contactId)
                    .orderBy("fromDate")
                    .filterByDate()
                    .queryFirst();
        } catch (GenericEntityException e) {
            Debug.logWarning(e.toString(), module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError,
                    "contactservices.could_not_delete_contact_info_read",
                    UtilMisc.toMap("errMessage", e.getMessage()), locale));
        }

        if (partyContact == null) {
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError,
                    "contactservices.could_not_delete_contact_info_no_contact_found", locale));
        }

        partyContact.set("thruDate", UtilDateTime.nowTimestamp());
        try {
            partyContact.store();
        } catch (GenericEntityException e) {
            Debug.logWarning(e.toString(), module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError,
                    "contactservices.could_not_delete_contact_info_write", locale));
        }

        result.put(ModelService.RESPONSE_MESSAGE, ModelService.RESPOND_SUCCESS);
        return result;
    }

    // ============================================================================
    // ============================================================================

    /**
     * Creates a PostalAddress
     * <b>security check</b>: userLogin partyId must equal partyId, or must have PARTYMGR_CREATE permission
     *@param ctx The DispatchContext that this service is operating in
     *@param context Map containing the input parameters
     *@return Map with the result of the service, the output parameters
     */
    public static Map<String, Object> createPostalAddress(DispatchContext ctx, Map<String, ? extends Object> context) {
        Map<String, Object> result = new HashMap<String, Object>();
        Delegator delegator = ctx.getDelegator();
        Security security = ctx.getSecurity();
        GenericValue userLogin = (GenericValue) context.get("userLogin");
        Locale locale = (Locale) context.get("locale");
        Timestamp now = UtilDateTime.nowTimestamp();
        List<GenericValue> toBeStored = new LinkedList<GenericValue>();

        String partyId = ServiceUtil.getPartyIdCheckSecurity(userLogin, security, context, result, "PARTYMGR", "_PCM_CREATE");
        
        if (result.size() > 0)
            return result;

        String contactTypeId = "POSTAL_ADDRESS";

        String newCmId = null;
        try {
            newCmId = delegator.getNextSeqId("Contact");
        } catch (IllegalArgumentException e) {
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError,
                    "contactservices.could_not_create_contact_info_id_generation_failure", locale));
        }

        GenericValue tempContact = delegator.makeValue("Contact", UtilMisc.toMap("contactId", newCmId, "contactTypeId", contactTypeId));
        toBeStored.add(tempContact);

        // don't create a PartyContact if there is no party; we define no party as sending _NA_ as partyId
        if (!partyId.equals("_NA_")) {
            toBeStored.add(delegator.makeValue("PartyContact",
                    UtilMisc.toMap("partyId", partyId, "contactId", newCmId,
                        "fromDate", now, "roleTypeId", context.get("roleTypeId"), "allowSolicitation",
                        context.get("allowSolicitation"), "extension", context.get("extension"))));
        }

        GenericValue newAddr = delegator.makeValue("PostalAddress");

        newAddr.set("contactId", newCmId);
        newAddr.set("toName", context.get("toName"));
        newAddr.set("attnName", context.get("attnName"));
        newAddr.set("address1", context.get("address1"));
        newAddr.set("address2", context.get("address2"));
        newAddr.set("directions", context.get("directions"));
        newAddr.set("city", context.get("city"));
        newAddr.set("postalCode", context.get("postalCode"));
        newAddr.set("postalCodeExt", context.get("postalCodeExt"));
        newAddr.set("stateProvinceGeoId", context.get("stateProvinceGeoId"));
        newAddr.set("countryGeoId", context.get("countryGeoId"));
        newAddr.set("postalCodeGeoId", context.get("postalCodeGeoId"));
        toBeStored.add(newAddr);

        try {
            delegator.storeAll(toBeStored);
        } catch (GenericEntityException e) {
            Debug.logWarning(e.toString(), module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError,
                    "contactservices.could_not_create_contact_info_write",
                    UtilMisc.toMap("errMessage", e.getMessage()), locale));
        }

        result.put("contactId", newCmId);
        result.put(ModelService.RESPONSE_MESSAGE, ModelService.RESPOND_SUCCESS);
        return result;
    }

    /**
     * Updates a PostalAddress
     * <b>security check</b>: userLogin partyId must equal partyId, or must have PARTYMGR_UPDATE permission
     *@param ctx The DispatchContext that this service is operating in
     *@param context Map containing the input parameters
     *@return Map with the result of the service, the output parameters
     */
    public static Map<String, Object> updatePostalAddress(DispatchContext ctx, Map<String, ? extends Object> context) {
        Map<String, Object> result = new HashMap<String, Object>();
        Delegator delegator = ctx.getDelegator();
        Security security = ctx.getSecurity();
        GenericValue userLogin = (GenericValue) context.get("userLogin");
        Locale locale = (Locale) context.get("locale");
        Timestamp now = UtilDateTime.nowTimestamp();
        List<GenericValue> toBeStored = new LinkedList<GenericValue>();
        boolean isModified = false;

        String partyId = ServiceUtil.getPartyIdCheckSecurity(userLogin, security, context, result, "PARTYMGR", "_PCM_UPDATE");
        
        if (result.size() > 0) {
            return result;
        }

        String newCmId = null;
        try {
            newCmId = delegator.getNextSeqId("Contact");
        } catch (IllegalArgumentException e) {
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError,
                    "contactservices.could_not_change_contact_info_id_generation_failure", locale));
        }

        String contactId = (String) context.get("contactId");
        GenericValue contact = null;
        GenericValue partyContact = null;

        try {
            contact = EntityQuery.use(delegator).from("Contact").where("contactId", contactId).queryOne();
        } catch (GenericEntityException e) {
            Debug.logWarning(e.getMessage(), module);
            contact = null;
        }

        if (!partyId.equals("_NA_")) {
            // try to find a PartyContact with a valid date range
            try {
                partyContact = EntityQuery.use(delegator).from("PartyContact")
                        .where("partyId", partyId, "contactId", contactId)
                        .orderBy("fromDate")
                        .filterByDate()
                        .queryFirst();
                if (partyContact == null) {
                    return ServiceUtil.returnError(UtilProperties.getMessage(resourceError,
                            "contactservices.cannot_update_specified_contact_info_not_corresponds", locale));
                } else {
                    toBeStored.add(partyContact);
                }
            } catch (GenericEntityException e) {
                Debug.logWarning(e.getMessage(), module);
                contact = null;
            }
        }
        if (contact == null) {
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError,
                    "contactservices.could_not_find_specified_contact_info_read", locale));
        }

        // never change a contact , just create a new one with the changes
        GenericValue newContact = GenericValue.create(contact);
        GenericValue newPartyContact = null;
        if (partyContact != null)
            newPartyContact = GenericValue.create(partyContact);
        GenericValue relatedEntityToSet = null;

        if ("POSTAL_ADDRESS".equals(contact.getString("contactTypeId"))) {
            GenericValue addr = null;

            try {
                addr = EntityQuery.use(delegator).from("PostalAddress").where("contactId", contactId).queryOne();
            } catch (GenericEntityException e) {
                Debug.logWarning(e.toString(), module);
                addr = null;
            }
            relatedEntityToSet = GenericValue.create(addr);
            relatedEntityToSet.set("toName", context.get("toName"));
            relatedEntityToSet.set("attnName", context.get("attnName"));
            relatedEntityToSet.set("address1", context.get("address1"));
            relatedEntityToSet.set("address2", context.get("address2"));
            relatedEntityToSet.set("directions", context.get("directions"));
            relatedEntityToSet.set("city", context.get("city"));
            relatedEntityToSet.set("postalCode", context.get("postalCode"));
            relatedEntityToSet.set("postalCodeExt", context.get("postalCodeExt"));
            relatedEntityToSet.set("stateProvinceGeoId", context.get("stateProvinceGeoId"));
            relatedEntityToSet.set("countryGeoId", context.get("countryGeoId"));
            relatedEntityToSet.set("postalCodeGeoId", context.get("postalCodeGeoId"));
            if (addr == null || !relatedEntityToSet.equals(addr)) {
                isModified = true;
            }
            relatedEntityToSet.set("contactId", newCmId);
        } else {
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError,
                    "contactservices.could_not_update_contact_as_POSTAL_ADDRESS_specified", 
                    UtilMisc.toMap("contactTypeId", contact.getString("contactTypeId")), locale));
        }

        if (newPartyContact != null) {
            newPartyContact.set("roleTypeId", context.get("roleTypeId"));
            newPartyContact.set("allowSolicitation", context.get("allowSolicitation"));
        }

        if (!newContact.equals(contact)) isModified = true;
        if (newPartyContact != null && !newPartyContact.equals(partyContact)) isModified = true;

        toBeStored.add(newContact);
        if (newPartyContact != null)
            toBeStored.add(newPartyContact);

        if (isModified) {
            if (relatedEntityToSet != null) toBeStored.add(relatedEntityToSet);

            newContact.set("contactId", newCmId);
            if (newPartyContact != null) {
                newPartyContact.set("contactId", newCmId);
                newPartyContact.set("fromDate", now);
                newPartyContact.set("thruDate", null);

                try {
                    Iterator<GenericValue> partyContactPurposes = UtilMisc.toIterator(partyContact.getRelated("PartyContactPurpose", null, null, false));

                    while (partyContactPurposes != null && partyContactPurposes.hasNext()) {
                        GenericValue tempVal = GenericValue.create(partyContactPurposes.next());

                        tempVal.set("contactId", newCmId);
                        toBeStored.add(tempVal);
                    }
                } catch (GenericEntityException e) {
                    Debug.logWarning(e.toString(), module);
                    return ServiceUtil.returnError(UtilProperties.getMessage(resourceError,
                            "contactservices.could_not_change_contact_info_read", 
                            UtilMisc.toMap("errMessage", e.getMessage()), locale));
                }

                partyContact.set("thruDate", now);
            }

            try {
                delegator.storeAll(toBeStored);
            } catch (GenericEntityException e) {
                Debug.logWarning(e.toString(), module);
                return ServiceUtil.returnError(UtilProperties.getMessage(resourceError,
                        "contactservices.could_not_change_contact_info_write", 
                        UtilMisc.toMap("errMessage", e.getMessage()), locale));
            }
        } else {
            result.put("newContactId", contactId);
            result.put(ModelService.RESPONSE_MESSAGE, ModelService.RESPOND_SUCCESS);
            result.put(ModelService.SUCCESS_MESSAGE, UtilProperties.getMessage(resourceError,
                    "contactservices.no_changes_made_not_updating", locale));
            return result;
        }

        result.put("newContactId", newCmId);
        result.put(ModelService.RESPONSE_MESSAGE, ModelService.RESPOND_SUCCESS);
        return result;
    }

    // ============================================================================
    // ============================================================================

    /**
     * Creates a TelecomNumber
     * <b>security check</b>: userLogin partyId must equal partyId, or must have PARTYMGR_CREATE permission
     *@param ctx The DispatchContext that this service is operating in
     *@param context Map containing the input parameters
     *@return Map with the result of the service, the output parameters
     */
    public static Map<String, Object> createTelecomNumber(DispatchContext ctx, Map<String, ? extends Object> context) {
        Map<String, Object> result = new HashMap<String, Object>();
        Delegator delegator = ctx.getDelegator();
        Security security = ctx.getSecurity();
        GenericValue userLogin = (GenericValue) context.get("userLogin");
        Locale locale = (Locale) context.get("locale");
        Timestamp now = UtilDateTime.nowTimestamp();
        List<GenericValue> toBeStored = new LinkedList<GenericValue>();

        String partyId = ServiceUtil.getPartyIdCheckSecurity(userLogin, security, context, result, "PARTYMGR", "_PCM_CREATE");
        
        if (result.size() > 0)
            return result;

        String contactTypeId = "TELECOM_NUMBER";

        String newCmId = null;
        try {
            newCmId = delegator.getNextSeqId("Contact");
        } catch (IllegalArgumentException e) {
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError,
                    "contactservices.could_not_create_contact_info_id_generation_failure", locale));
        }

        GenericValue tempContact = delegator.makeValue("Contact", UtilMisc.toMap("contactId", newCmId, "contactTypeId", contactTypeId));
        toBeStored.add(tempContact);

        toBeStored.add(delegator.makeValue("PartyContact", UtilMisc.toMap("partyId", partyId, "contactId", newCmId,
                    "fromDate", now, "roleTypeId", context.get("roleTypeId"), "allowSolicitation", context.get("allowSolicitation"), "extension", context.get("extension"))));

        toBeStored.add(delegator.makeValue("TelecomNumber", UtilMisc.toMap("contactId", newCmId,
                    "countryCode", context.get("countryCode"), "areaCode", context.get("areaCode"), "contactNumber", context.get("contactNumber"))));

        try {
            delegator.storeAll(toBeStored);
        } catch (GenericEntityException e) {
            Debug.logWarning(e.toString(), module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError,
                    "contactservices.could_not_create_contact_info_write",
                    UtilMisc.toMap("errMessage", e.getMessage()), locale));
        }

        result.put("contactId", newCmId);
        result.put(ModelService.RESPONSE_MESSAGE, ModelService.RESPOND_SUCCESS);
        return result;
    }

    /**
     * Updates a TelecomNumber
     * <b>security check</b>: userLogin partyId must equal partyId, or must have PARTYMGR_UPDATE permission
     *@param ctx The DispatchContext that this service is operating in
     *@param context Map containing the input parameters
     *@return Map with the result of the service, the output parameters
     */
    public static Map<String, Object> updateTelecomNumber(DispatchContext ctx, Map<String, ? extends Object> context) {
        Map<String, Object> result = new HashMap<String, Object>();
        Delegator delegator = ctx.getDelegator();
        Security security = ctx.getSecurity();
        GenericValue userLogin = (GenericValue) context.get("userLogin");
        Locale locale = (Locale) context.get("locale");
        Timestamp now = UtilDateTime.nowTimestamp();
        List<GenericValue> toBeStored = new LinkedList<GenericValue>();
        boolean isModified = false;

        String partyId = ServiceUtil.getPartyIdCheckSecurity(userLogin, security, context, result, "PARTYMGR", "_PCM_UPDATE");
        
        if (result.size() > 0)
            return result;

        String newCmId = null;
        try {
            newCmId = delegator.getNextSeqId("Contact");
        } catch (IllegalArgumentException e) {
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError,
                    "contactservices.could_not_change_contact_info_id_generation_failure", locale));
        }

        String contactId = (String) context.get("contactId");
        GenericValue contact = null;
        GenericValue partyContact = null;

        try {
            contact = EntityQuery.use(delegator).from("Contact").where("contactId", contactId).queryOne();
            // try to find a PartyContact with a valid date range
            partyContact = EntityQuery.use(delegator).from("PartyContact")
                    .where("partyId", partyId, "contactId", contactId)
                    .orderBy("fromDate")
                    .filterByDate()
                    .queryFirst();
        } catch (GenericEntityException e) {
            Debug.logWarning(e.getMessage(), module);
            contact = null;
            partyContact = null;
        }
        if (contact == null) {
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError,
                    "contactservices.could_not_find_specified_contact_info_read", locale));
        }
        if (partyContact == null) {
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError,
                    "contactservices.cannot_update_specified_contact_info_not_corresponds", locale));
        }
        toBeStored.add(partyContact);

        // never change a contact , just create a new one with the changes
        GenericValue newContact = GenericValue.create(contact);
        GenericValue newPartyContact = GenericValue.create(partyContact);
        GenericValue relatedEntityToSet = null;

        if ("TELECOM_NUMBER".equals(contact.getString("contactTypeId"))) {
            GenericValue telNum = null;

            try {
                telNum = EntityQuery.use(delegator).from("TelecomNumber").where("contactId", contactId).queryOne();
            } catch (GenericEntityException e) {
                Debug.logWarning(e.toString(), module);
                telNum = null;
            }
            relatedEntityToSet = GenericValue.create(telNum);
            relatedEntityToSet.set("countryCode", context.get("countryCode"));
            relatedEntityToSet.set("areaCode", context.get("areaCode"));
            relatedEntityToSet.set("contactNumber", context.get("contactNumber"));

            if (telNum == null || !relatedEntityToSet.equals(telNum)) {
                isModified = true;
            }
            relatedEntityToSet.set("contactId", newCmId);
            newPartyContact.set("extension", context.get("extension"));
        } else {
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError,
                    "contactservices.could_not_update_contact_as_TELECOM_NUMBER_specified", 
                    UtilMisc.toMap("contactTypeId", contact.getString("contactTypeId")), locale));
        }

        newPartyContact.set("roleTypeId", context.get("roleTypeId"));
        newPartyContact.set("allowSolicitation", context.get("allowSolicitation"));

        if (!newContact.equals(contact)) isModified = true;
        if (!newPartyContact.equals(partyContact)) isModified = true;

        toBeStored.add(newContact);
        toBeStored.add(newPartyContact);

        if (isModified) {
            if (relatedEntityToSet != null) toBeStored.add(relatedEntityToSet);

            newContact.set("contactId", newCmId);
            newPartyContact.set("contactId", newCmId);
            newPartyContact.set("fromDate", now);
            newPartyContact.set("thruDate", null);

            try {
                Iterator<GenericValue> partyContactPurposes = UtilMisc.toIterator(partyContact.getRelated("PartyContactPurpose", null, null, false));

                while (partyContactPurposes != null && partyContactPurposes.hasNext()) {
                    GenericValue tempVal = GenericValue.create(partyContactPurposes.next());

                    tempVal.set("contactId", newCmId);
                    toBeStored.add(tempVal);
                }
            } catch (GenericEntityException e) {
                Debug.logWarning(e.toString(), module);
                return ServiceUtil.returnError(UtilProperties.getMessage(resourceError,
                        "contactservices.could_not_change_contact_info_read", 
                        UtilMisc.toMap("errMessage", e.getMessage()), locale));
            }

            partyContact.set("thruDate", now);
            try {
                delegator.storeAll(toBeStored);
            } catch (GenericEntityException e) {
                Debug.logWarning(e.toString(), module);
                return ServiceUtil.returnError(UtilProperties.getMessage(resourceError,
                        "contactservices.could_not_change_contact_info_write", 
                        UtilMisc.toMap("errMessage", e.getMessage()), locale));
            }
        } else {
            result.put("newContactId", contactId);
            result.put(ModelService.RESPONSE_MESSAGE, ModelService.RESPOND_SUCCESS);
            result.put(ModelService.SUCCESS_MESSAGE, UtilProperties.getMessage(resourceError,
                    "contactservices.no_changes_made_not_updating", locale));
            return result;
        }

        result.put("newContactId", newCmId);
        result.put(ModelService.RESPONSE_MESSAGE, ModelService.RESPOND_SUCCESS);
        return result;
    }

    // ============================================================================
    // ============================================================================

    /**
     * Creates a EmailAddress
     * <b>security check</b>: userLogin partyId must equal partyId, or must have PARTYMGR_CREATE permission
     *@param ctx The DispatchContext that this service is operating in
     *@param context Map containing the input parameters
     *@return Map with the result of the service, the output parameters
     */
    public static Map<String, Object> createEmailAddress(DispatchContext ctx, Map<String, ? extends Object> context) {
        Map<String, Object> newContext = UtilMisc.makeMapWritable(context);

        newContext.put("infoString", newContext.get("emailAddress"));
        newContext.remove("emailAddress");
        newContext.put("contactTypeId", "EMAIL_ADDRESS");

        return createContact(ctx, newContext);
    }

    /**
     * Updates a EmailAddress
     * <b>security check</b>: userLogin partyId must equal partyId, or must have PARTYMGR_UPDATE permission
     *@param ctx The DispatchContext that this service is operating in
     *@param context Map containing the input parameters
     *@return Map with the result of the service, the output parameters
     */
    public static Map<String, Object> updateEmailAddress(DispatchContext ctx, Map<String, ? extends Object> context) {
        Map<String, Object> newContext = UtilMisc.makeMapWritable(context);

        newContext.put("infoString", newContext.get("emailAddress"));
        newContext.remove("emailAddress");
        return updateContact(ctx, newContext);
    }

    // ============================================================================
    // ============================================================================

    /**
     * Creates a PartyContactPurpose
     * <b>security check</b>: userLogin partyId must equal partyId, or must have PARTYMGR_CREATE permission
     *@param ctx The DispatchContext that this service is operating in
     *@param context Map containing the input parameters
     *@return Map with the result of the service, the output parameters
     */
    public static Map<String, Object> createPartyContactPurpose(DispatchContext ctx, Map<String, ? extends Object> context) {
        //Debug.logInfo(new Exception(), "In createPartyContactPurpose context: " + context, module);
        Map<String, Object> result = new HashMap<String, Object>();
        Delegator delegator = ctx.getDelegator();
        Security security = ctx.getSecurity();
        GenericValue userLogin = (GenericValue) context.get("userLogin");

        String partyId = ServiceUtil.getPartyIdCheckSecurity(userLogin, security, context, result, "PARTYMGR", "_PCM_CREATE");
        String errMsg = null;
        Locale locale = (Locale) context.get("locale");

        if (result.size() > 0) {
            return result;
        }

        // required parameters
        String contactId = (String) context.get("contactId");
        String contactPurposeTypeId = (String) context.get("contactPurposeTypeId");
        Timestamp fromDate = (Timestamp) context.get("fromDate");

        GenericValue tempVal = null;
        try {
            tempVal = EntityQuery.use(delegator).from("PartyContactWithPurpose")
                    .where("partyId", partyId, "contactId", contactId, "contactPurposeTypeId", contactPurposeTypeId)
                    .filterByDate("contactFromDate", "contactThruDate", "purposeFromDate", "purposeThruDate")
                    .queryFirst();
        } catch (GenericEntityException e) {
            Debug.logWarning(e.getMessage(), module);
            tempVal = null;
        }

        if (UtilValidate.isEmpty(fromDate)) {
            fromDate = UtilDateTime.nowTimestamp();
        }

        if (tempVal != null) {
            // exists already with valid date, show warning
            errMsg = UtilProperties.getMessage(resourceError,
                       "contactservices.could_not_create_new_purpose_already_exists", locale);
            errMsg += ": " + tempVal.getPrimaryKey().toString();
            return ServiceUtil.returnError(errMsg);
        } else {
            // no entry with a valid date range exists, create new with open thruDate
            GenericValue newPartyContactPurpose = delegator.makeValue("PartyContactPurpose",
                    UtilMisc.toMap("partyId", partyId, "contactId", contactId, "contactPurposeTypeId", contactPurposeTypeId,
                        "fromDate", fromDate));

            try {
                delegator.create(newPartyContactPurpose);
            } catch (GenericEntityException e) {
                Debug.logWarning(e.getMessage(), module);
                return ServiceUtil.returnError(UtilProperties.getMessage(resourceError,
                        "contactservices.could_not_add_purpose_write", 
                        UtilMisc.toMap("errMessage", e.getMessage()), locale));
            }
        }

        result.put("fromDate", fromDate);
        result.put(ModelService.RESPONSE_MESSAGE, ModelService.RESPOND_SUCCESS);
        return result;
    }

    public static Map<String, Object> deletePartyContactPurposeIfExists(DispatchContext ctx, Map<String, ? extends Object> context) {
        //Debug.logInfo(new Exception(), "In createPartyContactPurpose context: " + context, module);
        Map<String, Object> result = new HashMap<String, Object>();
        Delegator delegator = ctx.getDelegator();
        Security security = ctx.getSecurity();
        GenericValue userLogin = (GenericValue) context.get("userLogin");
        Locale locale = (Locale) context.get("locale");

        String partyId = ServiceUtil.getPartyIdCheckSecurity(userLogin, security, context, result, "PARTYMGR", "_PCM_DELETE");
        
        if (result.size() > 0) {
            return result;
        }

        // required parameters
        String contactId = (String) context.get("contactId");
        String contactPurposeTypeId = (String) context.get("contactPurposeTypeId");

        GenericValue tempVal = null;
        try {
            tempVal = EntityQuery.use(delegator).from("PartyContactWithPurpose")
                    .where("partyId", partyId, "contactId", contactId, "contactPurposeTypeId", contactPurposeTypeId)
                    .filterByDate("contactFromDate", "contactThruDate", "purposeFromDate", "purposeThruDate")
                    .queryFirst();
        } catch (GenericEntityException e) {
            Debug.logWarning(e.getMessage(), module);
            tempVal = null;
        }
        if (tempVal != null) {
            Map<String, Object> deletePcmCtx = UtilMisc.toMap("contactId", context.get("contactId"));
            deletePcmCtx.put("contactPurposeTypeId", context.get("contactPurposeTypeId"));
            deletePcmCtx.put("fromDate", tempVal.get("purposeFromDate"));
            deletePcmCtx.put("userLogin", context.get("userLogin"));
            deletePcmCtx.put("partyId", partyId);
            try {
                Map<String, Object> deletePcmResult = ctx.getDispatcher().runSync("deletePartyContactPurpose", deletePcmCtx);
                if (ServiceUtil.isError(deletePcmResult)) {
                    return deletePcmResult;
                }
            } catch (GenericServiceException e) {
                Debug.logWarning(e.getMessage(), module);
                return ServiceUtil.returnError(UtilProperties.getMessage(resourceError,
                        "contactservices.could_not_delete_purpose_from_contact_anism_read", 
                        UtilMisc.toMap("errMessage", e.getMessage()), locale));
            }
        }
        result.put(ModelService.RESPONSE_MESSAGE, ModelService.RESPOND_SUCCESS);
        return result;

    }
    /**
     * Deletes the PartyContactPurpose corresponding to the parameters in the context
     * <b>security check</b>: userLogin partyId must equal partyId, or must have PARTYMGR_DELETE permission
     *@param ctx The DispatchContext that this service is operating in
     *@param context Map containing the input parameters
     *@return Map with the result of the service, the output parameters
     */
    public static Map<String, Object> deletePartyContactPurpose(DispatchContext ctx, Map<String, ? extends Object> context) {
        Map<String, Object> result = new HashMap<String, Object>();
        Delegator delegator = ctx.getDelegator();
        Security security = ctx.getSecurity();
        GenericValue userLogin = (GenericValue) context.get("userLogin");
        Locale locale = (Locale) context.get("locale");

        String partyId = ServiceUtil.getPartyIdCheckSecurity(userLogin, security, context, result, "PARTYMGR", "_PCM_DELETE");
        
        if (result.size() > 0)
            return result;

        // required parameters
        String contactId = (String) context.get("contactId");
        String contactPurposeTypeId = (String) context.get("contactPurposeTypeId");
        Timestamp fromDate = (Timestamp) context.get("fromDate");

        GenericValue pcmp = null;

        try {
            pcmp = EntityQuery.use(delegator).from("PartyContactPurpose").where("partyId", partyId, "contactId", contactId, "contactPurposeTypeId", contactPurposeTypeId, "fromDate", fromDate).queryOne();
            if (pcmp == null) {
                return ServiceUtil.returnError(UtilProperties.getMessage(resourceError,
                        "contactservices.could_not_delete_purpose_from_contact_anism_not_found", locale));
            }
        } catch (GenericEntityException e) {
            Debug.logWarning(e.getMessage(), module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError,
                    "contactservices.could_not_delete_purpose_from_contact_anism_read",
                    UtilMisc.toMap("errMessage", e.getMessage()), locale));
        }

        pcmp.set("thruDate", UtilDateTime.nowTimestamp());
        try {
            pcmp.store();
        } catch (GenericEntityException e) {
            Debug.logWarning(e.getMessage(), module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError,
                    "contactservices.could_not_delete_purpose_from_contact_anism_write",
                    UtilMisc.toMap("errMessage", e.getMessage()), locale));
        }

        result.put(ModelService.RESPONSE_MESSAGE, ModelService.RESPOND_SUCCESS);
        return result;
    }

    /**
     * Just wraps the ContactWorker method of the same name.
     *
     *@param ctx The DispatchContext that this service is operating in
     *@param context Map containing the input parameters
     *@return Map with the result of the service, the output parameters
     */
    public static Map<String, Object> getPartyContactValueMaps(DispatchContext ctx, Map<String, ? extends Object> context) {
        Map<String, Object> result = ServiceUtil.returnSuccess();
        Delegator delegator = ctx.getDelegator();
        GenericValue userLogin = (GenericValue) context.get("userLogin");
        String partyId = (String)context.get("partyId");
        Locale locale = (Locale) context.get("locale");
        if (UtilValidate.isEmpty(partyId)) {
            if (userLogin != null) {
                partyId = userLogin.getString("partyId");
            } else {
                return ServiceUtil.returnError(UtilProperties.getMessage(resource,
                        "PartyCannotGetPartyContact", locale));
            }
        }
        Boolean bShowOld = (Boolean)context.get("showOld");
        boolean showOld = (bShowOld != null && bShowOld.booleanValue()) ? true : false;
        String contactTypeId = (String)context.get("contactTypeId");
        List<Map<String, Object>> valueMaps = ContactWorker.getPartyContactValueMaps(delegator, partyId, showOld, contactTypeId);
        result.put("valueMaps", valueMaps);
        return result;
    }

    /**
     * Copies all contact s from one party to another. Does not delete or overwrite any contact s.
     */
    public static Map<String, Object> copyPartyContacts(DispatchContext dctx, Map<String, ? extends Object> context) {
        Delegator delegator = dctx.getDelegator();
        LocalDispatcher dispatcher = dctx.getDispatcher();
        GenericValue userLogin = (GenericValue) context.get("userLogin");

        String partyIdFrom = (String) context.get("partyIdFrom");
        String partyIdTo = (String) context.get("partyIdTo");
        Locale locale = (Locale) context.get("locale");

        try {
            // grab all of the non-expired contact s using this party worker method
            List<Map<String, Object>> valueMaps = ContactWorker.getPartyContactValueMaps(delegator, partyIdFrom, false);

            // loop through results
            for (Map<String, Object> thisMap: valueMaps) {
                GenericValue contact = (GenericValue) thisMap.get("contact");
                GenericValue partyContact = (GenericValue) thisMap.get("partyContact");
                List<GenericValue> partyContactPurposes = UtilGenerics.checkList(thisMap.get("partyContactPurposes"));

                // get the contactId
                String contactId = contact.getString("contactId");

                // create a new party contact  for the partyIdTo
                Map<String, Object> serviceResults = dispatcher.runSync("createPartyContact", UtilMisc.<String, Object>toMap("partyId", partyIdTo, "userLogin", userLogin,
                            "contactId", contactId, "fromDate", UtilDateTime.nowTimestamp(),
                            "allowSolicitation", partyContact.getString("allowSolicitation"), "extension", partyContact.getString("extension")));
                if (ServiceUtil.isError(serviceResults)) {
                    return serviceResults;
                }

                // loop through purposes and copy each as a new purpose for the partyIdTo
                for (GenericValue purpose: partyContactPurposes) {
                    Map<String, Object> input = UtilMisc.toMap("partyId", partyIdTo, "contactId", contactId, "userLogin", userLogin);
                    input.put("contactPurposeTypeId", purpose.getString("contactPurposeTypeId"));
                    serviceResults = dispatcher.runSync("createPartyContactPurpose", input);
                    if (ServiceUtil.isError(serviceResults)) {
                        return serviceResults;
                    }
                }
            }
        } catch (GenericServiceException e) {
            Debug.logError(e, e.getMessage(), module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resource,
                    "PartyCannotCopyPartyContact", 
                    UtilMisc.toMap("errorString", e.getMessage()), locale));
        }
        return ServiceUtil.returnSuccess();
    }

    /**
     * Creates an EmailAddressVerification
     */
    public static Map<String, Object> createEmailAddressVerification(DispatchContext dctx, Map<String, ? extends Object> context) {
        Delegator delegator = dctx.getDelegator();
        String emailAddress = (String) context.get("emailAddress");
        String verifyHash = null;

        String expireTime = EntityUtilProperties.getPropertyValue("security", "email_verification.expire.hours", delegator);
        Integer expTime = Integer.valueOf(expireTime);
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR, expTime.intValue());
        Date date = calendar.getTime();
        Timestamp expireDate = UtilDateTime.toTimestamp(date);

        SecureRandom secureRandom = new SecureRandom();

        synchronized(ContactServices.class) {
            while (true) {
                Long random = secureRandom.nextLong();
                verifyHash = HashCrypt.digestHash("MD5", Long.toString(random).getBytes());
                List<GenericValue> emailAddVerifications = null;
                try {
                    emailAddVerifications = EntityQuery.use(delegator).from("EmailAddressVerification").where("verifyHash", verifyHash).queryList();
                } catch (GenericEntityException e) {
                    Debug.logError(e.getMessage(), module);
                    return ServiceUtil.returnError(e.getMessage());
                }
                if (UtilValidate.isEmpty(emailAddVerifications)) {
                    GenericValue emailAddressVerification = delegator.makeValue("EmailAddressVerification");
                    emailAddressVerification.set("emailAddress", emailAddress);
                    emailAddressVerification.set("verifyHash", verifyHash);
                    emailAddressVerification.set("expireDate", expireDate);
                    try {
                        delegator.create(emailAddressVerification);
                    } catch (GenericEntityException e) {
                        Debug.logError(e.getMessage(),module);
                        return ServiceUtil.returnError(e.getMessage());
                    }
                    break;
                }
            }
        }

        Map<String, Object> result = ServiceUtil.returnSuccess();
        result.put("verifyHash", verifyHash);
        return result;
    }

}
