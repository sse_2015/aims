package org.ofbiz.aims;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.sql.Date;

import javolution.util.FastList;
import javolution.util.FastMap;

import org.ofbiz.base.util.Debug;
import org.ofbiz.base.util.UtilDateTime;
import org.ofbiz.base.util.UtilGenerics;
import org.ofbiz.base.util.UtilMisc;
import org.ofbiz.base.util.UtilProperties;
import org.ofbiz.base.util.UtilValidate;
import org.ofbiz.entity.Delegator;
import org.ofbiz.entity.GenericEntityException;
import org.ofbiz.entity.GenericValue;
import org.ofbiz.entity.condition.EntityCondition;
import org.ofbiz.entity.condition.EntityConditionList;
import org.ofbiz.entity.condition.EntityExpr;
import org.ofbiz.entity.condition.EntityFunction;
import org.ofbiz.entity.condition.EntityOperator;
import org.ofbiz.entity.model.DynamicViewEntity;
import org.ofbiz.entity.model.ModelKeyMap;
import org.ofbiz.entity.util.EntityFindOptions;
import org.ofbiz.entity.util.EntityListIterator;
import org.ofbiz.entity.util.EntityTypeUtil;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.service.DispatchContext;
import org.ofbiz.service.GenericServiceException;
import org.ofbiz.service.LocalDispatcher;
import org.ofbiz.service.ModelService;
import org.ofbiz.service.ServiceUtil;

//for import upload
import org.ofbiz.base.util.*;
import org.ofbiz.entity.util.EntityUtil;
import org.ofbiz.base.util.string.FlexibleStringExpander;
import java.util.*;
import java.io.*;
import java.lang.Long;

public class ProfileJavaDirectUpdate{
    public static final String module = ProfileJavaDirectUpdate.class.getName();
    public static final String resource = "PartyUiLabels";
    public static final String resourceError = "PartyErrorUiLabels";

    public static String getPartyId(Map<String, ? extends Object> context) {
        String partyId = (String) context.get("partyId");
        if (UtilValidate.isEmpty(partyId)) {
            GenericValue userLogin = (GenericValue) context.get("userLogin");
            if (userLogin != null) {
                partyId = userLogin.getString("partyId");
            }
        }
        return partyId;
    }

     public static String createReqId(Delegator delegator, String prefix, int length) {
        final String seqName = "RequestUpdatePerson";
        String reqPartyId = prefix != null ? prefix : "";

        // generate the sequenced number and pad
        Long seq = delegator.getNextSeqIdLong(seqName);
        reqPartyId = reqPartyId + UtilFormatOut.formatPaddedNumber(seq.longValue(), (length - reqPartyId.length() - 1));

        // get the check digit
        int check = UtilValidate.getLuhnCheckDigit(reqPartyId);
        reqPartyId = reqPartyId + Integer.toString(check);

        return reqPartyId;
    }

    public static String createReqId2(String seqname, Delegator delegator, String prefix, int length) {
        final String seqName = seqname;
        String reqPartyId = prefix != null ? prefix : "";

        // generate the sequenced number and pad
        Long seq = delegator.getNextSeqIdLong(seqName);
        reqPartyId = reqPartyId + UtilFormatOut.formatPaddedNumber(seq.longValue(), (length - reqPartyId.length() - 1));

        // get the check digit
        int check = UtilValidate.getLuhnCheckDigit(reqPartyId);
        reqPartyId = reqPartyId + Integer.toString(check);

        return reqPartyId;
    }

    public static Map<String, Object> uploadFile(DispatchContext ctx, Map<String, ? extends Object> context) {
        Map<String, Object> result = FastMap.newInstance();
        Delegator delegator = ctx.getDelegator();
        LocalDispatcher dispatcher = ctx.getDispatcher();
        Locale locale = (Locale) context.get("locale");

        String contactId = (String)context.get("contactId");
        String infoString = (String)context.get("infoString");
        String partyId = getPartyId(context);
        String firstName = (String)context.get("firstName");
        String middleName = (String)context.get("middleName");
        String lastName = (String)context.get("lastName");
        String personalTitle = (String)context.get("personalTitle");
        String suffix = (String)context.get("suffix");
        String nickname = (String)context.get("nickname");
        String gender = (String)context.get("gender");
        Date birthDate = (Date)context.get("birthDate");
        Date deceasedDate = (Date)context.get("deceasedDate");
        Double height = (Double)context.get("height");
        Double weight = (Double)context.get("weight");
        String mothersMaidenName = (String)context.get("mothersMaidenName");
        String maritalStatus = (String)context.get("maritalStatus");
        String socialSecurityNumber = (String)context.get("socialSecurityNumber");
        String cardId = (String)context.get("cardId");
        String comments = (String)context.get("comments");
        String newId = "";
        String newCid = "";

        Timestamp fromDate = (Timestamp)context.get("fromDate");
        String address1 = (String)context.get("address1");
        String city = (String)context.get("city");
        String postalCode = (String)context.get("postalCode");
        String contactNumber = (String)context.get("contactNumber");

        int inPerson = 0;

        if (UtilValidate.isEmpty(partyId)) {
            return ServiceUtil.returnError(UtilProperties.getMessage(ServiceUtil.resource, 
                    "serviceUtil.party_id_missing", locale));
        }


        GenericValue profile = null;
        GenericValue partycontact = null;
        GenericValue telecomnumber = null;
        GenericValue contact = null;
        GenericValue contactDetail = null;

        GenericValue postaladdress = null;

        try{
            // newId = createReqId(delegator,"",5);
            // profile = delegator.makeValue("RequestUpdatePerson");
            // delegator.setNextSubSeqId(profile, "reqPartyId", 5, 1);
            profile = delegator.findOne("Person", UtilMisc.toMap("partyId", partyId), false);            
			/*partyContact = delegator.findOne("PartyContact", UtilMisc.toMap("partyId", partyId), false);
			if(partyContact == null)
				partyContact = delegator.makeValue("PartyContact");*/
				
            // profile = delegator.findOne("RequestUpdatePerson", UtilMisc.toMap("reqPartyId", newId), false);
        }catch(Exception e){
            //req party id not found]
            Debug.logWarning(e, module);
            //return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, "thesis.update.read_failure", new Object[] {e.getMessage() }, locale));
            inPerson = 1;
            profile = delegator.makeValue("Person");
        }    

        try
        {
            partycontact = delegator.findOne("PartyContact", UtilMisc.toMap("partyId", partyId,"contactId",contactId,"fromDate",fromDate), false);
            contactId = (String)partycontact.get("contactId");
            try
            {
                contactDetail = delegator.findOne("Contact", UtilMisc.toMap("contactId", contactId), false);
                contactDetail.set("infoString",infoString);
                contactDetail.store();
            }
            catch(Exception es)
            {
                Debug.logWarning(es, module);
                    return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, "create detail try contact error "+es, new Object[] {es.getMessage() }, locale));   
            }
            

            try
            {
                telecomnumber = delegator.findOne("TelecomNumber", UtilMisc.toMap("contactId", contactId), false);                
                telecomnumber.set("contactNumber",contactNumber);
                telecomnumber.store();
            }
            catch(Exception e)
            {   
                telecomnumber = delegator.makeValue("TelecomNumber");
                telecomnumber.set("contactId",contactId);
                telecomnumber.set("contactNumber",contactNumber);
                try
                {
                    delegator.create(telecomnumber);            
                }
                catch(Exception es)
                {
                    Debug.logWarning(es, module);
                    return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, "create tele contact error", new Object[] {e.getMessage() }, locale));

                }
            }

            try
            {
                postaladdress = delegator.findOne("PostalAddress", UtilMisc.toMap("contactId", contactId), false);    
                postaladdress.set("address1",address1);
                postaladdress.set("city",city);
                postaladdress.set("postalCode",postalCode);
                postaladdress.store();             
            }
            catch(Exception e)
            {
                postaladdress = delegator.makeValue("PostalAddress");
                postaladdress.set("contactId",contactId);
                postaladdress.set("address1",address1);
                postaladdress.set("city",city);
                postaladdress.set("postalCode",postalCode);
                try
                {
                    delegator.create(postaladdress);            
                }
                catch(Exception es)
                {
                    Debug.logWarning(es, module);
                    return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, "create tele contact error", new Object[] {e.getMessage() }, locale));

                }
            }
        }catch(Exception e)
        {
            Debug.logWarning(e, module);
            //return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, ""+contactId+" "+partyId+" "+fromDate, new Object[] {e.getMessage() }, locale));
            partycontact = delegator.makeValue("PartyContact"); 
            contactId = createReqId2("Contact",delegator,"",5);
            contact = delegator.makeValue("Contact");
            contact.set("contactId",contactId);
            contact.set("infoString",infoString);

            try
            {
                delegator.create(contact);
            }
            catch(Exception es)
            {
                Debug.logWarning(es, module);
                return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, " c : "+contactId+" contact error", new Object[] {e.getMessage() }, locale));

            }

            partycontact.set("partyId",partyId);
            partycontact.set("contactId",contactId);
            partycontact.set("fromDate",fromDate);
            try
            {
                delegator.create(partycontact);    
            }
            catch(Exception es)
            {
                Debug.logWarning(es, module);
                return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, " c : "+contactId+" party contact error "+es, new Object[] {e.getMessage() }, locale));

            }
            // try
            // {
            //     // contactDetail = delegator.findOne("PartyAndContact", UtilMisc.toMap("partyId", partyId,"contactId",contactId,"fromDate",fromDate,"paContactId",contactId,"tnContactId",contactId), false);
            //     // contactDetail.set("infoString",infoString);
            //     // contactDetail.store();    
            // }
            // catch(Exception es)
            // {    
            //     return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, infoString+" c : "+contactId+" contact detail error "+es, new Object[] {e.getMessage() }, locale));
            // }

            
            
            postaladdress = delegator.makeValue("PostalAddress");
            postaladdress.set("contactId",contactId);
            postaladdress.set("address1",address1);
            postaladdress.set("city",city);
            postaladdress.set("postalCode",postalCode);

            try
            {
                delegator.create(postaladdress);
            }
            catch(Exception es)
            {
                Debug.logWarning(es, module);
                return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, "create postal contact error "+contactId+" "+es, new Object[] {e.getMessage() }, locale));

            }
            telecomnumber = delegator.makeValue("TelecomNumber");
            telecomnumber.set("contactId",contactId);
            telecomnumber.set("contactNumber",contactNumber);
            try
            {
                delegator.create(telecomnumber);            
            }
            catch(Exception es)
            {
                Debug.logWarning(es, module);
                return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, "create tele contact error", new Object[] {e.getMessage() }, locale));

            }
        }

        if(profile == null){
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    "thesis.update.not_found", locale));
        }

        // Map<String, Object> tempThesis = (Map<String, Object>)thesis.getAllFields();

        /*
            for upload
        */

        //for Thesis Proposal
        //===================================================================================================================
        ByteBuffer fileDataProfile = null;
        String fileNameDataProfile = "";
        String contentTypeDataProfile = "";
        File file = null;        
        boolean f = true;
        try{
            fileDataProfile = (ByteBuffer) context.get("photo");
            fileNameDataProfile = (String) context.get("_photo_fileName");
            contentTypeDataProfile = (String) context.get("_photo_contentType");

            if (UtilValidate.isNotEmpty(fileNameDataProfile)) {
                file = new File(System.getProperty("user.dir")+ "/hot-deploy/aims/webapp/aims/images/" + fileNameDataProfile);
                Debug.logInfo("upload file to " + file.getAbsolutePath(), "");

                try {
                    RandomAccessFile out = new RandomAccessFile(file, "rw");
                    out.write(fileDataProfile.array());
                    out.close();
                } catch (FileNotFoundException e) {
                    Debug.logError(e, "");
                } catch (IOException e) {
                    Debug.logError(e, "");
                }
            }else{
                f = false;
                fileNameDataProfile = "noimage.jpg";
                GenericValue temp = null;
                temp = delegator.findOne("Person", UtilMisc.toMap("partyId", partyId), false);
                fileNameDataProfile = (String)temp.get("photo");
            }
        }catch(Exception e){
            fileNameDataProfile = "noimage.jpg"; 
        }

        if(f)
            fileNameDataProfile = "../images/" + fileNameDataProfile;
        //===================================================================================================================
        //end

        //update


        //for thesis proposal

        // if(file != null){
        //     profile.set("photo",fileNameDataProfile);
        profile.set("partyId",partyId);
        profile.set("photo",fileNameDataProfile);
        profile.set("firstName",firstName);
        profile.set("middleName",middleName);
        profile.set("lastName",lastName);
        profile.set("personalTitle",personalTitle);
        profile.set("suffix",suffix);
        profile.set("nickname",nickname);
        profile.set("gender",gender);
        profile.set("birthDate",birthDate);
        profile.set("deceasedDate",deceasedDate);
        profile.set("height",height);
        profile.set("weight",weight);
        profile.set("cardId",cardId);
        profile.set("mothersMaidenName",mothersMaidenName);
        profile.set("maritalStatus",maritalStatus);
        profile.set("socialSecurityNumber",socialSecurityNumber);
        //profile.set("comments",comments);        
        // }else{
        //     profile.set("photo", "awdawdawd");
        // }
        
        try {
            // EntityFindOptions findOpts = new EntityFindOptions(true,
            //         EntityFindOptions.TYPE_SCROLL_INSENSITIVE,
            //         EntityFindOptions.CONCUR_UPDATABLE, true);
            // EntityListIterator el = ((EntityListIterator)delegator.find("RequestUpdatePerson",null,null,null,null,null));            
            //el.add(profile);
            // el.getCompleteList().add(profile);
            //delegator.create(profile);
            if(inPerson == 0)
                profile.store();
            else
                delegator.create(profile);
            //((GenericValue)delegator.findOne("RequestUpdatePerson")).create(profile);

        } catch (GenericEntityException e) {
            Debug.logWarning(e.getMessage(), module);
            return ServiceUtil.returnError(UtilProperties.getMessage(resourceError, 
                    e.getMessage(), new Object[] { e.getMessage() }, locale));
        }

        result.put(ModelService.RESPONSE_MESSAGE, ModelService.RESPOND_SUCCESS);
        result.put(ModelService.SUCCESS_MESSAGE, 
                UtilProperties.getMessage(resourceError, fileNameDataProfile, locale));
        return result;
    }
}